﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AssetRegister
{
    public partial class frmLoadReport : Form
    {
        private clsCommonFunctions dbConReportPrinted = new clsCommonFunctions();
 
        public frmLoadReport()
        {
            InitializeComponent();
            foreach (ToolStrip ts in crViewer.Controls.OfType<ToolStrip>())
            {
                foreach (ToolStripButton tsb in ts.Items.OfType<ToolStripButton>())
                {
                    if(tsb.ToolTipText.ToLower().Contains("print"))
                    {
                        tsb.Click += new EventHandler(PrintButton_Click);
                    }
                    else if (tsb.ToolTipText.ToLower().Contains("export"))
                    {
                        tsb.Click += new EventHandler(ExportButton_Click);
                    }
                }
            } 
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {            
            string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                           "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
            string[] lVals = { frmLogin.userId.ToString(), "PRINT", frmLogin.ipAddress, 
                               "[AssetRegistar].[dbo].[AR_UsersLog]", this.Text, 
                               dbConReportPrinted.getMaxIdentity("[AR_UsersLog]", "[TrnNo]").ToString(), "", "" };

            dbConReportPrinted.DBCRUDFunction(dbConReportPrinted.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null), false);
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                           "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
            string[] lVals = { frmLogin.userId.ToString(), "EXPORT", frmLogin.ipAddress, 
                               "[AssetRegistar].[dbo].[AR_UsersLog]", this.Text, 
                               dbConReportPrinted.getMaxIdentity("[AR_UsersLog]", "[TrnNo]").ToString(), "", "" };

            dbConReportPrinted.DBCRUDFunction(dbConReportPrinted.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null), false);
        }
    }
}
