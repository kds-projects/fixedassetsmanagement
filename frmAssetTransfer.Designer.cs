﻿namespace AssetRegister
{
    partial class frmAssetTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnXfer = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvListData = new System.Windows.Forms.DataGridView();
            this.dgvAssetLoc = new System.Windows.Forms.DataGridView();
            this.dgvAssetSubLoc = new System.Windows.Forms.DataGridView();
            this.dgvAssetUser = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCross = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvApprovedBy = new System.Windows.Forms.DataGridView();
            this.dgvReceiveBy = new System.Windows.Forms.DataGridView();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblACode = new System.Windows.Forms.Label();
            this.lblCC = new System.Windows.Forms.Label();
            this.lblALocation = new System.Windows.Forms.Label();
            this.lblASLocation = new System.Windows.Forms.Label();
            this.lblAUser = new System.Windows.Forms.Label();
            this.lblCCode = new System.Windows.Forms.Label();
            this.lblAName = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtReceivedBy = new AssetRegister.FlatTextBox();
            this.txtApprovedBy = new AssetRegister.FlatTextBox();
            this.dtpRDate = new AssetRegister.FlatDateTimePicker();
            this.dtpTDate = new AssetRegister.FlatDateTimePicker();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.txtAssetUser = new AssetRegister.FlatTextBox();
            this.txtSubLoc = new AssetRegister.FlatTextBox();
            this.txtLoc = new AssetRegister.FlatTextBox();
            this.txtAsset = new AssetRegister.FlatTextBox();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            this.txtDesc = new AssetRegister.FlatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetSubLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetUser)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvApprovedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiveBy)).BeginInit();
            this.SuspendLayout();
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape5.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(897, 370);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 374);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(897, 39);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(378, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 234;
            this.label3.Text = "Company Code";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(503, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 232;
            this.label6.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(378, 203);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 229;
            this.label12.Text = "Asset User";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(378, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 13);
            this.label10.TabIndex = 223;
            this.label10.Text = "Asset Sub Location";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(503, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 220;
            this.label11.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(378, 159);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 221;
            this.label8.Text = "Asset Location";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(503, 181);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 224;
            this.label9.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(503, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 227;
            this.label7.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(378, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 228;
            this.label2.Text = "Cost Center";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(503, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 225;
            this.label5.Text = ":";
            // 
            // btnXfer
            // 
            this.btnXfer.Image = global::AssetRegister.Properties.Resources.Actions_stock_save_as_icon;
            this.btnXfer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXfer.Location = new System.Drawing.Point(823, 375);
            this.btnXfer.Name = "btnXfer";
            this.btnXfer.Size = new System.Drawing.Size(38, 38);
            this.btnXfer.TabIndex = 9;
            this.btnXfer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnXfer.UseVisualStyleBackColor = true;
            this.btnXfer.Click += new System.EventHandler(this.btnXfer_Click);
            this.btnXfer.MouseHover += new System.EventHandler(this.btnXfer_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(860, 375);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 10;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // dgvListData
            // 
            this.dgvListData.AllowUserToAddRows = false;
            this.dgvListData.AllowUserToDeleteRows = false;
            this.dgvListData.AllowUserToResizeColumns = false;
            this.dgvListData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvListData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListData.BackgroundColor = System.Drawing.Color.White;
            this.dgvListData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvListData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvListData.EnableHeadersVisualStyles = false;
            this.dgvListData.Location = new System.Drawing.Point(617, 2);
            this.dgvListData.MultiSelect = false;
            this.dgvListData.Name = "dgvListData";
            this.dgvListData.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvListData.RowHeadersVisible = false;
            this.dgvListData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListData.Size = new System.Drawing.Size(159, 371);
            this.dgvListData.TabIndex = 239;
            this.dgvListData.Visible = false;
            this.dgvListData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListData_CellDoubleClick);
            this.dgvListData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvListData_KeyDown);
            this.dgvListData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvListData_KeyPress);
            // 
            // dgvAssetLoc
            // 
            this.dgvAssetLoc.AllowUserToAddRows = false;
            this.dgvAssetLoc.AllowUserToDeleteRows = false;
            this.dgvAssetLoc.AllowUserToResizeColumns = false;
            this.dgvAssetLoc.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetLoc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvAssetLoc.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetLoc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetLoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetLoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvAssetLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetLoc.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvAssetLoc.EnableHeadersVisualStyles = false;
            this.dgvAssetLoc.Location = new System.Drawing.Point(571, 2);
            this.dgvAssetLoc.MultiSelect = false;
            this.dgvAssetLoc.Name = "dgvAssetLoc";
            this.dgvAssetLoc.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetLoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvAssetLoc.RowHeadersVisible = false;
            this.dgvAssetLoc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAssetLoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetLoc.Size = new System.Drawing.Size(205, 371);
            this.dgvAssetLoc.TabIndex = 404;
            this.dgvAssetLoc.Visible = false;
            this.dgvAssetLoc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetLoc_CellDoubleClick);
            this.dgvAssetLoc.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAssetLoc_DataError);
            this.dgvAssetLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetLoc_KeyDown);
            this.dgvAssetLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetLoc_KeyPress);
            // 
            // dgvAssetSubLoc
            // 
            this.dgvAssetSubLoc.AllowUserToAddRows = false;
            this.dgvAssetSubLoc.AllowUserToDeleteRows = false;
            this.dgvAssetSubLoc.AllowUserToResizeColumns = false;
            this.dgvAssetSubLoc.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetSubLoc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvAssetSubLoc.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetSubLoc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetSubLoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetSubLoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvAssetSubLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetSubLoc.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvAssetSubLoc.EnableHeadersVisualStyles = false;
            this.dgvAssetSubLoc.Location = new System.Drawing.Point(571, 2);
            this.dgvAssetSubLoc.MultiSelect = false;
            this.dgvAssetSubLoc.Name = "dgvAssetSubLoc";
            this.dgvAssetSubLoc.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetSubLoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvAssetSubLoc.RowHeadersVisible = false;
            this.dgvAssetSubLoc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAssetSubLoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetSubLoc.Size = new System.Drawing.Size(205, 371);
            this.dgvAssetSubLoc.TabIndex = 406;
            this.dgvAssetSubLoc.Visible = false;
            this.dgvAssetSubLoc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetSubLoc_CellDoubleClick);
            this.dgvAssetSubLoc.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAssetSubLoc_DataError);
            this.dgvAssetSubLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetSubLoc_KeyDown);
            this.dgvAssetSubLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetSubLoc_KeyPress);
            // 
            // dgvAssetUser
            // 
            this.dgvAssetUser.AllowUserToAddRows = false;
            this.dgvAssetUser.AllowUserToDeleteRows = false;
            this.dgvAssetUser.AllowUserToResizeColumns = false;
            this.dgvAssetUser.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetUser.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvAssetUser.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetUser.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetUser.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvAssetUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetUser.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvAssetUser.EnableHeadersVisualStyles = false;
            this.dgvAssetUser.Location = new System.Drawing.Point(571, 2);
            this.dgvAssetUser.MultiSelect = false;
            this.dgvAssetUser.Name = "dgvAssetUser";
            this.dgvAssetUser.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetUser.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvAssetUser.RowHeadersVisible = false;
            this.dgvAssetUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetUser.Size = new System.Drawing.Size(318, 371);
            this.dgvAssetUser.TabIndex = 407;
            this.dgvAssetUser.Visible = false;
            this.dgvAssetUser.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetUser_CellDoubleClick);
            this.dgvAssetUser.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAssetUser_DataError);
            this.dgvAssetUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetUser_KeyDown);
            this.dgvAssetUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetUser_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(503, 262);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(12, 13);
            this.label15.TabIndex = 220;
            this.label15.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(378, 262);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 13);
            this.label16.TabIndex = 229;
            this.label16.Text = "Description";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Controls.Add(this.label66);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(902, 44);
            this.panel1.TabIndex = 408;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(865, 7);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 421;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(16, 14);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(156, 16);
            this.label66.TabIndex = 420;
            this.label66.Text = "Asset Transfer Entry";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.lblAName);
            this.panel2.Controls.Add(this.lblACode);
            this.panel2.Controls.Add(this.lblCC);
            this.panel2.Controls.Add(this.lblALocation);
            this.panel2.Controls.Add(this.lblASLocation);
            this.panel2.Controls.Add(this.lblAUser);
            this.panel2.Controls.Add(this.lblCCode);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.label41);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.dgvApprovedBy);
            this.panel2.Controls.Add(this.dgvAssetLoc);
            this.panel2.Controls.Add(this.dgvAssetSubLoc);
            this.panel2.Controls.Add(this.dgvListData);
            this.panel2.Controls.Add(this.dgvAssetUser);
            this.panel2.Controls.Add(this.dgvReceiveBy);
            this.panel2.Controls.Add(this.txtReceivedBy);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.txtApprovedBy);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.dtpRDate);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.dtpTDate);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cmbCC);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cmbCompany);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtAssetUser);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtSubLoc);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtLoc);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtAsset);
            this.panel2.Controls.Add(this.btnXfer);
            this.panel2.Controls.Add(this.txtAssetCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtDesc);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Location = new System.Drawing.Point(5, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(902, 416);
            this.panel2.TabIndex = 408;
            // 
            // dgvApprovedBy
            // 
            this.dgvApprovedBy.AllowUserToAddRows = false;
            this.dgvApprovedBy.AllowUserToDeleteRows = false;
            this.dgvApprovedBy.AllowUserToResizeColumns = false;
            this.dgvApprovedBy.AllowUserToResizeRows = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvApprovedBy.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvApprovedBy.BackgroundColor = System.Drawing.Color.White;
            this.dgvApprovedBy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvApprovedBy.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvApprovedBy.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvApprovedBy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvApprovedBy.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgvApprovedBy.EnableHeadersVisualStyles = false;
            this.dgvApprovedBy.Location = new System.Drawing.Point(571, 2);
            this.dgvApprovedBy.MultiSelect = false;
            this.dgvApprovedBy.Name = "dgvApprovedBy";
            this.dgvApprovedBy.ReadOnly = true;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvApprovedBy.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvApprovedBy.RowHeadersVisible = false;
            this.dgvApprovedBy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvApprovedBy.Size = new System.Drawing.Size(318, 371);
            this.dgvApprovedBy.TabIndex = 420;
            this.dgvApprovedBy.Visible = false;
            this.dgvApprovedBy.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvApprovedBy_CellDoubleClick);
            this.dgvApprovedBy.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvApprovedBy_DataError);
            this.dgvApprovedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvApprovedBy_KeyDown);
            this.dgvApprovedBy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApprovedBy_KeyPress);
            // 
            // dgvReceiveBy
            // 
            this.dgvReceiveBy.AllowUserToAddRows = false;
            this.dgvReceiveBy.AllowUserToDeleteRows = false;
            this.dgvReceiveBy.AllowUserToResizeColumns = false;
            this.dgvReceiveBy.AllowUserToResizeRows = false;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvReceiveBy.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvReceiveBy.BackgroundColor = System.Drawing.Color.White;
            this.dgvReceiveBy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvReceiveBy.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReceiveBy.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvReceiveBy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReceiveBy.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgvReceiveBy.EnableHeadersVisualStyles = false;
            this.dgvReceiveBy.Location = new System.Drawing.Point(571, 2);
            this.dgvReceiveBy.MultiSelect = false;
            this.dgvReceiveBy.Name = "dgvReceiveBy";
            this.dgvReceiveBy.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReceiveBy.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvReceiveBy.RowHeadersVisible = false;
            this.dgvReceiveBy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReceiveBy.Size = new System.Drawing.Size(318, 371);
            this.dgvReceiveBy.TabIndex = 419;
            this.dgvReceiveBy.Visible = false;
            this.dgvReceiveBy.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReceiveBy_CellDoubleClick);
            this.dgvReceiveBy.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvReceiveBy_DataError);
            this.dgvReceiveBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvReceiveBy_KeyDown);
            this.dgvReceiveBy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvReceiveBy_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(378, 343);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 417;
            this.label23.Text = "Received By";
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(503, 343);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 418;
            this.label24.Text = ":";
            this.label24.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(378, 321);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 414;
            this.label21.Text = "Approved By";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(503, 321);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(12, 13);
            this.label22.TabIndex = 415;
            this.label22.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(378, 299);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 13);
            this.label19.TabIndex = 411;
            this.label19.Text = "Receiving Date";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(503, 299);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(12, 13);
            this.label20.TabIndex = 410;
            this.label20.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(378, 225);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 13);
            this.label17.TabIndex = 229;
            this.label17.Text = "Transfer Date";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(503, 225);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 13);
            this.label18.TabIndex = 220;
            this.label18.Text = ":";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1,
            this.rectangleShape5});
            this.shapeContainer2.Size = new System.Drawing.Size(902, 416);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(16, 115);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 13);
            this.label33.TabIndex = 425;
            this.label33.Text = "Asset Code";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(141, 115);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(12, 13);
            this.label34.TabIndex = 429;
            this.label34.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(141, 181);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(12, 13);
            this.label35.TabIndex = 428;
            this.label35.Text = ":";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(16, 181);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(76, 13);
            this.label36.TabIndex = 431;
            this.label36.Text = "Cost Center";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(141, 203);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(12, 13);
            this.label37.TabIndex = 430;
            this.label37.Text = ":";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(141, 225);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(12, 13);
            this.label38.TabIndex = 427;
            this.label38.Text = ":";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(16, 203);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(89, 13);
            this.label39.TabIndex = 424;
            this.label39.Text = "Asset Location";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(141, 247);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(12, 13);
            this.label40.TabIndex = 423;
            this.label40.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(16, 225);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(115, 13);
            this.label41.TabIndex = 426;
            this.label41.Text = "Asset Sub Location";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(16, 247);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(68, 13);
            this.label42.TabIndex = 434;
            this.label42.Text = "Asset User";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(141, 159);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(12, 13);
            this.label43.TabIndex = 435;
            this.label43.Text = ":";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(16, 159);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(96, 13);
            this.label44.TabIndex = 436;
            this.label44.Text = "Company Code";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(16, 137);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(38, 13);
            this.label46.TabIndex = 437;
            this.label46.Text = "Asset";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(141, 137);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(12, 13);
            this.label48.TabIndex = 438;
            this.label48.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(327, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 242;
            this.label13.Text = ":";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(246, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 241;
            this.label14.Text = "Asset";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(327, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 13);
            this.label4.TabIndex = 226;
            this.label4.Text = ":";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(246, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 222;
            this.label1.Text = "Asset Code";
            // 
            // lblACode
            // 
            this.lblACode.AutoSize = true;
            this.lblACode.Location = new System.Drawing.Point(157, 115);
            this.lblACode.Name = "lblACode";
            this.lblACode.Size = new System.Drawing.Size(72, 13);
            this.lblACode.TabIndex = 446;
            this.lblACode.Text = "Asset Code";
            // 
            // lblCC
            // 
            this.lblCC.AutoSize = true;
            this.lblCC.Location = new System.Drawing.Point(157, 181);
            this.lblCC.Name = "lblCC";
            this.lblCC.Size = new System.Drawing.Size(76, 13);
            this.lblCC.TabIndex = 448;
            this.lblCC.Text = "Cost Center";
            // 
            // lblALocation
            // 
            this.lblALocation.AutoSize = true;
            this.lblALocation.Location = new System.Drawing.Point(157, 203);
            this.lblALocation.Name = "lblALocation";
            this.lblALocation.Size = new System.Drawing.Size(89, 13);
            this.lblALocation.TabIndex = 445;
            this.lblALocation.Text = "Asset Location";
            // 
            // lblASLocation
            // 
            this.lblASLocation.AutoSize = true;
            this.lblASLocation.Location = new System.Drawing.Point(157, 225);
            this.lblASLocation.Name = "lblASLocation";
            this.lblASLocation.Size = new System.Drawing.Size(115, 13);
            this.lblASLocation.TabIndex = 447;
            this.lblASLocation.Text = "Asset Sub Location";
            // 
            // lblAUser
            // 
            this.lblAUser.AutoSize = true;
            this.lblAUser.Location = new System.Drawing.Point(157, 247);
            this.lblAUser.Name = "lblAUser";
            this.lblAUser.Size = new System.Drawing.Size(68, 13);
            this.lblAUser.TabIndex = 451;
            this.lblAUser.Text = "Asset User";
            // 
            // lblCCode
            // 
            this.lblCCode.AutoSize = true;
            this.lblCCode.Location = new System.Drawing.Point(157, 159);
            this.lblCCode.Name = "lblCCode";
            this.lblCCode.Size = new System.Drawing.Size(96, 13);
            this.lblCCode.TabIndex = 452;
            this.lblCCode.Text = "Company Code";
            // 
            // lblAName
            // 
            this.lblAName.AutoSize = true;
            this.lblAName.Location = new System.Drawing.Point(157, 137);
            this.lblAName.Name = "lblAName";
            this.lblAName.Size = new System.Drawing.Size(38, 13);
            this.lblAName.TabIndex = 455;
            this.lblAName.Text = "Asset";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Verdana", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.DarkGreen;
            this.label25.Location = new System.Drawing.Point(16, 70);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(186, 18);
            this.label25.TabIndex = 456;
            this.label25.Text = "Current Information";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Verdana", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.DarkGreen;
            this.label26.Location = new System.Drawing.Point(379, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(195, 18);
            this.label26.TabIndex = 457;
            this.label26.Text = "Transfer Information";
            // 
            // txtReceivedBy
            // 
            this.txtReceivedBy.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtReceivedBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtReceivedBy.Location = new System.Drawing.Point(521, 339);
            this.txtReceivedBy.Name = "txtReceivedBy";
            this.txtReceivedBy.Size = new System.Drawing.Size(335, 21);
            this.txtReceivedBy.TabIndex = 416;
            this.txtReceivedBy.Visible = false;
            this.txtReceivedBy.TextChanged += new System.EventHandler(this.txtReceivedBy_TextChanged);
            this.txtReceivedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReceivedBy_KeyDown);
            this.txtReceivedBy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReceivedBy_KeyPress);
            // 
            // txtApprovedBy
            // 
            this.txtApprovedBy.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtApprovedBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApprovedBy.Location = new System.Drawing.Point(521, 317);
            this.txtApprovedBy.Name = "txtApprovedBy";
            this.txtApprovedBy.Size = new System.Drawing.Size(335, 21);
            this.txtApprovedBy.TabIndex = 413;
            this.txtApprovedBy.TextChanged += new System.EventHandler(this.txtApprovedBy_TextChanged);
            this.txtApprovedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtApprovedBy_KeyDown);
            this.txtApprovedBy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApprovedBy_KeyPress);
            // 
            // dtpRDate
            // 
            this.dtpRDate.CustomFormat = "dd-MMM-yyyy dddd";
            this.dtpRDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRDate.Location = new System.Drawing.Point(521, 295);
            this.dtpRDate.Name = "dtpRDate";
            this.dtpRDate.Size = new System.Drawing.Size(335, 21);
            this.dtpRDate.TabIndex = 412;
            // 
            // dtpTDate
            // 
            this.dtpTDate.CustomFormat = "dd-MMM-yyyy dddd";
            this.dtpTDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTDate.Location = new System.Drawing.Point(521, 221);
            this.dtpTDate.Name = "dtpTDate";
            this.dtpTDate.Size = new System.Drawing.Size(335, 21);
            this.dtpTDate.TabIndex = 409;
            // 
            // cmbCC
            // 
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(521, 133);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(335, 21);
            this.cmbCC.TabIndex = 4;
            this.cmbCC.SelectedIndexChanged += new System.EventHandler(this.cmbCC_SelectedIndexChanged);
            // 
            // cmbCompany
            // 
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(521, 111);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(335, 21);
            this.cmbCompany.TabIndex = 3;
            // 
            // txtAssetUser
            // 
            this.txtAssetUser.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetUser.Location = new System.Drawing.Point(521, 199);
            this.txtAssetUser.Name = "txtAssetUser";
            this.txtAssetUser.Size = new System.Drawing.Size(335, 21);
            this.txtAssetUser.TabIndex = 7;
            this.txtAssetUser.TextChanged += new System.EventHandler(this.txtAssetUser_TextChanged);
            this.txtAssetUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetUser_KeyDown);
            this.txtAssetUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetUser_KeyPress);
            // 
            // txtSubLoc
            // 
            this.txtSubLoc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtSubLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubLoc.Location = new System.Drawing.Point(521, 177);
            this.txtSubLoc.Name = "txtSubLoc";
            this.txtSubLoc.Size = new System.Drawing.Size(335, 21);
            this.txtSubLoc.TabIndex = 6;
            this.txtSubLoc.TextChanged += new System.EventHandler(this.txtSubLoc_TextChanged);
            this.txtSubLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSubLoc_KeyDown);
            this.txtSubLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubLoc_KeyPress);
            // 
            // txtLoc
            // 
            this.txtLoc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoc.Location = new System.Drawing.Point(521, 155);
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.Size = new System.Drawing.Size(335, 21);
            this.txtLoc.TabIndex = 5;
            this.txtLoc.TextChanged += new System.EventHandler(this.txtLoc_TextChanged);
            this.txtLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLoc_KeyDown);
            this.txtLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLoc_KeyPress);
            // 
            // txtAsset
            // 
            this.txtAsset.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAsset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAsset.Location = new System.Drawing.Point(345, 38);
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.Size = new System.Drawing.Size(335, 21);
            this.txtAsset.TabIndex = 2;
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Location = new System.Drawing.Point(345, 16);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.Size = new System.Drawing.Size(335, 21);
            this.txtAssetCode.TabIndex = 1;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            // 
            // txtDesc
            // 
            this.txtDesc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDesc.Location = new System.Drawing.Point(521, 243);
            this.txtDesc.Multiline = true;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(335, 51);
            this.txtDesc.TabIndex = 8;
            // 
            // frmAssetTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(912, 470);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAssetTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asset Transfer";
            this.Load += new System.EventHandler(this.frmAssetTransfer_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmAssetTransfer_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetSubLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetUser)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvApprovedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiveBy)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnXfer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnClose;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private FlatTextBox txtLoc;
        private FlatTextBox txtSubLoc;
        private FlatTextBox txtAssetUser;
        private AssetRegister.FlatComboBox cmbCC;
        private AssetRegister.FlatComboBox cmbCompany;
        private System.Windows.Forms.DataGridView dgvListData;
        private System.Windows.Forms.DataGridView dgvAssetLoc;
        private System.Windows.Forms.DataGridView dgvAssetSubLoc;
        private System.Windows.Forms.DataGridView dgvAssetUser;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private FlatTextBox txtDesc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private AssetRegister.FlatDateTimePicker dtpTDate;
        private FlatTextBox txtReceivedBy;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private FlatTextBox txtApprovedBy;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private FlatDateTimePicker dtpRDate;
        private System.Windows.Forms.DataGridView dgvReceiveBy;
        private System.Windows.Forms.DataGridView dgvApprovedBy;
        private System.Windows.Forms.Label lblAName;
        private System.Windows.Forms.Label lblACode;
        private System.Windows.Forms.Label lblCC;
        private System.Windows.Forms.Label lblALocation;
        private System.Windows.Forms.Label lblASLocation;
        private System.Windows.Forms.Label lblAUser;
        private System.Windows.Forms.Label lblCCode;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private FlatTextBox txtAsset;
        private FlatTextBox txtAssetCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
    }
}