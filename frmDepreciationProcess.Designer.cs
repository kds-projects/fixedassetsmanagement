﻿namespace AssetRegister
{
    partial class frmDepreciationProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape6 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape4 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.cbGroup = new System.Windows.Forms.CheckBox();
            this.cbCC = new System.Windows.Forms.CheckBox();
            this.cbCompany = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvFrm = new System.Windows.Forms.DataGridView();
            this.dgvTo = new System.Windows.Forms.DataGridView();
            this.btnS = new System.Windows.Forms.Button();
            this.btnD = new System.Windows.Forms.Button();
            this.btnDAll = new System.Windows.Forms.Button();
            this.btnSAll = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.chkYear = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblTotAsset = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.btnCross = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpToDate = new AssetRegister.FlatDateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpFormDate = new AssetRegister.FlatDateTimePicker();
            this.chkDateRange = new System.Windows.Forms.CheckBox();
            this.dtpYear = new AssetRegister.FlatDateTimePicker();
            this.cmbGroup = new AssetRegister.FlatComboBox();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFrm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTo)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.Color.DarkGray;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(700, 353);
            // 
            // rectangleShape6
            // 
            this.rectangleShape6.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape6.Location = new System.Drawing.Point(335, 89);
            this.rectangleShape6.Name = "rectangleShape6";
            this.rectangleShape6.Size = new System.Drawing.Size(34, 262);
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape5.Location = new System.Drawing.Point(2, 357);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(700, 40);
            // 
            // rectangleShape4
            // 
            this.rectangleShape4.BorderColor = System.Drawing.Color.DarkGray;
            this.rectangleShape4.Location = new System.Drawing.Point(4, 87);
            this.rectangleShape4.Name = "rectangleShape4";
            this.rectangleShape4.Size = new System.Drawing.Size(696, 266);
            // 
            // rectangleShape3
            // 
            this.rectangleShape3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape3.Location = new System.Drawing.Point(337, 4);
            this.rectangleShape3.Name = "rectangleShape3";
            this.rectangleShape3.Size = new System.Drawing.Size(363, 81);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(4, 4);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(331, 81);
            // 
            // cbGroup
            // 
            this.cbGroup.AutoSize = true;
            this.cbGroup.BackColor = System.Drawing.Color.Gainsboro;
            this.cbGroup.Checked = true;
            this.cbGroup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGroup.Location = new System.Drawing.Point(651, 59);
            this.cbGroup.Name = "cbGroup";
            this.cbGroup.Size = new System.Drawing.Size(37, 17);
            this.cbGroup.TabIndex = 7;
            this.cbGroup.Text = "All";
            this.cbGroup.UseVisualStyleBackColor = false;
            this.cbGroup.CheckedChanged += new System.EventHandler(this.cbGroup_CheckedChanged);
            // 
            // cbCC
            // 
            this.cbCC.AutoSize = true;
            this.cbCC.BackColor = System.Drawing.Color.Gainsboro;
            this.cbCC.Checked = true;
            this.cbCC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCC.Location = new System.Drawing.Point(651, 37);
            this.cbCC.Name = "cbCC";
            this.cbCC.Size = new System.Drawing.Size(37, 17);
            this.cbCC.TabIndex = 5;
            this.cbCC.Text = "All";
            this.cbCC.UseVisualStyleBackColor = false;
            this.cbCC.CheckedChanged += new System.EventHandler(this.cbCC_CheckedChanged);
            // 
            // cbCompany
            // 
            this.cbCompany.AutoSize = true;
            this.cbCompany.BackColor = System.Drawing.Color.Gainsboro;
            this.cbCompany.Checked = true;
            this.cbCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCompany.Location = new System.Drawing.Point(651, 15);
            this.cbCompany.Name = "cbCompany";
            this.cbCompany.Size = new System.Drawing.Size(37, 17);
            this.cbCompany.TabIndex = 3;
            this.cbCompany.Text = "All";
            this.cbCompany.UseVisualStyleBackColor = false;
            this.cbCompany.CheckedChanged += new System.EventHandler(this.cbCompany_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(349, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "Asset Group";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(349, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Cost Center";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(349, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Company ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Purple;
            this.label6.Location = new System.Drawing.Point(135, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(410, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 13);
            this.label8.TabIndex = 53;
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(410, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 13);
            this.label9.TabIndex = 54;
            this.label9.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(410, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 13);
            this.label10.TabIndex = 55;
            this.label10.Text = ":";
            // 
            // dgvFrm
            // 
            this.dgvFrm.AllowUserToAddRows = false;
            this.dgvFrm.AllowUserToDeleteRows = false;
            this.dgvFrm.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PowderBlue;
            this.dgvFrm.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFrm.BackgroundColor = System.Drawing.Color.White;
            this.dgvFrm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvFrm.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvFrm.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFrm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SaddleBrown;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFrm.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFrm.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvFrm.EnableHeadersVisualStyles = false;
            this.dgvFrm.Location = new System.Drawing.Point(6, 89);
            this.dgvFrm.Name = "dgvFrm";
            this.dgvFrm.RowHeadersVisible = false;
            this.dgvFrm.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFrm.Size = new System.Drawing.Size(328, 263);
            this.dgvFrm.TabIndex = 233;
            // 
            // dgvTo
            // 
            this.dgvTo.AllowUserToAddRows = false;
            this.dgvTo.AllowUserToDeleteRows = false;
            this.dgvTo.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.PowderBlue;
            this.dgvTo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvTo.BackgroundColor = System.Drawing.Color.White;
            this.dgvTo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgvTo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SaddleBrown;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTo.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvTo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvTo.EnableHeadersVisualStyles = false;
            this.dgvTo.Location = new System.Drawing.Point(371, 89);
            this.dgvTo.Name = "dgvTo";
            this.dgvTo.RowHeadersVisible = false;
            this.dgvTo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTo.Size = new System.Drawing.Size(328, 263);
            this.dgvTo.TabIndex = 234;
            // 
            // btnS
            // 
            this.btnS.Enabled = false;
            this.btnS.Image = global::AssetRegister.Properties.Resources._1436197566_nav_arrow_left;
            this.btnS.Location = new System.Drawing.Point(336, 188);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(33, 33);
            this.btnS.TabIndex = 235;
            this.btnS.UseVisualStyleBackColor = true;
            this.btnS.Click += new System.EventHandler(this.btnS_Click);
            // 
            // btnD
            // 
            this.btnD.Enabled = false;
            this.btnD.Image = global::AssetRegister.Properties.Resources._1436197461_nav_arrow_right;
            this.btnD.Location = new System.Drawing.Point(336, 227);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(33, 33);
            this.btnD.TabIndex = 236;
            this.btnD.UseVisualStyleBackColor = true;
            this.btnD.Click += new System.EventHandler(this.btnD_Click);
            // 
            // btnDAll
            // 
            this.btnDAll.Enabled = false;
            this.btnDAll.Image = global::AssetRegister.Properties.Resources._1436197287_double_arrow_right;
            this.btnDAll.Location = new System.Drawing.Point(336, 266);
            this.btnDAll.Name = "btnDAll";
            this.btnDAll.Size = new System.Drawing.Size(33, 33);
            this.btnDAll.TabIndex = 238;
            this.btnDAll.UseVisualStyleBackColor = true;
            this.btnDAll.Click += new System.EventHandler(this.btnDAll_Click);
            // 
            // btnSAll
            // 
            this.btnSAll.Enabled = false;
            this.btnSAll.Image = global::AssetRegister.Properties.Resources._1436197523_double_arrow_left;
            this.btnSAll.Location = new System.Drawing.Point(336, 149);
            this.btnSAll.Name = "btnSAll";
            this.btnSAll.Size = new System.Drawing.Size(33, 33);
            this.btnSAll.TabIndex = 237;
            this.btnSAll.UseVisualStyleBackColor = true;
            this.btnSAll.Click += new System.EventHandler(this.btnSAll_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Image = global::AssetRegister.Properties.Resources.process_accept_icon;
            this.btnProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProcess.Location = new System.Drawing.Point(627, 359);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(38, 38);
            this.btnProcess.TabIndex = 9;
            this.btnProcess.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            this.btnProcess.MouseHover += new System.EventHandler(this.btnProcess_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(664, 359);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 11;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // chkYear
            // 
            this.chkYear.AutoSize = true;
            this.chkYear.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYear.ForeColor = System.Drawing.Color.Purple;
            this.chkYear.Location = new System.Drawing.Point(77, 15);
            this.chkYear.Name = "chkYear";
            this.chkYear.Size = new System.Drawing.Size(52, 17);
            this.chkYear.TabIndex = 1;
            this.chkYear.Text = "Year";
            this.chkYear.UseVisualStyleBackColor = true;
            this.chkYear.CheckedChanged += new System.EventHandler(this.chkYear_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Purple;
            this.label4.Location = new System.Drawing.Point(77, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 247;
            this.label4.Text = "Total Asset";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Purple;
            this.label7.Location = new System.Drawing.Point(154, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 247;
            this.label7.Text = ":";
            // 
            // lblTotAsset
            // 
            this.lblTotAsset.AutoSize = true;
            this.lblTotAsset.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotAsset.ForeColor = System.Drawing.Color.Purple;
            this.lblTotAsset.Location = new System.Drawing.Point(200, 65);
            this.lblTotAsset.Name = "lblTotAsset";
            this.lblTotAsset.Size = new System.Drawing.Size(14, 13);
            this.lblTotAsset.TabIndex = 247;
            this.lblTotAsset.Text = "0";
            // 
            // btnReset
            // 
            this.btnReset.Image = global::AssetRegister.Properties.Resources.Reset_icon;
            this.btnReset.Location = new System.Drawing.Point(590, 359);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(38, 38);
            this.btnReset.TabIndex = 10;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.MouseHover += new System.EventHandler(this.btnReset_MouseHover);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.label66);
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(705, 38);
            this.panel1.TabIndex = 248;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(7, 11);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(193, 18);
            this.label66.TabIndex = 419;
            this.label66.Text = "Process Depreciation";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(670, 5);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 420;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.dtpToDate);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.dtpFormDate);
            this.panel2.Controls.Add(this.chkDateRange);
            this.panel2.Controls.Add(this.dgvFrm);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.dtpYear);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnReset);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblTotAsset);
            this.panel2.Controls.Add(this.cbCompany);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cbCC);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cbGroup);
            this.panel2.Controls.Add(this.chkYear);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.cmbGroup);
            this.panel2.Controls.Add(this.btnProcess);
            this.panel2.Controls.Add(this.cmbCC);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cmbCompany);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.btnDAll);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.btnSAll);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.btnD);
            this.panel2.Controls.Add(this.dgvTo);
            this.panel2.Controls.Add(this.btnS);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(5, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(705, 400);
            this.panel2.TabIndex = 248;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Purple;
            this.label11.Location = new System.Drawing.Point(213, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 252;
            this.label11.Text = "To";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "MMM-yy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(237, 35);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(63, 21);
            this.dtpToDate.TabIndex = 251;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Purple;
            this.label5.Location = new System.Drawing.Point(135, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 250;
            this.label5.Text = ":";
            // 
            // dtpFormDate
            // 
            this.dtpFormDate.CustomFormat = "MMM-yy";
            this.dtpFormDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFormDate.Location = new System.Drawing.Point(146, 35);
            this.dtpFormDate.Name = "dtpFormDate";
            this.dtpFormDate.Size = new System.Drawing.Size(64, 21);
            this.dtpFormDate.TabIndex = 249;
            this.dtpFormDate.ValueChanged += new System.EventHandler(this.dtpFormDate_ValueChanged);
            // 
            // chkDateRange
            // 
            this.chkDateRange.AutoSize = true;
            this.chkDateRange.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDateRange.ForeColor = System.Drawing.Color.Purple;
            this.chkDateRange.Location = new System.Drawing.Point(77, 38);
            this.chkDateRange.Name = "chkDateRange";
            this.chkDateRange.Size = new System.Drawing.Size(62, 17);
            this.chkDateRange.TabIndex = 248;
            this.chkDateRange.Text = "Period";
            this.chkDateRange.UseVisualStyleBackColor = true;
            this.chkDateRange.CheckedChanged += new System.EventHandler(this.chkDateRange_CheckedChanged);
            // 
            // dtpYear
            // 
            this.dtpYear.CustomFormat = " ";
            this.dtpYear.Enabled = false;
            this.dtpYear.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Location = new System.Drawing.Point(146, 13);
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowUpDown = true;
            this.dtpYear.Size = new System.Drawing.Size(154, 21);
            this.dtpYear.TabIndex = 2;
            this.dtpYear.ValueChanged += new System.EventHandler(this.dtpYear_ValueChanged_1);
            // 
            // cmbGroup
            // 
            this.cmbGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGroup.Enabled = false;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(422, 57);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(223, 21);
            this.cmbGroup.TabIndex = 8;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // cmbCC
            // 
            this.cmbCC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCC.Enabled = false;
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(422, 35);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(223, 21);
            this.cmbCC.TabIndex = 6;
            this.cmbCC.SelectedIndexChanged += new System.EventHandler(this.cmbCC_SelectedIndexChanged);
            // 
            // cmbCompany
            // 
            this.cmbCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCompany.Enabled = false;
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(422, 13);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(223, 21);
            this.cmbCompany.TabIndex = 4;
            this.cmbCompany.SelectedIndexChanged += new System.EventHandler(this.cmbCompany_SelectedIndexChanged);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape6,
            this.rectangleShape5,
            this.rectangleShape4,
            this.rectangleShape3,
            this.rectangleShape1,
            this.rectangleShape2});
            this.shapeContainer2.Size = new System.Drawing.Size(705, 400);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // frmDepreciationProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(715, 448);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDepreciationProcess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Process Depreciation";
            this.Load += new System.EventHandler(this.frmDepreciationProcess_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDepreciationProcess_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFrm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.CheckBox cbGroup;
        private System.Windows.Forms.CheckBox cbCC;
        private System.Windows.Forms.CheckBox cbCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnClose;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private System.Windows.Forms.DataGridView dgvFrm;
        private System.Windows.Forms.DataGridView dgvTo;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape6;
        private System.Windows.Forms.Button btnS;
        private System.Windows.Forms.Button btnD;
        private System.Windows.Forms.Button btnDAll;
        private System.Windows.Forms.Button btnSAll;
        private AssetRegister.FlatComboBox cmbCompany;
        private AssetRegister.FlatComboBox cmbCC;
        private AssetRegister.FlatComboBox cmbGroup;
        private System.Windows.Forms.CheckBox chkYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblTotAsset;
        private System.Windows.Forms.Button btnReset;
        private AssetRegister.FlatDateTimePicker dtpYear;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.CheckBox chkDateRange;
        private FlatDateTimePicker dtpFormDate;
        private System.Windows.Forms.Label label11;
        private FlatDateTimePicker dtpToDate;
        private System.Windows.Forms.Label label5;
    }
}