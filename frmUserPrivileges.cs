﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using AssetRegister.Properties;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmUserPrivileges : Form
    {
        private clsCommonFunctions dbConCrUser = new clsCommonFunctions();
        private bool cmbLoadFlag = false;
        private string tmpFile = "";
        private string[] prList;
        private string UserName = "", UserNo = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmUserPrivileges()
        {
            InitializeComponent();
        }

        private void frmCreateUser_Load(object sender, EventArgs e)
        {
            dbConCrUser.FillDataGridView(dgvListData, @"EXEC prcCmbLoadEmployee 1");
            cmbLoadFlag = true;
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 200;
            InitializeDGV();
            LoadDGV();
        }

        private void InitializeDGV()
        {
            dgvForms.Columns.Clear();
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "X";
            checkColumn.HeaderText = "";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; 
            dgvForms.Columns.Add(checkColumn);
            dgvForms.ColumnHeadersDefaultCellStyle.Font = new Font("Verdana", 7.5F, FontStyle.Bold);
            dgvForms.EnableHeadersVisualStyles = false;
            dgvForms.ColumnHeadersHeight = 25;
        }

        private void LoadDGV()
        {            
            //dgvForms.DataSource =  GetFormLists();
            dbConCrUser.FillDataGridView(dgvForms, @"EXEC prcLoadPrivilegePlan");
            dgvForms.Columns[1].Visible = false;
            dgvForms.Columns[2].Width = 415;
        }

        private DataTable GetFormLists()
        {            
            DataTable dt = new DataTable();
            dt.Columns.Add("Forms", typeof(string));
            
            try
            {
                Assembly MyProject = Assembly.Load("AssetRegister");

                foreach (Type t in MyProject.GetTypes())
                {
                    if (t.BaseType == typeof(Form)
                        && t.Name != "MDIParent"
                        && t.Name != "frmLogin"
                        && t.Name != "frmLoadReport")
                    {
                        int len = t.Name.Length;
                        dt.Rows.Add(t.Name.Substring(3, t.Name.Length - 3));
                    }
                }                
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

            return dt;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbEmpFullName_SelectedIndexChanged(object sender, EventArgs e)
        {
            dbConCrUser.DisposeChecking(text);
            SqlDataReader dtRdr;
            //Image img = Resources.loading_gif;

            try
            {               
                if (cmbLoadFlag && UserNo != "")
                {
                    dtRdr = (SqlDataReader) dbConCrUser.DBCRUDFunction("EXEC prcLoadEmployeeDetails '" + UserNo + "'", true);

                    if (dtRdr.Read())
                    {
                        txtUserId.Text = dtRdr["Number"].ToString();
                        txtUserDept.Text = dtRdr["DepartmentName"].ToString();
                        txtEmail.Text = dtRdr["Email"].ToString();
                        txtUserType.Text = dtRdr["UserType"].ToString();

                        if (tmpFile != txtUserId.Text.Trim())
                        {
                            string website = @"http://www.kdsaccessories.com/kdsxs/EmployeeInfo/Employee_image/"
                                               + txtUserId.Text.Trim() + ".jpg";
                            picUser.ImageLocation = website;
                            picUser.Size = new System.Drawing.Size(88, 87);
                            picUser.SizeMode = PictureBoxSizeMode.StretchImage;
                            tmpFile = txtUserId.Text.Trim();
                        }

                        for (int i = 0; i < dgvForms.RowCount; i++)
                        {
                            dgvForms.Rows[i].Cells[0].Value = false;
                        }
                        
                        prList = dtRdr["PrivilegeList"].ToString().Split(',');
                        
                        for(int i = 0; i < dgvForms.RowCount; i++)
                        {
                            for (int j = 0; j < prList.Count(); j++)
                            {
                                if (prList[j].ToString() == dgvForms.Rows[i].Cells[1].Value.ToString())
                                {
                                    dgvForms.Rows[i].Cells[0].Value = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        txtUserId.Text = "";
                        txtUserDept.Text = "";
                        txtEmail.Text = "";
                    }

                    dtRdr.Close();
                }                
            }
            catch(Exception ex)
            {
                if (ex.Message == "The remote server returned an error: (404) Not Found.")
                {
                    picUser.Image = new Bitmap(Resources.NO_IMAGES);
                    picUser.Size = new System.Drawing.Size(88, 87);
                    picUser.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else
                    MessageBox.Show(ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }        

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAll.Checked)
            {
                foreach (DataGridViewRow row in dgvForms.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    row.Cells[0].Value = true;
                }
            }
            else
            {
                foreach (DataGridViewRow row in dgvForms.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    row.Cells[0].Value = false;
                }
            }            
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            int IsActive = 0;

            if (UserNo != "")
            {
                if (rbYes.Checked == true) IsActive = 1;
                string[] cols = { "[PrivilegeList]", "[IsActive]" };
                string[] vals = { GetAllCheckItem(), IsActive.ToString() };
                string[] whCols = { "[UserECode]" };
                string[] whVals = { UserNo };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "UPDATE", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_UserInfo]", this.Name, frmLogin.userId.ToString(),
                                   "[UserECode],[PrivilegeList],[IsActive]", 
                                   UserNo + ";" + GetAllCheckItem() + ";" + IsActive };

                string sql = dbConCrUser.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UserInfo]", cols, vals, "UPDATE", whCols, whVals);
                sql += dbConCrUser.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null); 


                if ((bool)dbConCrUser.DBCRUDFunction(sql, false))
                {
                    cmbEmpFullName_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Privilege assigned successfully.", "Update",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Select Employee", "User Privilege", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                text.Focus();
            }
        }

        private string GetAllCheckItem()
        {
            string chkdString = "",comma = "";
            
            for (int i = 0; i < dgvForms.RowCount; i++)
            {
                if (Convert.ToBoolean(dgvForms.Rows[i].Cells[0].Value) == true)
                {
                    chkdString += comma + dgvForms.Rows[i].Cells[1].Value;                    
                    comma = ",";
                }
            }

            return chkdString;
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvListData.Rows.Count > 0)
            {
                UserNo = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString().ToString();
                UserName = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                text.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                text.Focus();
                text.SelectionStart = text.TextLength;
                dgvListData.Visible = false;
                LoadDetails();
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                UserNo = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString().ToString();
                UserName = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                text.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                text.Focus();
                text.SelectionStart = text.TextLength;
                dgvListData.Visible = false;
                LoadDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            text.Focus();
            text.SelectionStart = text.TextLength;
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void LoadDetails()
        {
            dbConCrUser.DisposeChecking(text);
            SqlDataReader dtRdr;
            //Image img = Resources.loading_gif;

            try
            {
                if (cmbLoadFlag && UserNo != "")
                {
                    dtRdr = ( SqlDataReader)dbConCrUser.DBCRUDFunction("EXEC prcLoadEmployeeDetails '" + UserNo + "'", true);

                    if (dtRdr.Read())
                    {
                        txtUserId.Text = dtRdr["Number"].ToString();
                        txtUserDept.Text = dtRdr["DepartmentName"].ToString();
                        txtEmail.Text = dtRdr["Email"].ToString();
                        txtUserType.Text = dtRdr["UserType"].ToString();
                        if (dtRdr["IsActive"].ToString() == "1")
                            rbYes.Checked = true;
                        else
                            rbNo.Checked = true;

                        if (tmpFile != txtUserId.Text.Trim())
                        {
                            string website = @"http://www.kdsaccessories.com/kdsxs/EmployeeInfo/Employee_image/"
                                               + txtUserId.Text.Trim() + ".jpg";
                            picUser.ImageLocation = website;
                            picUser.Size = new System.Drawing.Size(88, 87);
                            picUser.SizeMode = PictureBoxSizeMode.StretchImage;
                            tmpFile = txtUserId.Text.Trim();
                        }

                        for (int i = 0; i < dgvForms.RowCount; i++)
                        {
                            dgvForms.Rows[i].Cells[0].Value = false;
                        }

                        prList = dtRdr["PrivilegeList"].ToString().Split(',');

                        for (int i = 0; i < dgvForms.RowCount; i++)
                        {
                            for (int j = 0; j < prList.Count(); j++)
                            {
                                if (prList[j].ToString() == dgvForms.Rows[i].Cells[1].Value.ToString())
                                {
                                    dgvForms.Rows[i].Cells[0].Value = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        txtUserId.Text = "";
                        txtUserDept.Text = "";
                        txtEmail.Text = "";
                    }

                    dtRdr.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "The remote server returned an error: (404) Not Found.")
                {
                    picUser.Image = new Bitmap(Resources.NO_IMAGES);
                    picUser.Size = new System.Drawing.Size(88, 87);
                    picUser.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else
                    MessageBox.Show(ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void text_TextChanged(object sender, EventArgs e)
        {
            dbConCrUser.DisposeChecking(text);
            dbConCrUser.MakeTextUppercase(text);

            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;

                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + text.Text + "%'";
            }
        }

        private void text_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void text_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            {
                text.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                UserNo = dgvListData.Rows[0].Cells[0].Value.ToString().ToString();
                UserName = dgvListData.Rows[0].Cells[1].Value.ToString();
                LoadDetails();
                dgvListData.Visible = false;
                text.SelectionStart = text.TextLength;
            }
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmUserPrivileges_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnApply_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnApply, "Apply");
        }
    }
}
