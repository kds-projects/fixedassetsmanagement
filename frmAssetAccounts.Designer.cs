﻿namespace AssetRegister
{
    partial class frmAssetAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.btnCross = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvCC = new System.Windows.Forms.DataGridView();
            this.dgvGroup = new System.Windows.Forms.DataGridView();
            this.dgvCompany = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.txtGroup = new AssetRegister.FlatTextBox();
            this.txtCC = new AssetRegister.FlatTextBox();
            this.txtCompnay = new AssetRegister.FlatTextBox();
            this.txtAdjCost = new AssetRegister.FlatTextBox();
            this.txtAddCost = new AssetRegister.FlatTextBox();
            this.dtpPeriodDate = new AssetRegister.FlatDateTimePicker();
            this.txtRevCost = new AssetRegister.FlatTextBox();
            this.txtAddDep = new AssetRegister.FlatTextBox();
            this.txtDispCost = new AssetRegister.FlatTextBox();
            this.txtAdjustDep = new AssetRegister.FlatTextBox();
            this.txtDispDep = new AssetRegister.FlatTextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompany)).BeginInit();
            this.SuspendLayout();
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 245);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(740, 39);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(131, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 300;
            this.label9.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 144);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 13);
            this.label16.TabIndex = 299;
            this.label16.Text = "Adjustment Amount";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(131, 122);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 295;
            this.label24.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(7, 122);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(101, 13);
            this.label45.TabIndex = 296;
            this.label45.Text = "Addition Amount";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(7, 188);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(122, 13);
            this.label43.TabIndex = 308;
            this.label43.Text = "Revaluation Amount";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(131, 188);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(12, 13);
            this.label44.TabIndex = 309;
            this.label44.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(504, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 323;
            this.label11.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(367, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 13);
            this.label12.TabIndex = 322;
            this.label12.Text = "Dep. Charged Addition";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(504, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 13);
            this.label14.TabIndex = 326;
            this.label14.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(367, 144);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(141, 13);
            this.label17.TabIndex = 325;
            this.label17.Text = "Adjust. on Depreciation";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(504, 166);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 13);
            this.label18.TabIndex = 329;
            this.label18.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(367, 166);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 13);
            this.label19.TabIndex = 328;
            this.label19.Text = "Dep. on Disposal";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
            this.btnUpdate.Location = new System.Drawing.Point(667, 246);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(38, 38);
            this.btnUpdate.TabIndex = 27;
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            this.btnUpdate.MouseHover += new System.EventHandler(this.btnUpdate_MouseHover);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::AssetRegister.Properties.Resources.Action_file_new_icon1;
            this.btnReset.Location = new System.Drawing.Point(593, 246);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(38, 38);
            this.btnReset.TabIndex = 26;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.MouseHover += new System.EventHandler(this.btnReset_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(704, 246);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 28;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::AssetRegister.Properties.Resources.Actions_stock_save_as_icon;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(630, 246);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(38, 38);
            this.btnSave.TabIndex = 25;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(131, 166);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(12, 13);
            this.label22.TabIndex = 345;
            this.label22.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 166);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 13);
            this.label23.TabIndex = 344;
            this.label23.Text = "Disposal Amount";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(367, 13);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(74, 13);
            this.label36.TabIndex = 331;
            this.label36.Text = "Period Date";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(504, 13);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(12, 13);
            this.label37.TabIndex = 332;
            this.label37.Text = ":";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.label66);
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(744, 38);
            this.panel1.TabIndex = 357;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(7, 9);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(215, 16);
            this.label66.TabIndex = 416;
            this.label66.Text = "Asset A/C Information Entry";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(711, 3);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 418;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.dgvCC);
            this.panel2.Controls.Add(this.dgvGroup);
            this.panel2.Controls.Add(this.dgvCompany);
            this.panel2.Controls.Add(this.txtGroup);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtCC);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtCompnay);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtAdjCost);
            this.panel2.Controls.Add(this.txtAddCost);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.dtpPeriodDate);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.txtRevCost);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtAddDep);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txtDispCost);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtAdjustDep);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.btnReset);
            this.panel2.Controls.Add(this.txtDispDep);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(5, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(744, 286);
            this.panel2.TabIndex = 358;
            // 
            // dgvCC
            // 
            this.dgvCC.AllowUserToAddRows = false;
            this.dgvCC.AllowUserToDeleteRows = false;
            this.dgvCC.AllowUserToResizeColumns = false;
            this.dgvCC.AllowUserToResizeRows = false;
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvCC.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle61;
            this.dgvCC.BackgroundColor = System.Drawing.Color.White;
            this.dgvCC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCC.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle62.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle62.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle62.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle62.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle62;
            this.dgvCC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle63.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle63.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCC.DefaultCellStyle = dataGridViewCellStyle63;
            this.dgvCC.EnableHeadersVisualStyles = false;
            this.dgvCC.Location = new System.Drawing.Point(2, 1);
            this.dgvCC.MultiSelect = false;
            this.dgvCC.Name = "dgvCC";
            this.dgvCC.ReadOnly = true;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle64.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle64.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle64.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCC.RowHeadersDefaultCellStyle = dataGridViewCellStyle64;
            this.dgvCC.RowHeadersVisible = false;
            this.dgvCC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCC.Size = new System.Drawing.Size(205, 285);
            this.dgvCC.TabIndex = 362;
            this.dgvCC.Visible = false;
            this.dgvCC.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCC_CellDoubleClick);
            this.dgvCC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvCC_KeyDown);
            this.dgvCC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvCC_KeyPress);
            // 
            // dgvGroup
            // 
            this.dgvGroup.AllowUserToAddRows = false;
            this.dgvGroup.AllowUserToDeleteRows = false;
            this.dgvGroup.AllowUserToResizeColumns = false;
            this.dgvGroup.AllowUserToResizeRows = false;
            dataGridViewCellStyle65.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvGroup.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle65;
            this.dgvGroup.BackgroundColor = System.Drawing.Color.White;
            this.dgvGroup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvGroup.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle66.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle66.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle66.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle66.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle66.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle66.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle66;
            this.dgvGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle67.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle67.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle67.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle67.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle67.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle67.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGroup.DefaultCellStyle = dataGridViewCellStyle67;
            this.dgvGroup.EnableHeadersVisualStyles = false;
            this.dgvGroup.Location = new System.Drawing.Point(536, 1);
            this.dgvGroup.MultiSelect = false;
            this.dgvGroup.Name = "dgvGroup";
            this.dgvGroup.ReadOnly = true;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle68.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle68.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle68.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle68.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle68.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle68.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGroup.RowHeadersDefaultCellStyle = dataGridViewCellStyle68;
            this.dgvGroup.RowHeadersVisible = false;
            this.dgvGroup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup.Size = new System.Drawing.Size(205, 285);
            this.dgvGroup.TabIndex = 361;
            this.dgvGroup.Visible = false;
            this.dgvGroup.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGroup_CellDoubleClick);
            this.dgvGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvGroup_KeyDown);
            this.dgvGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvGroup_KeyPress);
            // 
            // dgvCompany
            // 
            this.dgvCompany.AllowUserToAddRows = false;
            this.dgvCompany.AllowUserToDeleteRows = false;
            this.dgvCompany.AllowUserToResizeColumns = false;
            this.dgvCompany.AllowUserToResizeRows = false;
            dataGridViewCellStyle69.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvCompany.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle69;
            this.dgvCompany.BackgroundColor = System.Drawing.Color.White;
            this.dgvCompany.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCompany.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle70.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle70.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle70.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle70.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle70.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle70.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCompany.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle70;
            this.dgvCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle71.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle71.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle71.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle71.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle71.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle71.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCompany.DefaultCellStyle = dataGridViewCellStyle71;
            this.dgvCompany.EnableHeadersVisualStyles = false;
            this.dgvCompany.Location = new System.Drawing.Point(536, 0);
            this.dgvCompany.MultiSelect = false;
            this.dgvCompany.Name = "dgvCompany";
            this.dgvCompany.ReadOnly = true;
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle72.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle72.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle72.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle72.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle72.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle72.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCompany.RowHeadersDefaultCellStyle = dataGridViewCellStyle72;
            this.dgvCompany.RowHeadersVisible = false;
            this.dgvCompany.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCompany.Size = new System.Drawing.Size(205, 285);
            this.dgvCompany.TabIndex = 360;
            this.dgvCompany.Visible = false;
            this.dgvCompany.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompany_CellDoubleClick);
            this.dgvCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvCompany_KeyDown);
            this.dgvCompany.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvCompany_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 358;
            this.label10.Text = "Asset Group";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label10.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(131, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 359;
            this.label13.Text = ":";
            this.label13.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(367, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 355;
            this.label7.Text = "Costcentre";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(504, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 356;
            this.label8.Text = ":";
            this.label8.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 352;
            this.label3.Text = "Company";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(131, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 353;
            this.label6.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(386, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 350;
            this.label2.Text = "Depreciation";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 349;
            this.label1.Text = "Cost";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape1,
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(744, 286);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 364;
            this.lineShape3.X2 = 364;
            this.lineShape3.Y1 = 61;
            this.lineShape3.Y2 = 243;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 3;
            this.lineShape2.X2 = 742;
            this.lineShape2.Y1 = 85;
            this.lineShape2.Y2 = 85;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 2;
            this.lineShape1.X2 = 741;
            this.lineShape1.Y1 = 60;
            this.lineShape1.Y2 = 60;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 1);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(740, 242);
            // 
            // txtGroup
            // 
            this.txtGroup.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroup.Location = new System.Drawing.Point(148, 31);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(209, 21);
            this.txtGroup.TabIndex = 357;
            this.txtGroup.Visible = false;
            this.txtGroup.TextChanged += new System.EventHandler(this.txtGroup_TextChanged);
            this.txtGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGroup_KeyDown);
            this.txtGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGroup_KeyPress);
            // 
            // txtCC
            // 
            this.txtCC.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtCC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCC.Location = new System.Drawing.Point(522, 31);
            this.txtCC.Name = "txtCC";
            this.txtCC.Size = new System.Drawing.Size(209, 21);
            this.txtCC.TabIndex = 354;
            this.txtCC.Visible = false;
            this.txtCC.TextChanged += new System.EventHandler(this.txtCC_TextChanged);
            this.txtCC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCC_KeyDown);
            this.txtCC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCC_KeyPress);
            // 
            // txtCompnay
            // 
            this.txtCompnay.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtCompnay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCompnay.Location = new System.Drawing.Point(148, 9);
            this.txtCompnay.Name = "txtCompnay";
            this.txtCompnay.Size = new System.Drawing.Size(209, 21);
            this.txtCompnay.TabIndex = 1;
            this.txtCompnay.TextChanged += new System.EventHandler(this.txtCompnay_TextChanged);
            this.txtCompnay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCompnay_KeyDown);
            this.txtCompnay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCompnay_KeyPress);
            // 
            // txtAdjCost
            // 
            this.txtAdjCost.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAdjCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdjCost.Location = new System.Drawing.Point(148, 140);
            this.txtAdjCost.Name = "txtAdjCost";
            this.txtAdjCost.Size = new System.Drawing.Size(209, 21);
            this.txtAdjCost.TabIndex = 3;
            this.txtAdjCost.Leave += new System.EventHandler(this.txtAdjCost_Leave);
            this.txtAdjCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAdjCost_KeyPress);
            this.txtAdjCost.Enter += new System.EventHandler(this.txtAdjCost_Enter);
            // 
            // txtAddCost
            // 
            this.txtAddCost.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAddCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddCost.Location = new System.Drawing.Point(148, 118);
            this.txtAddCost.Name = "txtAddCost";
            this.txtAddCost.Size = new System.Drawing.Size(209, 21);
            this.txtAddCost.TabIndex = 2;
            this.txtAddCost.Leave += new System.EventHandler(this.txtAddCost_Leave);
            this.txtAddCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddCost_KeyPress);
            this.txtAddCost.Enter += new System.EventHandler(this.txtAddCost_Enter);
            // 
            // dtpPeriodDate
            // 
            this.dtpPeriodDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpPeriodDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPeriodDate.Location = new System.Drawing.Point(522, 9);
            this.dtpPeriodDate.Name = "dtpPeriodDate";
            this.dtpPeriodDate.Size = new System.Drawing.Size(209, 21);
            this.dtpPeriodDate.TabIndex = 9;
            this.dtpPeriodDate.ValueChanged += new System.EventHandler(this.dtpPeriodDate_ValueChanged);
            // 
            // txtRevCost
            // 
            this.txtRevCost.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtRevCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRevCost.Location = new System.Drawing.Point(148, 184);
            this.txtRevCost.Name = "txtRevCost";
            this.txtRevCost.Size = new System.Drawing.Size(209, 21);
            this.txtRevCost.TabIndex = 5;
            this.txtRevCost.Leave += new System.EventHandler(this.txtRevCost_Leave);
            this.txtRevCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRevCost_KeyPress);
            this.txtRevCost.Enter += new System.EventHandler(this.txtRevCost_Enter);
            // 
            // txtAddDep
            // 
            this.txtAddDep.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAddDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddDep.Location = new System.Drawing.Point(522, 118);
            this.txtAddDep.Name = "txtAddDep";
            this.txtAddDep.Size = new System.Drawing.Size(209, 21);
            this.txtAddDep.TabIndex = 6;
            this.txtAddDep.Leave += new System.EventHandler(this.txtAddDep_Leave);
            this.txtAddDep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddDep_KeyPress);
            this.txtAddDep.Enter += new System.EventHandler(this.txtAddDep_Enter);
            // 
            // txtDispCost
            // 
            this.txtDispCost.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDispCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDispCost.Location = new System.Drawing.Point(148, 162);
            this.txtDispCost.Name = "txtDispCost";
            this.txtDispCost.Size = new System.Drawing.Size(209, 21);
            this.txtDispCost.TabIndex = 4;
            this.txtDispCost.Leave += new System.EventHandler(this.txtDispCost_Leave);
            this.txtDispCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDispCost_KeyPress);
            this.txtDispCost.Enter += new System.EventHandler(this.txtDispCost_Enter);
            // 
            // txtAdjustDep
            // 
            this.txtAdjustDep.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAdjustDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdjustDep.Location = new System.Drawing.Point(522, 140);
            this.txtAdjustDep.Name = "txtAdjustDep";
            this.txtAdjustDep.Size = new System.Drawing.Size(209, 21);
            this.txtAdjustDep.TabIndex = 7;
            this.txtAdjustDep.Leave += new System.EventHandler(this.txtAdjustDep_Leave);
            this.txtAdjustDep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAdjustDep_KeyPress);
            this.txtAdjustDep.Enter += new System.EventHandler(this.txtAdjustDep_Enter);
            // 
            // txtDispDep
            // 
            this.txtDispDep.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDispDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDispDep.Location = new System.Drawing.Point(522, 162);
            this.txtDispDep.Name = "txtDispDep";
            this.txtDispDep.Size = new System.Drawing.Size(209, 21);
            this.txtDispDep.TabIndex = 8;
            this.txtDispDep.Leave += new System.EventHandler(this.txtDispDep_Leave);
            this.txtDispDep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDispDep_KeyPress);
            this.txtDispDep.Enter += new System.EventHandler(this.txtDispDep_Enter);
            // 
            // frmAssetAccounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(754, 335);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAssetAccounts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asset A/C Information Entry";
            this.Load += new System.EventHandler(this.frmAssetConfigLand_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmAssetConfigLand_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompany)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private FlatTextBox txtAddDep;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private FlatTextBox txtAdjustDep;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private FlatTextBox txtDispDep;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private FlatTextBox txtDispCost;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private FlatTextBox txtRevCost;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private AssetRegister.FlatDateTimePicker dtpPeriodDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private FlatTextBox txtAdjCost;
        private FlatTextBox txtAddCost;
        private System.Windows.Forms.Label label2;
        private FlatTextBox txtGroup;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private FlatTextBox txtCC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private FlatTextBox txtCompnay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.DataGridView dgvCC;
        private System.Windows.Forms.DataGridView dgvGroup;
        private System.Windows.Forms.DataGridView dgvCompany;
    }
}