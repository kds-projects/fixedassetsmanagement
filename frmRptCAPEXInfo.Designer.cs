﻿namespace AssetRegister
{
    partial class frmRptCAPEXInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvListData = new System.Windows.Forms.DataGridView();
            this.chkAllGroup = new System.Windows.Forms.CheckBox();
            this.chkAllCC = new System.Windows.Forms.CheckBox();
            this.chkAllCompany = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.chkAllAddDate = new System.Windows.Forms.CheckBox();
            this.rbAddition = new System.Windows.Forms.RadioButton();
            this.rbRevaluation = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkAllRevDate = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.btnCross = new System.Windows.Forms.Button();
            this.dtpTRevDate = new AssetRegister.FlatDateTimePicker();
            this.dtpFRevDate = new AssetRegister.FlatDateTimePicker();
            this.dtpTAddDate = new AssetRegister.FlatDateTimePicker();
            this.dtpFAddDate = new AssetRegister.FlatDateTimePicker();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.cmbGroup = new AssetRegister.FlatComboBox();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 45);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(421, 198);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BackColor = System.Drawing.SystemColors.Control;
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 245);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(421, 39);
            // 
            // btnShow
            // 
            this.btnShow.Image = global::AssetRegister.Properties.Resources.Actions_document_find_icon;
            this.btnShow.Location = new System.Drawing.Point(348, 246);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(38, 38);
            this.btnShow.TabIndex = 9;
            this.btnShow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            this.btnShow.MouseHover += new System.EventHandler(this.btnShow_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(385, 246);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 10;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // dgvListData
            // 
            this.dgvListData.AllowUserToAddRows = false;
            this.dgvListData.AllowUserToDeleteRows = false;
            this.dgvListData.AllowUserToResizeColumns = false;
            this.dgvListData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvListData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListData.BackgroundColor = System.Drawing.Color.White;
            this.dgvListData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvListData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvListData.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvListData.EnableHeadersVisualStyles = false;
            this.dgvListData.Location = new System.Drawing.Point(274, 45);
            this.dgvListData.MultiSelect = false;
            this.dgvListData.Name = "dgvListData";
            this.dgvListData.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvListData.RowHeadersVisible = false;
            this.dgvListData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListData.Size = new System.Drawing.Size(150, 199);
            this.dgvListData.TabIndex = 319;
            this.dgvListData.Visible = false;
            this.dgvListData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListData_CellDoubleClick);
            this.dgvListData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvListData_KeyDown);
            this.dgvListData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvListData_KeyPress);
            // 
            // chkAllGroup
            // 
            this.chkAllGroup.AutoSize = true;
            this.chkAllGroup.Checked = true;
            this.chkAllGroup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllGroup.Location = new System.Drawing.Point(373, 103);
            this.chkAllGroup.Name = "chkAllGroup";
            this.chkAllGroup.Size = new System.Drawing.Size(40, 17);
            this.chkAllGroup.TabIndex = 329;
            this.chkAllGroup.Text = "All";
            this.chkAllGroup.UseVisualStyleBackColor = true;
            this.chkAllGroup.CheckedChanged += new System.EventHandler(this.chkAllGroup_CheckedChanged);
            // 
            // chkAllCC
            // 
            this.chkAllCC.AutoSize = true;
            this.chkAllCC.Checked = true;
            this.chkAllCC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCC.Location = new System.Drawing.Point(373, 81);
            this.chkAllCC.Name = "chkAllCC";
            this.chkAllCC.Size = new System.Drawing.Size(40, 17);
            this.chkAllCC.TabIndex = 330;
            this.chkAllCC.Text = "All";
            this.chkAllCC.UseVisualStyleBackColor = true;
            this.chkAllCC.CheckedChanged += new System.EventHandler(this.chkAllCC_CheckedChanged);
            // 
            // chkAllCompany
            // 
            this.chkAllCompany.AutoSize = true;
            this.chkAllCompany.Checked = true;
            this.chkAllCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCompany.Location = new System.Drawing.Point(373, 59);
            this.chkAllCompany.Name = "chkAllCompany";
            this.chkAllCompany.Size = new System.Drawing.Size(40, 17);
            this.chkAllCompany.TabIndex = 331;
            this.chkAllCompany.Text = "All";
            this.chkAllCompany.UseVisualStyleBackColor = true;
            this.chkAllCompany.CheckedChanged += new System.EventHandler(this.chkAllCompany_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(140, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 325;
            this.label9.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 324;
            this.label16.Text = "Group";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 322;
            this.label4.Text = "Company Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(140, 83);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 320;
            this.label24.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(13, 83);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 13);
            this.label45.TabIndex = 321;
            this.label45.Text = "Cost Center";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(140, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 323;
            this.label5.Text = ":";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 334;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 333;
            this.label2.Text = "Asset Code";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(158, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 337;
            this.label3.Text = "To";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(158, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 338;
            this.label12.Text = "From";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(140, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 341;
            this.label6.Text = ":";
            // 
            // chkAllAddDate
            // 
            this.chkAllAddDate.AutoSize = true;
            this.chkAllAddDate.Checked = true;
            this.chkAllAddDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllAddDate.Enabled = false;
            this.chkAllAddDate.Location = new System.Drawing.Point(373, 158);
            this.chkAllAddDate.Name = "chkAllAddDate";
            this.chkAllAddDate.Size = new System.Drawing.Size(40, 17);
            this.chkAllAddDate.TabIndex = 342;
            this.chkAllAddDate.Text = "All";
            this.chkAllAddDate.UseVisualStyleBackColor = true;
            this.chkAllAddDate.CheckedChanged += new System.EventHandler(this.chkAllAddDate_CheckedChanged);
            // 
            // rbAddition
            // 
            this.rbAddition.AutoSize = true;
            this.rbAddition.Location = new System.Drawing.Point(13, 157);
            this.rbAddition.Name = "rbAddition";
            this.rbAddition.Size = new System.Drawing.Size(102, 17);
            this.rbAddition.TabIndex = 345;
            this.rbAddition.TabStop = true;
            this.rbAddition.Text = "Addition Date";
            this.rbAddition.UseVisualStyleBackColor = true;
            this.rbAddition.CheckedChanged += new System.EventHandler(this.rbAddition_CheckedChanged);
            // 
            // rbRevaluation
            // 
            this.rbRevaluation.AutoSize = true;
            this.rbRevaluation.Location = new System.Drawing.Point(13, 202);
            this.rbRevaluation.Name = "rbRevaluation";
            this.rbRevaluation.Size = new System.Drawing.Size(123, 17);
            this.rbRevaluation.TabIndex = 345;
            this.rbRevaluation.TabStop = true;
            this.rbRevaluation.Text = "Revaluation Date";
            this.rbRevaluation.UseVisualStyleBackColor = true;
            this.rbRevaluation.CheckedChanged += new System.EventHandler(this.rbRevaluation_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(140, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 341;
            this.label7.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 338;
            this.label8.Text = "From";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(158, 215);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 337;
            this.label10.Text = "To";
            // 
            // chkAllRevDate
            // 
            this.chkAllRevDate.AutoSize = true;
            this.chkAllRevDate.Checked = true;
            this.chkAllRevDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllRevDate.Enabled = false;
            this.chkAllRevDate.Location = new System.Drawing.Point(373, 203);
            this.chkAllRevDate.Name = "chkAllRevDate";
            this.chkAllRevDate.Size = new System.Drawing.Size(40, 17);
            this.chkAllRevDate.TabIndex = 346;
            this.chkAllRevDate.Text = "All";
            this.chkAllRevDate.UseVisualStyleBackColor = true;
            this.chkAllRevDate.CheckedChanged += new System.EventHandler(this.chkAllRevDate_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.dgvListData);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.dtpTRevDate);
            this.panel1.Controls.Add(this.btnShow);
            this.panel1.Controls.Add(this.dtpFRevDate);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dtpTAddDate);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.dtpFAddDate);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.chkAllRevDate);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.cmbCompany);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cmbGroup);
            this.panel1.Controls.Add(this.rbRevaluation);
            this.panel1.Controls.Add(this.cmbCC);
            this.panel1.Controls.Add(this.rbAddition);
            this.panel1.Controls.Add(this.chkAllCompany);
            this.panel1.Controls.Add(this.chkAllAddDate);
            this.panel1.Controls.Add(this.chkAllCC);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.chkAllGroup);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtAssetCode);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.shapeContainer2);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(427, 287);
            this.panel1.TabIndex = 351;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.DarkSlateGray;
            this.label11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(421, 40);
            this.label11.TabIndex = 352;
            this.label11.Text = "   CAPEX Info Report";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape3,
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(427, 287);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // rectangleShape3
            // 
            this.rectangleShape3.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape3.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape3.Name = "rectangleShape3";
            this.rectangleShape3.Size = new System.Drawing.Size(421, 41);
            // 
            // btnCross
            // 
            this.btnCross.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(393, 8);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 430;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = false;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // dtpTRevDate
            // 
            this.dtpTRevDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTRevDate.Enabled = false;
            this.dtpTRevDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTRevDate.Location = new System.Drawing.Point(223, 211);
            this.dtpTRevDate.Name = "dtpTRevDate";
            this.dtpTRevDate.Size = new System.Drawing.Size(144, 21);
            this.dtpTRevDate.TabIndex = 350;
            // 
            // dtpFRevDate
            // 
            this.dtpFRevDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFRevDate.Enabled = false;
            this.dtpFRevDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFRevDate.Location = new System.Drawing.Point(223, 189);
            this.dtpFRevDate.Name = "dtpFRevDate";
            this.dtpFRevDate.Size = new System.Drawing.Size(144, 21);
            this.dtpFRevDate.TabIndex = 349;
            // 
            // dtpTAddDate
            // 
            this.dtpTAddDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTAddDate.Enabled = false;
            this.dtpTAddDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTAddDate.Location = new System.Drawing.Point(223, 167);
            this.dtpTAddDate.Name = "dtpTAddDate";
            this.dtpTAddDate.Size = new System.Drawing.Size(144, 21);
            this.dtpTAddDate.TabIndex = 348;
            // 
            // dtpFAddDate
            // 
            this.dtpFAddDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFAddDate.Enabled = false;
            this.dtpFAddDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFAddDate.Location = new System.Drawing.Point(223, 145);
            this.dtpFAddDate.Name = "dtpFAddDate";
            this.dtpFAddDate.Size = new System.Drawing.Size(144, 21);
            this.dtpFAddDate.TabIndex = 347;
            // 
            // cmbCompany
            // 
            this.cmbCompany.Enabled = false;
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(158, 57);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(209, 21);
            this.cmbCompany.TabIndex = 1;
            // 
            // cmbGroup
            // 
            this.cmbGroup.Enabled = false;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(158, 101);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(209, 21);
            this.cmbGroup.TabIndex = 3;
            // 
            // cmbCC
            // 
            this.cmbCC.Enabled = false;
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(158, 79);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(209, 21);
            this.cmbCC.TabIndex = 2;
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Location = new System.Drawing.Point(158, 123);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.Size = new System.Drawing.Size(209, 21);
            this.txtAssetCode.TabIndex = 4;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            // 
            // frmRptCAPEXInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(437, 297);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRptCAPEXInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAPEX Info Report";
            this.Load += new System.EventHandler(this.frmRptCAPEXInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvListData;
        private System.Windows.Forms.CheckBox chkAllGroup;
        private System.Windows.Forms.CheckBox chkAllCC;
        private System.Windows.Forms.CheckBox chkAllCompany;
        private AssetRegister.FlatComboBox cmbCC;
        private AssetRegister.FlatComboBox cmbGroup;
        private AssetRegister.FlatComboBox cmbCompany;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label5;
        private FlatTextBox txtAssetCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkAllAddDate;
        private System.Windows.Forms.RadioButton rbAddition;
        private System.Windows.Forms.RadioButton rbRevaluation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkAllRevDate;
        private AssetRegister.FlatDateTimePicker dtpTAddDate;
        private AssetRegister.FlatDateTimePicker dtpFAddDate;
        private AssetRegister.FlatDateTimePicker dtpTRevDate;
        private AssetRegister.FlatDateTimePicker dtpFRevDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
        private System.Windows.Forms.Button btnCross;
    }
}