﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace AssetRegister
{
    public partial class frmRptCAPEXInfo : Form
    {
        private clsCommonFunctions dbConCAPEXInfo = new clsCommonFunctions();
        private int AssetId = 0;
        private string CompanyId = "", CostCentreCode = "", AssetCode = "", AssetGroupId = "";

        public frmRptCAPEXInfo()
        {
            InitializeComponent();
        }

        private void frmRptCAPEXInfo_Load(object sender, EventArgs e)
        {
            dbConCAPEXInfo.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConCAPEXInfo.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConCAPEXInfo.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 0");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;

            dbConCAPEXInfo.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '', '', 0");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConCAPEXInfo.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;

            string frmRevDate = "", toRevDate = "", frmAddDate = "", toAddDate = "";

            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "CP";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "PL";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }

            if (!chkAllGroup.Checked)
            {
                if (cmbGroup.SelectedIndex > -1)
                {
                    AssetGroupId = cmbGroup.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbGroup.Focus();
                    return;
                }
            }

            if (!chkAllAddDate.Checked)
            {
                frmAddDate = dtpFAddDate.Value.ToString("yyyy-MM-dd");
                toAddDate = dtpTAddDate.Value.ToString("yyyy-MM-dd");
            }

            if (!chkAllRevDate.Checked)
            {
                frmRevDate = dtpFRevDate.Value.ToString("yyyy-MM-dd");
                toRevDate = dtpTRevDate.Value.ToString("yyyy-MM-dd");
            }

            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptCAPEXInfo.rpt");
                cryReport.SetParameterValue("@CompanyId", CompanyId);
                cryReport.SetParameterValue("@CCCode", CostCentreCode);
                cryReport.SetParameterValue("@AssetGroupId", AssetGroupId);
                cryReport.SetParameterValue("@AssetId", AssetId);
                cryReport.SetParameterValue("@StAddDate", frmAddDate);
                cryReport.SetParameterValue("@EnAddDate", toAddDate);
                cryReport.SetParameterValue("@StRevDate", frmRevDate);
                cryReport.SetParameterValue("@EnRevDate", toRevDate);
                dbConCAPEXInfo.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.Text = "CAPEX Info Report";
                rptForm.crViewer.Refresh();
                rptForm.Show();
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void chkAllGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !chkAllGroup.Checked;
        }

        private void chkAllAddDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFAddDate.Enabled = !chkAllAddDate.Checked;
            dtpTAddDate.Enabled = !chkAllAddDate.Checked;
        }

        private void rbAddition_CheckedChanged(object sender, EventArgs e)
        {
            chkAllAddDate.Enabled = true;
            chkAllRevDate.Enabled = false;
            chkAllAddDate.Checked = true;
        }

        private void chkAllRevDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFRevDate.Enabled = !chkAllRevDate.Checked;
            dtpTRevDate.Enabled = !chkAllRevDate.Checked;
        }

        private void rbRevaluation_CheckedChanged(object sender, EventArgs e)
        {
            chkAllRevDate.Enabled = true;
            chkAllAddDate.Enabled = false;
            chkAllRevDate.Checked = true;
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
