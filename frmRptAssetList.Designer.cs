﻿namespace AssetRegister.Reports
{
    partial class frmRptAssetList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.chkAllCompany = new System.Windows.Forms.CheckBox();
            this.chkAllCC = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkAllDate = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chkOnlyCompany = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCross = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtpTRevDate = new AssetRegister.FlatDateTimePicker();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.dtpToDate = new AssetRegister.FlatDateTimePicker();
            this.dtpFrmDate = new AssetRegister.FlatDateTimePicker();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(400, 130);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 134);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(400, 39);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 287;
            this.label4.Text = "Company Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(118, 34);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 285;
            this.label24.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(17, 34);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 13);
            this.label45.TabIndex = 286;
            this.label45.Text = "Cost Center";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 288;
            this.label5.Text = ":";
            // 
            // btnShow
            // 
            this.btnShow.Image = global::AssetRegister.Properties.Resources.Actions_document_find_icon;
            this.btnShow.Location = new System.Drawing.Point(327, 135);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(38, 38);
            this.btnShow.TabIndex = 16;
            this.btnShow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            this.btnShow.MouseHover += new System.EventHandler(this.btnShow_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(364, 135);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 17;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // chkAllCompany
            // 
            this.chkAllCompany.AutoSize = true;
            this.chkAllCompany.Checked = true;
            this.chkAllCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCompany.Location = new System.Drawing.Point(348, 10);
            this.chkAllCompany.Name = "chkAllCompany";
            this.chkAllCompany.Size = new System.Drawing.Size(40, 17);
            this.chkAllCompany.TabIndex = 1;
            this.chkAllCompany.Text = "All";
            this.chkAllCompany.UseVisualStyleBackColor = true;
            this.chkAllCompany.CheckedChanged += new System.EventHandler(this.chkAllCompany_CheckedChanged);
            // 
            // chkAllCC
            // 
            this.chkAllCC.AutoSize = true;
            this.chkAllCC.Checked = true;
            this.chkAllCC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCC.Location = new System.Drawing.Point(348, 32);
            this.chkAllCC.Name = "chkAllCC";
            this.chkAllCC.Size = new System.Drawing.Size(40, 17);
            this.chkAllCC.TabIndex = 3;
            this.chkAllCC.Text = "All";
            this.chkAllCC.UseVisualStyleBackColor = true;
            this.chkAllCC.CheckedChanged += new System.EventHandler(this.chkAllCC_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(146, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 300;
            this.label12.Text = "From";
            // 
            // chkAllDate
            // 
            this.chkAllDate.AutoSize = true;
            this.chkAllDate.Checked = true;
            this.chkAllDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllDate.Location = new System.Drawing.Point(348, 64);
            this.chkAllDate.Name = "chkAllDate";
            this.chkAllDate.Size = new System.Drawing.Size(40, 17);
            this.chkAllDate.TabIndex = 7;
            this.chkAllDate.Text = "All";
            this.chkAllDate.UseVisualStyleBackColor = true;
            this.chkAllDate.CheckedChanged += new System.EventHandler(this.chkAllDate_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 297;
            this.label3.Text = "Purchase Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 300;
            this.label1.Text = "To";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(118, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 290;
            this.label2.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 177);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 13);
            this.label11.TabIndex = 303;
            this.label11.Text = "Revaluation Date";
            this.label11.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(146, 188);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 13);
            this.label14.TabIndex = 304;
            this.label14.Text = "To";
            this.label14.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(118, 177);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(12, 13);
            this.label15.TabIndex = 302;
            this.label15.Text = ":";
            this.label15.Visible = false;
            // 
            // chkOnlyCompany
            // 
            this.chkOnlyCompany.AutoSize = true;
            this.chkOnlyCompany.Checked = true;
            this.chkOnlyCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOnlyCompany.Location = new System.Drawing.Point(192, 99);
            this.chkOnlyCompany.Name = "chkOnlyCompany";
            this.chkOnlyCompany.Size = new System.Drawing.Size(152, 17);
            this.chkOnlyCompany.TabIndex = 306;
            this.chkOnlyCompany.Text = "Only Company Assets";
            this.chkOnlyCompany.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Controls.Add(this.label66);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(405, 44);
            this.panel1.TabIndex = 307;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(372, 7);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 421;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(17, 15);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(267, 16);
            this.label66.TabIndex = 419;
            this.label66.Text = "Fixed Assets - Land Register Report";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.chkOnlyCompany);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.btnShow);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.dtpTRevDate);
            this.panel2.Controls.Add(this.cmbCompany);
            this.panel2.Controls.Add(this.cmbCC);
            this.panel2.Controls.Add(this.dtpToDate);
            this.panel2.Controls.Add(this.chkAllCompany);
            this.panel2.Controls.Add(this.dtpFrmDate);
            this.panel2.Controls.Add(this.chkAllCC);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.chkAllDate);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(5, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(405, 176);
            this.panel2.TabIndex = 307;
            // 
            // dtpTRevDate
            // 
            this.dtpTRevDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTRevDate.Enabled = false;
            this.dtpTRevDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTRevDate.Location = new System.Drawing.Point(200, 184);
            this.dtpTRevDate.Name = "dtpTRevDate";
            this.dtpTRevDate.Size = new System.Drawing.Size(144, 21);
            this.dtpTRevDate.TabIndex = 15;
            this.dtpTRevDate.Visible = false;
            // 
            // cmbCompany
            // 
            this.cmbCompany.Enabled = false;
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(135, 8);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(209, 21);
            this.cmbCompany.TabIndex = 2;
            // 
            // cmbCC
            // 
            this.cmbCC.Enabled = false;
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(135, 30);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(209, 21);
            this.cmbCC.TabIndex = 4;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpToDate.Enabled = false;
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(200, 74);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(144, 21);
            this.dtpToDate.TabIndex = 9;
            // 
            // dtpFrmDate
            // 
            this.dtpFrmDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpFrmDate.Enabled = false;
            this.dtpFrmDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrmDate.Location = new System.Drawing.Point(200, 52);
            this.dtpFrmDate.Name = "dtpFrmDate";
            this.dtpFrmDate.Size = new System.Drawing.Size(144, 21);
            this.dtpFrmDate.TabIndex = 8;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(405, 176);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // frmRptAssetList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(415, 231);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRptAssetList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Land Register Report";
            this.Load += new System.EventHandler(this.frmRptAssetList_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmRptAssetList_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private AssetRegister.FlatComboBox cmbCompany;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label5;
        private AssetRegister.FlatComboBox cmbCC;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnClose;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private System.Windows.Forms.CheckBox chkAllCompany;
        private System.Windows.Forms.CheckBox chkAllCC;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkAllDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private AssetRegister.FlatDateTimePicker dtpFrmDate;
        private AssetRegister.FlatDateTimePicker dtpToDate;
        private AssetRegister.FlatDateTimePicker dtpTRevDate;
        private System.Windows.Forms.CheckBox chkOnlyCompany;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btnCross;
    }
}