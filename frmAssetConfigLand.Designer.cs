﻿namespace AssetRegister
{
    partial class frmAssetConfigLand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label30 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.dgvListData = new System.Windows.Forms.DataGridView();
            this.label29 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.dgvLocation = new System.Windows.Forms.DataGridView();
            this.dgvSubLoc = new System.Windows.Forms.DataGridView();
            this.dgvBank = new System.Windows.Forms.DataGridView();
            this.dgvBranch = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.btnCross = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.txtVoucherNo = new AssetRegister.FlatTextBox();
            this.txtRecFileNo = new AssetRegister.FlatTextBox();
            this.dtpPurchaseDate = new AssetRegister.FlatDateTimePicker();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.txtBranch = new AssetRegister.FlatTextBox();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.txtBank = new AssetRegister.FlatTextBox();
            this.cmbGroup = new AssetRegister.FlatComboBox();
            this.txtSelller = new AssetRegister.FlatTextBox();
            this.txtDeedValue = new AssetRegister.FlatTextBox();
            this.txtComments = new AssetRegister.FlatTextBox();
            this.txtAreaMutation = new AssetRegister.FlatTextBox();
            this.txtRegCost = new AssetRegister.FlatTextBox();
            this.txtAreaDeed = new AssetRegister.FlatTextBox();
            this.txtDevCost = new AssetRegister.FlatTextBox();
            this.txtOthers = new AssetRegister.FlatTextBox();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            this.txtDeedNo = new AssetRegister.FlatTextBox();
            this.txtAsset = new AssetRegister.FlatTextBox();
            this.txtDaagNo = new AssetRegister.FlatTextBox();
            this.txtKhatianNo = new AssetRegister.FlatTextBox();
            this.txtSubLoc = new AssetRegister.FlatTextBox();
            this.txtMouzaName = new AssetRegister.FlatTextBox();
            this.txtLoc = new AssetRegister.FlatTextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtAccCode = new AssetRegister.FlatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBranch)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 1);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(740, 294);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 297);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(740, 39);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(131, 142);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 13);
            this.label30.TabIndex = 303;
            this.label30.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 301;
            this.label2.Text = "Deed Value";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(131, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 300;
            this.label9.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 299;
            this.label16.Text = "Group";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 297;
            this.label4.Text = "Company Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(131, 32);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 295;
            this.label24.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(7, 32);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 13);
            this.label45.TabIndex = 296;
            this.label45.Text = "Cost Center";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(131, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 298;
            this.label5.Text = ":";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(7, 98);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(106, 13);
            this.label43.TabIndex = 308;
            this.label43.Text = "Registration Date";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(131, 98);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(12, 13);
            this.label44.TabIndex = 309;
            this.label44.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(504, 197);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 311;
            this.label13.Text = ":";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(367, 197);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 13);
            this.label15.TabIndex = 310;
            this.label15.Text = "Comments";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 314;
            this.label1.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 313;
            this.label3.Text = "Registration Cost";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(132, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 320;
            this.label8.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 13);
            this.label10.TabIndex = 319;
            this.label10.Text = "Developement Cost";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(504, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 323;
            this.label11.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(367, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 322;
            this.label12.Text = "Dalil No.";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(504, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 13);
            this.label14.TabIndex = 326;
            this.label14.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(367, 54);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 325;
            this.label17.Text = "Daag No.";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(504, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 13);
            this.label18.TabIndex = 329;
            this.label18.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(367, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 328;
            this.label19.Text = "Khatian No.";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(504, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(12, 13);
            this.label20.TabIndex = 332;
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(367, 98);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 331;
            this.label21.Text = "Mouza Name";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(504, 164);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(12, 13);
            this.label34.TabIndex = 336;
            this.label34.Text = ":";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(504, 142);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(12, 13);
            this.label32.TabIndex = 337;
            this.label32.Text = ":";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(367, 164);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(115, 13);
            this.label33.TabIndex = 335;
            this.label33.Text = "Asset Sub Location";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(367, 142);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 13);
            this.label31.TabIndex = 334;
            this.label31.Text = "Asset Location";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
            this.btnUpdate.Location = new System.Drawing.Point(667, 298);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(38, 38);
            this.btnUpdate.TabIndex = 27;
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            this.btnUpdate.MouseHover += new System.EventHandler(this.btnUpdate_MouseHover);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::AssetRegister.Properties.Resources.Action_file_new_icon1;
            this.btnReset.Location = new System.Drawing.Point(593, 298);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(38, 38);
            this.btnReset.TabIndex = 26;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.MouseHover += new System.EventHandler(this.btnReset_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(704, 298);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 28;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::AssetRegister.Properties.Resources.Actions_stock_save_as_icon;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(630, 298);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(38, 38);
            this.btnSave.TabIndex = 25;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(131, 76);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(12, 13);
            this.label22.TabIndex = 345;
            this.label22.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 13);
            this.label23.TabIndex = 344;
            this.label23.Text = "Asset";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(504, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(12, 13);
            this.label25.TabIndex = 348;
            this.label25.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(367, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(72, 13);
            this.label26.TabIndex = 347;
            this.label26.Text = "Asset Code";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvListData
            // 
            this.dgvListData.AllowUserToAddRows = false;
            this.dgvListData.AllowUserToDeleteRows = false;
            this.dgvListData.AllowUserToResizeColumns = false;
            this.dgvListData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvListData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListData.BackgroundColor = System.Drawing.Color.White;
            this.dgvListData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvListData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvListData.EnableHeadersVisualStyles = false;
            this.dgvListData.Location = new System.Drawing.Point(156, 1);
            this.dgvListData.MultiSelect = false;
            this.dgvListData.Name = "dgvListData";
            this.dgvListData.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvListData.RowHeadersVisible = false;
            this.dgvListData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListData.Size = new System.Drawing.Size(205, 295);
            this.dgvListData.TabIndex = 349;
            this.dgvListData.Visible = false;
            this.dgvListData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListData_CellDoubleClick);
            this.dgvListData.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvListData_DataError);
            this.dgvListData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvListData_KeyDown);
            this.dgvListData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvListData_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(132, 208);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 13);
            this.label29.TabIndex = 317;
            this.label29.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(8, 208);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(106, 13);
            this.label35.TabIndex = 316;
            this.label35.Text = "Others (Sur Plus)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(367, 120);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(132, 13);
            this.label36.TabIndex = 331;
            this.label36.Text = "Area Decim(Mutation)";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(504, 120);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(12, 13);
            this.label37.TabIndex = 332;
            this.label37.Text = ":";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(131, 120);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(12, 13);
            this.label38.TabIndex = 309;
            this.label38.Text = ":";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 120);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(118, 13);
            this.label39.TabIndex = 308;
            this.label39.Text = "Area Decim (Deed)";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 350;
            this.label6.Text = "Land Seller";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(132, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 351;
            this.label7.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(367, 230);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 13);
            this.label27.TabIndex = 310;
            this.label27.Text = "Mortgage Bank";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(504, 230);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 13);
            this.label28.TabIndex = 311;
            this.label28.Text = ":";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(504, 252);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(12, 13);
            this.label40.TabIndex = 311;
            this.label40.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(367, 252);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(137, 13);
            this.label41.TabIndex = 310;
            this.label41.Text = "Mortgage Bank Branch";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvLocation
            // 
            this.dgvLocation.AllowUserToAddRows = false;
            this.dgvLocation.AllowUserToDeleteRows = false;
            this.dgvLocation.AllowUserToResizeColumns = false;
            this.dgvLocation.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvLocation.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvLocation.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvLocation.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLocation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocation.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvLocation.EnableHeadersVisualStyles = false;
            this.dgvLocation.Location = new System.Drawing.Point(156, 1);
            this.dgvLocation.MultiSelect = false;
            this.dgvLocation.Name = "dgvLocation";
            this.dgvLocation.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLocation.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvLocation.RowHeadersVisible = false;
            this.dgvLocation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLocation.Size = new System.Drawing.Size(205, 295);
            this.dgvLocation.TabIndex = 353;
            this.dgvLocation.Visible = false;
            this.dgvLocation.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocation_CellDoubleClick);
            this.dgvLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvLocation_KeyDown);
            this.dgvLocation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvLocation_KeyPress);
            // 
            // dgvSubLoc
            // 
            this.dgvSubLoc.AllowUserToAddRows = false;
            this.dgvSubLoc.AllowUserToDeleteRows = false;
            this.dgvSubLoc.AllowUserToResizeColumns = false;
            this.dgvSubLoc.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvSubLoc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvSubLoc.BackgroundColor = System.Drawing.Color.White;
            this.dgvSubLoc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvSubLoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSubLoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvSubLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSubLoc.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvSubLoc.EnableHeadersVisualStyles = false;
            this.dgvSubLoc.Location = new System.Drawing.Point(156, 1);
            this.dgvSubLoc.MultiSelect = false;
            this.dgvSubLoc.Name = "dgvSubLoc";
            this.dgvSubLoc.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSubLoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvSubLoc.RowHeadersVisible = false;
            this.dgvSubLoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSubLoc.Size = new System.Drawing.Size(205, 295);
            this.dgvSubLoc.TabIndex = 354;
            this.dgvSubLoc.Visible = false;
            this.dgvSubLoc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSubLoc_CellDoubleClick);
            this.dgvSubLoc.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvSubLoc_DataError);
            this.dgvSubLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSubLoc_KeyDown);
            this.dgvSubLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvSubLoc_KeyPress);
            // 
            // dgvBank
            // 
            this.dgvBank.AllowUserToAddRows = false;
            this.dgvBank.AllowUserToDeleteRows = false;
            this.dgvBank.AllowUserToResizeColumns = false;
            this.dgvBank.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvBank.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvBank.BackgroundColor = System.Drawing.Color.White;
            this.dgvBank.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBank.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBank.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBank.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvBank.EnableHeadersVisualStyles = false;
            this.dgvBank.Location = new System.Drawing.Point(156, 1);
            this.dgvBank.MultiSelect = false;
            this.dgvBank.Name = "dgvBank";
            this.dgvBank.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBank.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvBank.RowHeadersVisible = false;
            this.dgvBank.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBank.Size = new System.Drawing.Size(205, 295);
            this.dgvBank.TabIndex = 355;
            this.dgvBank.Visible = false;
            this.dgvBank.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBank_CellDoubleClick);
            this.dgvBank.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvBank_DataError);
            this.dgvBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvBank_KeyDown);
            this.dgvBank.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvBank_KeyPress);
            // 
            // dgvBranch
            // 
            this.dgvBranch.AllowUserToAddRows = false;
            this.dgvBranch.AllowUserToDeleteRows = false;
            this.dgvBranch.AllowUserToResizeColumns = false;
            this.dgvBranch.AllowUserToResizeRows = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvBranch.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvBranch.BackgroundColor = System.Drawing.Color.White;
            this.dgvBranch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBranch.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBranch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvBranch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBranch.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgvBranch.EnableHeadersVisualStyles = false;
            this.dgvBranch.Location = new System.Drawing.Point(156, 1);
            this.dgvBranch.MultiSelect = false;
            this.dgvBranch.Name = "dgvBranch";
            this.dgvBranch.ReadOnly = true;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBranch.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvBranch.RowHeadersVisible = false;
            this.dgvBranch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBranch.Size = new System.Drawing.Size(205, 295);
            this.dgvBranch.TabIndex = 356;
            this.dgvBranch.Visible = false;
            this.dgvBranch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBranch_CellDoubleClick);
            this.dgvBranch.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvBranch_DataError);
            this.dgvBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvBranch_KeyDown);
            this.dgvBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvBranch_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.label66);
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(744, 38);
            this.panel1.TabIndex = 357;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(7, 9);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(176, 16);
            this.label66.TabIndex = 416;
            this.label66.Text = "Land Information Entry";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(711, 3);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 418;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label71);
            this.panel2.Controls.Add(this.label72);
            this.panel2.Controls.Add(this.txtAccCode);
            this.panel2.Controls.Add(this.dgvBank);
            this.panel2.Controls.Add(this.dgvListData);
            this.panel2.Controls.Add(this.dgvLocation);
            this.panel2.Controls.Add(this.dgvBranch);
            this.panel2.Controls.Add(this.dgvSubLoc);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.txtVoucherNo);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.txtRecFileNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.dtpPurchaseDate);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label41);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.cmbCompany);
            this.panel2.Controls.Add(this.txtBranch);
            this.panel2.Controls.Add(this.cmbCC);
            this.panel2.Controls.Add(this.txtBank);
            this.panel2.Controls.Add(this.cmbGroup);
            this.panel2.Controls.Add(this.txtSelller);
            this.panel2.Controls.Add(this.txtDeedValue);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.txtComments);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtAreaMutation);
            this.panel2.Controls.Add(this.txtRegCost);
            this.panel2.Controls.Add(this.txtAreaDeed);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.txtDevCost);
            this.panel2.Controls.Add(this.txtOthers);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtAssetCode);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.txtDeedNo);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txtAsset);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtDaagNo);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.btnReset);
            this.panel2.Controls.Add(this.txtKhatianNo);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.txtSubLoc);
            this.panel2.Controls.Add(this.txtMouzaName);
            this.panel2.Controls.Add(this.txtLoc);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(5, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(744, 339);
            this.panel2.TabIndex = 358;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 274);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(76, 13);
            this.label47.TabIndex = 363;
            this.label47.Text = "Voucher No.";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(132, 274);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(12, 13);
            this.label48.TabIndex = 364;
            this.label48.Text = ":";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(132, 252);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(12, 13);
            this.label42.TabIndex = 361;
            this.label42.Text = ":";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(8, 252);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(93, 13);
            this.label46.TabIndex = 360;
            this.label46.Text = "Record File No.";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(744, 339);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtVoucherNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVoucherNo.Location = new System.Drawing.Point(148, 270);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(209, 21);
            this.txtVoucherNo.TabIndex = 24;
            // 
            // txtRecFileNo
            // 
            this.txtRecFileNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtRecFileNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecFileNo.Location = new System.Drawing.Point(148, 248);
            this.txtRecFileNo.Name = "txtRecFileNo";
            this.txtRecFileNo.Size = new System.Drawing.Size(209, 21);
            this.txtRecFileNo.TabIndex = 12;
            // 
            // dtpPurchaseDate
            // 
            this.dtpPurchaseDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPurchaseDate.Location = new System.Drawing.Point(148, 94);
            this.dtpPurchaseDate.Name = "dtpPurchaseDate";
            this.dtpPurchaseDate.Size = new System.Drawing.Size(209, 21);
            this.dtpPurchaseDate.TabIndex = 5;
            // 
            // cmbCompany
            // 
            this.cmbCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(148, 6);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(209, 21);
            this.cmbCompany.TabIndex = 1;
            this.cmbCompany.SelectedIndexChanged += new System.EventHandler(this.cmbCompany_SelectedIndexChanged);
            this.cmbCompany.DropDown += new System.EventHandler(this.cmbCompany_DropDown);
            // 
            // txtBranch
            // 
            this.txtBranch.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBranch.Location = new System.Drawing.Point(522, 248);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.Size = new System.Drawing.Size(209, 21);
            this.txtBranch.TabIndex = 23;
            this.txtBranch.TextChanged += new System.EventHandler(this.txtBranch_TextChanged);
            this.txtBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBranch_KeyDown);
            this.txtBranch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBranch_KeyPress);
            this.txtBranch.Enter += new System.EventHandler(this.txtBranch_Enter);
            // 
            // cmbCC
            // 
            this.cmbCC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(148, 28);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(209, 21);
            this.cmbCC.TabIndex = 2;
            this.cmbCC.SelectedIndexChanged += new System.EventHandler(this.cmbCC_SelectedIndexChanged);
            this.cmbCC.DropDown += new System.EventHandler(this.cmbCC_DropDown);
            // 
            // txtBank
            // 
            this.txtBank.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBank.Location = new System.Drawing.Point(522, 226);
            this.txtBank.Name = "txtBank";
            this.txtBank.Size = new System.Drawing.Size(209, 21);
            this.txtBank.TabIndex = 22;
            this.txtBank.TextChanged += new System.EventHandler(this.txtBank_TextChanged);
            this.txtBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBank_KeyDown);
            this.txtBank.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBank_KeyPress);
            this.txtBank.Enter += new System.EventHandler(this.txtBank_Enter);
            // 
            // cmbGroup
            // 
            this.cmbGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(148, 50);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(209, 21);
            this.cmbGroup.TabIndex = 3;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            this.cmbGroup.DropDown += new System.EventHandler(this.cmbGroup_DropDown);
            // 
            // txtSelller
            // 
            this.txtSelller.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtSelller.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSelller.Location = new System.Drawing.Point(148, 226);
            this.txtSelller.Name = "txtSelller";
            this.txtSelller.Size = new System.Drawing.Size(209, 21);
            this.txtSelller.TabIndex = 11;
            this.txtSelller.Enter += new System.EventHandler(this.txtSelller_Enter);
            // 
            // txtDeedValue
            // 
            this.txtDeedValue.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDeedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeedValue.Location = new System.Drawing.Point(148, 138);
            this.txtDeedValue.Name = "txtDeedValue";
            this.txtDeedValue.Size = new System.Drawing.Size(209, 21);
            this.txtDeedValue.TabIndex = 7;
            this.txtDeedValue.Leave += new System.EventHandler(this.txtDeedValue_Leave);
            this.txtDeedValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDeedValue_KeyPress);
            this.txtDeedValue.Enter += new System.EventHandler(this.txtDeedValue_Enter);
            // 
            // txtComments
            // 
            this.txtComments.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComments.Location = new System.Drawing.Point(522, 182);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(209, 43);
            this.txtComments.TabIndex = 21;
            this.txtComments.Enter += new System.EventHandler(this.txtComments_Enter);
            // 
            // txtAreaMutation
            // 
            this.txtAreaMutation.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAreaMutation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAreaMutation.Location = new System.Drawing.Point(522, 116);
            this.txtAreaMutation.Name = "txtAreaMutation";
            this.txtAreaMutation.Size = new System.Drawing.Size(209, 21);
            this.txtAreaMutation.TabIndex = 18;
            this.txtAreaMutation.Leave += new System.EventHandler(this.txtAreaMutation_Leave);
            this.txtAreaMutation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAreaMutation_KeyPress);
            this.txtAreaMutation.Enter += new System.EventHandler(this.txtAreaMutation_Enter);
            // 
            // txtRegCost
            // 
            this.txtRegCost.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtRegCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegCost.Location = new System.Drawing.Point(148, 160);
            this.txtRegCost.Name = "txtRegCost";
            this.txtRegCost.Size = new System.Drawing.Size(209, 21);
            this.txtRegCost.TabIndex = 8;
            this.txtRegCost.Leave += new System.EventHandler(this.txtRegCost_Leave);
            this.txtRegCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegCost_KeyPress);
            this.txtRegCost.Enter += new System.EventHandler(this.txtRegCost_Enter);
            // 
            // txtAreaDeed
            // 
            this.txtAreaDeed.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAreaDeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAreaDeed.Location = new System.Drawing.Point(148, 116);
            this.txtAreaDeed.Name = "txtAreaDeed";
            this.txtAreaDeed.Size = new System.Drawing.Size(209, 21);
            this.txtAreaDeed.TabIndex = 6;
            this.txtAreaDeed.Leave += new System.EventHandler(this.txtAreaDeed_Leave);
            this.txtAreaDeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAreaDeed_KeyPress);
            this.txtAreaDeed.Enter += new System.EventHandler(this.txtAreaDeed_Enter);
            // 
            // txtDevCost
            // 
            this.txtDevCost.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDevCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDevCost.Location = new System.Drawing.Point(148, 182);
            this.txtDevCost.Name = "txtDevCost";
            this.txtDevCost.Size = new System.Drawing.Size(209, 21);
            this.txtDevCost.TabIndex = 9;
            this.txtDevCost.Leave += new System.EventHandler(this.txtDevCost_Leave);
            this.txtDevCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDevCost_KeyPress);
            this.txtDevCost.Enter += new System.EventHandler(this.txtDevCost_Enter);
            // 
            // txtOthers
            // 
            this.txtOthers.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtOthers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOthers.Location = new System.Drawing.Point(148, 204);
            this.txtOthers.Name = "txtOthers";
            this.txtOthers.Size = new System.Drawing.Size(209, 21);
            this.txtOthers.TabIndex = 10;
            this.txtOthers.Leave += new System.EventHandler(this.txtOthers_Leave);
            this.txtOthers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOthers_KeyPress);
            this.txtOthers.Enter += new System.EventHandler(this.txtOthers_Enter);
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssetCode.Location = new System.Drawing.Point(522, 6);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.Size = new System.Drawing.Size(209, 21);
            this.txtAssetCode.TabIndex = 13;
            this.txtAssetCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            // 
            // txtDeedNo
            // 
            this.txtDeedNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDeedNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDeedNo.Location = new System.Drawing.Point(522, 28);
            this.txtDeedNo.Name = "txtDeedNo";
            this.txtDeedNo.Size = new System.Drawing.Size(209, 21);
            this.txtDeedNo.TabIndex = 14;
            this.txtDeedNo.Enter += new System.EventHandler(this.txtDeedNo_Enter);
            // 
            // txtAsset
            // 
            this.txtAsset.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAsset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAsset.Location = new System.Drawing.Point(148, 72);
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.Size = new System.Drawing.Size(209, 21);
            this.txtAsset.TabIndex = 4;
            this.txtAsset.TextChanged += new System.EventHandler(this.txtAsset_TextChanged_1);
            this.txtAsset.Enter += new System.EventHandler(this.txtAsset_Enter);
            // 
            // txtDaagNo
            // 
            this.txtDaagNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDaagNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDaagNo.Location = new System.Drawing.Point(522, 50);
            this.txtDaagNo.Name = "txtDaagNo";
            this.txtDaagNo.Size = new System.Drawing.Size(209, 21);
            this.txtDaagNo.TabIndex = 15;
            this.txtDaagNo.Enter += new System.EventHandler(this.txtDaagNo_Enter);
            // 
            // txtKhatianNo
            // 
            this.txtKhatianNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtKhatianNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKhatianNo.Location = new System.Drawing.Point(522, 72);
            this.txtKhatianNo.Name = "txtKhatianNo";
            this.txtKhatianNo.Size = new System.Drawing.Size(209, 21);
            this.txtKhatianNo.TabIndex = 16;
            this.txtKhatianNo.Enter += new System.EventHandler(this.txtKhatianNo_Enter);
            // 
            // txtSubLoc
            // 
            this.txtSubLoc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtSubLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSubLoc.Location = new System.Drawing.Point(522, 160);
            this.txtSubLoc.Name = "txtSubLoc";
            this.txtSubLoc.Size = new System.Drawing.Size(209, 21);
            this.txtSubLoc.TabIndex = 20;
            this.txtSubLoc.TextChanged += new System.EventHandler(this.txtSubLoc_TextChanged);
            this.txtSubLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSubLoc_KeyDown);
            this.txtSubLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubLoc_KeyPress);
            this.txtSubLoc.Enter += new System.EventHandler(this.txtSubLoc_Enter);
            // 
            // txtMouzaName
            // 
            this.txtMouzaName.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtMouzaName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMouzaName.Location = new System.Drawing.Point(522, 94);
            this.txtMouzaName.Name = "txtMouzaName";
            this.txtMouzaName.Size = new System.Drawing.Size(209, 21);
            this.txtMouzaName.TabIndex = 17;
            this.txtMouzaName.Enter += new System.EventHandler(this.txtMouzaName_Enter);
            // 
            // txtLoc
            // 
            this.txtLoc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoc.Location = new System.Drawing.Point(522, 138);
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.Size = new System.Drawing.Size(209, 21);
            this.txtLoc.TabIndex = 19;
            this.txtLoc.TextChanged += new System.EventHandler(this.txtLoc_TextChanged);
            this.txtLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLoc_KeyDown);
            this.txtLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLoc_KeyPress);
            this.txtLoc.Enter += new System.EventHandler(this.txtLoc_Enter);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(367, 274);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(63, 13);
            this.label71.TabIndex = 428;
            this.label71.Text = "A/C Code";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(504, 274);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(12, 13);
            this.label72.TabIndex = 429;
            this.label72.Text = ":";
            // 
            // txtAccCode
            // 
            this.txtAccCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAccCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccCode.Location = new System.Drawing.Point(522, 270);
            this.txtAccCode.Name = "txtAccCode";
            this.txtAccCode.Size = new System.Drawing.Size(209, 21);
            this.txtAccCode.TabIndex = 427;
            // 
            // frmAssetConfigLand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(754, 388);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAssetConfigLand";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asset Information Entry (Land)";
            this.Load += new System.EventHandler(this.frmAssetConfigLand_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmAssetConfigLand_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBranch)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private FlatTextBox txtDeedValue;
        private AssetRegister.FlatComboBox cmbGroup;
        private AssetRegister.FlatComboBox cmbCC;
        private AssetRegister.FlatComboBox cmbCompany;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private FlatTextBox txtComments;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private FlatTextBox txtRegCost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private FlatTextBox txtDevCost;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private FlatTextBox txtDeedNo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private FlatTextBox txtDaagNo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private FlatTextBox txtKhatianNo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private FlatTextBox txtMouzaName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private FlatTextBox txtSubLoc;
        private FlatTextBox txtLoc;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private FlatTextBox txtAsset;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private FlatTextBox txtAssetCode;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridView dgvListData;
        private FlatTextBox txtOthers;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label35;
        private FlatTextBox txtAreaDeed;
        private FlatTextBox txtAreaMutation;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private FlatTextBox txtSelller;
        private FlatTextBox txtBank;
        private FlatTextBox txtBranch;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.DataGridView dgvLocation;
        private System.Windows.Forms.DataGridView dgvSubLoc;
        private System.Windows.Forms.DataGridView dgvBank;
        private System.Windows.Forms.DataGridView dgvBranch;
        private AssetRegister.FlatDateTimePicker dtpPurchaseDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label46;
        private FlatTextBox txtRecFileNo;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private FlatTextBox txtVoucherNo;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private FlatTextBox txtAccCode;
    }
}