﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmRptRevAmtDepCharge : Form
    {
        private clsCommonFunctions dbConRptDepJournalChargeAmt = new clsCommonFunctions();
        private string AssetGroupId = "", CompanyId = "", Company = "", CostCentreCode = "", CC = "";
        private int AssetId = 0;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmRptRevAmtDepCharge()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;

            string frmDate = "", toDate = "";
            //int IsCompany = 0;

            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                    Company = "Dominox IM Ltd.";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                    Company = "KDS Accessories Ltd.";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "PL";
                    Company = "KDS Poly Industries Ltd.";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "CP";
                    Company = "KDS Thread Limited.";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }
            else
                CompanyId = "";

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                    CC = cmbCC.Text.Trim();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }
            else
                CostCentreCode = "";

            if (!chkAllGroup.Checked)
            {
                if (cmbGroup.SelectedIndex > -1)
                {
                    AssetGroupId = cmbGroup.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbGroup.Focus();
                    return;
                }
            }
            else
                AssetGroupId = "";

            if (!chkAllDate.Checked)
            {
                frmDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
                toDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            }
            else
            {
                frmDate = "1900-01-01";
                toDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            }

            if (Convert.ToDateTime(frmDate) > Convert.ToDateTime(toDate))
            {
                MessageBox.Show("From date cannot be greater than to date", "Date Problem", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }            

            //if (chkOnlyCompany.Checked)
            //{
            //    IsCompany = 1;
            //}
            
            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptDepChargedOnRevAmt.rpt");
                cryReport.SetParameterValue("@FDate", frmDate);
                cryReport.SetParameterValue("@TDate", toDate);
                cryReport.SetParameterValue("@CompanyId", CompanyId);
                //cryReport.SetParameterValue("@Company", Company);
                cryReport.SetParameterValue("@CcCode", CostCentreCode);
                //cryReport.SetParameterValue("@CostCentre", CC);
                cryReport.SetParameterValue("@GroupId", AssetGroupId);
                cryReport.SetParameterValue("@AssetId", AssetId);
                //cryReport.SetParameterValue("@IsCompany", IsCompany);
                //cryReport.SetParameterValue("@UserName", frmLogin.userFullName);
                dbConRptDepJournalChargeAmt.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.Text = "Depreciation & Cost Details Report";
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.crViewer.Refresh();
                rptForm.Show();
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void frmRptAssetRegister_Load(object sender, EventArgs e)
        {
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 2");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;
            dtpFrmDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpToDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

            dbConRptDepJournalChargeAmt.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '', '', 0");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void chkAllGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !chkAllGroup.Checked;
        }

        private void frmRptDepNCost_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConRptDepJournalChargeAmt.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void chkAllAsset_CheckedChanged(object sender, EventArgs e)
        {
            txtAssetCode.Enabled = !chkAllAsset.Checked;
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }

        private void chkAllDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFrmDate.Enabled = !chkAllDate.Checked;
        }
    }
}
