﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAssetConfigLand : Form
    {
        private clsCommonFunctions dbConLand = new clsCommonFunctions();
        private bool cmbLoadFlag = false, IsUpdate = false;
        private int AssetId = 0, AssetLocId = 0, AssetSubLocId = 0, BankId = 0 , BranchId = 0;
        private string AssetCode = "", CCCode = "", GroupId ="", CompId = "", AssetLoc = "", AssetSubLoc = "", Bank = "", Branch = "",
            AreaDeed = "", AreaMutation = "", Asset = "", Seller = "", DalilNo ="", DaagNo = "", KhatianNo = "", Mouza="",
            Comments = "", RegDate= "";
        double CurrVal = 0.00, TotalCost = 0.00, DeedValue = 0.00, RegCost = 0.00, DevCost = 0.00, Others = 0.00;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAssetConfigLand()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmAssetConfigLand_Load(object sender, EventArgs e)
        {
            dbConLand.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConLand.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConLand.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 1");

            cmbLoadFlag = true;
            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;

            dbConLand.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '" + CCCode + "', '" + GroupId + "', '" + CompId + "', 1");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 200;

            dbConLand.FillDataGridView(dgvBank, @"SELECT BankId, BankName FROM [SalesCube].[dbo].[Bank] WHERE IsActive = 1");
            dgvBank.Columns[0].Visible = false;
            dgvBank.Columns[1].Width = 200;
            EnableUpdate(false);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConLand.CheckEmptyOrNull(cmbCompany, "Select Company") &&
                !dbConLand.CheckEmptyOrNull(cmbCC, "Select Cost Centre") &&
                !dbConLand.CheckEmptyOrNull(cmbGroup, "Select Group") &&
                //!dbConLand.CheckEmptyOrNull(cmbCustodianDept, "Select Custodian Dept.") &&
                !dbConLand.CheckEmptyOrNull(txtAsset, "Enter Asset") &&
                !dbConLand.CheckEmptyOrNull(txtAssetCode, "Enter Asset Code"))
            {
                if (!IsUpdate)
                {
                    if (cmbCompany.SelectedIndex > -1)
                        CompId = cmbCompany.SelectedValue.ToString();
                    else
                        CompId = "";

                    if (cmbCC.SelectedIndex > -1)
                        CCCode = cmbCC.SelectedValue.ToString();
                    else
                        CCCode = "00";

                    if (cmbGroup.SelectedIndex > -1)
                        GroupId = cmbGroup.SelectedValue.ToString();
                    else
                        GroupId = "000000";

                    if (!string.IsNullOrEmpty(txtAsset.Text.Trim()))
                        Asset = dbConLand.SingleQuoteHandler(txtAsset);
                    else
                        Asset = "";

                    RegDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

                    if (string.IsNullOrEmpty(txtAreaDeed.Text) || txtAreaDeed.Text == "")
                        AreaDeed = "0.00";
                    else
                        AreaDeed = txtAreaDeed.Text;

                    if (string.IsNullOrEmpty(txtAreaMutation.Text) || txtAreaMutation.Text == "")
                        AreaMutation = "0.00";
                    else
                        AreaMutation = txtAreaMutation.Text;

                    if (string.IsNullOrEmpty(txtDeedValue.Text) || txtDeedValue.Text == "")
                        DeedValue = 0;
                    else
                        DeedValue = Convert.ToDouble(txtDeedValue.Text);

                    if (string.IsNullOrEmpty(txtRegCost.Text) || txtRegCost.Text == "")
                        RegCost = 0;
                    else
                        RegCost = Convert.ToDouble(txtRegCost.Text);

                    if (string.IsNullOrEmpty(txtDevCost.Text) || txtDevCost.Text == "")
                        DevCost = 0;
                    else
                        DevCost = Convert.ToDouble(txtDevCost.Text);

                    if (string.IsNullOrEmpty(txtOthers.Text) || txtOthers.Text == "")
                        Others = 0;
                    else
                        Others = Convert.ToDouble(txtOthers.Text);

                    TotalCost = DeedValue + RegCost + DevCost;
                    CurrVal = TotalCost + Others;

                    if (!string.IsNullOrEmpty(txtSelller.Text))
                        Seller = dbConLand.SingleQuoteHandler(txtSelller);
                    else
                        Seller = "";

                    if (!string.IsNullOrEmpty(txtDeedNo.Text))
                        DalilNo = dbConLand.SingleQuoteHandler(txtDeedNo);
                    else
                        DalilNo = "";

                    if (!string.IsNullOrEmpty(txtDaagNo.Text))
                        DaagNo = dbConLand.SingleQuoteHandler(txtDaagNo);
                    else
                        DaagNo = "";

                    if (!string.IsNullOrEmpty(txtKhatianNo.Text))
                        KhatianNo = dbConLand.SingleQuoteHandler(txtKhatianNo);
                    else
                        KhatianNo = "";

                    if (!string.IsNullOrEmpty(txtMouzaName.Text))
                        Mouza = dbConLand.SingleQuoteHandler(txtMouzaName);
                    else
                        Mouza = "";

                    if (!string.IsNullOrEmpty(txtComments.Text))
                        Comments = dbConLand.SingleQuoteHandler(txtComments);
                    else
                        Comments = "";

                    string AssetDesc = "Deed No. : " + DalilNo
                                        + "; Daag No. : " + DaagNo
                                        + "; Khatian No. : " + KhatianNo
                                        + "; Mouza Name :" + Mouza + ";";

                    string[] cols1 = { "[AssetName]", "[CostCentreCode]", "[AssetCode]", "[AssetDescription]", "[AssetGroupId]",
                                       "[CompanyId]", "[PurchaseAmt]", "[InvoiceCostBDT]","[InvoiceCostUSD]","[DutiesnOthers]",
                                       "[CostAmt]","[SalvageAmt]","[PurchaseDate]","[DepreciationMethod]", 
                                       "[DepreciationRate]", "[AssetLife]", "[DepreciationEffectDate]", "[AssetLoc]", "[AssetLocId]", 
                                       "[AssetSubLoc]", "[AssetSubLocId]", "[AssetUser]", "[SupplierId]", "[Country]", "[Brand]",
                                       "[ModelNo]", "[LCNo]", "[VoucherNo]","[RegistrationNo]", "[IsActive]", "[Comments]", "[Warranty]", 
                                       "[IsSoldOrIsDamage]", "[SoldOrDamageDate]","[SoldOrDamageComments]","[UserId]","[IsDepCalc]", 
                                       "[RecordFileNo]", "[AccCode]" };
                    string[] vals1 = { Asset, CCCode, txtAssetCode.Text, AssetDesc, GroupId, 
                                      CompId, TotalCost.ToString(), TotalCost.ToString(), "0.00", "0.00",
                                      "0.00", "0.00", RegDate, "", 
                                      "0.00", "0.00", "1900-01-01", AssetLoc, AssetLocId.ToString(),
                                      AssetSubLoc, AssetSubLocId.ToString(), "", "0", "", "",
                                      "","",dbConLand.SingleQuoteHandler(txtVoucherNo.Text), DalilNo, "1", Comments, "0.00",
                                      "0", "1900-01-01", "", frmLogin.userId.ToString(), "0", dbConLand.SingleQuoteHandler(txtRecFileNo.Text),
                                      dbConLand.SingleQuoteHandler(txtAccCode.Text)};
                    string[] cols2 = { "[AssetId]", "[Mouza]", "[RegDate]", "[DalilNo]", "[KhatianNo]", "[DaagNo]",
                                       "[AreaDecimalDeed]", "[AreaDecimalMutation]", "[DeedValue]","[RegCost]","[DevCost]",
                                       "[RevalAmt]","[Others]","[CurVal]", "[Seller]", "[MortgageBankId]", "[MortgageBankBranchId]" };
                    string[] vals2 = { dbConLand.getMaxIdentity("[AR_AssetConfig]", "[AssetId]").ToString(),
                                       Mouza, 
                                       RegDate,
                                       DalilNo,
                                       KhatianNo,
                                       DaagNo,
                                       AreaDeed,
                                       AreaMutation,
                                       DeedValue.ToString(),
                                       RegCost.ToString(),
                                       DevCost.ToString(),
                                       "0.00",
                                       Others.ToString(),
                                       CurrVal.ToString(),
                                       Seller,
                                       BankId.ToString(),
                                       BranchId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "INSERT,INSERT", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_LandDetails]", this.Name, 
                                       dbConLand.getMaxIdentity("[AR_AssetConfig]", "[AssetId]").ToString(), "", "" };

                    string sql = dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols1, vals1, "INSERT", null, null);
                    sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_LandDetails]", cols2, vals2, "INSERT", null, null);
                    sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLand.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Information Saved Successfully.", "Save",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Faild to Save Data.", "Save",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void txtAsset_TextChanged(object sender, EventArgs e)
        {
            
        }
                
        private void LoadAssetDetails()
        {
            SqlDataReader dtRdr;
            string sql = @"EXEC prcLoadAssetConfigDetails " + AssetId;
            string company = "";
            string group = "";
            string ConstCentre = "";


            dtRdr = (SqlDataReader)dbConLand.DBCRUDFunction(sql, true);

            if (dtRdr.Read())
            {
                company = dtRdr["Company"].ToString();
                group = dtRdr["AssetGroup"].ToString();
                ConstCentre = dtRdr["CostCentre"].ToString();
                txtAsset.Text = dtRdr["AssetName"].ToString();
                txtDeedValue.Text = dtRdr["DeedValue"].ToString();
                dtpPurchaseDate.Value = Convert.ToDateTime(dtRdr["PurchaseDate"].ToString());
                txtAreaDeed.Text = dtRdr["AreaDecimalDeed"].ToString();
                txtAreaMutation.Text = dtRdr["AreaDecimalMutation"].ToString();
                txtDeedValue.Text = dtRdr["DeedValue"].ToString();
                txtRegCost.Text = dtRdr["RegCost"].ToString();
                txtDevCost.Text = dtRdr["DevCost"].ToString();
                txtOthers.Text = dtRdr["Others"].ToString();
                txtSelller.Text = dtRdr["Seller"].ToString();
                txtDeedNo.Text = dtRdr["DalilNo"].ToString();
                txtDaagNo.Text = dtRdr["DaagNo"].ToString();
                txtKhatianNo.Text = dtRdr["KhatianNo"].ToString();
                txtMouzaName.Text = dtRdr["Mouza"].ToString();
                txtLoc.Text = dtRdr["AssetLoc"].ToString(); 
                txtSubLoc.Text = dtRdr["AssetSubLoc"].ToString();
                txtComments.Text = dtRdr["Comments"].ToString();
                txtBranch.Text = dtRdr["Branch"].ToString();
                txtBank.Text = dtRdr["Bank"].ToString();
                txtVoucherNo.Text = dtRdr["VoucherNo"].ToString();
                txtAccCode.Text = dtRdr["AccCode"].ToString();
                txtRecFileNo.Text = dtRdr["RecordFileNo"].ToString();
            }

            cmbCompany.Text = company;
            cmbGroup.Text = group;
            cmbCC.Text = ConstCentre;

            dgvBank.Visible = false;
            dgvBranch.Visible = false;
            dgvLocation.Visible = false;
            dgvSubLoc.Visible = false;

            dtRdr.Close();
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
            LoadAssetDetails();
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                LoadAssetDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbCompany.SelectedIndex > -1)
            {
                CompId = cmbCompany.SelectedValue.ToString();
                dbConLand.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '" + CCCode + "', '" + GroupId + "', '" + CompId + "', 1");
            }
        }

        private void cmbCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbCC.SelectedIndex > -1)
            {
                CCCode = cmbCC.SelectedValue.ToString();
                dbConLand.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '" + CCCode + "', '" + GroupId + "', '" + CompId + "', 1");
            }
                        
            if (cmbCC.SelectedIndex > -1)
            {
                dbConLand.FillDataGridView(dgvLocation, @"SELECT AssetLocId,
                                                                 AssetLocation
                                                          FROM   AR_AssetLocation
                                                          WHERE  CostCentreCode = '" + cmbCC.SelectedValue + "'");
                dgvLocation.Columns[0].Visible = false;
                dgvLocation.Columns[1].Width = 200;
            }

            txtLoc.Text = "";
            dgvLocation.Visible = false;
            txtSubLoc.Text = "";
            dgvSubLoc.Visible = false;
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbGroup.SelectedIndex > -1 && !IsUpdate)
            {
                txtAssetCode.Text = AutoGenAssetCode(cmbGroup.SelectedValue.ToString());
            }
            dgvListData.Visible = false;
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConLand.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text.Replace("'", "''") + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvListData.Focus();

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count >= 1)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (cmbGroup.SelectedIndex > -1)
                AutoGenAssetCode(cmbGroup.SelectedValue.ToString());

            ResetAll();
        }

        private void ResetAll()
        {
            txtAsset.Text = "";
            cmbCC.Text = "";
            cmbGroup.Text = "";
            cmbCompany.Text = "";
            dtpPurchaseDate.Text = "";
            txtAreaDeed.Text = "";
            txtAreaMutation.Text = "";
            txtDeedValue.Text = "";
            txtRegCost.Text = "";
            txtDevCost.Text = "";
            txtOthers.Text = "";
            txtSelller.Text = "";
            txtDeedNo.Text = "";
            txtDaagNo.Text = "";
            txtKhatianNo.Text = "";
            txtMouzaName.Text = "";
            txtLoc.Text = "";
            txtVoucherNo.Text = "";
            dgvLocation.Visible = false;
            txtSubLoc.Text = "";
            dgvSubLoc.Visible = false;
            txtBank.Text = "";
            dgvBank.Visible = false;
            txtBranch.Text = "";
            dgvBranch.Visible = false;
            txtComments.Text = "";
            EnableUpdate(false);
            AssetCode = "";
            AssetId = 0;
            txtAccCode.Text = "";
        }

        private string AutoGenAssetCode(string GroupId)
        {
            SqlDataReader dr;
            string CodePrefix = "", Code = "";

            if (cmbGroup.SelectedIndex > -1)
            {
                switch (GroupId)
                {
                    //case "010102": CodePrefix = "AB"; break;
                    //case "010105": CodePrefix = "CS"; break;
                    //case "010107": CodePrefix = "EE"; break;
                    //case "010109": CodePrefix = "FB"; break;
                    //case "010106": CodePrefix = "FF"; break;
                    //case "010113": CodePrefix = "GS"; break;
                    //case "010112": CodePrefix = "LE"; break;
                    case "010110": CodePrefix = "LD"; break;
                    //case "010104": CodePrefix = "OE"; break;
                    //case "010103": CodePrefix = "PM"; break;
                    //case "010108": CodePrefix = "VH"; break;
                    //case "010111": CodePrefix = "IT"; break;
                }

                dr = (SqlDataReader)dbConLand.DBCRUDFunction(@"SELECT dbo.fnAutoGenAssetCode('" + CodePrefix + "') AssetCode", true);

                if (dr.Read())
                    Code = dr["AssetCode"].ToString();

                dr.Close();
            }

            return Code;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            EnableUpdate(true);

            if (!string.IsNullOrEmpty(txtAssetCode.Text.Trim()))
            {
                if (IsAssetAvail() && IsUpdate)
                {
                    if (cmbCompany.SelectedIndex > -1)
                        CompId = cmbCompany.SelectedValue.ToString();
                    else
                        CompId = "";

                    if (cmbCC.SelectedIndex > -1)
                        CCCode = cmbCC.SelectedValue.ToString();
                    else
                        CCCode = "00";

                    if (cmbGroup.SelectedIndex > -1)
                        GroupId = cmbGroup.SelectedValue.ToString();
                    else
                        GroupId = "000000";

                    if (!string.IsNullOrEmpty(txtAsset.Text.Trim()))
                        Asset = dbConLand.SingleQuoteHandler(txtAsset);
                    else
                        Asset = "";

                    RegDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

                    if (string.IsNullOrEmpty(txtAreaDeed.Text) || txtAreaDeed.Text == "")
                        AreaDeed = "0.00";
                    else
                        AreaDeed = txtAreaDeed.Text;

                    if (string.IsNullOrEmpty(txtAreaMutation.Text) || txtAreaMutation.Text == "")
                        AreaMutation = "0.00";
                    else
                        AreaMutation = txtAreaMutation.Text;

                    if (string.IsNullOrEmpty(txtDeedValue.Text) || txtDeedValue.Text == "")
                        DeedValue = 0;
                    else
                        DeedValue = Convert.ToDouble(txtDeedValue.Text);

                    if (string.IsNullOrEmpty(txtRegCost.Text) || txtRegCost.Text == "")
                        RegCost = 0;
                    else
                        RegCost = Convert.ToDouble(txtRegCost.Text);

                    if (string.IsNullOrEmpty(txtDevCost.Text) || txtDevCost.Text == "")
                        DevCost = 0;
                    else
                        DevCost = Convert.ToDouble(txtDevCost.Text);

                    if (string.IsNullOrEmpty(txtOthers.Text) || txtOthers.Text == "")
                        Others = 0;
                    else
                        Others = Convert.ToDouble(txtOthers.Text);

                    TotalCost = DeedValue + RegCost + DevCost;
                    CurrVal = TotalCost + Others;

                    if (!string.IsNullOrEmpty(txtSelller.Text))
                        Seller = dbConLand.SingleQuoteHandler(txtSelller);
                    else
                        Seller = "";

                    if (!string.IsNullOrEmpty(txtDeedNo.Text))
                        DalilNo = dbConLand.SingleQuoteHandler(txtDeedNo);
                    else
                        DalilNo = "";

                    if (!string.IsNullOrEmpty(txtDaagNo.Text))
                        DaagNo = dbConLand.SingleQuoteHandler(txtDaagNo);
                    else
                        DaagNo = "";

                    if (!string.IsNullOrEmpty(txtKhatianNo.Text))
                        KhatianNo = dbConLand.SingleQuoteHandler(txtKhatianNo);
                    else
                        KhatianNo = "";

                    if (!string.IsNullOrEmpty(txtMouzaName.Text))
                        Mouza = dbConLand.SingleQuoteHandler(txtMouzaName);
                    else
                        Mouza = "";

                    if (!string.IsNullOrEmpty(txtComments.Text))
                        Comments = dbConLand.SingleQuoteHandler(txtComments);
                    else
                        Comments = "";

                    string AssetDesc = "Deed No. : " + DalilNo
                                        + "; Daag No. : " + DaagNo
                                        + "; Khatian No. : " + KhatianNo
                                        + "; Mouza Name :" + Mouza + ";";

                    string[] cols1 = { "[AssetName]", "[CostCentreCode]", "[AssetDescription]", "[CompanyId]", 
                                       "[PurchaseAmt]","[PurchaseDate]", //"[AssetLoc]", "[AssetLocId]", 
                                       //"[AssetSubLoc]", "[AssetSubLocId]", 
                                       "[RegistrationNo]", "[IsActive]", 
                                       "[Comments]", "[UpdateUserId]", "[UpdateDateTime]", "[RecordFileNo]", "[VoucherNo]", "[AccCode]" };
                    string[] vals1 = { Asset, CCCode, AssetDesc, CompId, 
                                       TotalCost.ToString(), RegDate, //AssetLoc, AssetLocId.ToString(),
                                       //AssetSubLoc, AssetSubLocId.ToString(), 
                                       DalilNo, "1", 
                                       Comments, frmLogin.userId.ToString(), dbConLand.GetServerCurrentDateTime().ToString(), 
                                       dbConLand.SingleQuoteHandler(txtRecFileNo.Text),
                                       dbConLand.SingleQuoteHandler(txtVoucherNo.Text),
                                       dbConLand.SingleQuoteHandler(txtAccCode.Text) };
                    string[] cols2 = { "[Mouza]", "[RegDate]", "[DalilNo]", "[KhatianNo]", "[DaagNo]",
                                       "[AreaDecimalDeed]", "[AreaDecimalMutation]", "[DeedValue]","[RegCost]",
                                       "[DevCost]", "[Others]","[CurVal]", "[Seller]", "[MortgageBankId]", 
                                       "[MortgageBankBranchId]" };
                    string[] vals2 = { Mouza, RegDate, DalilNo, KhatianNo, DaagNo,
                                       AreaDeed, AreaMutation, DeedValue.ToString(), RegCost.ToString(), 
                                       DevCost.ToString(), Others.ToString(), CurrVal.ToString(), Seller,
                                       BankId.ToString(), BranchId.ToString() };
                    string[] whCols = { "[AssetId]" };
                    string[] whVals = { AssetId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE,UPDATE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_LandDetails]", this.Name, 
                                       AssetId.ToString(), 
                                       @"[AssetName],[CostCentreCode],[AssetDescription],[CompanyId],
                                        [PurchaseAmt],[PurchaseDate],[AssetLoc],[AssetLocId],
                                        [AssetSubLoc],[AssetSubLocId],[RegistrationNo],[IsActive],[Comments],
                                        [Mouza],[RegDate],[DalilNo],[KhatianNo],[DaagNo],
                                        [AreaDecimalDeed],[AreaDecimalMutation],[DeedValue],[RegCost],
                                        [DevCost],[Others],[CurVal],[Seller],[MortgageBankId],
                                        [MortgageBankBranchId]", 
                                        Asset + "," + CCCode + "," + AssetDesc + "," + CompId + "," + 
                                        TotalCost.ToString()+ "," + RegDate +  "," + AssetLoc + "," + 
                                        AssetLocId.ToString() + "," + AssetSubLoc + "," + AssetSubLocId.ToString()
                                        + "," + DalilNo + ",1," + Comments + "," +
                                        Mouza + "," + RegDate + "," + DalilNo+ "," + KhatianNo+ "," + DaagNo+ "," +
                                        AreaDeed+ "," + AreaMutation+ "," + DeedValue.ToString()+ "," + RegCost.ToString()+ "," + 
                                        DevCost.ToString()+ "," + Others.ToString()+ "," + CurrVal.ToString()+ "," + Seller + "," +
                                        BankId.ToString()+ "," + BranchId.ToString() };

                    string sql = dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols1, vals1, "UPDATE", whCols, whVals);
                    sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_LandDetails]", cols2, vals2, "UPDATE", whCols, whVals);
                    sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLand.DBCRUDFunction(sql, false))
                    {
                        btnReset_Click(btnReset, new EventArgs());

                        MessageBox.Show("Information Updated Successfully.", "Update",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(" Failed to Update Information.", "Update",
                                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("There is NO Asset Like AssetCode " + AssetCode + ".", "Not Found",
                                                          MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        private void EnableUpdate(bool Status)
        {
            if (Status)
            {
                //btnUpdate.Text = "Update";
                btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
                IsUpdate = true;
                txtAssetCode.ReadOnly = false;
                txtAssetCode.BackColor = Color.White;
                txtAssetCode.ForeColor = Color.Black;
                txtAssetCode.Focus();
                btnSave.Enabled = false;
                dgvListData.Visible = false;
            }
            else
            {
                dbConLand.DisposeChecking(txtAssetCode);
                txtAssetCode.Text = "";
                //btnUpdate.Text = "Edit";
                btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_properties_icon;
                IsUpdate = false;
                txtAssetCode.ReadOnly = true;
                txtAssetCode.BackColor = Color.DimGray;
                txtAssetCode.ForeColor = Color.White;
                btnSave.Enabled = true;
                dgvListData.Visible = false;
            }
        }

        private double AdditionCost(int AssetId)
        {
            SqlDataReader dr;
            double AddCost = 0.00;

            dr = (SqlDataReader)dbConLand.DBCRUDFunction(@"SELECT ISNULL(SUM(AdditionCost), 0) AdditionCost FROM AR_AssetAddition WHERE AssetId = " + AssetId, true);
            if (dr.Read())
                AddCost =  Convert.ToDouble(dr["AdditionCost"].ToString());

            dr.Close();

            return AddCost;
        }

        private bool IsAssetAvail()
        {
            SqlDataReader dr;

            dr = (SqlDataReader)dbConLand.DBCRUDFunction(@"SELECT * FROM [AssetRegistar].[dbo].[AR_AssetConfig] WHERE [AssetId] = " + AssetId, true);

            if (dr.HasRows)
                return true;
            else
                return false;
        }

        private void txtOthers_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbers(txtOthers, e); 
        }

        private void txtRevalAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbersWithSign(sender, e); 
        }

        private void txtAreaDeed_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbers(txtAreaDeed, e);
        }

        private void txtAreaMutation_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbers(txtAreaMutation, e);
        }

        private void dgvLocation_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetLocId = Convert.ToInt32(dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetLoc = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtLoc.Text = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtLoc.Focus();
            txtLoc.SelectionStart = txtLoc.TextLength;
            dgvLocation.Visible = false;
            LoadAssetSubLoc();
        }

        private void dgvLocation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetLocId = Convert.ToInt32(dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetLoc = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtLoc.Text = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtLoc.Focus();
                txtLoc.SelectionStart = txtLoc.TextLength;
                dgvLocation.Visible = false;
                LoadAssetSubLoc();
            }
        }

        private void dgvLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtLoc.Focus();
            txtLoc.SelectionStart = txtLoc.TextLength;
        }

        private void txtLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DisposeChecking(txtLoc);
            if (e.KeyChar == (char)Keys.Escape)
                dgvLocation.Visible = false;
        }

        private void txtLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvLocation.Focus();

            if (e.KeyCode == Keys.Enter && dgvLocation.Rows.Count > 0)
            {
                txtLoc.Text = dgvLocation.Rows[0].Cells[1].Value.ToString();
                AssetLocId = Convert.ToInt32(dgvLocation.Rows[0].Cells[0].Value.ToString());
                AssetLoc = dgvLocation.Rows[0].Cells[1].Value.ToString();
                LoadAssetSubLoc();
                dgvLocation.Visible = false;
                txtLoc.SelectionStart = txtLoc.TextLength;
            }
        }

        private void txtLoc_TextChanged(object sender, EventArgs e)
        {
            dbConLand.MakeTextUppercase(txtLoc);
            var dataTable = (DataTable)dgvLocation.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvLocation.Visible = true;
                dataView.RowFilter = dgvLocation.Columns[1].Name + " LIKE '%" + txtLoc.Text.Replace("'", "''") + "%'";
            }
        }

        private void LoadAssetSubLoc()
        {
            if (cmbCC.SelectedIndex > -1 && AssetLocId > 0)
            {
                dbConLand.FillDataGridView(dgvSubLoc, @"SELECT AssetSubLocId,
                                                               AssetSubLocation
                                                        FROM   AR_AssetSubLocation
                                                        WHERE  AssetLocId = " + AssetLocId);
                dgvSubLoc.Columns[0].Visible = false;
                dgvSubLoc.Columns[1].Width = 200;
            }
        }

        private void txtSubLoc_TextChanged(object sender, EventArgs e)
        {
            dbConLand.MakeTextUppercase(txtSubLoc);
            var dataTable = (DataTable)dgvSubLoc.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvSubLoc.Visible = true;
                dataView.RowFilter = dgvSubLoc.Columns[1].Name + " LIKE '%" + txtSubLoc.Text.Replace("'", "''") + "%'";
            }
        }

        private void txtSubLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DisposeChecking(txtSubLoc);
            if (e.KeyChar == (char)Keys.Escape)
                dgvSubLoc.Visible = false;
        }

        private void txtSubLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvSubLoc.Focus();

            if (e.KeyCode == Keys.Enter && dgvSubLoc.Rows.Count > 0)
            {
                txtSubLoc.Text = dgvSubLoc.Rows[0].Cells[1].Value.ToString();
                AssetSubLocId = Convert.ToInt32(dgvSubLoc.Rows[0].Cells[0].Value.ToString());
                AssetSubLoc = dgvSubLoc.Rows[0].Cells[1].Value.ToString();                
                dgvSubLoc.Visible = false;
                txtSubLoc.SelectionStart = txtSubLoc.TextLength;
            }
        }

        private void dgvSubLoc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetSubLocId = Convert.ToInt32(dgvSubLoc.Rows[dgvSubLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetSubLoc = dgvSubLoc.Rows[dgvSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSubLoc.Text = dgvSubLoc.Rows[dgvSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSubLoc.Focus();
            txtSubLoc.SelectionStart = txtSubLoc.TextLength;
            dgvSubLoc.Visible = false;
        }

        private void dgvSubLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetSubLocId = Convert.ToInt32(dgvSubLoc.Rows[dgvSubLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetSubLoc = dgvSubLoc.Rows[dgvSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSubLoc.Text = dgvSubLoc.Rows[dgvSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSubLoc.Focus();
                txtSubLoc.SelectionStart = txtSubLoc.TextLength;
                dgvSubLoc.Visible = false;
            }
        }

        private void dgvSubLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtSubLoc.Focus();
            txtSubLoc.SelectionStart = txtSubLoc.TextLength;
        }

        private void dgvSubLoc_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dgvBank_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BankId = Convert.ToInt32(dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[0].Value.ToString());
            Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtBank.Text = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtBank.Focus();
            txtBank.SelectionStart = txtBank.TextLength;
            dgvBank.Visible = false;
            LoadBranch();
        }

        private void dgvBank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BankId = Convert.ToInt32(dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[0].Value.ToString());
                Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtBank.Text = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtBank.Focus();
                txtBank.SelectionStart = txtBank.TextLength;
                dgvBank.Visible = false;
                LoadBranch();
            }
        }

        private void dgvBank_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtBank.Focus();
            txtBank.SelectionStart = txtBank.TextLength;
        }

        private void txtBank_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DisposeChecking(txtBank);
            if (e.KeyChar == (char)Keys.Escape)
                dgvBank.Visible = false;
        }

        private void txtBank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvBank.Focus();

            if (e.KeyCode == Keys.Enter && dgvLocation.Rows.Count > 0)
            {
                txtBank.Text = dgvBank.Rows[0].Cells[1].Value.ToString();
                AssetLocId = Convert.ToInt32(dgvBank.Rows[0].Cells[0].Value.ToString());
                AssetLoc = dgvBank.Rows[0].Cells[1].Value.ToString();
                LoadBranch();
                dgvBank.Visible = false;
                txtBank.SelectionStart = txtBank.TextLength;
            }
        }

        private void txtBank_TextChanged(object sender, EventArgs e)
        {
            dbConLand.MakeTextUppercase(txtBank);
            var dataTable = (DataTable)dgvBank.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvBank.Visible = true;
                dataView.RowFilter = dgvBank.Columns[1].Name + " LIKE '%" + txtBank.Text.Replace("'", "''") + "%'";
            }
        }

        private void LoadBranch()
        {
            if (BankId > 0)
            {
                dbConLand.FillDataGridView(dgvBranch, @"SELECT BranchId,BranchName FROM [SalesCube].[dbo].[BankBranch] WHERE IsActive = 1 AND BankId = " + BankId);
                dgvBranch.Columns[0].Visible = false;
                dgvBranch.Columns[1].Width = 200;
            }
        }

        private void dgvBranch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BranchId = Convert.ToInt32(dgvBranch.Rows[dgvBranch.CurrentCell.RowIndex].Cells[0].Value.ToString());
            Branch = dgvBranch.Rows[dgvBranch.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtBranch.Text = dgvBranch.Rows[dgvBranch.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtBranch.Focus();
            txtBranch.SelectionStart = txtBranch.TextLength;
            dgvBranch.Visible = false;
        }

        private void dgvBranch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BranchId = Convert.ToInt32(dgvBranch.Rows[dgvBranch.CurrentCell.RowIndex].Cells[0].Value.ToString());
                Branch = dgvBranch.Rows[dgvBranch.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtBranch.Text = dgvBranch.Rows[dgvBranch.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtBranch.Focus();
                txtBranch.SelectionStart = txtBranch.TextLength;
                dgvBranch.Visible = false;
            }
        }

        private void dgvBranch_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtBranch.Focus();
            txtBranch.SelectionStart = txtBranch.TextLength;
        }

        private void txtBranch_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DisposeChecking(txtBranch);
            if (e.KeyChar == (char)Keys.Escape)
                dgvBranch.Visible = false;
        }

        private void txtBranch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvBranch.Focus();

            if (e.KeyCode == Keys.Enter && dgvBranch.Rows.Count > 0)
            {
                txtBranch.Text = dgvBranch.Rows[0].Cells[1].Value.ToString();
                BranchId = Convert.ToInt32(dgvBranch.Rows[0].Cells[0].Value.ToString());
                Branch = dgvBranch.Rows[0].Cells[1].Value.ToString();
                dgvBranch.Visible = false;
                txtBranch.SelectionStart = txtBranch.TextLength;
            }
        }

        private void txtBranch_TextChanged(object sender, EventArgs e)
        {
            dbConLand.MakeTextUppercase(txtBranch);
            var dataTable = (DataTable)dgvBranch.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvBranch.Visible = true;
                dataView.RowFilter = dgvBranch.Columns[1].Name + " LIKE '%" + txtBranch.Text.Replace("'", "''") + "%'";
            }
        }

        private void dgvBranch_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dgvBank_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtDeedNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDaagNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtKhatianNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtMouzaName_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAreaMutation_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtLoc_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtSubLoc_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtComments_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtBank_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtBranch_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtOthers_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDevCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtRegCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDeedValue_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAreaDeed_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAsset_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtSelller_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDeedValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbers(txtDeedValue, e);
        }

        private void txtDeedValue_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDeedValue.Text))
            {
                double s = Convert.ToDouble(txtDeedValue.Text);
                txtDeedValue.Text = s.ToString("##,##0.00");
            }
        }

        private void txtRegCost_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRegCost.Text))
            {
                double s = Convert.ToDouble(txtRegCost.Text);
                txtRegCost.Text = s.ToString("##,##0.00");
            }
        }

        private void txtRegCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbers(txtRegCost, e);
        }

        private void txtDevCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLand.DecimalNumbers(txtDevCost, e);
        }

        private void txtDevCost_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDevCost.Text))
            {
                double s = Convert.ToDouble(txtDevCost.Text);
                txtDevCost.Text = s.ToString("##,##0.00");
            }
        }

        private void txtOthers_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtOthers.Text))
            {
                double s = Convert.ToDouble(txtOthers.Text);
                txtOthers.Text = s.ToString("##,##0.00");
            }
        }

        private void txtAreaDeed_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAreaDeed.Text))
            {
                double s = Convert.ToDouble(txtAreaDeed.Text);
                txtAreaDeed.Text = s.ToString("##,##0.00");
            }
        }

        private void txtAreaMutation_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAreaMutation.Text))
            {
                double s = Convert.ToDouble(txtAreaMutation.Text);
                txtAreaMutation.Text = s.ToString("##,##0.00");
            }
        }

        private void cmbCC_DropDown(object sender, EventArgs e)
        {
            dbConLand.DisposeChecking(cmbCC);
        }

        private void cmbCompany_DropDown(object sender, EventArgs e)
        {
            dbConLand.DisposeChecking(cmbCompany);
        }

        private void cmbGroup_DropDown(object sender, EventArgs e)
        {
            dbConLand.DisposeChecking(cmbGroup);
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "New");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();

            if (IsUpdate)
                ttip.SetToolTip(this.btnUpdate, "Update");
            else
                ttip.SetToolTip(this.btnUpdate, "Edit");
        }

        private void frmAssetConfigLand_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void txtAsset_TextChanged_1(object sender, EventArgs e)
        {
            dbConLand.MakeTextUppercase(txtAsset);
        }
    }
}
