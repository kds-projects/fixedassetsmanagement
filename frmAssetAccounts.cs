﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAssetAccounts : Form
    {
        private clsCommonFunctions dbConACInfo = new clsCommonFunctions();
        private bool IsUpdate = false;
        private string CompanyId = "", CcCode = "", GroupId = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAssetAccounts()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmAssetConfigLand_Load(object sender, EventArgs e)
        {
            dbConACInfo.FillDataGridView(dgvCompany, @"SELECT CompID, CompName FROM AUTOMATIONXS.KDSXS_Accounts.dbo.CompanyName");
            dgvCompany.Columns[0].Visible = false;
            dgvCompany.Columns[1].Width = 200;
            dbConACInfo.FillDataGridView(dgvGroup, @"SELECT ass_code,
	                                                           RTRIM(ass_head)  AS Grp
	                                                    FROM   AUTOMATIONXS.KDSXS_Accounts.dbo.ass_code
	                                                    WHERE  ac_code = '010000'
	                                                           AND ass_code NOT IN ('010110', '010101')
	                                                    ORDER BY
	                                                           ass_head");
            dgvGroup.Columns[0].Visible = false;
            dgvGroup.Columns[1].Width = 200;
            dbConACInfo.FillDataGridView(dgvCC, @"SELECT CC_CODE, RTRIM(CC_NAME) AS CostCentre FROM AUTOMATIONXS.KDSXS_Accounts.dbo.CostCentre");
            dgvCC.Columns[0].Visible = false;
            dgvCC.Columns[1].Width = 200;
            EnableUpdate(false);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConACInfo.CheckEmptyOrNull(txtCompnay, "Select Company") &&
                //!dbConACInfo.CheckEmptyOrNull(txtCC, "Select CostCentre") &&
                //!dbConACInfo.CheckEmptyOrNull(txtGroup, "Select Asset Group") &&
                !dbConACInfo.CheckEmptyOrNull(txtAddCost, "Enter Addition for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtAdjCost, "Enter Adjustment for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtDispCost, "Enter Disposal for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtRevCost, "Enter Revaluation for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtAddDep, "Enter Addition for Depreciation")&&
                !dbConACInfo.CheckEmptyOrNull(txtAdjustDep, "Enter Adjustment for Depreciation")&&
                !dbConACInfo.CheckEmptyOrNull(txtDispDep, "Enter Disposal for Depreciation"))
            {
                if (!IsUpdate)
                {
                    double OpenBalCost = 0.00, AdditionCost = 0.00, AdjsutmentCost = 0.00, DisposalCost = 0.00, RevaluationCost = 0.00, ClosingCost = 0.00,
                        OpenBalDep = 0.00, AdditionDep = 0.00, AdjustmentDep = 0.00, DisposalDep = 0.00, ClosingDep = 0.00;
                                        
                    if (!string.IsNullOrEmpty(txtAddCost.Text))
                        AdditionCost = Convert.ToDouble(txtAddCost.Text);
                    if (!string.IsNullOrEmpty(txtAdjCost.Text))
                        AdjsutmentCost = Convert.ToDouble(txtAdjCost.Text);
                    if (!string.IsNullOrEmpty(txtDispCost.Text))
                        DisposalCost = Convert.ToDouble(txtDispCost.Text);
                    if (!string.IsNullOrEmpty(txtRevCost.Text))
                        RevaluationCost = Convert.ToDouble(txtRevCost.Text);
                    if (!string.IsNullOrEmpty(txtAddDep.Text))
                        AdditionDep = Convert.ToDouble(txtAddDep.Text);
                    if (!string.IsNullOrEmpty(txtAdjustDep.Text))
                        AdjustmentDep = Convert.ToDouble(txtAdjustDep.Text);
                    if (!string.IsNullOrEmpty(txtDispDep.Text))
                        DisposalDep = Convert.ToDouble(txtDispDep.Text);
                    string RDate = dtpPeriodDate.Value.ToString("yyyy-MM-01");

                    string[] cols = { "[CompanyId]", "[CostcentreCode]", "[AssetGroupId]",
                                      "[Addition]", "[Adjustment]", "[Disposal]", "[Revaluation]",
                                      "[DepAddition]", "[DepAdjustment]", "[DepDisposal]",
                                      "[RecordDate]","[EntryBy]" };
                    string[] vals = { CompanyId, CcCode, GroupId,
                                      AdditionCost.ToString(), AdjsutmentCost.ToString(), DisposalCost.ToString(), RevaluationCost.ToString(),
                                      AdditionDep.ToString(), AdjustmentDep.ToString(), DisposalDep.ToString(), 
                                      RDate,frmLogin.userId.ToString() };

                    //string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
                    //                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    //string[] lVals = { frmLogin.userId.ToString(), "INSERT,INSERT", frmLogin.ipAddress, 
                    //                   "[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_LandDetails]", this.Name, 
                    //                   dbConLand.getMaxIdentity("[AR_AssetConfig]", "[AssetId]").ToString(), "", "" };

                    string sql = dbConACInfo.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetAccountsInfo]", cols, vals, "INSERT", null, null);
                    //sql += dbConACInfo.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConACInfo.DBCRUDFunction(sql, false))
                    {
                        ClearAll();
                        MessageBox.Show("Information Saved Successfully.", "Save",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Faild to Save Data.", "Save",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void ClearAll() 
        {           
            txtAddCost.Text = "";
            txtAdjCost.Text = "";
            txtDispCost.Text = "";
            txtRevCost.Text = "";
            txtAddDep.Text = "";
            txtAdjustDep.Text = "";
            txtDispDep.Text = "";
            txtCompnay.Text = "";
            CompanyId = "";
            txtCompnay.Focus();
            dgvCompany.Visible = false;
        }
                
        private void LoadDetails(string RDate)
        {
            SqlDataReader dtRdr;
            string sql = @"SELECT * FROM AR_AssetAccountsInfo WHERE CompanyId = '" + CompanyId 
                + "' AND CONVERT(VARCHAR(10), RecordDate, 120) = '" + RDate + "'";


            dtRdr = (SqlDataReader)dbConACInfo.DBCRUDFunction(sql, true);

            if (dtRdr.Read())
            {
                txtAddCost.Text = dtRdr["Addition"].ToString();
                txtAdjCost.Text = dtRdr["Adjustment"].ToString();
                txtDispCost.Text = dtRdr["Disposal"].ToString();
                txtRevCost.Text = dtRdr["Revaluation"].ToString();
                txtAddDep.Text = dtRdr["DepAddition"].ToString();
                txtAdjustDep.Text = dtRdr["DepAdjustment"].ToString();
                txtDispDep.Text = dtRdr["DepDisposal"].ToString();
                dbConACInfo.DisposeChecking(txtCompnay);
            }

            dtRdr.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void ResetAll()
        {
            btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_properties_icon;
            IsUpdate = false;
            btnSave.Enabled = true;
            txtAddCost.Text = "";            
            txtAdjCost.Text = "";
            txtDispCost.Text = "";
            txtRevCost.Text = "";
            txtAddDep.Text = "";
            txtAdjustDep.Text = "";
            txtDispDep.Text = "";
            txtCompnay.Text = "";
            CcCode = "";
            txtCompnay.Focus();
            dgvCompany.Visible = false;
        }
                
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            EnableUpdate(true);

            if (!dbConACInfo.CheckEmptyOrNull(txtCompnay, "Select Company") &&
                //!dbConACInfo.CheckEmptyOrNull(txtCC, "Select CostCentre") &&
                //!dbConACInfo.CheckEmptyOrNull(txtGroup, "Select Asset Group") &&
                !dbConACInfo.CheckEmptyOrNull(txtAddCost, "Enter Addition for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtAdjCost, "Enter Adjustment for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtDispCost, "Enter Disposal for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtRevCost, "Enter Revaluation for Cost") &&
                !dbConACInfo.CheckEmptyOrNull(txtAddDep, "Enter Addition for Depreciation") &&
                !dbConACInfo.CheckEmptyOrNull(txtAdjustDep, "Enter Adjustment for Depreciation") &&
                !dbConACInfo.CheckEmptyOrNull(txtDispDep, "Enter Disposal for Depreciation")) 
            {
                if (IsUpdate)
                {
                    double AdditionCost = 0.00, AdjsutmentCost = 0.00, DisposalCost = 0.00, RevaluationCost = 0.00, 
                            AdditionDep = 0.00, AdjustmentDep = 0.00, DisposalDep = 0.00;

                    if (!string.IsNullOrEmpty(txtAddCost.Text))
                        AdditionCost = Convert.ToDouble(txtAddCost.Text);
                    if (!string.IsNullOrEmpty(txtAdjCost.Text))
                        AdjsutmentCost = Convert.ToDouble(txtAdjCost.Text);
                    if (!string.IsNullOrEmpty(txtDispCost.Text))
                        DisposalCost = Convert.ToDouble(txtDispCost.Text);
                    if (!string.IsNullOrEmpty(txtRevCost.Text))
                        RevaluationCost = Convert.ToDouble(txtRevCost.Text);
                    if (!string.IsNullOrEmpty(txtAddDep.Text))
                        AdditionDep = Convert.ToDouble(txtAddDep.Text);
                    if (!string.IsNullOrEmpty(txtAdjustDep.Text))
                        AdjustmentDep = Convert.ToDouble(txtAdjustDep.Text);
                    if (!string.IsNullOrEmpty(txtDispDep.Text))
                        DisposalDep = Convert.ToDouble(txtDispDep.Text);
                   
                    string[] cols = { "[CostcentreCode]", "[AssetGroupId]",
                                      "[Addition]", "[Adjustment]", "[Disposal]", "[Revaluation]",
                                      "[DepAddition]", "[DepAdjustment]", "[DepDisposal]" };
                    string[] vals = { CcCode, GroupId,
                                      AdditionCost.ToString(), AdjsutmentCost.ToString(), DisposalCost.ToString(), RevaluationCost.ToString(),
                                      AdditionDep.ToString(), AdjustmentDep.ToString(), DisposalDep.ToString() };
                    string[] whCols = { "[CompanyId]", "[RecordDate]" };
                    string[] whVals = { CompanyId.ToString(), dtpPeriodDate.Value.ToString("yyyy-MM-01") };

//                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
//                                        "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
//                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE,UPDATE", frmLogin.ipAddress, 
//                                       "[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_LandDetails]", this.Name, 
//                                       AssetId.ToString(), 
//                                       @"[AssetName],[CostCentreCode],[AssetDescription],[CompanyId],
//                                        [PurchaseAmt],[PurchaseDate],[AssetLoc],[AssetLocId],
//                                        [AssetSubLoc],[AssetSubLocId],[RegistrationNo],[IsActive],[Comments],
//                                        [Mouza],[RegDate],[DalilNo],[KhatianNo],[DaagNo],
//                                        [AreaDecimalDeed],[AreaDecimalMutation],[DeedValue],[RegCost],
//                                        [DevCost],[Others],[CurVal],[Seller],[MortgageBankId],
//                                        [MortgageBankBranchId]", 
//                                        Asset + "," + CCCode + "," + AssetDesc + "," + CompId + "," + 
//                                        TotalCost.ToString()+ "," + RegDate +  "," + AssetLoc + "," + 
//                                        AssetLocId.ToString() + "," + AssetSubLoc + "," + AssetSubLocId.ToString()
//                                        + "," + DalilNo + ",1," + Comments + "," +
//                                        Mouza + "," + RegDate + "," + DalilNo+ "," + KhatianNo+ "," + DaagNo+ "," +
//                                        AreaDeed+ "," + AreaMutation+ "," + DeedValue.ToString()+ "," + RegCost.ToString()+ "," + 
//                                        DevCost.ToString()+ "," + Others.ToString()+ "," + CurrVal.ToString()+ "," + Seller + "," +
//                                        BankId.ToString()+ "," + BranchId.ToString() };

                    string sql = dbConACInfo.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetAccountsInfo]", cols, vals, "UPDATE", whCols, whVals);
                    //sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_LandDetails]", cols2, vals2, "UPDATE", whCols, whVals);
                    //sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConACInfo.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Information Updated Successfully.", "Update",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(" Failed to Update Information.", "Update",
                                           MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

//            if (!string.IsNullOrEmpty(txtAssetCode.Text.Trim()))
//            {
//                if (IsAssetAvail() && IsUpdate)
//                {
//                    if (cmbCompany.SelectedIndex > -1)
//                        CompId = cmbCompany.SelectedValue.ToString();
//                    else
//                        CompId = "";

//                    if (cmbCC.SelectedIndex > -1)
//                        CCCode = cmbCC.SelectedValue.ToString();
//                    else
//                        CCCode = "00";

//                    if (cmbGroup.SelectedIndex > -1)
//                        GroupId = cmbGroup.SelectedValue.ToString();
//                    else
//                        GroupId = "000000";

//                    if (!string.IsNullOrEmpty(txtAsset.Text.Trim()))
//                        Asset = dbConLand.SingleQuoteHandler(txtAsset);
//                    else
//                        Asset = "";

//                    RegDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

//                    if (string.IsNullOrEmpty(txtAreaDeed.Text) || txtAreaDeed.Text == "")
//                        AreaDeed = "0.00";
//                    else
//                        AreaDeed = txtAreaDeed.Text;

//                    if (string.IsNullOrEmpty(txtAreaMutation.Text) || txtAreaMutation.Text == "")
//                        AreaMutation = "0.00";
//                    else
//                        AreaMutation = txtAreaMutation.Text;

//                    if (!string.IsNullOrEmpty(txtDeedNo.Text))
//                        DalilNo = dbConLand.SingleQuoteHandler(txtDeedNo);
//                    else
//                        DalilNo = "";

//                    if (!string.IsNullOrEmpty(txtDaagNo.Text))
//                        DaagNo = dbConLand.SingleQuoteHandler(txtDaagNo);
//                    else
//                        DaagNo = "";

//                    if (!string.IsNullOrEmpty(txtKhatianNo.Text))
//                        KhatianNo = dbConLand.SingleQuoteHandler(txtKhatianNo);
//                    else
//                        KhatianNo = "";

//                    if (!string.IsNullOrEmpty(txtMouzaName.Text))
//                        Mouza = dbConLand.SingleQuoteHandler(txtMouzaName);
//                    else
//                        Mouza = "";
//                    else
//                        Comments = "";

//                    string AssetDesc = "Deed No. : " + DalilNo
//                                        + "; Daag No. : " + DaagNo
//                                        + "; Khatian No. : " + KhatianNo
//                                        + "; Mouza Name :" + Mouza + ";";

//                    string[] cols1 = { "[AssetName]", "[CostCentreCode]", "[AssetDescription]", "[CompanyId]", 
//                                       "[PurchaseAmt]","[PurchaseDate]", "[AssetLoc]", "[AssetLocId]", 
//                                       "[AssetSubLoc]", "[AssetSubLocId]", "[RegistrationNo]", "[IsActive]", 
//                                       "[Comments]", "[UpdateUserId]", "[UpdateDateTime]", "[RecordFileNo]", "[VoucherNo]", "[AccCode]" };
//                    string[] vals1 = { Asset, CCCode, AssetDesc, CompId, 
//                                       TotalCost.ToString(), RegDate, AssetLoc, AssetLocId.ToString(),
//                                       AssetSubLoc, AssetSubLocId.ToString(), DalilNo, "1", 
//                                       Comments, frmLogin.userId.ToString(), dbConLand.GetServerCurrentDateTime().ToString(), 
//                                       dbConLand.SingleQuoteHandler(txtRecFileNo.Text),
//                                       dbConLand.SingleQuoteHandler(txtVoucherNo.Text),
//                                       dbConLand.SingleQuoteHandler(txtAccCode.Text) };
//                    string[] cols2 = { "[Mouza]", "[RegDate]", "[DalilNo]", "[KhatianNo]", "[DaagNo]",
//                                       "[AreaDecimalDeed]", "[AreaDecimalMutation]", "[DeedValue]","[RegCost]",
//                                       "[DevCost]", "[Others]","[CurVal]", "[Seller]", "[MortgageBankId]", 
//                                       "[MortgageBankBranchId]" };
//                    string[] vals2 = { Mouza, RegDate, DalilNo, KhatianNo, DaagNo,
//                                       AreaDeed, AreaMutation, DeedValue.ToString(), RegCost.ToString(), 
//                                       DevCost.ToString(), Others.ToString(), CurrVal.ToString(), Seller,
//                                       BankId.ToString(), BranchId.ToString() };
//                    string[] whCols = { "[AssetId]" };
//                    string[] whVals = { AssetId.ToString() };
//                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
//                                       "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
//                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE,UPDATE", frmLogin.ipAddress, 
//                                       "[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_LandDetails]", this.Name, 
//                                       AssetId.ToString(), 
//                                       @"[AssetName],[CostCentreCode],[AssetDescription],[CompanyId],
//                                        [PurchaseAmt],[PurchaseDate],[AssetLoc],[AssetLocId],
//                                        [AssetSubLoc],[AssetSubLocId],[RegistrationNo],[IsActive],[Comments],
//                                        [Mouza],[RegDate],[DalilNo],[KhatianNo],[DaagNo],
//                                        [AreaDecimalDeed],[AreaDecimalMutation],[DeedValue],[RegCost],
//                                        [DevCost],[Others],[CurVal],[Seller],[MortgageBankId],
//                                        [MortgageBankBranchId]", 
//                                        Asset + "," + CCCode + "," + AssetDesc + "," + CompId + "," + 
//                                        TotalCost.ToString()+ "," + RegDate +  "," + AssetLoc + "," + 
//                                        AssetLocId.ToString() + "," + AssetSubLoc + "," + AssetSubLocId.ToString()
//                                        + "," + DalilNo + ",1," + Comments + "," +
//                                        Mouza + "," + RegDate + "," + DalilNo+ "," + KhatianNo+ "," + DaagNo+ "," +
//                                        AreaDeed+ "," + AreaMutation+ "," + DeedValue.ToString()+ "," + RegCost.ToString()+ "," + 
//                                        DevCost.ToString()+ "," + Others.ToString()+ "," + CurrVal.ToString()+ "," + Seller + "," +
//                                        BankId.ToString()+ "," + BranchId.ToString() };

//                    string sql = dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols1, vals1, "UPDATE", whCols, whVals);
//                    sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_LandDetails]", cols2, vals2, "UPDATE", whCols, whVals);
//                    sql += dbConLand.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

//                    if ((bool)dbConLand.DBCRUDFunction(sql, false))
//                    {
//                        MessageBox.Show("Information Updated Successfully.", "Update",
//                            MessageBoxButtons.OK, MessageBoxIcon.Information);
//                    }
//                    else
//                    {
//                        MessageBox.Show(" Failed to Update Information.", "Update",
//                                           MessageBoxButtons.OK, MessageBoxIcon.Error);
//                    }
//                }
//                else
//                {
//                    MessageBox.Show("There is NO Asset Like AssetCode " + AssetCode + ".", "Not Found",
//                                                          MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                //}
            //}
        }

        private void EnableUpdate(bool Status)
        {
            if (Status)
            {
                //btnUpdate.Text = "Update";
                btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
                IsUpdate = true;                
                btnSave.Enabled = false;
                //dgvListData.Visible = false;
            }
            else
            {
                //btnUpdate.Text = "Edit";
                btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_properties_icon;
                IsUpdate = false;
                btnSave.Enabled = true;
                //dgvListData.Visible = false;
            }
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }
                        
        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "New");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();

            if (IsUpdate)
                ttip.SetToolTip(this.btnUpdate, "Update");
            else
                ttip.SetToolTip(this.btnUpdate, "Edit");
        }

        private void frmAssetConfigLand_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void txtOpenBalCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAddCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAdjCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDispCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtRevCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtCloseBalCost_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtOpenBalDep_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAddDep_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAdjustDep_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDispDep_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtCloseBalDep_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAddCost_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAddCost.Text))
            {
                double s = Convert.ToDouble(txtAddCost.Text);
                txtAddCost.Text = s.ToString("##,##0.00");
            }
        }

        private void txtAdjCost_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAdjCost.Text))
            {
                double s = Convert.ToDouble(txtAdjCost.Text);
                txtAdjCost.Text = s.ToString("##,##0.00");
            }
        }

        private void txtDispCost_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDispCost.Text))
            {
                double s = Convert.ToDouble(txtDispCost.Text);
                txtDispCost.Text = s.ToString("##,##0.00");
            }
        }

        private void txtRevCost_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRevCost.Text))
            {
                double s = Convert.ToDouble(txtRevCost.Text);
                txtRevCost.Text = s.ToString("##,##0.00");
            }
        }

        private void txtAddDep_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAddDep.Text))
            {
                double s = Convert.ToDouble(txtAddDep.Text);
                txtAddDep.Text = s.ToString("##,##0.00");
            }
        }

        private void txtAdjustDep_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAdjustDep.Text))
            {
                double s = Convert.ToDouble(txtAdjustDep.Text);
                txtAdjustDep.Text = s.ToString("##,##0.00");
            }
        }

        private void txtDispDep_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDispDep.Text))
            {
                double s = Convert.ToDouble(txtDispDep.Text);
                txtDispDep.Text = s.ToString("##,##0.00");
            }
        }

        private void txtCompnay_TextChanged(object sender, EventArgs e)
        {
            dbConACInfo.MakeTextUppercase(txtCompnay);
            var dataTable = (DataTable)dgvCompany.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvCompany.Visible = true;
                dataView.RowFilter = dgvCompany.Columns[1].Name + " LIKE '%" + txtCompnay.Text + "%'";
            }
        }

        private void txtCompnay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvCompany.Visible = false;
        }

        private void txtCompnay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvCompany.Focus();

            if (e.KeyCode == Keys.Enter && dgvCompany.Rows.Count >= 1)
            {
                txtCompnay.Text = dgvCompany.Rows[0].Cells[1].Value.ToString();
                CompanyId = dgvCompany.Rows[0].Cells[0].Value.ToString();
                //AssetCode = dgvCompany.Rows[0].Cells[1].Value.ToString();
                //if(IsUpdate)
                //    LoadAssetDetails(dtpPeriodDate.Value.ToString("yyyy-MM"));
                dgvCompany.Visible = false;
                txtCompnay.SelectionStart = txtCompnay.TextLength;
            }
        }

        private void dgvCompany_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CompanyId = dgvCompany.Rows[dgvCompany.CurrentCell.RowIndex].Cells[0].Value.ToString();
            //Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtCompnay.Text = dgvCompany.Rows[dgvCompany.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtCompnay.Focus();
            txtCompnay.SelectionStart = txtCompnay.TextLength;
            dgvCompany.Visible = false;
            //if(IsUpdate)
            //    LoadAssetDetails(dtpPeriodDate.Value.ToString("yyyy-MM"));
        }

        private void dgvCompany_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CompanyId = dgvCompany.Rows[dgvCompany.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtCompnay.Text = dgvCompany.Rows[dgvCompany.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtCompnay.Focus();
                txtCompnay.SelectionStart = txtCompnay.TextLength;
                dgvCompany.Visible = false;
                //if(IsUpdate)
                //    LoadAssetDetails(dtpPeriodDate.Value.ToString("yyyy-MM"));
            }
        }

        private void dgvCompany_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtCompnay.Focus();
            txtCompnay.SelectionStart = txtCompnay.TextLength;
        }

        private void txtGroup_TextChanged(object sender, EventArgs e)
        {
            dbConACInfo.MakeTextUppercase(txtGroup);
            var dataTable = (DataTable)dgvGroup.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvGroup.Visible = true;
                dataView.RowFilter = dgvGroup.Columns[1].Name + " LIKE '%" + txtGroup.Text + "%'";
            }
        }

        private void txtGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvGroup.Visible = false;
        }

        private void txtGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvGroup.Focus();

            if (e.KeyCode == Keys.Enter && dgvGroup.Rows.Count >= 1)
            {
                txtGroup.Text = dgvGroup.Rows[0].Cells[1].Value.ToString();
                GroupId = dgvGroup.Rows[0].Cells[0].Value.ToString();
                //AssetCode = dgvCompany.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvGroup.Visible = false;
                txtGroup.SelectionStart = txtGroup.TextLength;
            }
        }

        private void dgvGroup_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GroupId = dgvGroup.Rows[dgvGroup.CurrentCell.RowIndex].Cells[0].Value.ToString();
            //Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtGroup.Text = dgvGroup.Rows[dgvGroup.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtGroup.Focus();
            txtGroup.SelectionStart = txtGroup.TextLength;
            dgvGroup.Visible = false;
        }

        private void dgvGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GroupId = dgvGroup.Rows[dgvGroup.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtGroup.Text = dgvGroup.Rows[dgvGroup.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtGroup.Focus();
                txtGroup.SelectionStart = txtGroup.TextLength;
                dgvGroup.Visible = false;
                //LoadBranch();
            }
        }

        private void dgvGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtGroup.Focus();
            txtGroup.SelectionStart = txtGroup.TextLength;
        }

        private void txtCC_TextChanged(object sender, EventArgs e)
        {
            dbConACInfo.MakeTextUppercase(txtCC);
            var dataTable = (DataTable)dgvCC.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvCC.Visible = true;
                dataView.RowFilter = dgvCC.Columns[1].Name + " LIKE '%" + txtCC.Text + "%'";
            }
        }

        private void txtCC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvCC.Visible = false;
        }

        private void txtCC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvCC.Focus();

            if (e.KeyCode == Keys.Enter && dgvCC.Rows.Count >= 1)
            {
                txtCC.Text = dgvCC.Rows[0].Cells[1].Value.ToString();
                CcCode = dgvCC.Rows[0].Cells[0].Value.ToString();
                //AssetCode = dgvCompany.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvCC.Visible = false;
                txtCC.SelectionStart = txtCC.TextLength;
            }
        }

        private void dgvCC_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CcCode = dgvCC.Rows[dgvCC.CurrentCell.RowIndex].Cells[0].Value.ToString();
            //Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtCC.Text = dgvCC.Rows[dgvCC.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtCC.Focus();
            txtCC.SelectionStart = txtCC.TextLength;
            dgvCC.Visible = false;
        }

        private void dgvCC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CcCode = dgvCC.Rows[dgvCC.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //Bank = dgvBank.Rows[dgvBank.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtCC.Text = dgvCC.Rows[dgvCC.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtCC.Focus();
                txtCC.SelectionStart = txtCC.TextLength;
                dgvCC.Visible = false;
                //LoadBranch();
            }
        }

        private void dgvCC_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtCC.Focus();
            txtCC.SelectionStart = txtCC.TextLength;
        }

        private void dtpPeriodDate_ValueChanged(object sender, EventArgs e)
        {
            if (IsUpdate && CompanyId != "")
            {
                LoadDetails(dtpPeriodDate.Value.ToString("yyyy-MM-01"));
            }
        }

        private void txtAddCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtAddCost, e);
        }

        private void txtAdjCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtAdjCost, e);
        }

        private void txtDispCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtDispCost, e);
        }

        private void txtRevCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtRevCost, e);
        }

        private void txtAddDep_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtAddDep, e);
        }

        private void txtAdjustDep_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtAdjustDep, e);
        }

        private void txtDispDep_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConACInfo.DecimalNumbersWithSign(txtDispDep, e);
        }
    }
}
