﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAssetTransfer : Form
    {
        private clsCommonFunctions dbConAXfer = new clsCommonFunctions();
        private int AssetId = 0, AssetLocId = 0, AssetSubLocId = 0, AssetLocIdL = 0, AssetSubLocIdL = 0, CustDeptIdL = 0, CustDeptIdN = 0;
        private string AssetCodeL = "", AssetCodeN = "", ccCodeL = "", ccCodeN = "", compIdL = "", compIdN = "",
            assetLocL = "", assetLocN = "", assetSubLocL = "", assetSubLocN = "", UserCodeL = "", UserCodeN = "",
            AssetLoc = "", AssetSubLoc = "", CCCode = "", AssetUserL = "", AssetUserN = "", ApprovedUserCodeN = "";
        private bool cmbLoadFlag = false;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAssetTransfer()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXfer_Click(object sender, EventArgs e)
        {
            string sql = "";
            if (cmbCC.SelectedValue != null)
                ccCodeN = cmbCC.SelectedValue.ToString();
            //AssetCodeN = dbConAXfer.SingleQuoteHandler(txtAssetCode);
            compIdN = cmbCompany.SelectedValue.ToString();
            assetLocN = dbConAXfer.SingleQuoteHandler(txtLoc);
            assetSubLocN = dbConAXfer.SingleQuoteHandler(txtSubLoc);
            AssetUserN = dbConAXfer.SingleQuoteHandler(txtAssetUser);
            //AssetUserN = dbConAXfer.SingleQuoteHandler(txtAssetUser);

            string[] updCols = { "[CostCentreCode]", "[AssetCode]", "[CompanyId]", "[AssetLocId]", "[AssetLoc]", 
                                 "[AssetSubLocId]", "[AssetSubLoc]", "[AssetUserCode]" , "[AssetUser]", 
                                 "[TransferDescription]"};
            string[] updVals = { ccCodeN, AssetCodeN, compIdN, AssetLocId.ToString(),
                                 assetLocN, AssetSubLocId.ToString(), assetSubLocN, UserCodeN, 
                                 dbConAXfer.SingleQuoteHandler(txtAssetUser),
                                 dbConAXfer.SingleQuoteHandler(txtDesc).ToUpper()
                               };
            string[] whCols = { "[AssetId]" };
            string[] whVals = { AssetId.ToString() };
            string[] insCols = { "[AssetId]", "[AssetCode]", "[LastCCCode]", "[LastCompanyId]", "[LastCustodianDeptCode]", 
                                 "[LastAssetLoc]", "[LastAssetSubLoc]", "[LastAssetUserCode]", "[LastAssetUser]", "[TransferDate]", 
                                 "[UserId]", "[ReceivingDate]", "[ApprovedByCode]", "[ApprovedBy]", "[ReceivedByCode]", "[ReceivedBy]" };
            string[] insVals = { AssetId.ToString(), dbConAXfer.SingleQuoteHandler(AssetCodeL), lblCC.Tag.ToString(), lblCCode.Tag.ToString(), 
                                 CustDeptIdL.ToString(), lblALocation.Tag.ToString(), lblASLocation.Tag.ToString(), 
                                 lblAUser.Tag.ToString(), lblAUser.Text.ToUpper(), dtpTDate.Value.ToString("yyyy-MM-dd"), 
                                 frmLogin.userId.ToString(), dtpRDate.Value.ToString("yyyy-MM-dd"), ApprovedUserCodeN,
                                 dbConAXfer.SingleQuoteHandler(txtApprovedBy), UserCodeN, txtAssetUser.Text.ToUpper()
                               };
            string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                           "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
            string[] lVals = { frmLogin.userId.ToString(), "UPDATE,INSERT", frmLogin.ipAddress, 
                               "[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_AsseetXfer]", this.Name, dbConAXfer.getMaxIdentity("[AR_AsseetXfer]", "[TrnNo]").ToString(),
                               "[AssetId],[CostCentreCode],[AssetCode],[CompanyId],[AssetLocId],[AssetLoc],[AssetSubLocId],[AssetSubLoc],[AssetUserCode],[AssetUser],[TransferDescription]", 
                               AssetId.ToString() + "," + ccCodeL + "," + dbConAXfer.SingleQuoteHandler(AssetCodeL) + ", " + compIdL + "," + AssetLocIdL + "," + assetLocL + "," + AssetSubLocIdL + "," + assetSubLocL + "," + UserCodeL + "," + AssetUserL + "," + dbConAXfer.SingleQuoteHandler(txtDesc.Text.ToUpper()) };

            sql = dbConAXfer.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", updCols, updVals, "UPDATE", whCols, whVals);
            sql += dbConAXfer.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AsseetXfer]", insCols, insVals, "INSERT", null, null);
            sql += dbConAXfer.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);
            
            DateTime d = Convert.ToDateTime(dtpTDate.Value.Year + "-" + dtpTDate.Value.Month.ToString() + "-01") ;
            //string s = d.ToString("yyyy-MM-dd");
            if (IsProcesDeprecitation(AssetId))
            {
                string[] uCols = { "CompanyId", "CostCentreCode" };
                string[] uVals = { compIdN, ccCodeN };
                string[] whUcols = { "[AssetId]", "[ProcessDate]" };
                string[] whUvals = { AssetId.ToString(), d.ToString("yyyy-MM-dd") };
                string[] whOp = { "=", ">="};

                sql += dbConAXfer.CreateSQLStatement("[AssetRegistar].[dbo].[AR_Process]", uCols, uVals, "UPDATE", whUcols, whUvals, whOp);
                sql += dbConAXfer.CreateSQLStatement("[AssetRegistar].[dbo].[AR_ProcessAddition]", uCols, uVals, "UPDATE", whUcols, whUvals, whOp);

                if ((bool)dbConAXfer.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Asset Transferred Successfully.", "Asset Transfer",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearAll();
                }
                else
                {
                    MessageBox.Show("Asset Transferred Failed.Please Try Again.", "Asset Transfer Failed",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if ((bool)dbConAXfer.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Asset Transferred Successfully.", "Asset Transfer",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearAll();
                }
                else
                {
                    MessageBox.Show("Asset Transferred Failed.Please Try Again.", "Asset Transfer Failed",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool IsProcesDeprecitation(int AssetId)
        {
            SqlDataReader dr;
            dr = (SqlDataReader)dbConAXfer.DBCRUDFunction(@"SELECT * FROM AR_Process WHERE AssetId = " + AssetId
                                                        + " AND YEAR(ProcessDate) = " + dtpTDate.Value.Year, true);

            if (dr.HasRows)
                return true;
            else
                return false;

        }

        private void ClearAll()
        {
            txtAsset.Text = "";
            AssetId = 0;
            cmbCC.SelectedIndex = -1;
            compIdN = "";
            compIdL = "";
            ccCodeL = "";
            ccCodeN = "";
            CCCode = "";
            cmbCC.SelectedIndex = -1;
            cmbCompany.SelectedIndex = -1;
            txtLoc.Text = "";
            AssetLocId = 0;
            AssetLoc = "";
            dgvAssetLoc.Visible = false;
            txtSubLoc.Text = "";
            AssetSubLocId = 0;
            AssetSubLoc = "";
            dgvAssetSubLoc.Visible = false;
            UserCodeL = "";
            txtAssetUser.Text = "";
            dgvAssetUser.Visible = false;
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
            ApprovedUserCodeN = "";
            txtApprovedBy.Text = "";
            dgvApprovedBy.Visible = false;
            UserCodeN = "";
            txtReceivedBy.Text = "";
            txtDesc.Text = "";
            dgvReceiveBy.Visible = false;

            lblACode.Text = "";
            lblACode.Tag = "0";
            lblAName.Text = "";
            lblCC.Text = "";
            lblCC.Tag = "0";
            lblCCode.Text = "";
            lblCCode.Tag = "0";
            lblALocation.Text = "";
            lblALocation.Tag = "0";
            lblASLocation.Text = "";
            lblASLocation.Tag = "0";
            lblAUser.Text = "";
            lblAUser.Tag = "0";
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConAXfer.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCodeL = dgvListData.Rows[0].Cells[1].Value.ToString();
                LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCodeL = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                LoadAssetDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCodeL = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
            LoadAssetDetails();
        }

        private void LoadAssetDetails() 
        {
            SqlDataReader dtRdr;
            dtRdr = (SqlDataReader)dbConAXfer.DBCRUDFunction(@"SELECT AssetCode, AssetName,
                                                                       aac.CostCentreCode,
                                                                       CC_Name,
                                                                       aac.CompanyId,
                                                                       cn.CompName,
                                                                       AssetLoc,
                                                                       AssetLocId,
                                                                       AssetLoc,
                                                                       AssetSubLocId,
                                                                       AssetSubLoc,
                                                                       ISNULL(EI.EmpCode, '') AssetUserCode,
                                                                       EI.FirstName As AssetUser,
                                                                       ISNULL(CustodianDeptCode, 0) CustodianDeptCode FROM [AssetRegistar].[dbo].[AR_AssetConfig] aac
                                                               INNER JOIN AUTOMATIONXS.KDSXS_Accounts.dbo.CompanyName AS cn
	                                                                    ON  cn.CompID = aac.CompanyId
                                                               INNER JOIN AUTOMATIONXS.KDSXS_Accounts.dbo.CostCentre cc
                                                                        ON  cc.CC_Code = aac.CostCentreCode
															   INNER JOIN HRMCUBE.HRMCube.dbo.EmpInfo As EI
																		ON aac.AssetUserCode = EI.EmpCode
                                                               WHERE AssetId = " + AssetId, true);

            if (dtRdr.Read())
            {
                AssetCodeN = dtRdr["AssetCode"].ToString();
                txtAsset.Text = dtRdr["AssetName"].ToString();
                ccCodeL = dtRdr["CostCentreCode"].ToString();
                ccCodeN = dtRdr["CostCentreCode"].ToString();
                cmbCC.Text = dtRdr["CC_Name"].ToString();
                compIdL = dtRdr["CompanyId"].ToString();
                compIdN = dtRdr["CompanyId"].ToString();
                cmbCompany.Text = dtRdr["CompName"].ToString();
                assetLocL = dtRdr["AssetLoc"].ToString();
                assetLocN = dtRdr["AssetLoc"].ToString();
                AssetLocIdL = Convert.ToInt32(dtRdr["AssetLocId"].ToString());
                AssetLocId = Convert.ToInt32(dtRdr["AssetLocId"].ToString());
                txtLoc.Text = dtRdr["AssetLoc"].ToString();
                AssetSubLocIdL = Convert.ToInt32(dtRdr["AssetSubLocId"].ToString());
                AssetSubLocId = Convert.ToInt32(dtRdr["AssetSubLocId"].ToString());
                assetSubLocL = dtRdr["AssetSubLoc"].ToString();
                txtSubLoc.Text = dtRdr["AssetSubLoc"].ToString();
                AssetUserL = dtRdr["AssetUser"].ToString();
                AssetUserN = dtRdr["AssetUser"].ToString();
                UserCodeL = dtRdr["AssetUserCode"].ToString();
                UserCodeN = dtRdr["AssetUserCode"].ToString();
                txtAssetUser.Text = dtRdr["AssetUser"].ToString();
                //txtDesc.Text = dtRdr["TransferDescription"].ToString();
                CustDeptIdL = Convert.ToInt32(dtRdr["CustodianDeptCode"].ToString());
                dgvAssetLoc.Visible = false;
                dgvAssetSubLoc.Visible = false;
                dgvAssetUser.Visible = false;
                
                lblACode.Text = dtRdr["AssetCode"].ToString();
                lblACode.Tag = AssetId.ToString();
                lblAName.Text = txtAsset.Text;
                lblCC.Text = cmbCC.Text;
                lblCC.Tag = ccCodeL.ToString();
                lblCCode.Text = cmbCompany.Text;
                lblCCode.Tag = compIdL.ToString();
                lblALocation.Text = txtLoc.Text;
                lblALocation.Tag = assetLocL.ToString();
                lblASLocation.Text = txtSubLoc.Text;
                lblASLocation.Tag = assetSubLocL.ToString();
                lblAUser.Text = txtAssetUser.Text;
                lblAUser.Tag = UserCodeL.ToString();
            }
        }

        private void frmAssetTransfer_Load(object sender, EventArgs e)
        {
            dbConAXfer.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConAXfer.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");            

            cmbLoadFlag = true;
            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            dbConAXfer.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '', '', 0");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;

            dbConAXfer.FillDataGridView(dgvAssetUser, @"EXEC prcCmbLoadEmployee 0");
            dgvAssetUser.Columns[0].Visible = false;
            dgvAssetUser.Columns[1].Width = 200;

            dbConAXfer.FillDataGridView(dgvApprovedBy, @"EXEC prcCmbLoadEmployee 0, 1");
            dgvApprovedBy.Columns[0].Visible = false;
            dgvApprovedBy.Columns[1].Width = 200;

            dbConAXfer.FillDataGridView(dgvReceiveBy, @"EXEC prcCmbLoadEmployee 0, 1");
            dgvReceiveBy.Columns[0].Visible = false;
            dgvReceiveBy.Columns[1].Width = 200;
        }

        private void txtLoc_TextChanged(object sender, EventArgs e)
        {
            dbConAXfer.MakeTextUppercase(txtLoc);
            var dataTable = (DataTable)dgvAssetLoc.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetLoc.Visible = true;
                dataView.RowFilter = dgvAssetLoc.Columns[1].Name + " LIKE '%" + txtLoc.Text + "%'";
            }
        }

        private void txtLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAXfer.DisposeChecking(txtLoc);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetLoc.Visible = false;
        }

        private void txtLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetLoc.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetLoc.Rows.Count > 0)
            {
                txtLoc.Text = dgvAssetLoc.Rows[0].Cells[1].Value.ToString();
                AssetLocId = Convert.ToInt32(dgvAssetLoc.Rows[0].Cells[0].Value.ToString());
                AssetLoc = dgvAssetLoc.Rows[0].Cells[1].Value.ToString();
                LoadAssetSubLoc();
                dgvAssetLoc.Visible = false;
                txtLoc.SelectionStart = txtLoc.TextLength;
            }
        }

        private void LoadAssetSubLoc()
        {
            if (cmbCC.SelectedIndex > -1 && AssetLocId > 0)
            {
                dbConAXfer.FillDataGridView(dgvAssetSubLoc, @"SELECT AssetSubLocId,
                                                                          AssetSubLocation
                                                                   FROM   AR_AssetSubLocation
                                                                   WHERE  AssetLocId =" + AssetLocId);
                dgvAssetSubLoc.Columns[0].Visible = false;
                dgvAssetSubLoc.Columns[1].Width = 200;
            }
        }

        private void dgvAssetLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetLocId = Convert.ToInt32(dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtLoc.Text = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtLoc.Focus();
                txtLoc.SelectionStart = txtLoc.TextLength;
                dgvAssetLoc.Visible = false;
                LoadAssetSubLoc();
            }
        }

        private void dgvAssetLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtLoc.Focus();
            txtLoc.SelectionStart = txtLoc.TextLength;
        }

        private void dgvAssetLoc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetLocId = Convert.ToInt32(dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtLoc.Text = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtLoc.Focus();
            txtLoc.SelectionStart = txtLoc.TextLength;
            dgvAssetLoc.Visible = false;
            LoadAssetSubLoc();
        }

        private void dgvAssetLoc_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void txtSubLoc_TextChanged(object sender, EventArgs e)
        {
            dbConAXfer.MakeTextUppercase(txtSubLoc);
            var dataTable = (DataTable)dgvAssetSubLoc.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetSubLoc.Visible = true;
                dataView.RowFilter = dgvAssetSubLoc.Columns[1].Name + " LIKE '%" + txtSubLoc.Text + "%'";
            }
        }

        private void txtSubLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAXfer.DisposeChecking(txtSubLoc);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetSubLoc.Visible = false;
        }

        private void txtSubLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetSubLoc.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetSubLoc.Rows.Count > 0)
            {
                txtSubLoc.Text = dgvAssetSubLoc.Rows[0].Cells[1].Value.ToString();
                AssetSubLocId = Convert.ToInt32(dgvAssetSubLoc.Rows[0].Cells[0].Value.ToString());
                AssetSubLoc = dgvAssetSubLoc.Rows[0].Cells[1].Value.ToString();
                dgvAssetSubLoc.Visible = false;
                txtSubLoc.SelectionStart = txtSubLoc.TextLength;
            }
        }

        private void dgvAssetSubLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetSubLocId = Convert.ToInt32(dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetSubLoc = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSubLoc.Text = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSubLoc.Focus();
                txtSubLoc.SelectionStart = txtSubLoc.TextLength;
                dgvAssetSubLoc.Visible = false;                
            }
        }

        private void dgvAssetSubLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtSubLoc.Focus();
            txtSubLoc.SelectionStart = txtSubLoc.TextLength;
        }

        private void dgvAssetSubLoc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetSubLocId = Convert.ToInt32(dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetSubLoc = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSubLoc.Text = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSubLoc.Focus();
            txtSubLoc.SelectionStart = txtSubLoc.TextLength;
            dgvAssetSubLoc.Visible = false;
        }

        private void dgvAssetSubLoc_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void cmbCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCC.SelectedIndex > -1)
            {
                CCCode = cmbCC.SelectedValue.ToString();
                dbConAXfer.FillDataGridView(dgvAssetLoc, @"SELECT AssetLocId,
                                                                       AssetLocation
                                                                FROM   AR_AssetLocation
                                                                WHERE  CostCentreCode = '" + CCCode + "'");
                dgvAssetLoc.Columns[0].Visible = false;
                dgvAssetLoc.Columns[1].Width = 200;
            }

            txtLoc.Text = "";
            dgvAssetLoc.Visible = false;
            txtSubLoc.Text = "";
            dgvAssetSubLoc.Visible = false;
        }

        private void txtAssetUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetUser.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetUser.Rows.Count > 0)
            {
                txtAssetUser.Text = dgvAssetUser.Rows[0].Cells[1].Value.ToString().Trim();
                UserCodeN = dgvAssetUser.Rows[0].Cells[0].Value.ToString();
                dgvAssetUser.Visible = false;
                txtAssetUser.SelectionStart = txtAssetUser.TextLength;
            }
        }

        private void txtAssetUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAXfer.DisposeChecking(txtAssetUser);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetUser.Visible = false;
        }

        private void txtAssetUser_TextChanged(object sender, EventArgs e)
        {
            dbConAXfer.MakeTextUppercase(txtAssetUser);
            var dataTable = (DataTable)dgvAssetUser.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetUser.Visible = true;
                dataView.RowFilter = dgvAssetUser.Columns[1].Name + " LIKE '%" + txtAssetUser.Text + "%'";
            }
        }

        private void dgvAssetUser_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            UserCodeL = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[0].Value.ToString();
            txtAssetUser.Text = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
            txtAssetUser.Focus();
            txtAssetUser.SelectionStart = txtAssetUser.TextLength;
            dgvAssetUser.Visible = false;
        }

        private void dgvAssetUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                UserCodeN = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetUser.Text = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetUser.Focus();
                txtAssetUser.SelectionStart = txtAssetUser.TextLength;
                dgvAssetUser.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvAssetUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetUser.Focus();
            txtAssetUser.SelectionStart = txtAssetUser.TextLength;
        }

        private void dgvAssetUser_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void txtReceivedBy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvReceiveBy.Focus();

            if (e.KeyCode == Keys.Enter && dgvReceiveBy.Rows.Count > 0)
            {
                txtReceivedBy.Text = dgvReceiveBy.Rows[0].Cells[1].Value.ToString().Trim();
                UserCodeN = dgvReceiveBy.Rows[0].Cells[0].Value.ToString();
                dgvReceiveBy.Visible = false;
                txtReceivedBy.SelectionStart = txtReceivedBy.TextLength;
            }
        }

        private void txtReceivedBy_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAXfer.DisposeChecking(txtReceivedBy);
            if (e.KeyChar == (char)Keys.Escape)
                dgvReceiveBy.Visible = false;
        }

        private void txtReceivedBy_TextChanged(object sender, EventArgs e)
        {
            dbConAXfer.MakeTextUppercase(txtReceivedBy);
            var dataTable = (DataTable)dgvReceiveBy.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvReceiveBy.Visible = true;
                dataView.RowFilter = dgvReceiveBy.Columns[1].Name + " LIKE '%" + txtReceivedBy.Text + "%'";
            }
        }

        private void dgvReceiveBy_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            UserCodeN = dgvReceiveBy.Rows[dgvReceiveBy.CurrentCell.RowIndex].Cells[0].Value.ToString();
            txtReceivedBy.Text = dgvReceiveBy.Rows[dgvReceiveBy.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
            txtReceivedBy.Focus();
            txtReceivedBy.SelectionStart = txtReceivedBy.TextLength;
            dgvReceiveBy.Visible = false;
        }

        private void dgvReceiveBy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                UserCodeN = dgvReceiveBy.Rows[dgvReceiveBy.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtReceivedBy.Text = dgvReceiveBy.Rows[dgvReceiveBy.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtReceivedBy.Focus();
                txtReceivedBy.SelectionStart = txtReceivedBy.TextLength;
                dgvReceiveBy.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvReceiveBy_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtReceivedBy.Focus();
            txtReceivedBy.SelectionStart = txtReceivedBy.TextLength;
        }

        private void dgvReceiveBy_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void txtApprovedBy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvApprovedBy.Focus();

            if (e.KeyCode == Keys.Enter && dgvApprovedBy.Rows.Count > 0)
            {
                txtApprovedBy.Text = dgvApprovedBy.Rows[0].Cells[1].Value.ToString().Trim();
                ApprovedUserCodeN = dgvApprovedBy.Rows[0].Cells[0].Value.ToString();
                dgvApprovedBy.Visible = false;
                txtApprovedBy.SelectionStart = txtApprovedBy.TextLength;
            }
        }

        private void txtApprovedBy_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAXfer.DisposeChecking(txtApprovedBy);
            if (e.KeyChar == (char)Keys.Escape)
                dgvApprovedBy.Visible = false;
        }

        private void txtApprovedBy_TextChanged(object sender, EventArgs e)
        {
            dbConAXfer.MakeTextUppercase(txtApprovedBy);
            var dataTable = (DataTable)dgvApprovedBy.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvApprovedBy.Visible = true;
                dataView.RowFilter = dgvApprovedBy.Columns[1].Name + " LIKE '%" + txtApprovedBy.Text + "%'";
            }
        }

        private void dgvApprovedBy_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ApprovedUserCodeN = dgvApprovedBy.Rows[dgvApprovedBy.CurrentCell.RowIndex].Cells[0].Value.ToString();
            txtApprovedBy.Text = dgvApprovedBy.Rows[dgvApprovedBy.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
            txtApprovedBy.Focus();
            txtApprovedBy.SelectionStart = txtApprovedBy.TextLength;
            dgvApprovedBy.Visible = false;
        }

        private void dgvApprovedBy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ApprovedUserCodeN = dgvApprovedBy.Rows[dgvApprovedBy.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtApprovedBy.Text = dgvApprovedBy.Rows[dgvApprovedBy.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtApprovedBy.Focus();
                txtApprovedBy.SelectionStart = txtApprovedBy.TextLength;
                dgvApprovedBy.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvApprovedBy_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtApprovedBy.Focus();
            txtApprovedBy.SelectionStart = txtApprovedBy.TextLength;
        }

        private void dgvApprovedBy_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void frmAssetTransfer_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnXfer_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnXfer, "Save");
        }
    }
}
