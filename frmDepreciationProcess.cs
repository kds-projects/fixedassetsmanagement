﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmDepreciationProcess : Form
    {
        private clsCommonFunctions dbConP = new clsCommonFunctions();
        DateTime dttm;
        private bool cmbLoadFlag = false;
        ErrorProvider errPvdr = new ErrorProvider();
        frmLoading frmLoading = new frmLoading();
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmDepreciationProcess()
        {
            InitializeComponent();
        }

        private void cbCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !cbCompany.Checked;
            if (cbCompany.Checked)
                cmbCompany.SelectedIndex = -1;
            else
                cmbCompany.SelectedIndex = 0;

            FillDGV();
        }

        private void cbCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !cbCC.Checked;
            if (cbCC.Checked)
                cmbCC.SelectedIndex = -1;
            else
                cmbCC.SelectedIndex = 0;

            FillDGV();
        }

        private void cbGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !cbGroup.Checked;
            if (cbGroup.Checked)
                cmbGroup.SelectedIndex = -1;
            else
                cmbGroup.SelectedIndex = 0;
            FillDGV();
        }

        private void frmDepreciationProcess_Load(object sender, EventArgs e)
        {
            dttm = dbConP.GetServerCurrentDateTime();
            Dictionary<string, string> MonthList = new Dictionary<string, string>();
            SqlDataReader dtMonthReader;
            string sql = @"SELECT GETDATE() as dt";
            dtMonthReader = (SqlDataReader)dbConP.DBCRUDFunction(sql, true);

            if (dtMonthReader.Read())
            {
                dgvFrm.Columns.Add("ASSET", "ASSET");
                dgvFrm.Columns.Add("ASSETID", "ASSETID");
                dgvFrm.Columns["ASSET"].ReadOnly = true;
                dgvFrm.Columns["ASSET"].Width = 327;
                dgvFrm.Columns["ASSETID"].Visible = false;

                dgvTo.Columns.Add("ASSET", "ASSET");
                dgvTo.Columns.Add("ASSETID", "ASSETID");
                dgvTo.Columns["ASSET"].ReadOnly = true;
                dgvTo.Columns["ASSET"].Width = 327;
                dgvTo.Columns["ASSETID"].Visible = false;
            }

            dbConP.FillComboBox(cmbCompany, @"SELECT CompID,
                                                       RTRIM(CompName)
                                                FROM   AUTOMATIONXS.KDSXS_Accounts.dbo.CompanyName
                                                ORDER BY
                                                       CompName");

            dbConP.FillComboBox(cmbCC, @"SELECT CC_Code AS CostCentreCode,
                                                   RTRIM(CC_NAME)
                                            FROM   AUTOMATIONXS.KDSXS_Accounts.dbo.CostCentre
                                            WHERE  CC_Type NOT IN ('Trading')
                                            ORDER BY
                                                   CC_NAME");

            dbConP.FillComboBox(cmbGroup, @"SELECT ass_code,
                                                   RTRIM(ass_head)
                                            FROM   AUTOMATIONXS.KDSXS_Accounts.dbo.ass_code
                                            WHERE  ac_code = '010000' AND ass_code NOT IN ('010101', '010110')
                                            ORDER BY ass_head");

            cmbLoadFlag = true;
            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;

            dtMonthReader.Close();
        }

        private void cmbCompany_SelectedValueChanged(object sender, EventArgs e)
        {            
            //FillDGV();
        }

        private void cmbCC_SelectedValueChanged(object sender, EventArgs e)
        {            
            //FillDGV();
        }

        private void cmbGroup_SelectedValueChanged(object sender, EventArgs e)
        {            
            //FillDGV();
        }
        
        private void FillDGV()
        {
            Cursor.Current = Cursors.WaitCursor;
            dgvFrm.Rows.Clear();           
            int Row = 0;

            try
            {
                SqlDataReader dr1,dr2;
                string sql = @"EXEC prcRptFillListView ";
                                
                if (cmbCompany.SelectedIndex > -1 && cmbCompany.Enabled == true)
                    sql += @"'" + cmbCompany.SelectedValue + "'";
                else
                    sql += @"''";

                if (cmbCC.SelectedIndex > -1 && cmbCC.Enabled == true)
                    sql += @", '" + cmbCC.SelectedValue + "'";
                else
                    sql += @", ''";

                if (cmbGroup.SelectedIndex > -1 && cmbGroup.Enabled == true)
                    sql += @", '" + cmbGroup.SelectedValue + "' ";
                else
                    sql += @", ''";

                if (chkYear.Checked == true && dtpYear.Enabled == true)
                    sql += @", " + dtpYear.Value.Year;
                else 
                    sql += @", 0";

                dr1 = (SqlDataReader) dbConP.DBCRUDFunction(sql, true);
                
                while (dr1.Read())
                {
                    dgvFrm.Rows.Add();
                    dgvFrm.Rows[Row].Cells["ASSET"].Value = dr1["Asset"].ToString();
                    dgvFrm.Rows[Row].Cells["ASSETID"].Value = dr1["AssetId"].ToString();
                    Row++;
                }
                
                dr1.Close();

                dr2 = (SqlDataReader)dbConP.DBCRUDFunction(sql, true);

                if (dr2.Read())
                    lblTotAsset.Text = dr2["Cnt"].ToString();
                else
                    lblTotAsset.Text = "0";

                dr2.Close();

                dgvFrm.ClearSelection();

                if (dgvFrm.Rows.Count > 0)
                {
                    btnD.Enabled = true;
                    btnDAll.Enabled = true;
                }

                //dtpFormDate.Value = DateTime.Parse("01-Jan-" + dtpYear.Value.Year);
                //dtpToDate.Value = DateTime.Parse("31-Dec-" + dtpYear.Value.Year);

                Cursor.Current = Cursors.Default;   
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (chkYear.Checked)            
            {
                if (dgvTo.Rows.Count > 0)
                    ProcessAll();//ProcessDate().ToString("yyyy"));
                else
                    MessageBox.Show("Select Asset from the List.", "Select",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            } 
            else if (chkDateRange.Checked)
            {
                if (dgvTo.Rows.Count > 0)
                    ProcessAll();//ProcessDate().ToString("yyyy"));
                else
                    MessageBox.Show("Select Asset from the List.", "Select",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Select Year from List.", "Process", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void ProcessAll()//DateTime FDate, DateTime TDate)
        {
            if (frmLoading == null)
                frmLoading = new frmLoading();

            frmLoading.Show();
            frmLoading.TopMost = true;
            frmLoading.label1.Text = "Processing, Please Wait . . .";

            try
            {
                //SqlDataReader dtRdr;
                //string sql = @"EXEC prcCheckProcess '" + prcDate + "','" + GetAllSelectedItem() + "'";
                
                //dtRdr = (SqlDataReader) dbConP.DBCRUDFunction(sql, true);

                //if (!dtRdr.HasRows)
                //{
                    Cursor.Current = Cursors.WaitCursor;

                    string strSql = @"EXEC prcProcessDepreciation '" + GetAllSelectedItem() + "', " +
                        "'" + (!cbCompany.Checked ? cmbCompany.SelectedValue.ToString() : "") + "', " +
                        "'" + dtpFormDate.Value.AddDays(-dtpFormDate.Value.Day + 1).ToString("dd-MMM-yyyy") + "', " +
                        "'" + dtpToDate.Value.AddDays(DateTime.DaysInMonth(dtpToDate.Value.Year, dtpToDate.Value.Month) - dtpToDate.Value.Day).ToString("dd-MMM-yyyy") + "', " +
                        frmLogin.userId + ", '" +
                        frmLogin.ipAddress + "', 0, 0";

                    if ((bool)dbConP.DBCRUDFunction(strSql, false))
                    {
                        //dgvTo.Rows.Clear();
                        //chkYear.Checked = false;
                        //cbCompany.Checked = true;
                        //cbCC.Checked = true;
                        //cbGroup.Checked = true;
                        Cursor.Current = Cursors.Default;
                        frmLoading.Hide();
                        MessageBox.Show("Data processsed successfully.", "Data Process",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        Cursor.Current = Cursors.Default;
                        frmLoading.Hide();
                    }
                //}
                //else
                //{
                //    Cursor.Current = Cursors.Default;
                //    frmLoading.Hide();
                //    MessageBox.Show("Data already processsed for this month.", "Data Process",
                //                       MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //}
                //dtRdr.Close();
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private string GetAllSelectedItem()
        {
            string assetId = "", comma = "";


            for (int i = 0; i < dgvTo.Rows.Count; i++)
            {                
                assetId += comma + dgvTo.Rows[i].Cells[1].Value.ToString();
                comma = ",";         
            }

            return assetId;
        }

        private DateTime ProcessDate()
        {
            DateTime processDate = DateTime.Today;
            int pMon, pYear, pDay;

            pMon = 12;//dtpMonth.Value.Month;
            pYear = dtpYear.Value.Year;            
            pDay = DateTime.DaysInMonth(pYear, pMon);
            processDate = Convert.ToDateTime(pYear + "-" + pMon + "-" + pDay);

            return processDate;
        }

        private void btnDAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in dgvFrm.Rows)
            {
                List<string> cells = new List<string>();

                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    cells.Add(dgvc.Value.ToString());
                }

                dgvTo.Rows.Add(cells.ToArray());
            }

            dgvFrm.Rows.Clear();
            dgvTo.ClearSelection();

            btnDAll.Enabled = false;
            btnSAll.Enabled = true;
            btnD.Enabled = false;
            btnS.Enabled = true;
        }

        private void btnSAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in dgvTo.Rows)
            {
                List<string> cells = new List<string>();

                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    cells.Add(dgvc.Value.ToString());
                }

                dgvFrm.Rows.Add(cells.ToArray());
            }

            dgvTo.Rows.Clear();
            dgvFrm.ClearSelection();

            btnDAll.Enabled = true;
            btnSAll.Enabled = false;
            btnD.Enabled = true;
            btnS.Enabled = false;
        }

        private void btnD_Click(object sender, EventArgs e)
        {                        
            foreach (DataGridViewRow row in dgvFrm.SelectedRows)
            {
                object[] items = new object[row.Cells.Count];
                for (int i = 0; i < row.Cells.Count; i++)
                    items[i] = row.Cells[i].Value;
                dgvTo.Rows.Add(items);
                dgvFrm.Rows.Remove(row);
            }

            if (dgvFrm.Rows.Count == 0)
            {
                btnD.Enabled = false;
                btnDAll.Enabled = false;
            }

            if (dgvTo.Rows.Count > 0)
            {
                btnS.Enabled = true;
                btnSAll.Enabled = true;
            }
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvTo.SelectedRows)
            {
                object[] items = new object[row.Cells.Count];
                for (int i = 0; i < row.Cells.Count; i++)
                    items[i] = row.Cells[i].Value;
                dgvFrm.Rows.Add(items);
                dgvTo.Rows.Remove(row);
            }

            if (dgvTo.Rows.Count == 0)
            {
                btnS.Enabled = false;
                btnSAll.Enabled = false;
            }

            if (dgvFrm.Rows.Count > 0)
            {
                btnD.Enabled = true;
                btnDAll.Enabled = true;
            }
        }

        private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag)
                FillDGV();
        }

        private void cmbCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag)
                FillDGV();
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag)
                FillDGV();
        }

        private void cmbMonth_Click(object sender, EventArgs e)
        {
            dbConP.errPvdr.Dispose();
        }

        private void chkYear_CheckedChanged(object sender, EventArgs e)
        {            
            if (chkYear.Checked)
            {
                dtpYear.Enabled = true;
                chkDateRange.Checked = false;
                dtpYear.CustomFormat = "yyyy";
                dtpFormDate.Enabled = false;
                dtpToDate.Enabled = false;

                dtpFormDate.Value = DateTime.Parse("01-Jan-" + dtpYear.Value.Year);
                dtpToDate.Value = DateTime.Parse("31-Dec-" + dtpYear.Value.Year);
            }
            else
            {
                dtpYear.Enabled = false;
                dtpYear.CustomFormat = " ";
            }

            dgvTo.Rows.Clear();
            FillDGV();
        }

        private void dtpYear_ValueChanged(object sender, EventArgs e)
        {
            FillDGV();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            chkYear.Checked = false;
            cbCC.Checked = true;
            cbCompany.Checked = true;
            cbGroup.Checked = true;
            dgvTo.Rows.Clear();
            FillDGV();
            Cursor.Current = Cursors.Default;
        }

        private void dtpYear_ValueChanged_1(object sender, EventArgs e)
        {
            FillDGV();
        }

        private void frmDepreciationProcess_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnProcess_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnProcess, "Process");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "Reset");
        }

        private void chkDateRange_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDateRange.Checked)
            {
                dtpFormDate.Enabled = true;
                dtpToDate.Enabled = true;
                chkYear.Checked = false;
            }
            else
            {
                dtpFormDate.Enabled = false;
                dtpToDate.Enabled = false;
            }
        }

        private void dtpFormDate_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
