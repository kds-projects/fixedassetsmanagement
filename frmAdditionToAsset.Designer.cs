﻿namespace AssetRegister
{
    partial class frmAdditionToAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgvListData = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCross = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.dgvSupplier = new System.Windows.Forms.DataGridView();
            this.cmbEntryType = new AssetRegister.FlatComboBox();
            this.txtAddItem = new AssetRegister.FlatTextBox();
            this.txtAddAmt = new AssetRegister.FlatTextBox();
            this.rbOthers = new System.Windows.Forms.RadioButton();
            this.rbLand = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btnRfrshSupplier = new System.Windows.Forms.Button();
            this.cmbCurrencyType = new AssetRegister.FlatComboBox();
            this.txtConvRate = new AssetRegister.FlatTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtInvCostUsd = new AssetRegister.FlatTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtComments = new AssetRegister.FlatTextBox();
            this.txtVoucherNo = new AssetRegister.FlatTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpAssetRecDate = new AssetRegister.FlatDateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtCountry = new AssetRegister.FlatTextBox();
            this.txtSupplier = new AssetRegister.FlatTextBox();
            this.txtLCNo = new AssetRegister.FlatTextBox();
            this.txtBrand = new AssetRegister.FlatTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtModelNo = new AssetRegister.FlatTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtDepRate = new AssetRegister.FlatTextBox();
            this.txtAssetLife = new AssetRegister.FlatTextBox();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            this.dtpAddDate = new AssetRegister.FlatDateTimePicker();
            this.dtpYear = new AssetRegister.FlatDateTimePicker();
            this.txtAddDesc = new AssetRegister.FlatTextBox();
            this.dtpMonth = new AssetRegister.FlatDateTimePicker();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtAccCode = new AssetRegister.FlatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplier)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(177, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 164;
            this.label3.Text = ":";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 163;
            this.label1.Text = "Asset Code";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 162;
            this.label9.Text = "Description";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(177, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 13);
            this.label10.TabIndex = 161;
            this.label10.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 160;
            this.label7.Text = "Addition Item";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 158;
            this.label5.Text = "Dep. Effect From";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(177, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 159;
            this.label8.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 157;
            this.label6.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 155;
            this.label2.Text = "Total Cost";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(177, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 13);
            this.label4.TabIndex = 156;
            this.label4.Text = ":";
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(423, 494);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 498);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(423, 39);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(387, 499);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 19;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::AssetRegister.Properties.Resources.Actions_stock_save_as_icon;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(313, 499);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(38, 38);
            this.btnSave.TabIndex = 18;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // dgvListData
            // 
            this.dgvListData.AllowUserToAddRows = false;
            this.dgvListData.AllowUserToDeleteRows = false;
            this.dgvListData.AllowUserToResizeColumns = false;
            this.dgvListData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvListData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListData.BackgroundColor = System.Drawing.Color.White;
            this.dgvListData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvListData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvListData.EnableHeadersVisualStyles = false;
            this.dgvListData.Location = new System.Drawing.Point(276, 2);
            this.dgvListData.MultiSelect = false;
            this.dgvListData.Name = "dgvListData";
            this.dgvListData.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvListData.RowHeadersVisible = false;
            this.dgvListData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListData.Size = new System.Drawing.Size(150, 495);
            this.dgvListData.TabIndex = 345;
            this.dgvListData.Visible = false;
            this.dgvListData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListData_CellDoubleClick);
            this.dgvListData.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvListData_DataError);
            this.dgvListData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvListData_KeyDown);
            this.dgvListData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvListData_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 347;
            this.label11.Text = "Purchase Date";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(177, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(12, 13);
            this.label12.TabIndex = 348;
            this.label12.Text = ":";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(177, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 161;
            this.label13.Text = ":";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(20, 191);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 162;
            this.label14.Text = "Dep. Life";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(342, 169);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(59, 13);
            this.label63.TabIndex = 404;
            this.label63.Text = " Per Year";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(177, 169);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(12, 13);
            this.label15.TabIndex = 161;
            this.label15.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(20, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 13);
            this.label16.TabIndex = 162;
            this.label16.Text = "Dep. Rate";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Controls.Add(this.label66);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(428, 38);
            this.panel1.TabIndex = 405;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(395, 4);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 431;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(7, 11);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(345, 16);
            this.label66.TabIndex = 430;
            this.label66.Text = "Fixed Assets - Addition Entry to Existing Asset";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.dgvSupplier);
            this.panel2.Controls.Add(this.dgvListData);
            this.panel2.Controls.Add(this.label71);
            this.panel2.Controls.Add(this.label72);
            this.panel2.Controls.Add(this.txtAccCode);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.btnReset);
            this.panel2.Controls.Add(this.cmbEntryType);
            this.panel2.Controls.Add(this.txtAddItem);
            this.panel2.Controls.Add(this.txtAddAmt);
            this.panel2.Controls.Add(this.rbOthers);
            this.panel2.Controls.Add(this.rbLand);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.btnRfrshSupplier);
            this.panel2.Controls.Add(this.cmbCurrencyType);
            this.panel2.Controls.Add(this.txtConvRate);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtInvCostUsd);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.txtComments);
            this.panel2.Controls.Add(this.txtVoucherNo);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.dtpAssetRecDate);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label68);
            this.panel2.Controls.Add(this.txtCountry);
            this.panel2.Controls.Add(this.txtSupplier);
            this.panel2.Controls.Add(this.txtLCNo);
            this.panel2.Controls.Add(this.txtBrand);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.txtModelNo);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.label56);
            this.panel2.Controls.Add(this.label49);
            this.panel2.Controls.Add(this.label41);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.label53);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label63);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtDepRate);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtAssetLife);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtAssetCode);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.dtpAddDate);
            this.panel2.Controls.Add(this.dtpYear);
            this.panel2.Controls.Add(this.txtAddDesc);
            this.panel2.Controls.Add(this.dtpMonth);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(5, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(428, 540);
            this.panel2.TabIndex = 406;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
            this.btnUpdate.Location = new System.Drawing.Point(350, 499);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(38, 38);
            this.btnUpdate.TabIndex = 408;
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::AssetRegister.Properties.Resources.Action_file_new_icon1;
            this.btnReset.Location = new System.Drawing.Point(276, 499);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(38, 38);
            this.btnReset.TabIndex = 407;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // dgvSupplier
            // 
            this.dgvSupplier.AllowUserToAddRows = false;
            this.dgvSupplier.AllowUserToDeleteRows = false;
            this.dgvSupplier.AllowUserToResizeColumns = false;
            this.dgvSupplier.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvSupplier.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvSupplier.BackgroundColor = System.Drawing.Color.White;
            this.dgvSupplier.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvSupplier.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSupplier.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvSupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSupplier.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvSupplier.EnableHeadersVisualStyles = false;
            this.dgvSupplier.Location = new System.Drawing.Point(215, 2);
            this.dgvSupplier.MultiSelect = false;
            this.dgvSupplier.Name = "dgvSupplier";
            this.dgvSupplier.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSupplier.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvSupplier.RowHeadersVisible = false;
            this.dgvSupplier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSupplier.Size = new System.Drawing.Size(211, 495);
            this.dgvSupplier.TabIndex = 448;
            this.dgvSupplier.Visible = false;
            this.dgvSupplier.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSupplier_CellDoubleClick);
            this.dgvSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSupplier_KeyDown);
            this.dgvSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvSupplier_KeyPress);
            // 
            // cmbEntryType
            // 
            this.cmbEntryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEntryType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEntryType.FormattingEnabled = true;
            this.cmbEntryType.Items.AddRange(new object[] {
            "LAND DEED VALUE",
            "LAND REGISTRATION",
            "LAND DEVELOPMENT",
            "LAND REVALUATION",
            "LAND OTHERS"});
            this.cmbEntryType.Location = new System.Drawing.Point(193, 55);
            this.cmbEntryType.Name = "cmbEntryType";
            this.cmbEntryType.Size = new System.Drawing.Size(209, 21);
            this.cmbEntryType.TabIndex = 453;
            this.cmbEntryType.Visible = false;
            // 
            // txtAddItem
            // 
            this.txtAddItem.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAddItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddItem.Location = new System.Drawing.Point(193, 55);
            this.txtAddItem.Name = "txtAddItem";
            this.txtAddItem.Size = new System.Drawing.Size(209, 21);
            this.txtAddItem.TabIndex = 2;
            this.txtAddItem.TextChanged += new System.EventHandler(this.txtAddItem_TextChanged);
            this.txtAddItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddItem_KeyPress);
            // 
            // txtAddAmt
            // 
            this.txtAddAmt.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAddAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddAmt.Location = new System.Drawing.Point(193, 77);
            this.txtAddAmt.Name = "txtAddAmt";
            this.txtAddAmt.Size = new System.Drawing.Size(209, 21);
            this.txtAddAmt.TabIndex = 3;
            this.txtAddAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAddAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddAmt_KeyPress);
            // 
            // rbOthers
            // 
            this.rbOthers.AutoSize = true;
            this.rbOthers.Location = new System.Drawing.Point(276, 11);
            this.rbOthers.Name = "rbOthers";
            this.rbOthers.Size = new System.Drawing.Size(63, 17);
            this.rbOthers.TabIndex = 452;
            this.rbOthers.TabStop = true;
            this.rbOthers.Text = "Others";
            this.rbOthers.UseVisualStyleBackColor = true;
            this.rbOthers.CheckedChanged += new System.EventHandler(this.rbOthers_CheckedChanged);
            // 
            // rbLand
            // 
            this.rbLand.AutoSize = true;
            this.rbLand.Location = new System.Drawing.Point(193, 11);
            this.rbLand.Name = "rbLand";
            this.rbLand.Size = new System.Drawing.Size(52, 17);
            this.rbLand.TabIndex = 452;
            this.rbLand.TabStop = true;
            this.rbLand.Text = "Land";
            this.rbLand.UseVisualStyleBackColor = true;
            this.rbLand.CheckedChanged += new System.EventHandler(this.rbLand_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(20, 13);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 450;
            this.label23.Text = "Asset Type";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(177, 13);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 451;
            this.label24.Text = ":";
            // 
            // btnRfrshSupplier
            // 
            this.btnRfrshSupplier.Image = global::AssetRegister.Properties.Resources.Refresh_2_icon;
            this.btnRfrshSupplier.Location = new System.Drawing.Point(147, 353);
            this.btnRfrshSupplier.Name = "btnRfrshSupplier";
            this.btnRfrshSupplier.Size = new System.Drawing.Size(30, 21);
            this.btnRfrshSupplier.TabIndex = 449;
            this.btnRfrshSupplier.UseVisualStyleBackColor = true;
            this.btnRfrshSupplier.Click += new System.EventHandler(this.btnRfrshSupplier_Click);
            // 
            // cmbCurrencyType
            // 
            this.cmbCurrencyType.FormattingEnabled = true;
            this.cmbCurrencyType.Items.AddRange(new object[] {
            "USD",
            "EUR",
            "GBP",
            "JPY",
            "CHF"});
            this.cmbCurrencyType.Location = new System.Drawing.Point(129, 99);
            this.cmbCurrencyType.Name = "cmbCurrencyType";
            this.cmbCurrencyType.Size = new System.Drawing.Size(48, 21);
            this.cmbCurrencyType.TabIndex = 443;
            // 
            // txtConvRate
            // 
            this.txtConvRate.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtConvRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConvRate.Location = new System.Drawing.Point(332, 99);
            this.txtConvRate.Name = "txtConvRate";
            this.txtConvRate.Size = new System.Drawing.Size(70, 21);
            this.txtConvRate.TabIndex = 445;
            this.txtConvRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtConvRate.Leave += new System.EventHandler(this.txtConvRate_Leave);
            this.txtConvRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConvRate_KeyPress);
            this.txtConvRate.Enter += new System.EventHandler(this.txtConvRate_Enter);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(20, 103);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(108, 13);
            this.label22.TabIndex = 446;
            this.label22.Text = "Invoice Cost (FC)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInvCostUsd
            // 
            this.txtInvCostUsd.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtInvCostUsd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvCostUsd.Location = new System.Drawing.Point(193, 99);
            this.txtInvCostUsd.Name = "txtInvCostUsd";
            this.txtInvCostUsd.Size = new System.Drawing.Size(138, 21);
            this.txtInvCostUsd.TabIndex = 4;
            this.txtInvCostUsd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInvCostUsd.Leave += new System.EventHandler(this.txtInvCostUsd_Leave);
            this.txtInvCostUsd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInvCostUsd_KeyPress);
            this.txtInvCostUsd.Enter += new System.EventHandler(this.txtInvCostUsd_Enter);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(177, 103);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 13);
            this.label30.TabIndex = 447;
            this.label30.Text = ":";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(177, 434);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 13);
            this.label18.TabIndex = 440;
            this.label18.Text = ":";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 401);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 13);
            this.label19.TabIndex = 441;
            this.label19.Text = "Voucher No.";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(20, 434);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 439;
            this.label20.Text = "Comments";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(177, 401);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(12, 13);
            this.label21.TabIndex = 442;
            this.label21.Text = ":";
            // 
            // txtComments
            // 
            this.txtComments.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComments.Location = new System.Drawing.Point(193, 419);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(209, 43);
            this.txtComments.TabIndex = 17;
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtVoucherNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVoucherNo.Location = new System.Drawing.Point(193, 397);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(209, 21);
            this.txtVoucherNo.TabIndex = 16;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(177, 379);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(12, 13);
            this.label17.TabIndex = 407;
            this.label17.Text = ":";
            // 
            // dtpAssetRecDate
            // 
            this.dtpAssetRecDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpAssetRecDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAssetRecDate.Location = new System.Drawing.Point(193, 375);
            this.dtpAssetRecDate.Name = "dtpAssetRecDate";
            this.dtpAssetRecDate.Size = new System.Drawing.Size(209, 21);
            this.dtpAssetRecDate.TabIndex = 15;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(20, 269);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(106, 13);
            this.label35.TabIndex = 428;
            this.label35.Text = "Country of Origin";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(20, 379);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(98, 13);
            this.label68.TabIndex = 436;
            this.label68.Text = "Asset Rec. Date";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCountry
            // 
            this.txtCountry.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.Location = new System.Drawing.Point(193, 265);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(209, 21);
            this.txtCountry.TabIndex = 10;
            this.txtCountry.TextChanged += new System.EventHandler(this.txtCountry_TextChanged);
            // 
            // txtSupplier
            // 
            this.txtSupplier.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtSupplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSupplier.Location = new System.Drawing.Point(193, 353);
            this.txtSupplier.Name = "txtSupplier";
            this.txtSupplier.Size = new System.Drawing.Size(209, 21);
            this.txtSupplier.TabIndex = 14;
            this.txtSupplier.TextChanged += new System.EventHandler(this.txtSupplier_TextChanged);
            this.txtSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupplier_KeyDown);
            this.txtSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSupplier_KeyPress);
            // 
            // txtLCNo
            // 
            this.txtLCNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtLCNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLCNo.Location = new System.Drawing.Point(193, 287);
            this.txtLCNo.Name = "txtLCNo";
            this.txtLCNo.Size = new System.Drawing.Size(209, 21);
            this.txtLCNo.TabIndex = 11;
            // 
            // txtBrand
            // 
            this.txtBrand.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtBrand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBrand.Location = new System.Drawing.Point(193, 309);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(209, 21);
            this.txtBrand.TabIndex = 12;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(177, 269);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(12, 13);
            this.label36.TabIndex = 429;
            this.label36.Text = ":";
            // 
            // txtModelNo
            // 
            this.txtModelNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtModelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtModelNo.Location = new System.Drawing.Point(193, 331);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(209, 21);
            this.txtModelNo.TabIndex = 13;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(20, 313);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 13);
            this.label50.TabIndex = 430;
            this.label50.Text = "Brand";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(177, 357);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(12, 13);
            this.label56.TabIndex = 427;
            this.label56.Text = ":";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(177, 313);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(12, 13);
            this.label49.TabIndex = 431;
            this.label49.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(177, 291);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(12, 13);
            this.label41.TabIndex = 435;
            this.label41.Text = ":";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(20, 335);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(63, 13);
            this.label52.TabIndex = 432;
            this.label52.Text = "Model No.";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(20, 357);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(54, 13);
            this.label53.TabIndex = 426;
            this.label53.Text = "Supplier";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(177, 335);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(12, 13);
            this.label51.TabIndex = 433;
            this.label51.Text = ":";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(20, 291);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(45, 13);
            this.label39.TabIndex = 434;
            this.label39.Text = "LC No.";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDepRate
            // 
            this.txtDepRate.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDepRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDepRate.Location = new System.Drawing.Point(193, 165);
            this.txtDepRate.Name = "txtDepRate";
            this.txtDepRate.Size = new System.Drawing.Size(144, 21);
            this.txtDepRate.TabIndex = 7;
            this.txtDepRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDepRate.TextChanged += new System.EventHandler(this.txtDepRate_TextChanged);
            this.txtDepRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDepRate_KeyPress);
            // 
            // txtAssetLife
            // 
            this.txtAssetLife.BackColor = System.Drawing.Color.White;
            this.txtAssetLife.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetLife.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetLife.Location = new System.Drawing.Point(193, 187);
            this.txtAssetLife.Name = "txtAssetLife";
            this.txtAssetLife.ReadOnly = true;
            this.txtAssetLife.Size = new System.Drawing.Size(209, 21);
            this.txtAssetLife.TabIndex = 8;
            this.txtAssetLife.Text = "0.00";
            this.txtAssetLife.Leave += new System.EventHandler(this.txtAssetLife_Leave);
            this.txtAssetLife.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetLife_KeyPress);
            this.txtAssetLife.Enter += new System.EventHandler(this.txtAssetLife_Enter);
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Location = new System.Drawing.Point(193, 33);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.Size = new System.Drawing.Size(209, 21);
            this.txtAssetCode.TabIndex = 1;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            // 
            // dtpAddDate
            // 
            this.dtpAddDate.CustomFormat = "dd-MMM-yyyy dddd";
            this.dtpAddDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAddDate.Location = new System.Drawing.Point(193, 121);
            this.dtpAddDate.Name = "dtpAddDate";
            this.dtpAddDate.Size = new System.Drawing.Size(209, 21);
            this.dtpAddDate.TabIndex = 5;
            // 
            // dtpYear
            // 
            this.dtpYear.CustomFormat = "yyyy";
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Location = new System.Drawing.Point(309, 143);
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowUpDown = true;
            this.dtpYear.Size = new System.Drawing.Size(93, 21);
            this.dtpYear.TabIndex = 6;
            // 
            // txtAddDesc
            // 
            this.txtAddDesc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAddDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddDesc.Location = new System.Drawing.Point(193, 209);
            this.txtAddDesc.Multiline = true;
            this.txtAddDesc.Name = "txtAddDesc";
            this.txtAddDesc.Size = new System.Drawing.Size(209, 55);
            this.txtAddDesc.TabIndex = 9;
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MMMM";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(193, 143);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Size = new System.Drawing.Size(102, 21);
            this.dtpMonth.TabIndex = 6;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(428, 540);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(20, 467);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(63, 13);
            this.label71.TabIndex = 455;
            this.label71.Text = "A/C Code";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(177, 467);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(12, 13);
            this.label72.TabIndex = 456;
            this.label72.Text = ":";
            // 
            // txtAccCode
            // 
            this.txtAccCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAccCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccCode.Location = new System.Drawing.Point(193, 463);
            this.txtAccCode.Name = "txtAccCode";
            this.txtAccCode.Size = new System.Drawing.Size(209, 21);
            this.txtAccCode.TabIndex = 454;
            // 
            // frmAdditionToAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(437, 588);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAdditionToAsset";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Other Addition to Asset ";
            this.Load += new System.EventHandler(this.frmAdditionToAsset_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmAdditionToAsset_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private FlatTextBox txtAssetCode;
        private FlatTextBox txtAddItem;
        private FlatTextBox txtAddAmt;
        private FlatTextBox txtAddDesc;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private System.Windows.Forms.DataGridView dgvListData;
        private AssetRegister.FlatDateTimePicker dtpYear;
        private AssetRegister.FlatDateTimePicker dtpMonth;
        private AssetRegister.FlatDateTimePicker dtpAddDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private FlatTextBox txtAssetLife;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label63;
        private FlatTextBox txtDepRate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Label label66;
        private AssetRegister.FlatDateTimePicker dtpAssetRecDate;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label68;
        private FlatTextBox txtCountry;
        private FlatTextBox txtSupplier;
        private FlatTextBox txtLCNo;
        private FlatTextBox txtBrand;
        private System.Windows.Forms.Label label36;
        private FlatTextBox txtModelNo;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private FlatTextBox txtComments;
        private FlatTextBox txtVoucherNo;
        private AssetRegister.FlatComboBox cmbCurrencyType;
        private FlatTextBox txtConvRate;
        private System.Windows.Forms.Label label22;
        private FlatTextBox txtInvCostUsd;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridView dgvSupplier;
        private System.Windows.Forms.Button btnRfrshSupplier;
        private System.Windows.Forms.RadioButton rbOthers;
        private System.Windows.Forms.RadioButton rbLand;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private AssetRegister.FlatComboBox cmbEntryType;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private FlatTextBox txtAccCode;
    }
}