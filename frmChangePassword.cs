﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmChangePassword : Form
    {
        private clsCommonFunctions dbConCP = new clsCommonFunctions();
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmChangePassword()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!dbConCP.CheckEmptyOrNull(txtNPassword, "Enter New Password") &&
                !dbConCP.CheckEmptyOrNull(txtCNPassword, "Enter Confirm Password"))
            {
                if (dbConCP.SingleQuoteHandler(txtNPassword) == dbConCP.SingleQuoteHandler(txtCNPassword))
                {
                    string[] cols = { "[UserPassword]" };
                    string[] vals = { dbConCP.SingleQuoteHandler(txtNPassword) };
                    string[] whCols = { "[UserId]" };
                    string[] whVals = { frmLogin.userId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_UserInfo]", this.Name, 
                                       frmLogin.userId.ToString(), "[UserPassword]", getPrevPass() };

                    string sql = dbConCP.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UserInfo]", cols, vals, "UPDATE", whCols, whVals);
                    sql += dbConCP.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConCP.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Password updated successfully.", "Update",
                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else 
                    {
                        MessageBox.Show("Failed to update new password.", "Failed",
                                              MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Confirm password is not equal to new password.", "Password Unmatched", 
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtNPassword.Focus();
                }
            }
        }

        private string getPrevPass() 
        {
            SqlDataReader sdr;
            string PrevPass = "";

            sdr = (SqlDataReader)dbConCP.DBCRUDFunction(@"SELECT UserPassword FROM AR_UserInfo WHERE UserId = " + frmLogin.userId, true);

            if(sdr.Read())
                PrevPass = sdr["UserPassword"].ToString();

            sdr.Close();

            return PrevPass;
        }

        private void txtNPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConCP.DisposeChecking(txtNPassword);
        }

        private void txtCNPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConCP.DisposeChecking(txtCNPassword);
        }

        private void frmChangePassword_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnUpdate, "Update");
        }
    }
}
