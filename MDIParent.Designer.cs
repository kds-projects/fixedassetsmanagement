﻿namespace AssetRegister
{
    partial class MDIParent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuCreateUser = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuUserPrivilege = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.assetLocationEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetSubLocationEntryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierEntryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuChangePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.assetConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.landToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.additionToAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.soldOrDamageEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAdjustCost = new System.Windows.Forms.ToolStripMenuItem();
            this.assetRevaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmLandAdjustEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.depreciationConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processDeprecitationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.AcInfoEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.assetListReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAssetReigisterReport = new System.Windows.Forms.ToolStripMenuItem();
            this.fixedAssetsStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDepNCost = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.depreciationJournalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDepJournalSummaryRpt = new System.Windows.Forms.ToolStripMenuItem();
            this.depreciationJournalChargeAmountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMothwiseDepCharge = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMothwiseCostAddition = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMothwiseCostDisposal = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.additionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDisposalOfFARegister = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.assetFullProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.landDetailsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CAPEXInfoReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depreciationExpenseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetAdditionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetListingManualReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetTransferReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetHistoricalRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miscelleneousReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.costcentrewiseStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountLedgerFARReconciliationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.tsbAcusition = new System.Windows.Forms.ToolStripButton();
            this.tsbTransfer = new System.Windows.Forms.ToolStripButton();
            this.tsbDisposal = new System.Windows.Forms.ToolStripButton();
            this.tsbAdjustment = new System.Windows.Forms.ToolStripButton();
            this.tsbDepreciation = new System.Windows.Forms.ToolStripButton();
            this.tsbReports = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tslblUserFullName = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslblCurrentDateTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbLoginTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.systimer = new System.Windows.Forms.Timer(this.components);
            this.toolStripSub = new System.Windows.Forms.ToolStrip();
            this.tslAssetInfo = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.tslLandInfo = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.tslAdditionInfo = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.tslAdjustmentInfo = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.tslRevalutionInfo = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.tslDepSetiings = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.tslDepProcess = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tslRptLandRegister = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tslRptAssetRegister = new System.Windows.Forms.ToolStripLabel();
            this.tsmMonthWiseAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.toolStripMain.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStripSub.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.configMenu,
            this.reportsMenu,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(737, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuCreateUser,
            this.toolStripMenuUserPrivilege,
            this.toolStripSeparator3,
            this.toolStripMenuItem2,
            this.toolStripMenuChangePassword,
            this.toolStripSeparator5,
            this.toolStripMenuItem1,
            this.toolStripSeparator4,
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // toolStripMenuCreateUser
            // 
            this.toolStripMenuCreateUser.Enabled = false;
            this.toolStripMenuCreateUser.Name = "toolStripMenuCreateUser";
            this.toolStripMenuCreateUser.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuCreateUser.Text = "Create User";
            this.toolStripMenuCreateUser.Click += new System.EventHandler(this.toolStripMenuCreateUser_Click);
            // 
            // toolStripMenuUserPrivilege
            // 
            this.toolStripMenuUserPrivilege.Enabled = false;
            this.toolStripMenuUserPrivilege.Name = "toolStripMenuUserPrivilege";
            this.toolStripMenuUserPrivilege.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuUserPrivilege.Text = "User Privilege";
            this.toolStripMenuUserPrivilege.Click += new System.EventHandler(this.toolStripMenuUserPrivilege_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(165, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assetLocationEntryToolStripMenuItem,
            this.assetSubLocationEntryToolStripMenuItem1,
            this.supplierEntryToolStripMenuItem1});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuItem2.Text = "Master Settings";
            // 
            // assetLocationEntryToolStripMenuItem
            // 
            this.assetLocationEntryToolStripMenuItem.Enabled = false;
            this.assetLocationEntryToolStripMenuItem.Name = "assetLocationEntryToolStripMenuItem";
            this.assetLocationEntryToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.assetLocationEntryToolStripMenuItem.Text = "Asset Location Entry";
            this.assetLocationEntryToolStripMenuItem.Click += new System.EventHandler(this.assetLocationEntryToolStripMenuItem1_Click);
            // 
            // assetSubLocationEntryToolStripMenuItem1
            // 
            this.assetSubLocationEntryToolStripMenuItem1.Enabled = false;
            this.assetSubLocationEntryToolStripMenuItem1.Name = "assetSubLocationEntryToolStripMenuItem1";
            this.assetSubLocationEntryToolStripMenuItem1.Size = new System.Drawing.Size(204, 22);
            this.assetSubLocationEntryToolStripMenuItem1.Text = "Asset Sub Location Entry";
            this.assetSubLocationEntryToolStripMenuItem1.Click += new System.EventHandler(this.assetSubLocationEntryToolStripMenuItem1_Click);
            // 
            // supplierEntryToolStripMenuItem1
            // 
            this.supplierEntryToolStripMenuItem1.Enabled = false;
            this.supplierEntryToolStripMenuItem1.Name = "supplierEntryToolStripMenuItem1";
            this.supplierEntryToolStripMenuItem1.Size = new System.Drawing.Size(204, 22);
            this.supplierEntryToolStripMenuItem1.Text = "Supplier Entry";
            this.supplierEntryToolStripMenuItem1.Click += new System.EventHandler(this.supplierEntryToolStripMenuItem1_Click);
            // 
            // toolStripMenuChangePassword
            // 
            this.toolStripMenuChangePassword.Name = "toolStripMenuChangePassword";
            this.toolStripMenuChangePassword.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuChangePassword.Text = "Change Password";
            this.toolStripMenuChangePassword.Click += new System.EventHandler(this.toolStripMenuChangePassword_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(165, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuItem1.Text = "Log Off";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(165, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarToolStripMenuItem,
            this.toolStripToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.statusBarToolStripMenuItem.Text = "&Status Bar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.StatusBarToolStripMenuItem_Click);
            // 
            // toolStripToolStripMenuItem
            // 
            this.toolStripToolStripMenuItem.Checked = true;
            this.toolStripToolStripMenuItem.CheckOnClick = true;
            this.toolStripToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripToolStripMenuItem.Name = "toolStripToolStripMenuItem";
            this.toolStripToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.toolStripToolStripMenuItem.Text = "ToolStrip";
            this.toolStripToolStripMenuItem.Click += new System.EventHandler(this.toolStripToolStripMenuItem_Click);
            // 
            // configMenu
            // 
            this.configMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assetConfigurationToolStripMenuItem,
            this.landToolStripMenu,
            this.toolStripSeparator6,
            this.additionToAssetToolStripMenuItem,
            this.assetTransferToolStripMenuItem,
            this.soldOrDamageEntryToolStripMenuItem,
            this.tsmAdjustCost,
            this.assetRevaluationToolStripMenuItem,
            this.tsmLandAdjustEntry,
            this.toolStripSeparator7,
            this.depreciationConfigurationToolStripMenuItem,
            this.processDeprecitationToolStripMenuItem,
            this.toolStripSeparator9,
            this.AcInfoEntryToolStripMenuItem});
            this.configMenu.Name = "configMenu";
            this.configMenu.Size = new System.Drawing.Size(54, 20);
            this.configMenu.Text = "&Entries";
            // 
            // assetConfigurationToolStripMenuItem
            // 
            this.assetConfigurationToolStripMenuItem.Enabled = false;
            this.assetConfigurationToolStripMenuItem.Name = "assetConfigurationToolStripMenuItem";
            this.assetConfigurationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.assetConfigurationToolStripMenuItem.Text = "Asset Info. Entry";
            this.assetConfigurationToolStripMenuItem.Click += new System.EventHandler(this.assetConfigurationToolStripMenuItem_Click);
            // 
            // landToolStripMenu
            // 
            this.landToolStripMenu.Enabled = false;
            this.landToolStripMenu.Name = "landToolStripMenu";
            this.landToolStripMenu.Size = new System.Drawing.Size(197, 22);
            this.landToolStripMenu.Text = "Asset Info. Entry (Land)";
            this.landToolStripMenu.Click += new System.EventHandler(this.landToolStripMenu_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(194, 6);
            // 
            // additionToAssetToolStripMenuItem
            // 
            this.additionToAssetToolStripMenuItem.Enabled = false;
            this.additionToAssetToolStripMenuItem.Name = "additionToAssetToolStripMenuItem";
            this.additionToAssetToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.additionToAssetToolStripMenuItem.Text = "Addition to Asset Entry";
            this.additionToAssetToolStripMenuItem.Click += new System.EventHandler(this.additionToAssetToolStripMenuItem_Click);
            // 
            // assetTransferToolStripMenuItem
            // 
            this.assetTransferToolStripMenuItem.Enabled = false;
            this.assetTransferToolStripMenuItem.Name = "assetTransferToolStripMenuItem";
            this.assetTransferToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.assetTransferToolStripMenuItem.Text = "Asset Transfer Entry";
            this.assetTransferToolStripMenuItem.Click += new System.EventHandler(this.assetTransferToolStripMenuItem_Click);
            // 
            // soldOrDamageEntryToolStripMenuItem
            // 
            this.soldOrDamageEntryToolStripMenuItem.Enabled = false;
            this.soldOrDamageEntryToolStripMenuItem.Name = "soldOrDamageEntryToolStripMenuItem";
            this.soldOrDamageEntryToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.soldOrDamageEntryToolStripMenuItem.Text = "Disposal Entry";
            this.soldOrDamageEntryToolStripMenuItem.Click += new System.EventHandler(this.soldOrDamageEntryToolStripMenuItem_Click);
            // 
            // tsmAdjustCost
            // 
            this.tsmAdjustCost.Enabled = false;
            this.tsmAdjustCost.Name = "tsmAdjustCost";
            this.tsmAdjustCost.Size = new System.Drawing.Size(197, 22);
            this.tsmAdjustCost.Text = "Adjustment Entry";
            this.tsmAdjustCost.Click += new System.EventHandler(this.tsmAdjustCost_Click);
            // 
            // assetRevaluationToolStripMenuItem
            // 
            this.assetRevaluationToolStripMenuItem.Enabled = false;
            this.assetRevaluationToolStripMenuItem.Name = "assetRevaluationToolStripMenuItem";
            this.assetRevaluationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.assetRevaluationToolStripMenuItem.Text = "Asset Revaluation Entry";
            this.assetRevaluationToolStripMenuItem.Click += new System.EventHandler(this.assetRevaluationToolStripMenuItem_Click);
            // 
            // tsmLandAdjustEntry
            // 
            this.tsmLandAdjustEntry.Enabled = false;
            this.tsmLandAdjustEntry.Name = "tsmLandAdjustEntry";
            this.tsmLandAdjustEntry.Size = new System.Drawing.Size(197, 22);
            this.tsmLandAdjustEntry.Text = "Land Adjustment Entry";
            this.tsmLandAdjustEntry.Visible = false;
            this.tsmLandAdjustEntry.Click += new System.EventHandler(this.tsmLandAdjustEntry_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(194, 6);
            // 
            // depreciationConfigurationToolStripMenuItem
            // 
            this.depreciationConfigurationToolStripMenuItem.Enabled = false;
            this.depreciationConfigurationToolStripMenuItem.Name = "depreciationConfigurationToolStripMenuItem";
            this.depreciationConfigurationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.depreciationConfigurationToolStripMenuItem.Text = "Depreciation Settings";
            this.depreciationConfigurationToolStripMenuItem.Click += new System.EventHandler(this.depreciationConfigurationToolStripMenuItem_Click);
            // 
            // processDeprecitationToolStripMenuItem
            // 
            this.processDeprecitationToolStripMenuItem.Enabled = false;
            this.processDeprecitationToolStripMenuItem.Name = "processDeprecitationToolStripMenuItem";
            this.processDeprecitationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.processDeprecitationToolStripMenuItem.Text = "Process Depreciation";
            this.processDeprecitationToolStripMenuItem.Click += new System.EventHandler(this.processDeprecitationToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(194, 6);
            // 
            // AcInfoEntryToolStripMenuItem
            // 
            this.AcInfoEntryToolStripMenuItem.Enabled = false;
            this.AcInfoEntryToolStripMenuItem.Name = "AcInfoEntryToolStripMenuItem";
            this.AcInfoEntryToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.AcInfoEntryToolStripMenuItem.Text = "A/C Info. Entry";
            this.AcInfoEntryToolStripMenuItem.Click += new System.EventHandler(this.AcInfoEntryToolStripMenuItem_Click);
            // 
            // reportsMenu
            // 
            this.reportsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assetListReportToolStripMenuItem,
            this.tsmAssetReigisterReport,
            this.fixedAssetsStatementToolStripMenuItem,
            this.tsmDepNCost,
            this.toolStripSeparator10,
            this.depreciationJournalToolStripMenuItem,
            this.tsmDepJournalSummaryRpt,
            this.depreciationJournalChargeAmountToolStripMenuItem,
            this.tsmMothwiseDepCharge,
            this.tsmMothwiseCostAddition,
            this.tsmMothwiseCostDisposal,
            this.tsmMonthWiseAll,
            this.toolStripSeparator11,
            this.additionReportToolStripMenuItem,
            this.tsmDisposalOfFARegister,
            this.toolStripSeparator12,
            this.assetFullProfileToolStripMenuItem,
            this.landDetailsReportToolStripMenuItem,
            this.CAPEXInfoReportToolStripMenuItem,
            this.depreciationExpenseReportToolStripMenuItem,
            this.assetAdditionReportToolStripMenuItem,
            this.assetListingManualReportToolStripMenuItem,
            this.assetTransferReportToolStripMenuItem,
            this.assetHistoricalRecordToolStripMenuItem,
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem,
            this.miscelleneousReportToolStripMenuItem,
            this.costcentrewiseStatementToolStripMenuItem,
            this.accountLedgerFARReconciliationToolStripMenuItem});
            this.reportsMenu.Name = "reportsMenu";
            this.reportsMenu.Size = new System.Drawing.Size(59, 20);
            this.reportsMenu.Text = "Reports";
            // 
            // assetListReportToolStripMenuItem
            // 
            this.assetListReportToolStripMenuItem.Enabled = false;
            this.assetListReportToolStripMenuItem.Name = "assetListReportToolStripMenuItem";
            this.assetListReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.assetListReportToolStripMenuItem.Text = "Land Registar Report";
            this.assetListReportToolStripMenuItem.Click += new System.EventHandler(this.assetListReportToolStripMenuItem_Click);
            // 
            // tsmAssetReigisterReport
            // 
            this.tsmAssetReigisterReport.Enabled = false;
            this.tsmAssetReigisterReport.Name = "tsmAssetReigisterReport";
            this.tsmAssetReigisterReport.Size = new System.Drawing.Size(335, 22);
            this.tsmAssetReigisterReport.Text = "Asset Register Report";
            this.tsmAssetReigisterReport.Click += new System.EventHandler(this.tsmAssetReigisterReport_Click);
            // 
            // fixedAssetsStatementToolStripMenuItem
            // 
            this.fixedAssetsStatementToolStripMenuItem.Enabled = false;
            this.fixedAssetsStatementToolStripMenuItem.Name = "fixedAssetsStatementToolStripMenuItem";
            this.fixedAssetsStatementToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.fixedAssetsStatementToolStripMenuItem.Text = "Fixed Assets Statement";
            this.fixedAssetsStatementToolStripMenuItem.Click += new System.EventHandler(this.fixedAssetsStatementToolStripMenuItem_Click);
            // 
            // tsmDepNCost
            // 
            this.tsmDepNCost.Enabled = false;
            this.tsmDepNCost.Name = "tsmDepNCost";
            this.tsmDepNCost.Size = new System.Drawing.Size(335, 22);
            this.tsmDepNCost.Text = "Fixed Assets - At Cost Less Acc. Depr. Report";
            this.tsmDepNCost.Click += new System.EventHandler(this.tsmDepNCost_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(332, 6);
            // 
            // depreciationJournalToolStripMenuItem
            // 
            this.depreciationJournalToolStripMenuItem.Enabled = false;
            this.depreciationJournalToolStripMenuItem.Name = "depreciationJournalToolStripMenuItem";
            this.depreciationJournalToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.depreciationJournalToolStripMenuItem.Text = "Depreciation Journal Report (A)";
            this.depreciationJournalToolStripMenuItem.Click += new System.EventHandler(this.depreciationJournalToolStripMenuItem_Click);
            // 
            // tsmDepJournalSummaryRpt
            // 
            this.tsmDepJournalSummaryRpt.Enabled = false;
            this.tsmDepJournalSummaryRpt.Name = "tsmDepJournalSummaryRpt";
            this.tsmDepJournalSummaryRpt.Size = new System.Drawing.Size(335, 22);
            this.tsmDepJournalSummaryRpt.Text = "Depreciation Journal Summary (B)";
            this.tsmDepJournalSummaryRpt.Click += new System.EventHandler(this.tsmDepJournalSummaryRpt_Click);
            // 
            // depreciationJournalChargeAmountToolStripMenuItem
            // 
            this.depreciationJournalChargeAmountToolStripMenuItem.Enabled = false;
            this.depreciationJournalChargeAmountToolStripMenuItem.Name = "depreciationJournalChargeAmountToolStripMenuItem";
            this.depreciationJournalChargeAmountToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.depreciationJournalChargeAmountToolStripMenuItem.Text = "Depreciation Journal (Charge Amount) Report (C)";
            this.depreciationJournalChargeAmountToolStripMenuItem.Click += new System.EventHandler(this.depreciationJournalChargeAmountToolStripMenuItem_Click);
            // 
            // tsmMothwiseDepCharge
            // 
            this.tsmMothwiseDepCharge.Enabled = false;
            this.tsmMothwiseDepCharge.Name = "tsmMothwiseDepCharge";
            this.tsmMothwiseDepCharge.Size = new System.Drawing.Size(335, 22);
            this.tsmMothwiseDepCharge.Text = "Mohthwise Depreciation Charged Report";
            this.tsmMothwiseDepCharge.Click += new System.EventHandler(this.tsmMothwiseDepCharge_Click);
            // 
            // tsmMothwiseCostAddition
            // 
            this.tsmMothwiseCostAddition.Enabled = false;
            this.tsmMothwiseCostAddition.Name = "tsmMothwiseCostAddition";
            this.tsmMothwiseCostAddition.Size = new System.Drawing.Size(335, 22);
            this.tsmMothwiseCostAddition.Text = "Mohthwise Cost Addition Report";
            this.tsmMothwiseCostAddition.Click += new System.EventHandler(this.tsmMothwiseCostAddition_Click);
            // 
            // tsmMothwiseCostDisposal
            // 
            this.tsmMothwiseCostDisposal.Enabled = false;
            this.tsmMothwiseCostDisposal.Name = "tsmMothwiseCostDisposal";
            this.tsmMothwiseCostDisposal.Size = new System.Drawing.Size(335, 22);
            this.tsmMothwiseCostDisposal.Text = "Mohthwise Cost Disposal Report";
            this.tsmMothwiseCostDisposal.Click += new System.EventHandler(this.tsmMothwiseCostDisposal_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(332, 6);
            // 
            // additionReportToolStripMenuItem
            // 
            this.additionReportToolStripMenuItem.Enabled = false;
            this.additionReportToolStripMenuItem.Name = "additionReportToolStripMenuItem";
            this.additionReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.additionReportToolStripMenuItem.Text = "Addition Report";
            this.additionReportToolStripMenuItem.Click += new System.EventHandler(this.additionReportToolStripMenuItem_Click);
            // 
            // tsmDisposalOfFARegister
            // 
            this.tsmDisposalOfFARegister.Enabled = false;
            this.tsmDisposalOfFARegister.Name = "tsmDisposalOfFARegister";
            this.tsmDisposalOfFARegister.Size = new System.Drawing.Size(335, 22);
            this.tsmDisposalOfFARegister.Text = "Disposal of Fixed Asset Register Report";
            this.tsmDisposalOfFARegister.Click += new System.EventHandler(this.tsmDisposalOfFARegister_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(332, 6);
            // 
            // assetFullProfileToolStripMenuItem
            // 
            this.assetFullProfileToolStripMenuItem.Enabled = false;
            this.assetFullProfileToolStripMenuItem.Name = "assetFullProfileToolStripMenuItem";
            this.assetFullProfileToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.assetFullProfileToolStripMenuItem.Text = "Asset Full Profile Report";
            this.assetFullProfileToolStripMenuItem.Click += new System.EventHandler(this.assetFullProfileToolStripMenuItem_Click);
            // 
            // landDetailsReportToolStripMenuItem
            // 
            this.landDetailsReportToolStripMenuItem.Enabled = false;
            this.landDetailsReportToolStripMenuItem.Name = "landDetailsReportToolStripMenuItem";
            this.landDetailsReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.landDetailsReportToolStripMenuItem.Text = "Land Details Report";
            this.landDetailsReportToolStripMenuItem.Click += new System.EventHandler(this.landDetailsReportToolStripMenuItem_Click);
            // 
            // CAPEXInfoReportToolStripMenuItem
            // 
            this.CAPEXInfoReportToolStripMenuItem.Enabled = false;
            this.CAPEXInfoReportToolStripMenuItem.Name = "CAPEXInfoReportToolStripMenuItem";
            this.CAPEXInfoReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.CAPEXInfoReportToolStripMenuItem.Text = "CAPEX Info Report";
            this.CAPEXInfoReportToolStripMenuItem.Visible = false;
            this.CAPEXInfoReportToolStripMenuItem.Click += new System.EventHandler(this.cAPEXInfoReportToolStripMenuItem_Click);
            // 
            // depreciationExpenseReportToolStripMenuItem
            // 
            this.depreciationExpenseReportToolStripMenuItem.Enabled = false;
            this.depreciationExpenseReportToolStripMenuItem.Name = "depreciationExpenseReportToolStripMenuItem";
            this.depreciationExpenseReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.depreciationExpenseReportToolStripMenuItem.Text = "Depreciation Expense Report";
            this.depreciationExpenseReportToolStripMenuItem.Visible = false;
            this.depreciationExpenseReportToolStripMenuItem.Click += new System.EventHandler(this.depreciationExpenseReportToolStripMenuItem_Click);
            // 
            // assetAdditionReportToolStripMenuItem
            // 
            this.assetAdditionReportToolStripMenuItem.Enabled = false;
            this.assetAdditionReportToolStripMenuItem.Name = "assetAdditionReportToolStripMenuItem";
            this.assetAdditionReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.assetAdditionReportToolStripMenuItem.Text = "Asset Addition Report (Specific)";
            this.assetAdditionReportToolStripMenuItem.Visible = false;
            this.assetAdditionReportToolStripMenuItem.Click += new System.EventHandler(this.assetAdditionReportToolStripMenuItem_Click);
            // 
            // assetListingManualReportToolStripMenuItem
            // 
            this.assetListingManualReportToolStripMenuItem.Enabled = false;
            this.assetListingManualReportToolStripMenuItem.Name = "assetListingManualReportToolStripMenuItem";
            this.assetListingManualReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.assetListingManualReportToolStripMenuItem.Text = "Asset Listing Manual Report ";
            this.assetListingManualReportToolStripMenuItem.Click += new System.EventHandler(this.assetListingManualReportToolStripMenuItem_Click);
            // 
            // assetTransferReportToolStripMenuItem
            // 
            this.assetTransferReportToolStripMenuItem.Enabled = false;
            this.assetTransferReportToolStripMenuItem.Name = "assetTransferReportToolStripMenuItem";
            this.assetTransferReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.assetTransferReportToolStripMenuItem.Text = "Asset Transfer Report";
            this.assetTransferReportToolStripMenuItem.Click += new System.EventHandler(this.assetTransferReportToolStripMenuItem_Click);
            // 
            // assetHistoricalRecordToolStripMenuItem
            // 
            this.assetHistoricalRecordToolStripMenuItem.Enabled = false;
            this.assetHistoricalRecordToolStripMenuItem.Name = "assetHistoricalRecordToolStripMenuItem";
            this.assetHistoricalRecordToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.assetHistoricalRecordToolStripMenuItem.Text = "Asset Historical Record";
            this.assetHistoricalRecordToolStripMenuItem.Click += new System.EventHandler(this.assetHistoricalRecordToolStripMenuItem_Click);
            // 
            // depreciationChagedOnRevaluedAssetToolStripMenuItem
            // 
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem.Enabled = false;
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem.Name = "depreciationChagedOnRevaluedAssetToolStripMenuItem";
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem.Text = "Depreciation Chaged on Revalued Asset";
            this.depreciationChagedOnRevaluedAssetToolStripMenuItem.Click += new System.EventHandler(this.depreciationChagedOnRevaluedAssetToolStripMenuItem_Click);
            // 
            // miscelleneousReportToolStripMenuItem
            // 
            this.miscelleneousReportToolStripMenuItem.Enabled = false;
            this.miscelleneousReportToolStripMenuItem.Name = "miscelleneousReportToolStripMenuItem";
            this.miscelleneousReportToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.miscelleneousReportToolStripMenuItem.Text = "Miscelleneous Report";
            this.miscelleneousReportToolStripMenuItem.Click += new System.EventHandler(this.miscelleneousReportToolStripMenuItem_Click);
            // 
            // costcentrewiseStatementToolStripMenuItem
            // 
            this.costcentrewiseStatementToolStripMenuItem.Enabled = false;
            this.costcentrewiseStatementToolStripMenuItem.Name = "costcentrewiseStatementToolStripMenuItem";
            this.costcentrewiseStatementToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.costcentrewiseStatementToolStripMenuItem.Text = "Costcentrewise Statement";
            this.costcentrewiseStatementToolStripMenuItem.Click += new System.EventHandler(this.costcentrewiseStatementToolStripMenuItem_Click);
            // 
            // accountLedgerFARReconciliationToolStripMenuItem
            // 
            this.accountLedgerFARReconciliationToolStripMenuItem.Enabled = false;
            this.accountLedgerFARReconciliationToolStripMenuItem.Name = "accountLedgerFARReconciliationToolStripMenuItem";
            this.accountLedgerFARReconciliationToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.accountLedgerFARReconciliationToolStripMenuItem.Text = "Account Ledger && FAR Reconciliation";
            this.accountLedgerFARReconciliationToolStripMenuItem.Click += new System.EventHandler(this.accountLedgerFARReconciliationToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            this.contentsToolStripMenuItem.Visible = false;
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
            this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            this.indexToolStripMenuItem.Visible = false;
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
            this.searchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            this.searchToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(165, 6);
            this.toolStripSeparator8.Visible = false;
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.aboutToolStripMenuItem.Text = "&About ... ...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripMain
            // 
            this.toolStripMain.AutoSize = false;
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAcusition,
            this.tsbTransfer,
            this.tsbDisposal,
            this.tsbAdjustment,
            this.tsbDepreciation,
            this.tsbReports});
            this.toolStripMain.Location = new System.Drawing.Point(0, 24);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStripMain.Size = new System.Drawing.Size(737, 40);
            this.toolStripMain.TabIndex = 1;
            this.toolStripMain.Text = "ToolStrip";
            // 
            // tsbAcusition
            // 
            this.tsbAcusition.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAcusition.Image = global::AssetRegister.Properties.Resources.Microsoft_Info_icon1;
            this.tsbAcusition.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAcusition.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAcusition.Name = "tsbAcusition";
            this.tsbAcusition.Size = new System.Drawing.Size(36, 37);
            this.tsbAcusition.Text = "Acquisition";
            this.tsbAcusition.MouseHover += new System.EventHandler(this.tsbAcusition_MouseHover);
            // 
            // tsbTransfer
            // 
            this.tsbTransfer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbTransfer.Image = global::AssetRegister.Properties.Resources.folder_documents_icon1;
            this.tsbTransfer.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbTransfer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTransfer.Name = "tsbTransfer";
            this.tsbTransfer.Size = new System.Drawing.Size(36, 37);
            this.tsbTransfer.Text = "Transfer";
            this.tsbTransfer.MouseHover += new System.EventHandler(this.tsbTransfer_MouseHover);
            this.tsbTransfer.Click += new System.EventHandler(this.tsbTransfer_Click);
            // 
            // tsbDisposal
            // 
            this.tsbDisposal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDisposal.Image = global::AssetRegister.Properties.Resources.Actions_process_stop_icon1;
            this.tsbDisposal.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDisposal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDisposal.Name = "tsbDisposal";
            this.tsbDisposal.Size = new System.Drawing.Size(36, 37);
            this.tsbDisposal.Text = "Disposal";
            this.tsbDisposal.MouseHover += new System.EventHandler(this.tsbDisposal_MouseHover);
            this.tsbDisposal.Click += new System.EventHandler(this.tsbDisposal_Click);
            // 
            // tsbAdjustment
            // 
            this.tsbAdjustment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAdjustment.Image = global::AssetRegister.Properties.Resources.Actions_document_edit_icon1;
            this.tsbAdjustment.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAdjustment.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdjustment.Name = "tsbAdjustment";
            this.tsbAdjustment.Size = new System.Drawing.Size(36, 37);
            this.tsbAdjustment.Text = "Adjustment";
            this.tsbAdjustment.MouseHover += new System.EventHandler(this.tsbAdjustment_MouseHover);
            // 
            // tsbDepreciation
            // 
            this.tsbDepreciation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDepreciation.Image = global::AssetRegister.Properties.Resources.Lite_Icon_icon1;
            this.tsbDepreciation.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDepreciation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDepreciation.Name = "tsbDepreciation";
            this.tsbDepreciation.Size = new System.Drawing.Size(36, 37);
            this.tsbDepreciation.Text = "Depreciation";
            this.tsbDepreciation.MouseHover += new System.EventHandler(this.tsbDepreciation_MouseHover);
            // 
            // tsbReports
            // 
            this.tsbReports.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbReports.Image = global::AssetRegister.Properties.Resources.Actions_document_find_icon;
            this.tsbReports.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbReports.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbReports.Name = "tsbReports";
            this.tsbReports.Size = new System.Drawing.Size(36, 37);
            this.tsbReports.Text = "Reports";
            this.tsbReports.MouseHover += new System.EventHandler(this.tsbReports_MouseHover);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblUserFullName,
            this.tslblCurrentDateTime,
            this.tslblVersion,
            this.tslbLoginTime});
            this.statusStrip.Location = new System.Drawing.Point(0, 429);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(737, 24);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // tslblUserFullName
            // 
            this.tslblUserFullName.BackColor = System.Drawing.SystemColors.Control;
            this.tslblUserFullName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslblUserFullName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblUserFullName.ForeColor = System.Drawing.Color.DarkMagenta;
            this.tslblUserFullName.Name = "tslblUserFullName";
            this.tslblUserFullName.Size = new System.Drawing.Size(90, 19);
            this.tslblUserFullName.Text = "User Full Name";
            // 
            // tslblCurrentDateTime
            // 
            this.tslblCurrentDateTime.BackColor = System.Drawing.SystemColors.Control;
            this.tslblCurrentDateTime.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslblCurrentDateTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblCurrentDateTime.ForeColor = System.Drawing.Color.DarkMagenta;
            this.tslblCurrentDateTime.Name = "tslblCurrentDateTime";
            this.tslblCurrentDateTime.Size = new System.Drawing.Size(109, 19);
            this.tslblCurrentDateTime.Text = "CurrentDateTime";
            // 
            // tslblVersion
            // 
            this.tslblVersion.BackColor = System.Drawing.SystemColors.Control;
            this.tslblVersion.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslblVersion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslblVersion.ForeColor = System.Drawing.Color.DarkMagenta;
            this.tslblVersion.Name = "tslblVersion";
            this.tslblVersion.Size = new System.Drawing.Size(52, 19);
            this.tslblVersion.Text = "Version";
            // 
            // tslbLoginTime
            // 
            this.tslbLoginTime.BackColor = System.Drawing.SystemColors.Control;
            this.tslbLoginTime.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tslbLoginTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslbLoginTime.ForeColor = System.Drawing.Color.DarkMagenta;
            this.tslbLoginTime.Name = "tslbLoginTime";
            this.tslbLoginTime.Size = new System.Drawing.Size(69, 19);
            this.tslbLoginTime.Text = "LoginTime";
            // 
            // systimer
            // 
            this.systimer.Interval = 990;
            this.systimer.Tick += new System.EventHandler(this.systimer_Tick);
            // 
            // toolStripSub
            // 
            this.toolStripSub.AutoSize = false;
            this.toolStripSub.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSub.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslAssetInfo,
            this.toolStripSeparator14,
            this.tslLandInfo,
            this.toolStripSeparator15,
            this.tslAdditionInfo,
            this.toolStripSeparator16,
            this.tslAdjustmentInfo,
            this.toolStripSeparator19,
            this.tslRevalutionInfo,
            this.toolStripSeparator20,
            this.tslDepSetiings,
            this.toolStripSeparator21,
            this.tslDepProcess,
            this.toolStripSeparator1,
            this.tslRptLandRegister,
            this.toolStripSeparator2,
            this.tslRptAssetRegister});
            this.toolStripSub.Location = new System.Drawing.Point(0, 64);
            this.toolStripSub.Name = "toolStripSub";
            this.toolStripSub.Size = new System.Drawing.Size(737, 25);
            this.toolStripSub.TabIndex = 4;
            this.toolStripSub.Text = "toolStrip1";
            this.toolStripSub.Visible = false;
            // 
            // tslAssetInfo
            // 
            this.tslAssetInfo.Name = "tslAssetInfo";
            this.tslAssetInfo.Size = new System.Drawing.Size(89, 22);
            this.tslAssetInfo.Text = "Asset Info Entry";
            this.tslAssetInfo.MouseHover += new System.EventHandler(this.tslAssetInfo_MouseHover);
            this.tslAssetInfo.MouseLeave += new System.EventHandler(this.tslAssetInfo_MouseLeave);
            this.tslAssetInfo.Click += new System.EventHandler(this.tslAssetInfo_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // tslLandInfo
            // 
            this.tslLandInfo.Name = "tslLandInfo";
            this.tslLandInfo.Size = new System.Drawing.Size(87, 22);
            this.tslLandInfo.Text = "Land Info Entry";
            this.tslLandInfo.MouseHover += new System.EventHandler(this.tslLandInfo_MouseHover);
            this.tslLandInfo.MouseLeave += new System.EventHandler(this.tslLandInfo_MouseLeave);
            this.tslLandInfo.Click += new System.EventHandler(this.tslLandInfo_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 25);
            // 
            // tslAdditionInfo
            // 
            this.tslAdditionInfo.Name = "tslAdditionInfo";
            this.tslAdditionInfo.Size = new System.Drawing.Size(83, 22);
            this.tslAdditionInfo.Text = "Addition Entry";
            this.tslAdditionInfo.MouseHover += new System.EventHandler(this.tslAdditionInfo_MouseHover);
            this.tslAdditionInfo.MouseLeave += new System.EventHandler(this.tslAdditionInfo_MouseLeave);
            this.tslAdditionInfo.Click += new System.EventHandler(this.tslAdditionInfo_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 25);
            // 
            // tslAdjustmentInfo
            // 
            this.tslAdjustmentInfo.Name = "tslAdjustmentInfo";
            this.tslAdjustmentInfo.Size = new System.Drawing.Size(123, 22);
            this.tslAdjustmentInfo.Text = "Adjustment Info Entry";
            this.tslAdjustmentInfo.MouseHover += new System.EventHandler(this.tslAdjustmentInfo_MouseHover);
            this.tslAdjustmentInfo.MouseLeave += new System.EventHandler(this.tslAdjustmentInfo_MouseLeave);
            this.tslAdjustmentInfo.Click += new System.EventHandler(this.tslAdjustmentInfo_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(6, 25);
            // 
            // tslRevalutionInfo
            // 
            this.tslRevalutionInfo.Name = "tslRevalutionInfo";
            this.tslRevalutionInfo.Size = new System.Drawing.Size(123, 22);
            this.tslRevalutionInfo.Text = "Revaluation Info Entry";
            this.tslRevalutionInfo.MouseHover += new System.EventHandler(this.tslRevalutionInfo_MouseHover);
            this.tslRevalutionInfo.MouseLeave += new System.EventHandler(this.tslRevalutionInfo_MouseLeave);
            this.tslRevalutionInfo.Click += new System.EventHandler(this.tslRevalutionInfo_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(6, 25);
            // 
            // tslDepSetiings
            // 
            this.tslDepSetiings.Name = "tslDepSetiings";
            this.tslDepSetiings.Size = new System.Drawing.Size(149, 22);
            this.tslDepSetiings.Text = "Depreciation Settings Entry";
            this.tslDepSetiings.MouseHover += new System.EventHandler(this.tslDepSetiings_MouseHover);
            this.tslDepSetiings.MouseLeave += new System.EventHandler(this.tslDepSetiings_MouseLeave);
            this.tslDepSetiings.Click += new System.EventHandler(this.tslDepSetiings_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(6, 25);
            // 
            // tslDepProcess
            // 
            this.tslDepProcess.Name = "tslDepProcess";
            this.tslDepProcess.Size = new System.Drawing.Size(114, 15);
            this.tslDepProcess.Text = "Deprecaiton Process";
            this.tslDepProcess.MouseHover += new System.EventHandler(this.tslDepProcess_MouseHover);
            this.tslDepProcess.MouseLeave += new System.EventHandler(this.tslDepProcess_MouseLeave);
            this.tslDepProcess.Click += new System.EventHandler(this.tslDepProcess_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // tslRptLandRegister
            // 
            this.tslRptLandRegister.Name = "tslRptLandRegister";
            this.tslRptLandRegister.Size = new System.Drawing.Size(116, 15);
            this.tslRptLandRegister.Text = "Land Register Report";
            this.tslRptLandRegister.MouseHover += new System.EventHandler(this.tslRptLandRegister_MouseHover);
            this.tslRptLandRegister.MouseLeave += new System.EventHandler(this.tslRptLandRegister_MouseLeave);
            this.tslRptLandRegister.Click += new System.EventHandler(this.tslRptLandRegister_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // tslRptAssetRegister
            // 
            this.tslRptAssetRegister.Name = "tslRptAssetRegister";
            this.tslRptAssetRegister.Size = new System.Drawing.Size(118, 15);
            this.tslRptAssetRegister.Text = "Asset Registar Report";
            this.tslRptAssetRegister.MouseHover += new System.EventHandler(this.tslRptAssetRegister_MouseHover);
            this.tslRptAssetRegister.MouseLeave += new System.EventHandler(this.tslRptAssetRegister_MouseLeave);
            this.tslRptAssetRegister.Click += new System.EventHandler(this.tslRptAssetRegister_Click);
            // 
            // tsmMonthWiseAll
            // 
            this.tsmMonthWiseAll.Enabled = false;
            this.tsmMonthWiseAll.Name = "tsmMonthWiseAll";
            this.tsmMonthWiseAll.Size = new System.Drawing.Size(341, 22);
            this.tsmMonthWiseAll.Text = "Monthwise Addition Decp. And Disposal Summary";
            this.tsmMonthWiseAll.Click += new System.EventHandler(this.tsmMonthWiseAll_Click);
            // 
            // MDIParent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(737, 453);
            this.Controls.Add(this.toolStripSub);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStripMain);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIParent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fixed Asset Management System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIParent_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDIParent_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStripSub.ResumeLayout(false);
            this.toolStripSub.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuCreateUser;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuChangePassword;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Timer systimer;
        private System.Windows.Forms.ToolStripMenuItem assetConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depreciationConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem processDeprecitationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem soldOrDamageEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsMenu;
        private System.Windows.Forms.ToolStripMenuItem assetListReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem landToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem assetFullProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuUserPrivilege;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem landDetailsReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CAPEXInfoReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assetTransferToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem additionToAssetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assetRevaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depreciationExpenseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem assetAdditionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tslblUserFullName;
        private System.Windows.Forms.ToolStripStatusLabel tslblCurrentDateTime;
        private System.Windows.Forms.ToolStripStatusLabel tslblVersion;
        private System.Windows.Forms.ToolStripStatusLabel tslbLoginTime;
        private System.Windows.Forms.ToolStripMenuItem tsmAssetReigisterReport;
        private System.Windows.Forms.ToolStripMenuItem depreciationJournalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depreciationJournalChargeAmountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem additionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmDepJournalSummaryRpt;
        private System.Windows.Forms.ToolStripMenuItem fixedAssetsStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem tsmDepNCost;
        private System.Windows.Forms.ToolStripMenuItem assetListingManualReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assetTransferReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmAdjustCost;
        private System.Windows.Forms.ToolStripMenuItem tsmMothwiseDepCharge;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem assetLocationEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assetSubLocationEntryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem supplierEntryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton tsbAcusition;
        private System.Windows.Forms.ToolStripButton tsbTransfer;
        private System.Windows.Forms.ToolStripButton tsbDisposal;
        private System.Windows.Forms.ToolStripButton tsbAdjustment;
        private System.Windows.Forms.ToolStripButton tsbDepreciation;
        private System.Windows.Forms.ToolStrip toolStripSub;
        private System.Windows.Forms.ToolStripLabel tslAssetInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripLabel tslLandInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripLabel tslAdditionInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripLabel tslAdjustmentInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripLabel tslRevalutionInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripLabel tslDepSetiings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripLabel tslDepProcess;
        private System.Windows.Forms.ToolStripMenuItem toolStripToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbReports;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel tslRptLandRegister;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel tslRptAssetRegister;
        private System.Windows.Forms.ToolStripMenuItem tsmLandAdjustEntry;
        private System.Windows.Forms.ToolStripMenuItem tsmDisposalOfFARegister;
        private System.Windows.Forms.ToolStripMenuItem assetHistoricalRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depreciationChagedOnRevaluedAssetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miscelleneousReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem costcentrewiseStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem AcInfoEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountLedgerFARReconciliationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmMothwiseCostAddition;
        private System.Windows.Forms.ToolStripMenuItem tsmMothwiseCostDisposal;
        private System.Windows.Forms.ToolStripMenuItem tsmMonthWiseAll;
    }
}



