﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAssetRevaluation : Form
    {
        private clsCommonFunctions dbConAssetReval = new clsCommonFunctions();
        private int AssetId = 0;
        private string AssetCode = "", CompanyId = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAssetRevaluation()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmAssetRevaluation_Load(object sender, EventArgs e)
        {
            dbConAssetReval.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode 0, '', '', 2");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
            dgvListData.Columns[2].Visible = false;
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConAssetReval.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetReval.DisposeChecking(txtAssetCode);
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                CompanyId = dgvListData.Rows[0].Cells[2].Value.ToString();
                //LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                CompanyId = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[2].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            CompanyId = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[2].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
            //LoadAssetDetails();
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConAssetReval.CheckEmptyOrNull(txtAssetCode, "Enter Asset") &&
                !dbConAssetReval.CheckEmptyOrNull(txtRevalAmt, "Enter Amount"))
            {
                try 
                {
                    string RevDate = dtpMonth.Value.Year.ToString()+ "-" + dtpYear.Value.Month.ToString() + "-01";
                    string RDate = dtpRevDate.Value.ToString("yyyy-MM-dd");
                    if (!CheckIfRevalued(Convert.ToDateTime(RevDate).Year))
                    {
                        string[] cols = { "[AssetId]", "[CompanyId]", "[RevaluationCost]", "[RevDate]",
                                          "[RevaluationDate]", "[Description]", "[UserId]"
                                        };
                        string[] vals = { AssetId.ToString(),
                                          CompanyId, 
                                          txtRevalAmt.Text, 
                                          RDate,
                                          RevDate,
                                          dbConAssetReval.SingleQuoteHandler(txtRevalDesc),                              
                                          frmLogin.userId.ToString()
                                        };
                        string[] uCols = { "DepreciationEffectDate" };
                        string[] uVals = { dtpMonth.Value.Year.ToString() + "-01-01" };
                        string[] whCols = { "AssetId" };
                        string[] whVals = { AssetId.ToString() };
                        string[] whDCols = { "AssetId", "YEAR(ProcessDate)"};
                        string[] whDVals = { AssetId.ToString(), dtpMonth.Text.ToString() };
                        string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                       "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                        string[] lVals = { frmLogin.userId.ToString(), "INSERT,UPDATE,DELETE", frmLogin.ipAddress, 
                                           "[AssetRegistar].[dbo].[AR_AssetRevaluation],[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_Process]", this.Name, 
                                           dbConAssetReval.getMaxIdentity("[AR_AssetRevaluation]","[TrnNo]").ToString(), "", "" };
                                                

                        string sql = dbConAssetReval.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetRevaluation]", cols, vals, "INSERT", null, null)
                                     +
                                     dbConAssetReval.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", uCols, uVals, "UPDATE", whCols, whVals)
                                     +
                                     dbConAssetReval.CreateSQLStatement("[AssetRegistar].[dbo].[AR_Process]", null, null, "DELETE", whDCols, whDVals)
                                     +
                                     dbConAssetReval.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                        DateTime? LastProcessDate = IsProcesDeprecitation(AssetId);
                        if (LastProcessDate != null)
                        {
                            DialogResult result = MessageBox.Show(@"Process has been done for the Asset you want to Revalued for the given year." + Environment.NewLine +
                                "Would you like to Revalue this asset with Process?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (result == DialogResult.Yes)
                            {
                                if ((bool)dbConAssetReval.DBCRUDFunction(sql, false))
                                {
                                    ProcessRevaluation(AssetId, DateTime.Parse(RevDate), LastProcessDate.Value);
                                    MessageBox.Show("Information Saved Successfully.", "Save",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ClearAll();
                                }
                                else
                                {
                                    MessageBox.Show("Failed to Save Information.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Failed to Revalue the Asset.", "Failed",
                                                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            if ((bool)dbConAssetReval.DBCRUDFunction(sql, false))
                            {
                                MessageBox.Show("Information Saved Successfully.", "Save",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ClearAll();
                            }
                            else
                            {
                                MessageBox.Show("Failed to Save Information.", "Error",
                                                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    else
                        MessageBox.Show("Revaluation for the Asset for given year is entered.", "Stop",
                                               MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }                
            }
        }

        private bool ProcessRevaluation(int AssetId, DateTime FDate, DateTime TDate)
        {
                //@"EXEC prcProcessDepreciationOnCarryingOnly " + AssetId + ",'" + PDate + "', " + frmLogin.userId, false))
            if ((bool)dbConAssetReval.DBCRUDFunction(@"EXEC prcProcessDepreciation '" + AssetId + "', '', " + "'" + FDate.ToString("dd-MMM-yyyy") + "', " + "'" + TDate.ToString("dd-MMM-yyyy") + "', " + frmLogin.userId + ", '" + frmLogin.ipAddress + "', 1, 0", false))
                return true;
            else
                return false;

        }

        private DateTime? IsProcesDeprecitation(int AssetId)
        {
            SqlDataReader dr; DateTime? LastDepDate = null;
            dr = (SqlDataReader)dbConAssetReval.DBCRUDFunction(@"SELECT Top 1 * FROM AR_Process "
                                                        + "WHERE ProcessDate >= '" + dtpMonth.Value + "' " 
                                                        + "Order By ProcessDate Desc", true);

            while (dr.Read())
            {
                LastDepDate = Convert.ToDateTime(dr["ProcessDate"]);
            }

            return LastDepDate;

        }

        private bool CheckIfRevalued(int RevYear)
        {
            SqlDataReader sdr;
            sdr = (SqlDataReader)dbConAssetReval.DBCRUDFunction(@"SELECT * FROM AssetRegistar.dbo.AR_AssetRevaluation 
                                                                    WHERE AssetId = " + AssetId + " AND YEAR(RevaluationDate) = " + RevYear, true);

            if (sdr.Read())
                return true;
            else
                return false;
        }

        private void ClearAll()
        {
            txtAssetCode.Text = "";
            AssetId = 0;
            dgvListData.Visible = false;
            txtRevalAmt.Text = "";
            txtRevalDesc.Text = "";
            txtAssetCode.Focus();
        }

        private void txtRevalAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetReval.DisposeChecking(txtRevalAmt);
            dbConAssetReval.DecimalNumbersWithSign(sender, e); 
        }

        private void frmAssetRevaluation_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }
    }
}
