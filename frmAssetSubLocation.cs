﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAssetSubLocation : Form
    {
        private clsCommonFunctions dbConLoc = new clsCommonFunctions();
        private bool IsUpdate = false;
        private int SubLocId = 0;
        private string pAssetSubLoc = "", Loc = "";
        private int pAssetLocId = 0, LocId = 0;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public bool cmbLoadFlag = false;

        public frmAssetSubLocation()
        {
            InitializeComponent();
        }

        private void frmAssetLocation_Load(object sender, EventArgs e)
        {
            dbConLoc.FillDataGridView(dgvLocation, @"SELECT AssetLocId, AssetLocation FROM AR_AssetLocation");
            dgvLocation.Columns[0].Visible = false;
            dgvLocation.Columns[1].Width = 200;
            dbConLoc.FillDataGridView(dgvSubLocation, @"SELECT aasl.AssetSubLocId,
                                                               aasl.AssetSubLocation,
                                                               aasl.AssetLocId,
                                                               aal.AssetLocation
                                                        FROM   AR_AssetSubLocation aasl
                                                               INNER JOIN AR_AssetLocation aal
                                                                    ON  aal.AssetLocId = aasl.AssetLocId");
                                  
            dgvSubLocation.Columns["AssetSubLocId"].Visible = false;
            dgvSubLocation.Columns["AssetLocId"].Visible = false;
            dgvSubLocation.Columns["AssetSubLocation"].HeaderText = "Sub Location";
            dgvSubLocation.Columns["AssetLocation"].HeaderText = "Location";
            dgvSubLocation.Columns["AssetSubLocation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSubLocation.Columns["AssetLocation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            if (!IsUpdate)
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }

            dgvSubLocation.Columns["AssetSubLocation"].Width = 350;
            dgvSubLocation.Columns["AssetLocation"].Width = 300;
        }

        private void dgvLocation_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSubLocation.Rows.Count > 0)
            {
                SubLocId = Convert.ToInt32(dgvSubLocation.Rows[dgvSubLocation.CurrentCell.RowIndex].Cells["AssetSubLocId"].Value.ToString());
                LoadLocDetails(SubLocId);
                IsUpdate = true;
            }
            if (!IsUpdate)
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }
        }

        private void LoadLocDetails(int LocId)
        {
            SqlDataReader dr;
            dr = (SqlDataReader)dbConLoc.DBCRUDFunction(@"SELECT aasl.AssetLocId,
                                                                 aal.AssetLocation,
                                                                 aasl.AssetSubLocation
                                                          FROM   AR_AssetSubLocation aasl
                                                                 INNER JOIN AR_AssetLocation aal
                                                                      ON  aal.AssetLocId = aasl.AssetLocId
                                                          WHERE  aasl.AssetSubLocId = " + SubLocId, true);
            if (dr.Read())
            {
                txtLocation.Text = dr["AssetLocation"].ToString().Trim();
                dgvLocation.Visible = false;
                pAssetLocId = Convert.ToInt32(dr["AssetLocId"].ToString().Trim());
                txtSubLoc.Text = dr["AssetSubLocation"].ToString();
                pAssetSubLoc = dr["AssetSubLocation"].ToString();
                txtLocation.Tag = pAssetLocId;
            }

            dr.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            dbConLoc.MakeTextUppercase(txtSearch);
            var dataTable = (DataTable)dgvSubLocation.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dataView.RowFilter = dgvSubLocation.Columns["AssetSubLocation"].Name + " LIKE '%" + txtSearch.Text + "%' OR "
                                    + dgvSubLocation.Columns["AssetLocation"].Name + " LIKE '%" + txtSearch.Text + "%'";
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            txtSearch.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (LocId < 1)
            {
                MessageBox.Show("Select Location", "Location", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtLocation.Focus();
                dgvLocation.Visible = false;
                return;
            }
            else if (!dbConLoc.CheckEmptyOrNull(txtSubLoc, "Enter Sub Location Name"))
            {
                DialogResult result = MessageBox.Show(@"Would You Like to Save New Sub Location " + txtSubLoc.Text.Trim() 
                                                        + " for Location " + txtLocation.Text.Trim(), "Save?", 
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes) 
                {
                    string[] cols = { "[AssetSubLocation]", "[AssetLocId]", "[EntryBy]" };
                    string[] vals = { dbConLoc.SingleQuoteHandler(txtSubLoc), LocId.ToString(), frmLogin.userId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetSubLocation]", this.Name, 
                                       dbConLoc.getMaxIdentity("[AR_AssetSubLocation]", "[AssetSubLocId]").ToString(), "", "" };

                    string sql = dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSubLocation]", cols, vals, "INSERT", null, null)
                                 +
                                 dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLoc.DBCRUDFunction(sql, false))
                    {
                        LocId = 0;
                        Loc = "";
                        txtLocation.Text = "";
                        dgvLocation.Visible = false;
                        txtSubLoc.Text = "";
                        MessageBox.Show("Sub Location Saved Successfully.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dbConLoc.FillDataGridView(dgvSubLocation, @"SELECT aasl.AssetSubLocId,
                                                                           aasl.AssetSubLocation,
                                                                           aasl.AssetLocId,
                                                                           aal.AssetLocation
                                                                    FROM   [AssetRegistar].[dbo].[AR_AssetSubLocation] aasl
                                                                           INNER JOIN [AssetRegistar].[dbo].[AR_AssetLocation] aal
                                                                                ON  aal.AssetLocId = aasl.AssetLocId");
                    }
                    else
                        MessageBox.Show("Failed to Save Location.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            pAssetLocId = 0;
            pAssetSubLoc = "";
            Loc = "";
            LocId = 0;
            txtLocation.Text = "";
            dgvLocation.Visible = false;
            txtSubLoc.Text = "";
            IsUpdate = false;

            if (!IsUpdate)
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (IsUpdate && SubLocId > 0 && int.Parse(txtLocation.Tag.ToString()) > 0 && !dbConLoc.CheckEmptyOrNull(txtSubLoc, "Enter Location"))
            {
                DialogResult result = MessageBox.Show(@"Would You Like to Update Location " + txtSubLoc.Text.Trim() 
                                                        + " of Cost Centre " + txtLocation.Text.Trim(), "Update?", 
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    string[] cols = { "[AssetSubLocation]", "[AssetLocId]", 
                                  "[UpdateDate]", "[UpdateBy]"
                                };
                    string[] vals = { dbConLoc.SingleQuoteHandler(txtSubLoc), txtLocation.Tag.ToString(),
                                      dbConLoc.GetServerCurrentDateTime().ToString(), frmLogin.userId.ToString() 
                                    };
                    string[] whCols = { "[AssetSubLocId]" };
                    string[] whVals = { SubLocId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetSubLocation]", this.Name, SubLocId.ToString(),
                                       "[AssetSubLocId], [AssetSubLocation], [AssetLocId]", 
                                       SubLocId.ToString() + ", " + dbConLoc.SingleQuoteHandler(pAssetSubLoc) 
                                       + ", " + pAssetLocId };

                    string sql = dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSubLocation]", cols, vals, "UPDATE", whCols, whVals)
                                 +
                                 dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLoc.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Sub Location Info Updated Successfully.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dbConLoc.FillDataGridView(dgvSubLocation, @"SELECT aasl.AssetSubLocId,
                                                                           aasl.AssetSubLocation,
                                                                           aasl.AssetLocId,
                                                                           aal.AssetLocation
                                                                    FROM   AR_AssetSubLocation aasl
                                                                           INNER JOIN AR_AssetLocation aal
                                                                                ON  aal.AssetLocId = aasl.AssetLocId");
                    }
                    else
                    {
                        MessageBox.Show("Failed to Sub Update Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid Operation to Sub Update Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (SubLocId > 0 && LocId > 0 && !dbConLoc.CheckEmptyOrNull(txtSubLoc, "Enter Location"))
            {
                DialogResult result = MessageBox.Show(@"Would You Like to Delete Location " + txtSubLoc.Text.Trim() 
                                                        + " of Cost Centre " + txtLocation.Text.Trim(), "Delete?", 
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    string[] whCols = { "[AssetSubLocId]" };
                    string[] whVals = { SubLocId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "DELETE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetSubLocation]", this.Name, SubLocId.ToString(),
                                       "[AssetSubLocId], [AssetSubLocation], [AssetLocId]", 
                                       SubLocId.ToString() + ", " + dbConLoc.SingleQuoteHandler(pAssetSubLoc) 
                                       + ", " + pAssetLocId };

                    string sql = dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSubLocation]", null, null, "DELETE", whCols, whVals)
                                 + 
                                 dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null,null);

                    if ((bool)dbConLoc.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Location Info Deleted Successfully.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dbConLoc.FillDataGridView(dgvSubLocation, @"SELECT aasl.AssetSubLocId,
                                                                           aasl.AssetSubLocation,
                                                                           aasl.AssetLocId,
                                                                           aal.AssetLocation
                                                                    FROM   [AssetRegistar].[dbo].[AR_AssetSubLocation] aasl
                                                                           INNER JOIN [AssetRegistar].[dbo].[AR_AssetLocation] aal
                                                                                ON  aal.AssetLocId = aasl.AssetLocId");
                        SubLocId = 0;
                        Loc = "";
                        LocId = 0;
                        pAssetLocId = 0;
                        txtLocation.Text = "";
                        dgvLocation.Visible = false;
                        txtSubLoc.Text = "";
                        btnDelete.Visible = false;
                        btnSave.Visible = true;
                        btnUpdate.Enabled = false;
                        IsUpdate = false;
                    }
                    else
                    {
                        MessageBox.Show("Failed to Delete Sub Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid Operation to Delete Sub Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtLocation_TextChanged(object sender, EventArgs e)
        {
            dbConLoc.MakeTextUppercase(txtSubLoc);
        }

        private void txtLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLoc.DisposeChecking(txtSubLoc);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtSubLoc_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void frmAssetSubLocation_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnUpdate, "Update");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "New");
        }

        private void btnDelete_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnDelete, "Delete");
        }

        private void txtLocation_TextChanged_1(object sender, EventArgs e)
        {
            dbConLoc.MakeTextUppercase(txtLocation);
            var dataTable = (DataTable)dgvLocation.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvLocation.Visible = true;
                dataView.RowFilter = dgvLocation.Columns[1].Name + " LIKE '%" + txtLocation.Text + "%'";
            }
        }

        private void txtLocation_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            dbConLoc.DisposeChecking(txtLocation);
            if (e.KeyChar == (char)Keys.Escape)
                dgvLocation.Visible = false;
        }

        private void txtLocation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvLocation.Focus();

            if (e.KeyCode == Keys.Enter && dgvLocation.Rows.Count > 0)
            {
                txtLocation.Text = dgvLocation.Rows[0].Cells[1].Value.ToString();
                txtLocation.Tag = dgvLocation.Rows[0].Cells[0].Value.ToString();
                LocId = Convert.ToInt32(dgvLocation.Rows[0].Cells[0].Value.ToString());
                Loc = dgvLocation.Rows[0].Cells[1].Value.ToString();
                dgvLocation.Visible = false;
                txtLocation.SelectionStart = txtLocation.TextLength;
            }
        }

        private void txtLocation_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void dgvLocation_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            LocId = Convert.ToInt32(dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[0].Value.ToString());
            Loc = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtLocation.Text = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtLocation.Tag = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[0].Value.ToString();
            txtLocation.Focus();
            txtLocation.SelectionStart = txtLocation.TextLength;
            dgvLocation.Visible = false;
        }

        private void dgvLocation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LocId = Convert.ToInt32(dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[0].Value.ToString());
                Loc = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtLocation.Text = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtLocation.Tag = dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells[0].Value.ToString();
                txtLocation.Focus();
                txtLocation.SelectionStart = txtLocation.TextLength;
                dgvLocation.Visible = false;
            }
        }

        private void dgvLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtLocation.Focus();
            txtLocation.SelectionStart = txtLocation.TextLength;
        }
    }
}
