﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AssetRegister
{
    public partial class MDIParent : Form
    {
        private clsCommonFunctions dbMdiCon = new clsCommonFunctions();
        private int childFormNumber = 0;
        private bool isLogin = true;
        public DateTime cTime;

        public MDIParent(bool isLoggedin)
        {
            isLogin = isLoggedin;
            InitializeComponent();
            LoadPermission();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDIParent_Load(object sender, EventArgs e)
        {
            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is MdiClient)                
                    ctrl.BackColor = Color.DarkSlateGray;

            }

            tslblUserFullName.Text = "User Full Name : " + frmLogin.userFullName;
            cTime = dbMdiCon.GetServerCurrentDateTime();
            systimer.Enabled = true;
            tslblVersion.Text = "Version : " + frmLogin.GetPublishedVersion().ToString();

            //if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            //    tslblVersion.Text = "Version : " + System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            //else
            //    tslblVersion.Text = "Version : " + Application.ProductVersion.ToString();
            tslbLoginTime.Text = "Login Time : " + cTime.ToString("hh:mm:ss tt");
        }

        private void MDIParent_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isLogin)
            {
                DialogResult result = MessageBox.Show("Would you like to close the application", "Exit?",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.No)
                    e.Cancel = true;
                else
                    if (IsLogout())
                    {
                        isLogin = false;
                        Application.Exit();
                    }
            }
        }

        public bool IsLogout()
        {
            try
            {
                dbMdiCon.DBCRUDFunction("EXEC prcLogout " + frmLogin.userId.ToString() + ",'" + frmLogin.ipAddress + "', 'LOGOUT', '[AssetRegistar].[dbo].[AR_UsersLog]'", false);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void systimer_Tick(object sender, EventArgs e)
        {
            cTime = cTime.AddMilliseconds(systimer.Interval);
            //tslblCurrentDateTime.Text = "Today is : " + cTime.ToString("dddd, dd-MMM-yyyy hh:mm:ss tt");
            tslblCurrentDateTime.Text = "Today is : " + cTime.ToString("dddd, dd-MMM-yyyy") + " && Now : "
                                                      + cTime.ToString("hh:mm:ss tt");
        }

        private void assetConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetConfigN frmAssetConfig = new frmAssetConfigN();            
            frmAssetConfig.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetConfig))
                frmAssetConfig.Show();            
            Cursor.Current = Cursors.Default;
        }

        private void assetListReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            Reports.frmRptAssetList frmRptAssetList = new Reports.frmRptAssetList();
            frmRptAssetList.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetList))
                frmRptAssetList.Show();
            Cursor.Current = Cursors.Default;
        }

        private void processDeprecitationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmDepreciationProcess frmDepreciationProcess = new frmDepreciationProcess();
            frmDepreciationProcess.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmDepreciationProcess))
                frmDepreciationProcess.Show();
            Cursor.Current = Cursors.Default;
        }

        private void landToolStripMenu_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetConfigLand frmAssetConfigLand = new frmAssetConfigLand();
            frmAssetConfigLand.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetConfigLand))
                frmAssetConfigLand.Show();
            Cursor.Current = Cursors.Default;
        }

        private void depreciationConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmDepreciationConfig frmDepreciationConfig = new frmDepreciationConfig();
            frmDepreciationConfig.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmDepreciationConfig))
                frmDepreciationConfig.Show();
            Cursor.Current = Cursors.Default;
        }

        private void soldOrDamageEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmSoldDamage frmSoldDamage = new frmSoldDamage();
            frmSoldDamage.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmSoldDamage))
                frmSoldDamage.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetFullProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAssetProfile frmRptAssetProfile = new frmRptAssetProfile();
            frmRptAssetProfile.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetProfile))
                frmRptAssetProfile.Show();
            Cursor.Current = Cursors.Default;
        }

        private void toolStripMenuCreateUser_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmCreateUser frmCreateUser = new frmCreateUser();
            frmCreateUser.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmCreateUser))
                frmCreateUser.Show();
            Cursor.Current = Cursors.Default;
        }

        private void toolStripMenuChangePassword_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmChangePassword frmChangePassword = new frmChangePassword();
            frmChangePassword.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmChangePassword))
                frmChangePassword.Show();
            Cursor.Current = Cursors.Default;
        }

        private void toolStripMenuUserPrivilege_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmUserPrivileges frmUserPrivileges = new frmUserPrivileges();
            frmUserPrivileges.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmUserPrivileges))
                frmUserPrivileges.Show();
            Cursor.Current = Cursors.Default;
        }

        private void LoadPermission()
        {
            string Var = "";
            SqlDataReader dtReader;

            try
            {
                List<ToolStripItem> menues = new List<ToolStripItem>();
                foreach (ToolStripItem t in menuStrip.Items)
                {
                    GetMenues(t, menues);
                }

                dtReader = (SqlDataReader) dbMdiCon.DBCRUDFunction(@"EXEC GetPrivilegeItem " + frmLogin.userId, true);

                while (dtReader.Read())
                {
                    foreach (ToolStripItem t in menues)
                    {
                        Var = dtReader["MenuItemText"].ToString();
                        if (t.Text == Var)
                        {
                            t.Enabled = true;
                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        public void GetMenues(ToolStripItem Current, List<ToolStripItem> menues)
        {
            //Current.Enabled = true;
            menues.Add(Current);

            if ((Current) is ToolStripMenuItem)
            {
                foreach (ToolStripItem menu in ((ToolStripMenuItem)Current).DropDownItems)
                {
                    GetMenues(menu, menues);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isLogin)
            {
                DialogResult result = MessageBox.Show("Would you like to close the application", "Exit?",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    if (IsLogout())
                    {
                        isLogin = false;
                        Application.Exit();
                    }
                }
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (isLogin)
            {
                DialogResult result = MessageBox.Show("Are you sure to logout?", "Logoout",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    if (IsLogout())
                    {
                        isLogin = false;
                        this.Hide();
                        frmLogin frmlogin = new frmLogin(isLogin);
                        frmlogin.Show();
                        //Application.Exit();
                    }
                }
            }  
        }

        private void landDetailsReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptLanDetails frmRptLanDetails = new frmRptLanDetails();
            frmRptLanDetails.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptLanDetails))
                frmRptLanDetails.Show();
            Cursor.Current = Cursors.Default;
        }

        private void cAPEXInfoReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptCAPEXInfo frmRptCAPEXInfo = new frmRptCAPEXInfo();
            frmRptCAPEXInfo.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptCAPEXInfo))
                frmRptCAPEXInfo.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetLocationEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void assetSubLocationEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void supplierEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void assetTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetTransfer frmAssetTransfer = new frmAssetTransfer();
            frmAssetTransfer.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetTransfer))
                frmAssetTransfer.Show();
            Cursor.Current = Cursors.Default;
        }

        private void additionToAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAdditionToAsset frmAdditionToAsset = new frmAdditionToAsset();
            frmAdditionToAsset.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAdditionToAsset))
                frmAdditionToAsset.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetRevaluationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetRevaluation frmAssetRevaluation = new frmAssetRevaluation();
            frmAssetRevaluation.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetRevaluation))
                frmAssetRevaluation.Show();
            Cursor.Current = Cursors.Default;
        }

        private void depreciationExpenseReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptDepExp frmRptDepExp = new frmRptDepExp();
            frmRptDepExp.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptDepExp))
                frmRptDepExp.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetAdditionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAdditionList frmRptAdditionList = new frmRptAdditionList();
            frmRptAdditionList.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAdditionList))
                frmRptAdditionList.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmAssetReigisterReport_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAssetRegister frmRptAssetRegister = new frmRptAssetRegister();
            frmRptAssetRegister.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetRegister))
                frmRptAssetRegister.Show();
            Cursor.Current = Cursors.Default;
        }

        private void depreciationJournalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptDepJournal frmRptDepJournal = new frmRptDepJournal();
            frmRptDepJournal.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptDepJournal))
                frmRptDepJournal.Show();
            Cursor.Current = Cursors.Default;
        }

        private void depreciationJournalChargeAmountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptDepJournalChargeAmt frmRptDepJournalChargeAmt = new frmRptDepJournalChargeAmt();
            frmRptDepJournalChargeAmt.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptDepJournalChargeAmt))
                frmRptDepJournalChargeAmt.Show();
            Cursor.Current = Cursors.Default;
        }

        private void additionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAdditionFA frmRptAdditionFA = new frmRptAdditionFA();
            frmRptAdditionFA.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAdditionFA))
                frmRptAdditionFA.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmDepJournalSummaryRpt_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptDepJournalSummary frmRptDepJournalSummary = new frmRptDepJournalSummary();
            frmRptDepJournalSummary.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptDepJournalSummary))
                frmRptDepJournalSummary.Show();
            Cursor.Current = Cursors.Default;
        }

        private void fixedAssetsStatementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptFAStmt frmRptFAStmt = new frmRptFAStmt();
            frmRptFAStmt.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptFAStmt))
                frmRptFAStmt.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmDepNCost_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptDepNCost frmRptDepNCost = new frmRptDepNCost();
            frmRptDepNCost.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptDepNCost))
                frmRptDepNCost.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetListingManualReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAssetListing frmRptAssetListing = new frmRptAssetListing();
            frmRptAssetListing.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetListing))
                frmRptAssetListing.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetTransferReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAssetTransfer frmRptAssetTransfer = new frmRptAssetTransfer();
            frmRptAssetTransfer.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetTransfer))
                frmRptAssetTransfer.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmAdjustCost_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAdjustmentCost frmAdjustmentCost = new frmAdjustmentCost();
            frmAdjustmentCost.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAdjustmentCost))
                frmAdjustmentCost.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmMothwiseDepCharge_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptMohtDepCharged frmRptMohtDepCharged = new frmRptMohtDepCharged();
            frmRptMohtDepCharged.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptMohtDepCharged))
                frmRptMohtDepCharged.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmMothwiseCostAddition_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmrptMonthlyCostAddition frmRptMohtDepCharged = new frmrptMonthlyCostAddition();
            frmRptMohtDepCharged.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptMohtDepCharged))
                frmRptMohtDepCharged.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmMothwiseCostDisposal_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmrptMonthlyCostDisposal frmRptMohtDepCharged = new frmrptMonthlyCostDisposal();
            frmRptMohtDepCharged.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptMohtDepCharged))
                frmRptMohtDepCharged.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmMonthWiseAll_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmrptMonthlyCostAddDisp frmRptMohtDepCharged = new frmrptMonthlyCostAddDisp();
            frmRptMohtDepCharged.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptMohtDepCharged))
                frmRptMohtDepCharged.Show();
            Cursor.Current = Cursors.Default;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAbout frmAbout = new frmAbout();
            frmAbout.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAbout))
                frmAbout.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetLocationEntryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetLocation frmAssetLocation = new frmAssetLocation();
            frmAssetLocation.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetLocation))
                frmAssetLocation.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetSubLocationEntryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetSubLocation frmAssetSubLocation = new frmAssetSubLocation();
            frmAssetSubLocation.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetSubLocation))
                frmAssetSubLocation.Show();
            Cursor.Current = Cursors.Default;
        }

        private void supplierEntryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmSupplier frmSupplier = new frmSupplier();
            frmSupplier.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmSupplier))
                frmSupplier.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsbAcusition_MouseHover(object sender, EventArgs e)
        {
            toolStripSub.Visible = true;
            tslAssetInfo.Visible = true;
            tslLandInfo.Visible = true;
            tslAdditionInfo.Visible = true;            
            tslAdjustmentInfo.Visible = false;
            tslRevalutionInfo.Visible = false;
            tslDepSetiings.Visible = false;
            tslDepProcess.Visible = false;
            tslRptAssetRegister.Visible = false;
            tslRptLandRegister.Visible = false;
            toolStripSeparator14.Visible = true;
            toolStripSeparator15.Visible = true;
            toolStripSeparator16.Visible = false;
            toolStripSeparator19.Visible = false;
            toolStripSeparator20.Visible = false;
            toolStripSeparator21.Visible = false;
            toolStripSeparator1.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void tsbTransfer_MouseHover(object sender, EventArgs e)
        {
            toolStripSub.Visible = false;
            tslAssetInfo.Visible = false;
            tslLandInfo.Visible = false;
            tslAdditionInfo.Visible = false;
            tslAdjustmentInfo.Visible = false;
            tslRevalutionInfo.Visible = false;
            tslDepSetiings.Visible = false;
            tslDepProcess.Visible = false;
            tslRptAssetRegister.Visible = false;
            tslRptLandRegister.Visible = false;
            toolStripSeparator14.Visible = false;
            toolStripSeparator15.Visible = false;
            toolStripSeparator16.Visible = false;
            toolStripSeparator19.Visible = false;
            toolStripSeparator20.Visible = false;
            toolStripSeparator21.Visible = false;
            toolStripSeparator1.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void tsbDisposal_MouseHover(object sender, EventArgs e)
        {
            toolStripSub.Visible = false;
            tslAssetInfo.Visible = false;
            tslLandInfo.Visible = false;
            tslAdditionInfo.Visible = false;
             tslAdjustmentInfo.Visible = false;
            tslRevalutionInfo.Visible = false;
            tslDepSetiings.Visible = false;
            tslDepProcess.Visible = false;
            tslRptAssetRegister.Visible = false;
            tslRptLandRegister.Visible = false;
            toolStripSeparator14.Visible = false;
            toolStripSeparator15.Visible = false;
            toolStripSeparator16.Visible = false;
            toolStripSeparator19.Visible = false;
            toolStripSeparator20.Visible = false;
            toolStripSeparator21.Visible = false;
            toolStripSeparator1.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void tsbAdjustment_MouseHover(object sender, EventArgs e)
        {
            toolStripSub.Visible = true;
            tslAssetInfo.Visible = false;
            tslLandInfo.Visible = false;
            tslAdditionInfo.Visible = false;           
            tslAdjustmentInfo.Visible = true;
            tslRevalutionInfo.Visible = true;
            tslDepSetiings.Visible = false;
            tslDepProcess.Visible = false;
            tslRptAssetRegister.Visible = false;
            tslRptLandRegister.Visible = false;
            toolStripSeparator14.Visible = false;
            toolStripSeparator15.Visible = false;
            toolStripSeparator16.Visible = false;
            toolStripSeparator19.Visible = true;
            toolStripSeparator20.Visible = false;
            toolStripSeparator21.Visible = false;
            toolStripSeparator1.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void tsbDepreciation_MouseHover(object sender, EventArgs e)
        {
            toolStripSub.Visible = true;
            tslAssetInfo.Visible = false;
            tslLandInfo.Visible = false;
            tslAdditionInfo.Visible = false;
            tslAdjustmentInfo.Visible = false;
            tslRevalutionInfo.Visible = false;
            tslDepSetiings.Visible = true;
            tslDepProcess.Visible = true;
            tslRptAssetRegister.Visible = false;
            tslRptLandRegister.Visible = false;
            toolStripSeparator14.Visible = false;
            toolStripSeparator15.Visible = false;
            toolStripSeparator16.Visible = false;
            toolStripSeparator19.Visible = false;
            toolStripSeparator20.Visible = false;
            toolStripSeparator21.Visible = true;
            toolStripSeparator1.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void tslAssetInfo_MouseHover(object sender, EventArgs e)
        {
            tslAssetInfo.ForeColor = Color.Blue;
            tslAssetInfo.Font = new Font(tslAssetInfo.Font.Name, tslAssetInfo.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslAssetInfo_MouseLeave(object sender, EventArgs e)
        {
            tslAssetInfo.ForeColor = Color.Black;
            tslAssetInfo.Font = new Font(tslAssetInfo.Font.Name, tslAssetInfo.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslAssetInfo_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetConfigN frmAssetConfig = new frmAssetConfigN();
            frmAssetConfig.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetConfig))
                frmAssetConfig.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslLandInfo_MouseHover(object sender, EventArgs e)
        {
            tslLandInfo.ForeColor = Color.Blue;
            tslLandInfo.Font = new Font(tslLandInfo.Font.Name, tslLandInfo.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslLandInfo_MouseLeave(object sender, EventArgs e)
        {
            tslLandInfo.ForeColor = Color.Black;
            tslLandInfo.Font = new Font(tslLandInfo.Font.Name, tslLandInfo.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslLandInfo_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetConfigLand frmAssetConfigLand = new frmAssetConfigLand();
            frmAssetConfigLand.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetConfigLand))
                frmAssetConfigLand.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslAdditionInfo_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAdditionToAsset frmAdditionToAsset = new frmAdditionToAsset();
            frmAdditionToAsset.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAdditionToAsset))
                frmAdditionToAsset.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslAdditionInfo_MouseHover(object sender, EventArgs e)
        {
            tslAdditionInfo.ForeColor = Color.Blue;
            tslAdditionInfo.Font = new Font(tslAdditionInfo.Font.Name, tslAdditionInfo.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslAdditionInfo_MouseLeave(object sender, EventArgs e)
        {
            tslAdditionInfo.ForeColor = Color.Black;
            tslAdditionInfo.Font = new Font(tslAdditionInfo.Font.Name, tslAdditionInfo.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslTransferEntry_MouseLeave(object sender, EventArgs e)
        {

        }

        private void tslDisposalInfo_MouseHover(object sender, EventArgs e)
        {

        }

        private void tslAdjustmentInfo_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAdjustmentCost frmAdjustmentCost = new frmAdjustmentCost();
            frmAdjustmentCost.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAdjustmentCost))
                frmAdjustmentCost.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslAdjustmentInfo_MouseHover(object sender, EventArgs e)
        {
            tslAdjustmentInfo.ForeColor = Color.Blue;
            tslAdjustmentInfo.Font = new Font(tslAdjustmentInfo.Font.Name, tslAdjustmentInfo.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslAdjustmentInfo_MouseLeave(object sender, EventArgs e)
        {
            tslAdjustmentInfo.ForeColor = Color.Black;
            tslAdjustmentInfo.Font = new Font(tslAdjustmentInfo.Font.Name, tslAdjustmentInfo.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslRevalutionInfo_MouseLeave(object sender, EventArgs e)
        {
            tslRevalutionInfo.ForeColor = Color.Black;
            tslRevalutionInfo.Font = new Font(tslRevalutionInfo.Font.Name, tslRevalutionInfo.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;        
        }

        private void tslRevalutionInfo_MouseHover(object sender, EventArgs e)
        {
            tslRevalutionInfo.ForeColor = Color.Blue;
            tslRevalutionInfo.Font = new Font(tslRevalutionInfo.Font.Name, tslRevalutionInfo.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslRevalutionInfo_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetRevaluation frmAssetRevaluation = new frmAssetRevaluation();
            frmAssetRevaluation.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetRevaluation))
                frmAssetRevaluation.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslDepSetiings_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmDepreciationConfig frmDepreciationConfig = new frmDepreciationConfig();
            frmDepreciationConfig.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmDepreciationConfig))
                frmDepreciationConfig.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslDepSetiings_MouseLeave(object sender, EventArgs e)
        {
            tslDepSetiings.ForeColor = Color.Black;
            tslDepSetiings.Font = new Font(tslDepSetiings.Font.Name, tslDepSetiings.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslDepSetiings_MouseHover(object sender, EventArgs e)
        {
            tslDepSetiings.ForeColor = Color.Blue;
            tslDepSetiings.Font = new Font(tslDepSetiings.Font.Name, tslDepSetiings.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslDepProcess_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmDepreciationProcess frmDepreciationProcess = new frmDepreciationProcess();
            frmDepreciationProcess.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmDepreciationProcess))
                frmDepreciationProcess.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslDepProcess_MouseHover(object sender, EventArgs e)
        {
            tslDepProcess.ForeColor = Color.Blue;
            tslDepProcess.Font = new Font(tslDepProcess.Font.Name, tslDepProcess.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslDepProcess_MouseLeave(object sender, EventArgs e)
        {
            tslDepProcess.ForeColor = Color.Black;
            tslDepProcess.Font = new Font(tslDepProcess.Font.Name, tslDepProcess.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void toolStripToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripMain.Visible = toolStripToolStripMenuItem.Checked;
            toolStripSub.Visible = false;
        }

        private void tsbReports_MouseHover(object sender, EventArgs e)
        {
            toolStripSub.Visible = true;
            tslAssetInfo.Visible = false;
            tslLandInfo.Visible = false;
            tslAdditionInfo.Visible = false;
            tslAdjustmentInfo.Visible = false;
            tslRevalutionInfo.Visible = false;
            tslDepSetiings.Visible = false;
            tslDepProcess.Visible = false;
            tslRptAssetRegister.Visible = true;
            tslRptLandRegister.Visible = true;
            toolStripSeparator14.Visible = false;
            toolStripSeparator15.Visible = false;
            toolStripSeparator16.Visible = false;
            toolStripSeparator19.Visible = false;
            toolStripSeparator20.Visible = false;
            toolStripSeparator21.Visible = false;
            toolStripSeparator1.Visible = false;
            toolStripSeparator2.Visible = true;
        }

        private void tslRptLandRegister_MouseHover(object sender, EventArgs e)
        {
            tslRptLandRegister.ForeColor = Color.Blue;
            tslRptLandRegister.Font = new Font(tslRptLandRegister.Font.Name, tslRptLandRegister.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslRptLandRegister_MouseLeave(object sender, EventArgs e)
        {
            tslRptLandRegister.ForeColor = Color.Black;
            tslRptLandRegister.Font = new Font(tslRptLandRegister.Font.Name, tslRptLandRegister.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslRptLandRegister_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            Reports.frmRptAssetList frmRptAssetList = new Reports.frmRptAssetList();
            frmRptAssetList.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetList))
                frmRptAssetList.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tslRptAssetRegister_MouseHover(object sender, EventArgs e)
        {
            tslRptAssetRegister.ForeColor = Color.Blue;
            tslRptAssetRegister.Font = new Font(tslRptAssetRegister.Font.Name, tslRptAssetRegister.Font.SizeInPoints, FontStyle.Underline);
            Cursor = Cursors.Hand;
        }

        private void tslRptAssetRegister_MouseLeave(object sender, EventArgs e)
        {
            tslRptAssetRegister.ForeColor = Color.Black;
            tslRptAssetRegister.Font = new Font(tslRptAssetRegister.Font.Name, tslRptAssetRegister.Font.SizeInPoints, FontStyle.Regular);
            Cursor = Cursors.Default;
        }

        private void tslRptAssetRegister_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAssetRegister frmRptAssetRegister = new frmRptAssetRegister();
            frmRptAssetRegister.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetRegister))
                frmRptAssetRegister.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsbTransfer_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetTransfer frmAssetTransfer = new frmAssetTransfer();
            frmAssetTransfer.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetTransfer))
                frmAssetTransfer.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsbDisposal_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmSoldDamage frmSoldDamage = new frmSoldDamage();
            frmSoldDamage.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmSoldDamage))
                frmSoldDamage.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmLandAdjustEntry_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmLandAdjustmentCost frmLandAdjustmentCost = new frmLandAdjustmentCost();
            frmLandAdjustmentCost.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmLandAdjustmentCost))
                frmLandAdjustmentCost.Show();
            Cursor.Current = Cursors.Default;
        }

        private void tsmDisposalOfFARegister_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptDisposalInfo frmRptDisposalInfo = new frmRptDisposalInfo();
            frmRptDisposalInfo.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptDisposalInfo))
                frmRptDisposalInfo.Show();
            Cursor.Current = Cursors.Default;
        }

        private void assetHistoricalRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAssetLifeHistory frmRptAssetLifeHistory = new frmRptAssetLifeHistory();
            frmRptAssetLifeHistory.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAssetLifeHistory))
                frmRptAssetLifeHistory.Show();
            Cursor.Current = Cursors.Default;
        }

        private void depreciationChagedOnRevaluedAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptRevAmtDepCharge frmRptRevAmtDepCharge = new frmRptRevAmtDepCharge();
            frmRptRevAmtDepCharge.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptRevAmtDepCharge))
                frmRptRevAmtDepCharge.Show();
            Cursor.Current = Cursors.Default;
        }

        private void miscelleneousReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptMiscelleneous frmRptMiscelleneous = new frmRptMiscelleneous();
            frmRptMiscelleneous.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptMiscelleneous))
                frmRptMiscelleneous.Show();
            Cursor.Current = Cursors.Default;
        }

        private void costcentrewiseStatementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptCostcentrewiseStmt frmRptCostcentrewiseStmt = new frmRptCostcentrewiseStmt();
            frmRptCostcentrewiseStmt.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptCostcentrewiseStmt))
                frmRptCostcentrewiseStmt.Show();
            Cursor.Current = Cursors.Default;
        }

        private void AcInfoEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmAssetAccounts frmAssetAccounts = new frmAssetAccounts();
            frmAssetAccounts.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmAssetAccounts))
                frmAssetAccounts.Show();
            Cursor.Current = Cursors.Default;
        }

        private void accountLedgerFARReconciliationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            frmRptAcFarReconciliation frmRptAcFarReconciliation = new frmRptAcFarReconciliation();
            frmRptAcFarReconciliation.MdiParent = this;
            if (!dbMdiCon.IsFormOpen(frmRptAcFarReconciliation))
                frmRptAcFarReconciliation.Show();
            Cursor.Current = Cursors.Default;
        }
    }
}
