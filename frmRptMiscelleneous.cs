﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace AssetRegister
{
    public partial class frmRptMiscelleneous : Form
    {
        private clsCommonFunctions dbConRptAddList = new clsCommonFunctions();
        private int AssetId = 0;

        public frmRptMiscelleneous()
        {
            InitializeComponent();
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConRptAddList.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvListData.Focus();

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                //AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                //AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            //AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void frmRptAdditionList_Load(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT AssetId,
                                                                   AssetCode
                                                            FROM   AssetRegistar.dbo.AR_AssetConfig");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            
            try
            {
                if (rbNewAssetForm.Checked || rbTransferForm.Checked || rbDisposalForm.Checked || rbLandDoc.Checked || rbExistAddition.Checked
                    || rbLandExisting.Checked)
                {

                    frmLoading frmLoading = new frmLoading();
                    frmLoading.Show();
                    frmLoading.TopMost = true;

                    if (AssetId > 0)
                    {
                        if (rbNewAssetForm.Checked)
                        {
                            cryReport.Load(Application.StartupPath + @"\Reports\rptNewAssetForm.rpt");
                            cryReport.SetParameterValue("@AssetId", AssetId);
                        }
                        else if (rbTransferForm.Checked)
                        {
                            //cryReport.Load(Application.StartupPath + @"\Reports\rptAssetTransferForm.rpt");
                            //cryReport.SetParameterValue("@AssetId", AssetId);
                            cryReport.Load(Application.StartupPath + @"\Reports\rptAssetTransferForm_new.rpt");
                            cryReport.SetParameterValue("@AssetId", AssetId);
                        }
                        else if (rbDisposalForm.Checked) 
                        {
                            cryReport.Load(Application.StartupPath + @"\Reports\rptAssetDisposalForm.rpt");
                            cryReport.SetParameterValue("@AssetId", AssetId);
                        }
                        else if (rbLandDoc.Checked) 
                        {
                            cryReport.Load(Application.StartupPath + @"\Reports\rptLandDocForm.rpt");
                            cryReport.SetParameterValue("@AssetId", AssetId);
                        }
                        else if (rbExistAddition.Checked)
                        {
                            cryReport.Load(Application.StartupPath + @"\Reports\rptExistingAssetAddForm.rpt");
                            cryReport.SetParameterValue("@AddTrnNo", AssetId);
                        }
                        else if (rbLandExisting.Checked) 
                        {
                            cryReport.Load(Application.StartupPath + @"\Reports\rptExistingAssetAddForm(Land).rpt");
                            cryReport.SetParameterValue("@AddTrnNo", AssetId);
                        }

                        //cryReport.SetParameterValue("@AssetId", AssetId);
                        //cryReport.SetParameterValue("@UserId", frmLogin.userId);
                        dbConRptAddList.SetReportLogon(cryReport);
                        rptForm.crViewer.ReportSource = null;
                        rptForm.crViewer.ReportSource = cryReport;
                        rptForm.Text = "Asset Miscelleneous Report";
                        rptForm.crViewer.Refresh();
                        rptForm.Show();
                        frmLoading.Hide();
                    }
                    else
                    {
                        frmLoading.Hide();
                        MessageBox.Show("Please Select Specific Asset.","Select", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }                    
                }
                else
                {
                    //frmLoading.Hide();
                    MessageBox.Show("Please check at least one report to display", "Not checked", 
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                //frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }

        private void rbNewAssetForm_CheckedChanged(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT AssetId,
                                                                   AssetCode
                                                            FROM   AssetRegistar.dbo.AR_AssetConfig WHERE AssetCode NOT LIKE 'KDSALD%'");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }

        private void rbTransferForm_CheckedChanged(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT aac.AssetId,
                                                                   aac.AssetCode
                                                            FROM   AssetRegistar.dbo.AR_AssetConfig aac 
                                                                INNER JOIN AR_AsseetXfer aax ON aax.AssetId = aac.AssetId");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }

        private void rbDisposalForm_CheckedChanged(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT aac.AssetId,
                                                                   aac.AssetCode
                                                            FROM   AssetRegistar.dbo.AR_AssetConfig aac
                                                            INNER JOIN AR_AssetDisposalView aadv ON aadv.AssetId = aac.AssetId");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }

        private void rbLandDoc_CheckedChanged(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT aac.AssetId,
                                                                   aac.AssetCode
                                                            FROM   AssetRegistar.dbo.AR_AssetConfig aac
                                                            INNER JOIN AR_LandDetails ald ON ald.AssetId = aac.AssetId");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }

        private void rbExistAddition_CheckedChanged(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT TrnNo, AdditionItemCode FROM AR_AssetAddition WHERE AdditionItemCode NOT LIKE 'KDSALD%'");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }

        private void rbLandExisting_CheckedChanged(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"SELECT TrnNo, AdditionItemCode FROM AR_AssetAddition WHERE AdditionItemCode LIKE 'KDSALD%'");
            if (dgvListData.Rows.Count > 0)
            {
                dgvListData.Columns[0].Visible = false;
                dgvListData.Columns[1].Width = 145;
            }
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
        }
    }
}
