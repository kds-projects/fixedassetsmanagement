﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using AssetRegister.Properties;
using System.Xml;
using System.Reflection;
using System.Net.Sockets;
using System.Net;

namespace AssetRegister
{
    public partial class frmLogin : Form
    {
        private clsCommonFunctions dbConLogin = new clsCommonFunctions();
        public static string userFullName = "", userType = "", ipAddress = "",
            pList = "", loginId = "", userEmail = "";
        public static int userId = 0;
        IPHostEntry HostEntry = null;
        public static bool isLogin = false;

        public frmLogin(bool isloggedin)
        {
            isLogin = isloggedin;
            InitializeComponent();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Application.Exit();
                return true;
            }

            return false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dbConLogin.CheckEmptyOrNull(txtLoginId, "Enter Login ID") &&
                    !dbConLogin.CheckEmptyOrNull(txtLoginPassword, "Enter Password"))
                {
                    SqlDataReader loginDtRdr;

                    loginDtRdr = (SqlDataReader)dbConLogin.DBCRUDFunction("Execute prcLogin '"
                                                                + dbConLogin.SingleQuoteHandler(txtLoginId) + "','"
                                                                + dbConLogin.SingleQuoteHandler(txtLoginPassword) + "','"
                                                                + ipAddress + "','LOGIN', '[AssetRegistar].[dbo].[AR_UsersLog]', '" + this.Name + "'", true);


                    if (loginDtRdr.Read())
                    {
                        userId = Convert.ToInt32(loginDtRdr["UserId"].ToString());
                        loginId = txtLoginId.Text.Trim();
                        userFullName = loginDtRdr["UserFullName"].ToString();
                        userType = loginDtRdr["UserType"].ToString();
                        userEmail = loginDtRdr["Email"].ToString();
                        pList = loginDtRdr["PrivilegeList"].ToString();
                        isLogin = true;
                    }

                    if (isLogin)
                    {
                        Settings.Default.LoginId = txtLoginId.Text;
                        Settings.Default.Save();
                        this.Hide();
                        MDIParent MDIMain = new MDIParent(isLogin);
                        MDIMain.Show();
                    }
                    else
                    {
                        MessageBox.Show("Invalid Login", "Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            lblVersion.Text = "Version : " + GetPublishedVersion().ToString();
            
            HostEntry = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress IP in HostEntry.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = IP.ToString();
                    break;
                }
            }

            if (frmLogin.ipAddress == "192.168.0.26"
                || frmLogin.ipAddress == "192.168.0.111"
                || frmLogin.ipAddress == "192.168.0.96"
                || frmLogin.ipAddress == "192.168.0.99"
                || frmLogin.ipAddress == "192.168.0.97"
                || frmLogin.ipAddress == "192.168.0.50"
                || frmLogin.ipAddress == "192.168.0.5"
                || frmLogin.ipAddress == "192.168.0.98"
                || frmLogin.ipAddress == "192.168.0.100")
            {
                txtLoginId.Text = "Admin";
                txtLoginPassword.Text = "faAdmin123";
            }
            else
            {
                txtLoginId.Text = Settings.Default.LoginId;
                txtLoginPassword.Focus();
            }  
        }

        public static Version GetPublishedVersion()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Assembly asmCurrent = Assembly.GetExecutingAssembly();
            string executePath = new Uri(asmCurrent.GetName().CodeBase).LocalPath;

            xmlDoc.Load(executePath + ".manifest");
            string retval = string.Empty;
            if (xmlDoc.HasChildNodes)
            {
                retval = xmlDoc.ChildNodes[1].ChildNodes[0].Attributes.GetNamedItem("version").Value.ToString();
            }
            return new Version(retval);
        }

        private void txtLoginId_TextChanged(object sender, EventArgs e)
        {
            dbConLogin.MakeTextUppercase(txtLoginId);
        }
    }
}
