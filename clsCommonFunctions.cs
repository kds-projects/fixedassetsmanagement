﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace AssetRegister
{
    class clsCommonFunctions : clsDBCon
    {
        private ConnectionInfo rptConnection = new ConnectionInfo();
        public ToolTip tTip;
        public ErrorProvider errPvdr = new ErrorProvider();
        public DateTime ServerCurrentDateTime;
        public string LinkServer = "[SalesCube].[dbo].";

        public void SetReportLogon(ReportDocument rptDoc)
        {
            rptConnection.ServerName = DBHostConnected;
            rptConnection.DatabaseName = DBName;
            rptConnection.UserID = DBUserId;
            rptConnection.Password = DBPassword;

            foreach (ReportDocument subRpt in rptDoc.Subreports)
            {
                foreach (Table crTable in subRpt.Database.Tables)
                {
                    TableLogOnInfo logInfo = crTable.LogOnInfo;
                    logInfo.ConnectionInfo = rptConnection;
                    crTable.ApplyLogOnInfo(logInfo);
                }
            }

            foreach (Table crTable in rptDoc.Database.Tables)
            {
                TableLogOnInfo loi = crTable.LogOnInfo;
                loi.ConnectionInfo = rptConnection;
                crTable.ApplyLogOnInfo(loi);
            }
        }

        public string SingleQuoteHandler(TextBox txt)
        {
            return txt.Text.Trim().Replace("'", "''");
        }
        
        public string SingleQuoteHandler(string txt)
        {
            return txt.Trim().Replace("'", "''");
        }

        public bool CheckEmptyOrNull(Control ctrl, string msg)
        {
            if (string.IsNullOrEmpty(ctrl.Text.Trim()))
            {
                errPvdr.SetError(ctrl, msg);
                ctrl.BackColor = Color.Red;
                tTip = new ToolTip();
                tTip.Show(msg, ctrl, 3000);
                ctrl.Focus();
                return true;
            }

            return false;
        }

        public void DisposeChecking(Control ctrl)
        {
            errPvdr.Dispose();
            ctrl.BackColor = Color.White;
            //tTip.Dispose();
        }

        public bool IsFormOpen(Form frm)
        {
            foreach (Form form in Application.OpenForms)
                if (form.Name == frm.Name)
                    return true;
            return false;
        }

        public void MakeTextUppercase(TextBox txt)
        {
            int start = txt.SelectionStart;
            txt.Text = txt.Text.ToUpper();
            txt.SelectionStart = start;
        }

        public DateTime GetServerCurrentDateTime()
        {
            SqlDataReader dtRdr;
            dtRdr = (SqlDataReader)DBCRUDFunction("EXEC prcGetServerDateTime", true);
            if (dtRdr.Read())
            {
                ServerCurrentDateTime = Convert.ToDateTime(dtRdr["ServerDateTime"].ToString());
            }
            dtRdr.Close();
            return ServerCurrentDateTime;
        }

        public void SelectionTextString(TextBox txt)
        {
            if (!String.IsNullOrEmpty(txt.Text))
            {
                txt.SelectionStart = 0;
                txt.SelectionLength = txt.ToString().Length;
            }
        }

        public void DecimalNumbers(TextBox txt, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar))
            {
                int dotIndex = txt.Text.IndexOf('.');
                if (char.IsDigit(e.KeyChar))
                {
                    if (dotIndex != -1 && dotIndex < txt.SelectionStart && txt.Text.Substring(dotIndex + 1).Length >= 2)
                    {
                        e.Handled = true;
                    }
                }
                else
                    e.Handled = e.KeyChar != '.' ||
                                dotIndex != -1 ||
                                txt.Text.Length == 0 ||
                                txt.SelectionStart + 2 < txt.Text.Length;
            }
        }

        public void DecimalNumbersWithSign(object sender, KeyPressEventArgs e) 
        {
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != '.' && e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }

            if (e.KeyChar == '-' && (sender as TextBox).SelectionStart > 0)
            {
                e.Handled = true;
            }
        }

        public void IntergersOnly(TextBox txt, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar))
            {                
                if (!char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
        }

        public void FillComboBox(ComboBox cmb, string sql)
        {
            SqlDataReader cmbDtRdr;
            cmbDtRdr = (SqlDataReader)DBCRUDFunction(sql, true);

            DataTable cmbDtTbl = new DataTable();
            cmbDtTbl.Load(cmbDtRdr);

            cmb.DataSource = cmbDtTbl;
            cmb.ValueMember = cmbDtTbl.Columns[0].ToString();
            cmb.DisplayMember = cmbDtTbl.Columns[1].ToString();
            cmb.Invalidate();
            cmbDtRdr.Close();
        }

        public void FillDataGridView(DataGridView dgv, string sql)
        {
            SqlDataReader dgvDtRdr;
            dgvDtRdr = (SqlDataReader)DBCRUDFunction(sql, true);

            DataTable dgvDtTbl = new DataTable();
            dgvDtTbl.Load(dgvDtRdr);

	        foreach (DataColumn col in dgvDtTbl.Columns)
                col.ReadOnly = false;

            dgv.DataSource = dgvDtTbl;
            dgvDtRdr.Close();
        }

        public void SentEmail(string frmEmail, string toEmail, string emailSubject, string emailBody, string ccEmail, string bccEmail)
        {
            //string sql = "";

            try
            {
                SmtpClient SMTPServer = new SmtpClient();
                MailMessage Mail = new MailMessage();
                SMTPServer.Credentials = new NetworkCredential("abdul.moktadir@kdsgroup.net", "C023019");
                SMTPServer.Port = 25;
                SMTPServer.Host = "203.82.207.6";
                Mail.From = new MailAddress(frmEmail);
                Mail.To.Add(toEmail);
                if (!string.IsNullOrEmpty(ccEmail))
                {
                    Mail.CC.Add(ccEmail);
                }
                if (!string.IsNullOrEmpty(bccEmail))
                {
                    Mail.Bcc.Add(bccEmail);
                }
                Mail.Subject = emailSubject;
                Mail.IsBodyHtml = true;
                Mail.Body = emailBody;
                Mail.Priority = MailPriority.Normal;
                SMTPServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SMTPServer.Send(Mail);

                //                sql = @"INSERT INTO NSDMailQ (FrmMail,ToMail,CCMail,BCCMail,Subject,Body,SentTime,Status) 
                //                        VALUES('" + frmEmail + "','" + toEmail + "','" + ccEmail + "','" + bccEmail
                //                        + "','" + emailSubject + "','" + AddSlashes(emailBody) + "','" + DateTime.Today + " "
                //                        + DateTime.Now + "',1)";

                //                DBInsertUpdateDeleteFunctin(sql);
            }
            catch (Exception ex)
            {
                //                sql = @sql = @"INSERT INTO NSDMailQ (FrmMail,ToMail,CCMail,BCCMail,Subject,Body,SentTime,Status) 
                //                            VALUES('" + frmEmail + "','" + toEmail + "','" + ccEmail + "','" + bccEmail
                //                            + "','" + emailSubject + "','" + AddSlashes(emailBody) + "','" + DateTime.Today + " "
                //                            + DateTime.Now + "',0)";

                //                DBInsertUpdateDeleteFunctin(sql);
                MessageBox.Show(ex.Message, "Mail Server Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public bool CheckNullorEmptyDGV(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                for (int j = 0; j < dgv.Columns.Count; j++)
                {
                    if (j != 0)
                    {
                        if (dgv.Rows[i].Cells[j].Value == null)
                            return true;
                    }
                }
            }

            return false;
        }

        public bool IsAvail(DataGridView dgv, string item)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].Cells[1].Value.ToString() == item)
                    return true;
            }
            return false;
        }

        public bool InsertSentEmail(string frmEmail, string tEmail, string ccEmail, string bccEmail, string subject,
            string body, string emailType, string projectName, string tempFileName, string eFormat, Stream attachmentStream)
        {
            if (attachmentStream == null)
            {
                string sql = @"INSERT INTO [EmailCube].[dbo].[EMailCollection] ([EmailType],[ModuleName],[EmailFrom],[EmailTo],[EmailCC],[EmailBCC] "
                               + "           ,[EmailSubject],[EmailBody],[EmailAttachmentLink],[HasAttachment],[CreateAttachment],[AllatchmentFileName] "
                               + "           ,[ReportName],[ParameterList],[ParameterValueList],[RelationType],[RelationId],[RelationSubId] "
                               + "           ,[EntryDate],[CreatedBy],[SendingStatus]) "
                               + "     VALUES "
                               + "           ('" + emailType + "' "
                               + "           ,'" + projectName + "' "
                               + "           ,'" + frmEmail + "' "
                               + "           ,'" + tEmail + "' "
                               + "           ,'" + ccEmail + "' "
                               + "           ,'" + bccEmail + "' "
                               + "           ,'" + subject + "' "
                               + "           ,'" + body + "' "
                               + "           ,'' "
                               + "           ,0 "
                               + "           ,0 "
                               + "           ,'" + tempFileName + "." + eFormat + "' "
                               + "           ,'' "
                               + "           ,'' "
                               + "           ,'' "
                               + "           ,'' "
                               + "           ,0 "
                               + "           ,0 "
                               + "           ,GETDATE() "
                               + "           ,1 "
                               + "           ,0 )";

                if ((bool)DBCRUDFunction(sql, false))
                    return true;
            }
            else
            {
                string sql = "INSERT INTO [EmailCube].[dbo].[EMailCollection] ([EmailType],[ModuleName],[EmailFrom],[EmailTo],[EmailCC],[EmailBCC] "
                           + "           ,[EmailSubject],[EmailBody],[EmailAttachmentLink],[HasAttachment],[CreateAttachment],[AllatchmentFileName] "
                           + "           ,[Attachment],[ReportName],[ParameterList],[ParameterValueList],[RelationType],[RelationId],[RelationSubId] "
                           + "           ,[EntryDate],[CreatedBy],[SendingStatus]) "
                           + "     VALUES "
                           + "           ('" + emailType + "' "
                           + "           ,'" + projectName + "' "
                           + "           ,'" + frmEmail + "' "
                           + "           ,'" + tEmail + "' "
                           + "           ,'" + ccEmail + "' "
                           + "           ,'" + bccEmail + "' "
                           + "           ,'" + subject + "' "
                           + "           ,'" + body + "' "
                           + "           ,'' "
                           + "           ,1 "
                           + "           ,0 "
                           + "           ,'" + tempFileName + "." + eFormat + "' "
                           + "           ,@binaryValue"
                           + "           ,'' "
                           + "           ,'' "
                           + "           ,'' "
                           + "           ,'' "
                           + "           ,0 "
                           + "           ,0 "
                           + "           ,getdate() "
                           + "           ,1 "
                           + "           ,0 )";
                byte[] bytesInStream = new byte[attachmentStream.Length];
                attachmentStream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                if (DBCon.State.Equals(ConnectionState.Open))
                    DBCon.Close();
                DBCon.Open();
                using (SqlCommand cmd = new SqlCommand(sql, DBCon))
                {
                    // Replace 8000, below, with the correct size of the field
                    cmd.Parameters.Add("@binaryValue", SqlDbType.VarBinary, 2000000).Value = bytesInStream;
                    cmd.ExecuteNonQuery();
                    return true;
                    //MessageBox.Show("sENT");
                    // MyLogEvent.WriteEntry("Successfully sent file " + tempFileName + " on " + DateTime.Now.ToString());                                
                }

                //if (DBInsertUpdateDeleteFunction(sql))
                //    return true;
            }

            return false;
        }
        
        public string CreateSQLStatement(string table, string[] column, string[] values,
            string operation, string[] whCols, string[] whVals)
        {
            string SQL = "";
            string comma = "";

            if (operation == "INSERT")
            {
                SQL = operation + " INTO " + table + " (";

                for (int i = 0; i < column.Count(); i++)
                {
                    SQL = SQL + comma + column[i];
                    comma = ",";
                }

                SQL += ") VALUES (";

                comma = "";

                for (int j = 0; j < values.Count(); j++)
                {
                    if (IsNumeric(values[j]))
                    {
                        SQL += comma + values[j];
                    }
                    else
                    {
                        SQL += comma + "'" + values[j] + "'";
                    }

                    comma = ",";
                }

                SQL += ");";
            }
            else if (operation == "UPDATE")
            {
                SQL = operation + " " + table + " SET ";

                for (int i = 0; i < column.Count(); i++)
                {                    
                    if (IsNumeric(values[i]))
                    {
                        SQL = SQL + comma + column[i] + " = " + values[i];
                    }
                    else
                    {
                        SQL = SQL + comma + column[i] + " = '" + values[i] + "'";
                    }

                    comma = ",";
                }

                comma = " WHERE ";

                for (int i = 0; i < whCols.Count(); i++)
                {
                    if (IsNumeric(whVals[i]))
                    {
                        SQL = SQL + comma + whCols[i] + " = " + whVals[i];
                    }
                    else
                    {
                        SQL = SQL + comma + whCols[i] + " = '" + whVals[i] + "'";
                    }

                    comma = " AND ";
                }

                SQL += ";";
            }
            else if (operation == "DELETE")
            {
                SQL = operation + " " + table;
                comma = " WHERE ";
                for (int i = 0; i < whCols.Count(); i++)
                {
                    if (IsNumeric(whVals[i]))
                    {
                        SQL = SQL + comma + whCols[i] + " = " + whVals[i];
                    }
                    else
                    {
                        SQL = SQL + comma + whCols[i] + " = '" + whVals[i] + "'";
                    }

                    comma = " AND ";
                }

                SQL += ";";
            }

            return SQL;
        }

        public string CreateSQLStatement(string table, string[] column, string[] values,
            string operation, string[] whCols, string[] whVals, string[] whOperators)
        {
            string SQL = "";
            string comma = "";

            if (operation == "INSERT")
            {
                SQL = operation + " INTO " + table + " (";

                for (int i = 0; i < column.Count(); i++)
                {
                    SQL = SQL + comma + column[i];
                    comma = ",";
                }

                SQL += ") VALUES (";

                comma = "";

                for (int j = 0; j < values.Count(); j++)
                {
                    if (IsNumeric(values[j]))
                    {
                        SQL += comma + values[j];
                    }
                    else
                    {
                        SQL += comma + "'" + values[j] + "'";
                    }

                    comma = ",";
                }

                SQL += ");";
            }
            else if (operation == "UPDATE")
            {
                SQL = operation + " " + table + " SET ";

                for (int i = 0; i < column.Count(); i++)
                {
                    if (IsNumeric(values[i]))
                    {
                        SQL = SQL + comma + column[i] + " = " + values[i];
                    }
                    else
                    {
                        SQL = SQL + comma + column[i] + " = '" + values[i] + "'";
                    }

                    comma = ",";
                }

                comma = " WHERE ";

                for (int i = 0; i < whCols.Count(); i++)
                {
                    if (IsNumeric(whVals[i]))
                    {
                        SQL = SQL + comma + whCols[i] + whOperators[i] + whVals[i];
                    }
                    else
                    {
                        SQL = SQL + comma + whCols[i] + whOperators[i] + "'" + whVals[i] + "'";
                    }

                    comma = " AND ";
                }

                SQL += ";";
            }
            else if (operation == "DELETE")
            {
                SQL = operation + " " + table;
                comma = " WHERE ";
                for (int i = 0; i < whCols.Count(); i++)
                {
                    if (IsNumeric(whVals[i]))
                    {
                        SQL = SQL + comma + whCols[i] + whOperators[i] + whVals[i];
                    }
                    else
                    {
                        SQL = SQL + comma + whCols[i] + whOperators[i] + "'" + whVals[i] + "'";
                    }

                    comma = " AND ";
                }

                SQL += ";";
            }

            return SQL;
        }

        public void SetTextBoxBorderColor(TextBox[] txt, PaintEventArgs e, Color color)
        {
            Pen p = new Pen(color);
            Graphics g = e.Graphics;
            int variance = 2;

            for (int i = 0; i < txt.Count(); i++)
            {
                txt[i].BorderStyle = BorderStyle.None;
                g.DrawRectangle(p, new Rectangle(txt[i].Location.X - variance,
                               txt[i].Location.Y - variance, txt[i].Width + variance,
                               txt[i].Height + variance));
            }
        }

        public DataTable GetContentAsDataTable(DataGridView dgv, string[] columns)
        {
            try
            {
                var dt = new DataTable();

                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    if (IsColMatched(dgv.Columns[i].Name, columns))
                    {
                        dt.Columns.Add();
                    }
                }

                object[] cellValues = new object[dt.Columns.Count];
                foreach (DataGridViewRow dr in dgv.Rows)
                {
                    if (Convert.ToBoolean(dr.Cells["chkBox"].Value) == true)
                    {
                        for (int i = 0, j = 0; i < dr.Cells.Count; i++)
                        {
                            if (j < dt.Columns.Count)
                            {
                                if (dgv.Columns[i].Name == columns[j])
                                {
                                    if (dr.Cells[i].Value.GetType() == typeof(string) || dr.Cells[i].Value.GetType() == typeof(DateTime) ||
                                        dr.Cells[i].Value.GetType() == typeof(int) || dr.Cells[i].Value.GetType() == typeof(double))
                                    {
                                        int num;
                                        if (int.TryParse(dr.Cells[i].Value.ToString(), out num))
                                        {
                                            cellValues[j] = (dr.Cells[i].Value == null) ? 0 : Convert.ToInt32(dr.Cells[i].Value);
                                            j++;
                                        }
                                        else
                                        {
                                            cellValues[j] = (dr.Cells[i].Value == null) ? "" : dr.Cells[i].Value;
                                            j++;
                                        }
                                    }
                                }
                            }
                        }

                        dt.Rows.Add(cellValues);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                return null;
            }
        }

        private bool IsColMatched(string dgvCol, string[] col)
        {
            for (int i = 0; i < col.Count(); i++)
            {
                if (col[i] == dgvCol && col[i] != "chkBox")
                    return true;
            }

            return false;
        }

        public bool IsNumeric(object obj)
        {
            if (Equals(obj, null))
            {
                return false;
            }

            Type objType = obj.GetType();
            objType = Nullable.GetUnderlyingType(objType) ?? objType;

            if (objType.IsPrimitive)
            {
                return objType != typeof(bool) &&
                    objType != typeof(char) &&
                    objType != typeof(IntPtr) &&
                    objType != typeof(UIntPtr);
            }

            return objType == typeof(decimal);
        }

        public int getMaxIdentity(string TableName, string ColumnName)
        {
            SqlDataReader sdr;
            string sql = "SELECT ISNULL(MAX(" + ColumnName + "), 0) + 1 TrnNo FROM [AssetRegistar].[dbo]." + TableName;
            int MaxVal = 0;

            sdr = (SqlDataReader)DBCRUDFunction(sql, true);

            if(sdr.Read())
                MaxVal = Convert.ToInt32(sdr["TrnNo"].ToString());

            sdr.Close();

            return MaxVal;
        }
    }
}
