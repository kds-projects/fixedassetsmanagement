﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AssetRegister
{
    public partial class frmLandAdjustmentCost : Form
    {
        private clsCommonFunctions dbConAdjustCost = new clsCommonFunctions();
        int AssetId = 0;
        string AssetCode = "";

        public frmLandAdjustmentCost()
        {
            InitializeComponent();
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAdjustCost.DecimalNumbers(txtAmount, e);
        }

        private void txtAmount_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAmount.Text))
            {
                double s = Convert.ToDouble(txtAmount.Text);
                txtAmount.Text = s.ToString("##,##0.00");
            }
            else
                txtAmount.Text = "0.00";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConAdjustCost.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAdjustCost.DisposeChecking(txtAssetCode);
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
            //LoadAssetDetails();
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void frmAdjustmentCost_Load(object sender, EventArgs e)
        {
            dbConAdjustCost.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '0', '', '', 1");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbEntryType.SelectedIndex > -1 && (AssetId > 0 
                && !dbConAdjustCost.CheckEmptyOrNull(txtAssetCode, "Select Asset")))
            {
                string[] cols = { "[AssetId]", "[AddDate]", "[AdditionDate]", "[AdditionCost]", "[RecDate]", "[AdditionItem]", "[Comments]", "[EntryBy]" };
                string[] vals = { AssetId.ToString(), 
                                  dtpSDDate.Value.ToString("yyyy-MM-dd"), txtAmount.Text,
                                  cmbEntryType.Text.Trim(), 
                                  dbConAdjustCost.SingleQuoteHandler(txtReason), 
                                  frmLogin.userId.ToString() };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetAddition]", this.Name, 
                                   dbConAdjustCost.getMaxIdentity ("[AR_AssetAddition]", "[TrnNo]").ToString(), "", "" };

                string sql = dbConAdjustCost.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetAddition]", cols, vals, "INSERT", null, null)
                             + dbConAdjustCost.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                if ((bool)dbConAdjustCost.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Informatin saved successfully.", "Save",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssetCode.Text = "";
                    AssetId = 0;
                    AssetCode = "";
                    txtAssetCode.Focus();
                    dgvListData.Visible = false;
                    txtReason.Text = "";
                    txtAmount.Text = "";
                    cmbEntryType.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Failed to save information.", "Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }
    }
}
