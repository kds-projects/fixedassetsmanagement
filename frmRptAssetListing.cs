﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace AssetRegister
{
    public partial class frmRptAssetListing : Form
    {
        private clsCommonFunctions dbConRptAddList = new clsCommonFunctions();
        private int AssetId = 0;

        public frmRptAssetListing()
        {
            InitializeComponent();
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConRptAddList.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvListData.Focus();

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                //AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                //AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            //AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void frmRptAdditionList_Load(object sender, EventArgs e)
        {
            dbConRptAddList.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '', '', 0");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            
            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptAssetPrintPreview.rpt");
                cryReport.SetParameterValue("@AssetId", AssetId);
                cryReport.SetParameterValue("@UserId", frmLogin.userId);
                dbConRptAddList.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.Text = "Asset Record Report";
                rptForm.crViewer.Refresh();
                rptForm.Show();
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
