﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace AssetRegister
{
    public partial class frmAssetConfigN : Form
    {
        private clsCommonFunctions dbConAssetEntry = new clsCommonFunctions();
        private bool cmbLoadFlag = false;
        public static string AssetCode = "";
        private int AssetLocId = 0, AssetSubLocId = 0, SupplierId = 0, AssetId = 0;
        private string AssetLoc = "", AssetSubLoc = "", AssetUser = "", Supplier = "",
            Compld = "", CCCode = "", GroupId = "", CustodianDeptCode = "", LSupplierId = "", LSupplier = "", LAssetName = "", LGenName = "", LQuantity = "", LUnitPriceBdt = "",
            LInvCostBdt = "", LInvCostUsd = "", LDutiesNothers = "", LAccumulatDep = "", LSalvageValue = "", LPurchaseDate = "",
            LDepMethod = "", LDepRate = "", LAssetLife = "", LActive = "", LDepEffectDate = "";
        bool IsUpdate = false;

        public static int sactive = 0;
        public static double sAccDep, sSalvageAmt, sDepRate, sAssetLife, sWarranty, sQuantity, sUnitPriceBdt, sInvCostBdt, sInvCostUsd,
            sDutiesNothers, sWDV, sConvRate, WDV;
        public static string sCC = "", sGroup = "", sCompany = "", sSupplier = "", sAsset, sGenName, sCountry, sLCNo, sBrand, sModelNo, sRegNo, sAssetDesc,
            sPurchaseDate, sDepMethod, sDepEffDate, sVoucherNo, sComments, sCustDeptId, sAssetSLNo, sCurType, sAssetRecDate,
            sSupplierId, sAssetLocId, sAssetSubLocId, sAssetUserCode, sAssetUser, sAccCode;
        public static string[] spChar = {"~", "(", ")", "#", "/", "=", ">", "<", "+", "-", "*", "%", "&", "|", "^", "'", "[", "]" };

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAssetConfigN()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConAssetEntry.CheckEmptyOrNull(cmbCompany, "Select Company") &&
                !dbConAssetEntry.CheckEmptyOrNull(cmbCC, "Select Cost Centre") &&
                !dbConAssetEntry.CheckEmptyOrNull(cmbGroup, "Select Group") &&
                //!dbConAssetEntry.CheckEmptyOrNull(cmbCustodianDept, "Select Custodian Dept.") &&
                !dbConAssetEntry.CheckEmptyOrNull(txtAsset, "Enter Asset") &&
                !dbConAssetEntry.CheckEmptyOrNull(txtAssetCode, "Enter Asset Code") &&
                !dbConAssetEntry.CheckEmptyOrNull(txtInvCostBdt, "Enter Invoice Cost (BDT)"))
            {
                int active = 0;

                if (cmbActive.SelectedIndex == 0)
                    active = 1;

                int Quantity;
                double AccDep, SalvageAmt, DepRate, AssetLife, Warranty, UnitPriceBdt, InvCostBdt, InvCostUsd, DutiesNothers, ConvRate;
                string Asset, GenName, Country, LCNo, Brand, ModelNo, RegNo, AssetDesc, PurchaseDate, DepMethod, DepEffDate, VoucherNo,
                    CustDeptId = "0", Comments, AssetRecDate, CurType, AssetSLNo, BillDate;
                
                if (!string.IsNullOrEmpty(txtAsset.Text))
                    Asset = dbConAssetEntry.SingleQuoteHandler(txtAsset.Text);
                else
                    Asset = "";

                if (!string.IsNullOrEmpty(txtGeneralName.Text))
                    GenName = dbConAssetEntry.SingleQuoteHandler(txtGeneralName.Text);
                else
                    GenName = "";
               
                if (!string.IsNullOrEmpty(txtCountry.Text))
                    Country = dbConAssetEntry.SingleQuoteHandler(txtCountry.Text);
                else
                    Country = "";

                if (!string.IsNullOrEmpty(txtLCNo.Text))
                    LCNo = dbConAssetEntry.SingleQuoteHandler(txtLCNo.Text);
                else
                    LCNo = "";

                if (!string.IsNullOrEmpty(txtBrand.Text))
                    Brand = dbConAssetEntry.SingleQuoteHandler(txtBrand.Text);
                else
                    Brand = "";

                if (!string.IsNullOrEmpty(txtModelNo.Text))
                    ModelNo = dbConAssetEntry.SingleQuoteHandler(txtModelNo.Text);
                else
                    ModelNo = "";

                if (!string.IsNullOrEmpty(txtRegNo.Text))
                    RegNo = dbConAssetEntry.SingleQuoteHandler(txtRegNo.Text);
                else
                    RegNo = "";

                if (!string.IsNullOrEmpty(txtAssetDesc.Text))
                    AssetDesc = dbConAssetEntry.SingleQuoteHandler(txtAssetDesc.Text);
                else
                    AssetDesc = "";

                PurchaseDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

                if (!string.IsNullOrEmpty(txtInvCostUsd.Text))
                    InvCostUsd = Convert.ToDouble(txtInvCostUsd.Text);
                else
                    InvCostUsd = 0.00;

                if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                    DutiesNothers = Convert.ToDouble(txtDutiesNothers.Text);
                else
                    DutiesNothers = 0.00;

                if (cmbDepMethod.SelectedIndex > -1)
                {
                    if (cmbDepMethod.SelectedIndex == 2)
                    {
                        MessageBox.Show(@"Double Declninig Method has not been set for Depreciation Process. Please,
                                          Contact with IT Person to Set.","Stop", 
                                         MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                        DepMethod = cmbDepMethod.Text;
                }
                else
                    DepMethod = "";

                DepEffDate = dtpYear.Value.Year + "-" + dtpMonth.Value.Month + "-01";

                if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                    AccDep = Convert.ToDouble(txtAccumulatDep.Text);
                else
                    AccDep = 0.00;

                if (!string.IsNullOrEmpty(txtQuantity.Text))
                    Quantity = Convert.ToInt32(txtQuantity.Text.Replace(",", ""));
                else
                    Quantity = 1;

                if (!string.IsNullOrEmpty(txtUnitPrice.Text))
                    UnitPriceBdt = Convert.ToDouble(txtUnitPrice.Text);
                else
                    UnitPriceBdt = 0.00;

                if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                {
                    InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
                    WDV = Convert.ToDouble(txtInvCostBdt.Text) - AccDep;
                }
                else
                {
                    InvCostBdt = 0.00;
                    WDV = 0.00;
                }

                if (!string.IsNullOrEmpty(txtDepRate.Text))
                    DepRate = Convert.ToDouble(txtDepRate.Text);
                else
                    DepRate = 0.00;

                if (!string.IsNullOrEmpty(txtAssetLife.Text))
                    AssetLife = Convert.ToDouble(txtAssetLife.Text);
                else
                    AssetLife = 0.00;

                //if (!string.IsNullOrEmpty(txtWrittenDownVal.Text))
                //    WDV = Convert.ToDouble(txtWrittenDownVal.Text);
                //else
                //    WDV = 0.00;

                if (!string.IsNullOrEmpty(txtSalvageValue.Text))
                    SalvageAmt = Convert.ToDouble(txtSalvageValue.Text);
                else
                    SalvageAmt = 0.00;

                if (cmbActive.SelectedIndex == 1)
                    active = 1;
                else if (cmbActive.SelectedIndex == 2)
                    active = 0;

                if (!string.IsNullOrEmpty(txtVoucherNo.Text))
                    VoucherNo = dbConAssetEntry.SingleQuoteHandler(txtVoucherNo.Text);
                else
                    VoucherNo = "";

                if (!string.IsNullOrEmpty(txtWPeriod.Text))
                    Warranty = Convert.ToDouble(txtWPeriod.Text);
                else
                    Warranty = 0.00;

                if (cmbCustodianDept.SelectedIndex > -1)
                    CustDeptId = cmbCustodianDept.SelectedValue.ToString();

                if (!string.IsNullOrEmpty(txtComments.Text))
                    Comments = dbConAssetEntry.SingleQuoteHandler(txtComments.Text);
                else
                    Comments = "";

                if (!string.IsNullOrEmpty(txtAssetSlNo.Text))
                    AssetSLNo = txtAssetSlNo.Text;
                else
                    AssetSLNo = "0";

                if (!string.IsNullOrEmpty(txtConvRate.Text))
                    ConvRate = Convert.ToDouble(txtConvRate.Text);
                else
                    ConvRate = 0.00;

                if (!string.IsNullOrEmpty(cmbCurrencyType.Text))
                    CurType = cmbCurrencyType.Text;
                else
                    CurType = "";

                //WDV = (InvCostUsd * ConvRate) + DutiesNothers;

                AssetRecDate = dtpAssetRecDate.Value.ToString("yyyy-MM-dd");
                BillDate = dtpInvBillDate.Value.ToString("yyyy-MM-dd");

                string[] cols = { "[AssetName]", "[GeneralName]", "[CostCentreCode]", "[AssetCode]", "[AssetDescription]", "[AssetGroupId]",
                                  "[CompanyId]", "[PurchaseAmt]", "[Quantity]", "[UnitPriceBDT]", "[InvoiceCostBDT]", "[InvoiceCostUSD]", "[DutiesnOthers]", "[WDV]",
                                  "[CostAmt]", "[SalvageAmt]", "[PurchaseDate]", "[DepreciationMethod]", "[DepreciationRate]",
                                  "[AssetLife]","DepreciationStartDate, DepreciationEndDate, [DepreciationEffectDate]", "[AssetLocId]",
                                  "[AssetSubLocId]", "[AssetUserCode]", "[AssetUser]", "[SupplierId]", "[Country]", "[Brand]", "[ModelNo]", 
                                  "[LCNo]", "[VoucherNo]", "[BillDate]", "[RegistrationNo]", "[IsActive]", "[Comments]", 
                                  "[Warranty]", "[UserId]", "[IsDepCalc]", "[IsCompanyAsset]", "[CustodianDeptCode]", "[AssetSLNo]", 
                                  "[ConvRate]", "[CurType]", "[AssetRecDate]", "[AccCode]"
                                };

                string[] vals = { Asset, GenName, cmbCC.SelectedValue.ToString(), AutoGenAssetCode(cmbGroup.SelectedValue.ToString()),
                                  AssetDesc, cmbGroup.SelectedValue.ToString(),cmbCompany.SelectedValue.ToString(), InvCostBdt.ToString(), 
                                  Quantity.ToString(), UnitPriceBdt.ToString(), InvCostBdt.ToString(), InvCostUsd.ToString(), DutiesNothers.ToString(), WDV.ToString(), AccDep.ToString(), 
                                  SalvageAmt.ToString(), PurchaseDate, DepMethod, DepRate.ToString(), AssetLife.ToString(), DepEffDate, 
                                  DateTime.Parse(DepEffDate).AddDays(1).AddMonths(Convert.ToInt32(AssetLife)*12).AddDays(-1).ToString("yyyy-MM-dd"), 
                                  DepEffDate, AssetLocId.ToString(), AssetSubLocId.ToString(), AssetUser, txtAssetUser.Text, SupplierId.ToString(),
                                  Country, Brand, ModelNo, LCNo, VoucherNo, BillDate, RegNo, active.ToString(), 
                                  Comments, Warranty.ToString(), frmLogin.userId.ToString(), "1", "1", 
                                  CustDeptId, AssetSLNo, ConvRate.ToString(), CurType, AssetRecDate, dbConAssetEntry.SingleQuoteHandler(txtAccCode.Text)
                                };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetConfig]", this.Name, 
                                   dbConAssetEntry.getMaxIdentity("[AR_AssetConfig]","[AssetId]").ToString(), "", "" };

                string sql = dbConAssetEntry.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols, vals, "INSERT", null, null);
                sql += dbConAssetEntry.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);
                
                if ((bool)dbConAssetEntry.DBCRUDFunction(sql, false))
                {
                    DialogResult result = MessageBox.Show("Information Saved Successfully. Would you like to Print ?", "Print?",
                                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        ReportDocument cryReport = new ReportDocument();
                        frmLoadReport rptForm = new frmLoadReport();
                        frmLoading frmLoading = new frmLoading();
                        frmLoading.Show();
                        frmLoading.TopMost = true;
                        Cursor.Current = Cursors.AppStarting;

                        try
                        {
                            //MessageBox.Show("Information Saved Successfully. But Print Option is not available now.", "Save", 
                            //    MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cryReport.Load(Application.StartupPath + @"\Reports\rptAssetPrintPreview.rpt");
                            cryReport.SetParameterValue("@AssetId", AssetId);
                            dbConAssetEntry.SetReportLogon(cryReport);
                            rptForm.crViewer.ReportSource = null;
                            rptForm.Text = "Asset Info. Entry Report";
                            rptForm.crViewer.ReportSource = cryReport;
                            rptForm.crViewer.Refresh();
                            //rptForm.Show();
                            cryReport.PrintToPrinter(1, false, 0, 0);
                            Cursor.Current = Cursors.Default;
                            frmLoading.Hide();
                        }
                        catch (Exception ex)
                        {
                            frmLoading.Hide();
                            MessageBox.Show(ex.Message);
                        }
                    }
                    //else
                    //    MessageBox.Show("Information Saved Successfully.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ClearAll();
                }
                else
                    MessageBox.Show("Failed to Save Information.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearAll()
        {
            AssetId = 0;
            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;
            txtAsset.Text = "";
            txtGeneralName.Text = "";
            txtAssetLoc.Text = "";            
            AssetLoc = "";
            AssetLocId = 0;
            dgvAssetLoc.Visible = false;
            txtAssetSubLoc.Text = "";
            AssetSubLoc = "";
            AssetSubLocId = 0;
            dgvAssetSubLoc.Visible = false;
            txtAssetUser.Text = "";
            dgvAssetUser.Visible = false;
            AssetUser = "";
            txtCountry.Text = "";
            txtLCNo.Text = "";
            txtModelNo.Text = "";
            txtBrand.Text = "";
            txtSupplier.Text = "";
            dgvSupplier.Visible = false;
            Supplier = "";
            SupplierId = 0;
            txtRegNo.Text = "";
            txtAssetDesc.Text = "";
            txtInvCostUsd.Text = "";
            txtQuantity.Text = "1";
            txtUnitPrice.Text = "";
            txtInvCostBdt.Text = "";
            txtDutiesNothers.Text = "";
            cmbDepMethod.SelectedIndex = -1;
            txtAccumulatDep.Text = "";
            txtDepRate.Text = "";
            txtAssetLife.Text = "0.00";
            txtWrittenDownVal.Text = "";
            txtSalvageValue.Text = "";
            txtVoucherNo.Text = "";
            rbNo.Checked = true;
            txtComments.Text = "";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (cmbGroup.SelectedIndex > -1)
                AutoGenAssetCode(cmbGroup.SelectedValue.ToString());
            
            ResetAll();
        }

        private void ResetAll()
        {
            txtAsset.Text = "";
            txtGeneralName.Text = "";
            cmbCC.Text = "";
            txtAssetDesc.Text = "";
            cmbGroup.Text = "";
            cmbCompany.Text = "";
            txtInvCostBdt.Text = "";
            txtQuantity.Text = "1";
            txtUnitPrice.Text = "";
            txtInvCostUsd.Text = "";
            txtDutiesNothers.Text = "";
            txtAccumulatDep.Text = "";
            txtSalvageValue.Text = "";
            dtpPurchaseDate.Text = "";
            cmbDepMethod.Text = "";
            txtDepRate.Text = "";
            txtAssetLife.Text = "";
            txtAssetLoc.Text = "";
            dgvAssetLoc.Visible = false;
            txtAssetSubLoc.Text = "";
            dgvAssetSubLoc.Visible = false;
            txtAssetUser.Text = "";
            dgvAssetUser.Visible = false;
            txtSupplier.Text = "";
            dgvSupplier.Visible = false;
            txtCountry.Text = "";
            txtBrand.Text = "";
            txtModelNo.Text = "";
            txtAccCode.Text = "";
            txtLCNo.Text = "";
            txtVoucherNo.Text = "";
            txtRegNo.Text = "";
            cmbActive.SelectedIndex = -1;
            txtComments.Text = "";
            txtWPeriod.Text = "";
            rbNo.Checked = true;
            chkWDV.Checked = false;
            WDV = 0;

            dtpPurchaseDate.Value = DateTime.Today.Date;
            DateTime DepEffectDate = dtpPurchaseDate.Value.Date.AddMonths(2).AddDays(-dtpPurchaseDate.Value.Day);
            dtpMonth.Value = DepEffectDate; dtpYear.Value = DepEffectDate;
            dtpDepEffectDate.Value = DepEffectDate;

            EnableUpdate(false);
        }

        private string AutoGenAssetCode(string GroupId) 
        {
            SqlDataReader dr;
            string CodePrefix = "", Code = "";

            if (cmbGroup.SelectedIndex > -1)
            {
                switch (GroupId)
                {
                    case "010102": CodePrefix = "AB"; break;
                    case "010105": CodePrefix = "CS"; break;
                    case "010107": CodePrefix = "EE"; break;
                    case "010109": CodePrefix = "FB"; break;
                    case "010106": CodePrefix = "FF"; break;
                    case "010113": CodePrefix = "GS"; break;
                    case "010112": CodePrefix = "LE"; break;
                    case "010110": CodePrefix = "LD"; break;
                    case "010104": CodePrefix = "OE"; break;
                    case "010103": CodePrefix = "PM"; break;
                    case "010108": CodePrefix = "VH"; break;
                    case "010111": CodePrefix = "IA"; break;
                }

                dr = (SqlDataReader)dbConAssetEntry.DBCRUDFunction(@"SELECT dbo.fnAutoGenAssetCode('" + CodePrefix + "') AssetCode", true);

                if (dr.Read())
                    Code = dr["AssetCode"].ToString();

                dr.Close();
            }

            return Code;
        }

        private void frmAssetConfigN_Load(object sender, EventArgs e)
        {
            dbConAssetEntry.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConAssetEntry.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConAssetEntry.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 0");
            dbConAssetEntry.FillComboBox(cmbCustodianDept, @"SELECT DepartmentCode, DepartmentName FROM AUTOMATIONXS.B_Employer2000.dbo.Department WHERE DELETED = 0");
            

            cmbLoadFlag = true;
            if (cmbLoadFlag)
            {
                cmbCompany.SelectedIndex = -1;
                cmbCC.SelectedIndex = -1;
                cmbGroup.SelectedIndex = -1;
                cmbActive.SelectedIndex = 0;
                cmbCustodianDept.SelectedIndex = -1;
            }

            dgvAssetCode.Left = 366;
            dgvAssetCode.Top = 12;
            dgvAssetCode.Height = 464;
            dgvAssetCode.BringToFront();
            dgvSupplier.Left = 366;
            dgvSupplier.Top = 12;
            dgvSupplier.Height = 464;
            dgvSupplier.BringToFront();
            dgvAssetLoc.Left = 366;
            dgvAssetLoc.Top = 12;
            dgvAssetLoc.Height = 464;
            dgvAssetLoc.BringToFront();
            dgvAssetSubLoc.Left = 366;
            dgvAssetSubLoc.Top = 12;
            dgvAssetSubLoc.Height = 464;
            dgvAssetSubLoc.BringToFront();
            dgvAssetUser.Left = 366;
            dgvAssetUser.Top = 12;
            dgvAssetUser.Height = 464;
            dgvAssetUser.BringToFront();

            dbConAssetEntry.FillDataGridView(dgvAssetUser, @"EXEC prcCmbLoadEmployee 0, -1");
            dgvAssetUser.Columns[0].Visible = false;
            dgvAssetUser.Columns[1].Width = 200;
            dbConAssetEntry.FillDataGridView(dgvSupplier, @"SELECT SupplierId, Supplier FROM [AssetRegistar].[dbo].[AR_AssetSupplier] WHERE IsActive = 1");
            dgvSupplier.Columns[0].Visible = false;
            dgvSupplier.Columns[1].Width = 200;
            dbConAssetEntry.FillDataGridView(dgvAssetCode, @"SELECT AssetId, AssetCode FROM [AssetRegistar].[dbo].[AR_AssetConfig] WHERE IsDepCalc = 1");
            dgvAssetCode.Columns[0].Visible = false;
            dgvAssetCode.Columns[1].Width = 200;
            EnableUpdate(false);
        }

        private void txtDepRate_TextChanged(object sender, EventArgs e)
        {
            double DepRate = 0.00, InvCostBdt = 0.00, DutNoth = 0.00, OriginalAmt = 0.00;

            if (!string.IsNullOrEmpty(txtDepRate.Text))
                DepRate = Convert.ToDouble(txtDepRate.Text);
            if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
            if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                DutNoth = Convert.ToDouble(txtDutiesNothers.Text);

            OriginalAmt = InvCostBdt + DutNoth;

            if (DepRate > 0.00)
                txtAssetLife.Text = Math.Round((100 / Convert.ToDouble(txtDepRate.Text))).ToString();
            else
                txtAssetLife.Text = "0.00";
        }

        private void txtInvCostUsd_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtInvCostUsd, e);
        }

        private void txtInvCostBdt_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtInvCostBdt);
            dbConAssetEntry.DecimalNumbers(txtInvCostBdt, e);
        }

        private void txtDutiesNothers_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtDutiesNothers, e);
        }

        private void txtDepRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtDepRate, e);
        }

        private void txtWrittenDownVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //dbConAssetEntry.DecimalNumbers(txtWrittenDownVal, e);
        }

        private void txtSalvageValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtSalvageValue, e);
        }

        private void txtWPeriod_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtWPeriod, e);
        }

        private void cmbCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCC.SelectedIndex > -1)
            {
                CCCode = cmbCC.SelectedValue.ToString();
                dbConAssetEntry.FillDataGridView(dgvAssetLoc, @"SELECT AssetLocId,
                                                                       AssetLocation
                                                                FROM   AR_AssetLocation
                                                                WHERE  CostCentreCode = '" + CCCode + "'");
                dgvAssetLoc.Columns[0].Visible = false;
                dgvAssetLoc.Columns[1].Width = 200;
            }

            txtAssetLoc.Text = "";
            dgvAssetLoc.Visible = false;
            txtAssetSubLoc.Text = "";
            dgvAssetSubLoc.Visible = false;
        }

        private void cmbAssetLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void rbYes_CheckedChanged(object sender, EventArgs e)
        {
            txtWPeriod.ReadOnly = false;
            txtWPeriod.Focus();
        }

        private void rbNo_CheckedChanged(object sender, EventArgs e)
        {
            txtWPeriod.Text = "";
            txtWPeriod.ReadOnly = true;            
        }

        private void txtAssetLoc_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtAssetLoc);
            var dataTable = (DataTable)dgvAssetLoc.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetLoc.Visible = true;

                dataView.RowFilter = dgvAssetLoc.Columns[1].Name + " LIKE '%" + CheckValue(txtAssetLoc.Text) + "%'";
            }
        }

        public static string CheckValue(string value)
        {
            StringBuilder sBuilder = new StringBuilder();

            string pattern = @"([-\]\[<>\?\*\\\""/\|\~\(\)\#/=><+\%&\^\'])";

            Regex expression = new Regex(pattern);

            //if (expression.IsMatch(value))
            //{
            //    sBuilder.Replace(@"\", @"\\");
            //    sBuilder.Replace("]", @"\]");
            //    sBuilder.Insert(0, "[");
            //    sBuilder.Append("]");
            //}
            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                if (c == '*' || c == '%' || c == '[' || c == ']')
                    sBuilder.Append("[").Append(c).Append("]");
                else if (c == '\'')
                    sBuilder.Append("''");
                else
                    sBuilder.Append(c);
            }
            return sBuilder.ToString();
        }

        private void txtAssetLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtAssetLoc);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetLoc.Visible = false;
        }

        private void txtAssetLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetLoc.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetLoc.Rows.Count > 0)
            {
                txtAssetLoc.Text = dgvAssetLoc.Rows[0].Cells[1].Value.ToString();
                AssetLocId = Convert.ToInt32(dgvAssetLoc.Rows[0].Cells[0].Value.ToString());
                AssetLoc = dgvAssetLoc.Rows[0].Cells[1].Value.ToString();
                LoadAssetSubLoc();
                dgvAssetLoc.Visible = false;
                txtAssetLoc.SelectionStart = txtAssetLoc.TextLength;
            }
        }

        private void dgvAssetLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetLocId = Convert.ToInt32(dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetLoc.Text = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetLoc.Focus();
                txtAssetLoc.SelectionStart = txtAssetLoc.TextLength;
                dgvAssetLoc.Visible = false;
                LoadAssetSubLoc();
            }
        }

        private void dgvAssetLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetLoc.Focus();
            txtAssetLoc.SelectionStart = txtAssetLoc.TextLength;
        }

        private void dgvAssetLoc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetLocId = Convert.ToInt32(dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetLoc.Text = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetLoc.Focus();
            txtAssetLoc.SelectionStart = txtAssetLoc.TextLength;
            dgvAssetLoc.Visible = false;
            LoadAssetSubLoc();
        }

        private void dgvAssetLoc_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void txtAssetUser_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtAssetUser);
            var dataTable = (DataTable)dgvAssetUser.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetUser.Visible = true;
                //dataView.RowFilter = dgvAssetUser.Columns[1].Name + " LIKE '%" + txtAssetUser.Text + "%'";
                dataView.RowFilter = string.Format("CONVERT(" + dgvAssetUser.Columns[1].Name + ", 'System.String') LIKE '%" + txtAssetUser.Text + "%'") + " OR "
                                   + string.Format("CONVERT(" + dgvAssetUser.Columns[2].Name + ", 'System.String') LIKE '%" + txtAssetUser.Text + "%'");
            }
        }

        private void txtAssetUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtAssetUser);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetUser.Visible = false;
        }

        private void txtAssetUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetUser.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetUser.Rows.Count > 0)
            {
                txtAssetUser.Text = dgvAssetUser.Rows[0].Cells[1].Value.ToString().Trim();
                AssetUser = dgvAssetUser.Rows[0].Cells[0].Value.ToString();
                //AssetLoc = dgvAssetUser.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvAssetUser.Visible = false;
                txtAssetUser.SelectionStart = txtAssetUser.TextLength;
            }
        }

        private void dgvAssetUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetUser = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[0].Value.ToString();
                //AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetUser.Text = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetUser.Focus();
                txtAssetUser.SelectionStart = txtAssetUser.TextLength;
                dgvAssetUser.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvAssetUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetUser.Focus();
            txtAssetUser.SelectionStart = txtAssetUser.TextLength;
        }

        private void dgvAssetUser_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetUser = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[0].Value.ToString();            
            txtAssetUser.Text = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
            txtAssetUser.Focus();
            txtAssetUser.SelectionStart = txtAssetUser.TextLength;
            dgvAssetUser.Visible = false;
            //LoadAssetDetails();
        }

        private void dgvAssetUser_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void txtAssetSubLoc_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtAssetSubLoc);
            var dataTable = (DataTable)dgvAssetSubLoc.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetSubLoc.Visible = true;
                dataView.RowFilter = dgvAssetSubLoc.Columns[1].Name + " LIKE '%" + txtAssetSubLoc.Text + "%'";
            }
        }

        private void txtAssetSubLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtAssetSubLoc);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetSubLoc.Visible = false;
        }

        private void txtAssetSubLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetSubLoc.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetSubLoc.Rows.Count > 0)
            {
                txtAssetSubLoc.Text = dgvAssetSubLoc.Rows[0].Cells[1].Value.ToString();
                AssetSubLocId = Convert.ToInt32(dgvAssetSubLoc.Rows[0].Cells[0].Value.ToString());
                AssetSubLoc = dgvAssetSubLoc.Rows[0].Cells[1].Value.ToString();                
                dgvAssetSubLoc.Visible = false;
                txtAssetSubLoc.SelectionStart = txtAssetSubLoc.TextLength;
            }
        }

        private void dgvAssetSubLoc_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetSubLocId = Convert.ToInt32(dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetSubLoc = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetSubLoc.Text = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetSubLoc.Focus();
            txtAssetSubLoc.SelectionStart = txtAssetSubLoc.TextLength;
            dgvAssetSubLoc.Visible = false;
            //LoadAssetDetails();
        }

        private void dgvAssetSubLoc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetSubLocId = Convert.ToInt32(dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetSubLoc = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetSubLoc.Text = dgvAssetSubLoc.Rows[dgvAssetSubLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetSubLoc.Focus();
                txtAssetSubLoc.SelectionStart = txtAssetSubLoc.TextLength;
                dgvAssetSubLoc.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvAssetSubLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetSubLoc.Focus();
            txtAssetSubLoc.SelectionStart = txtAssetSubLoc.TextLength;
        }

        private void dgvAssetSubLoc_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void LoadAssetSubLoc() 
        {
            if (cmbCC.SelectedIndex > -1 && AssetLocId > 0)
            {
                dbConAssetEntry.FillDataGridView(dgvAssetSubLoc, @"SELECT AssetSubLocId,
                                                                          AssetSubLocation
                                                                   FROM   AR_AssetSubLocation
                                                                   WHERE  AssetLocId =" + AssetLocId);
                dgvAssetSubLoc.Columns[0].Visible = false;
                dgvAssetSubLoc.Columns[1].Width = 200;
            }
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbGroup.SelectedIndex > -1 && !IsUpdate)
            {
                txtAssetCode.Text = AutoGenAssetCode(cmbGroup.SelectedValue.ToString());
            }
            dgvAssetCode.Visible = false;
        }

        private void txtSupplier_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtSupplier);
            var dataTable = (DataTable)dgvSupplier.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvSupplier.Visible = true;
                dataView.RowFilter = dgvSupplier.Columns[1].Name + " LIKE '%" + txtSupplier.Text + "%'";
            }
        }

        private void txtSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtSupplier);
            if (e.KeyChar == (char)Keys.Escape)
                dgvSupplier.Visible = false;
        }

        private void txtSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvSupplier.Focus();

            if (e.KeyCode == Keys.Enter && dgvSupplier.Rows.Count > 0)
            {
                txtSupplier.Text = dgvSupplier.Rows[0].Cells[1].Value.ToString();
                SupplierId = Convert.ToInt32(dgvSupplier.Rows[0].Cells[0].Value.ToString());
                Supplier = dgvSupplier.Rows[0].Cells[1].Value.ToString();
                dgvSupplier.Visible = false;
                txtSupplier.SelectionStart = txtSupplier.TextLength;
            }
        }

        private void dgvSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SupplierId = Convert.ToInt32(dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[0].Value.ToString());
                Supplier = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSupplier.Text = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSupplier.Focus();
                txtSupplier.SelectionStart = txtSupplier.TextLength;
                dgvSupplier.Visible = false;
            }
        }

        private void dgvSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtSupplier.Focus();
            txtSupplier.SelectionStart = txtSupplier.TextLength;
        }

        private void dgvSupplier_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SupplierId = Convert.ToInt32(dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[0].Value.ToString());
            Supplier = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSupplier.Text = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSupplier.Focus();
            txtSupplier.SelectionStart = txtSupplier.TextLength;
            dgvSupplier.Visible = false;
        }

        private void txtAsset_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtAsset);
        }

        private void txtInvCostBdt_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
            {
                double s = Convert.ToDouble(txtInvCostBdt.Text);
                txtInvCostBdt.Text = s.ToString("##,##0.00");
            }
            else
                txtInvCostBdt.Text = "0.00";
        }

        private void txtInvCostUsd_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtInvCostUsd.Text))
            {
                double s = Convert.ToDouble(txtInvCostUsd.Text);
                txtInvCostUsd.Text = s.ToString("##,##0.00");
            }
            else
                txtInvCostUsd.Text = "0.00";
        }

        private void txtDutiesNothers_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
            {
                double s = Convert.ToDouble(txtDutiesNothers.Text);
                txtDutiesNothers.Text = s.ToString("##,##0.00");
            }
            else
                txtDutiesNothers.Text = "0.00";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            EnableUpdate(true);
            
            if (IsUpdate)
            {
                if (!dbConAssetEntry.CheckEmptyOrNull(txtAssetCode, "Enter Asset Code") && 
                    !dbConAssetEntry.CheckEmptyOrNull(cmbCompany, "Select Company") &&
                    !dbConAssetEntry.CheckEmptyOrNull(cmbCC, "Select Cost Centre") &&
                    !dbConAssetEntry.CheckEmptyOrNull(cmbGroup, "Select Group") &&
                    //!dbConAssetEntry.CheckEmptyOrNull(cmbCustodianDept, "Select Custodian Dept.") &&
                    !dbConAssetEntry.CheckEmptyOrNull(txtAsset, "Enter Asset") &&
                    !dbConAssetEntry.CheckEmptyOrNull(txtInvCostBdt, "Enter Invoice Cost (BDT)"))
                {
                    int active = 0, Quantity;
                    double AccDep, SalvageAmt, DepRate, AssetLife, Warranty, UnitPriceBdt, InvCostBdt, InvCostUsd, 
                        DutiesNothers;
                    string Asset, GenName, Country, LCNo, Brand, ModelNo, RegNo, AssetDesc, 
                        purchaseDate, DepMethod, DepEffDate, VoucherNo, Comments;

                    if (!string.IsNullOrEmpty(txtAsset.Text))
                        Asset = dbConAssetEntry.SingleQuoteHandler(txtAsset.Text);
                    else
                        Asset = "";

                    if (!string.IsNullOrEmpty(txtGeneralName.Text))
                        GenName = dbConAssetEntry.SingleQuoteHandler(txtGeneralName.Text);
                    else
                        GenName = "";

                    if (!string.IsNullOrEmpty(txtCountry.Text))
                        Country = dbConAssetEntry.SingleQuoteHandler(txtCountry.Text);
                    else
                        Country = "";

                    if (!string.IsNullOrEmpty(txtLCNo.Text))
                        LCNo = dbConAssetEntry.SingleQuoteHandler(txtLCNo.Text);
                    else
                        LCNo = "";

                    if (!string.IsNullOrEmpty(txtBrand.Text))
                        Brand = dbConAssetEntry.SingleQuoteHandler(txtBrand.Text);
                    else
                        Brand = "";

                    if (!string.IsNullOrEmpty(txtModelNo.Text))
                        ModelNo = dbConAssetEntry.SingleQuoteHandler(txtModelNo.Text);
                    else
                        ModelNo = "";

                    if (!string.IsNullOrEmpty(txtRegNo.Text))
                        RegNo = dbConAssetEntry.SingleQuoteHandler(txtRegNo.Text);
                    else
                        RegNo = "";

                    if (!string.IsNullOrEmpty(txtAssetDesc.Text))
                        AssetDesc = dbConAssetEntry.SingleQuoteHandler(txtAssetDesc.Text);
                    else
                        AssetDesc = "";

                    purchaseDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

                    if (!string.IsNullOrEmpty(txtQuantity.Text))
                        Quantity = Convert.ToInt32(txtQuantity.Text.Replace(",", ""));
                    else
                        Quantity = 1;

                    if (!string.IsNullOrEmpty(txtUnitPrice.Text))
                        UnitPriceBdt = Convert.ToDouble(txtUnitPrice.Text);
                    else
                        UnitPriceBdt = 0.00;

                    if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                        InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
                    else
                        InvCostBdt = 0.00;

                    if (!string.IsNullOrEmpty(txtInvCostUsd.Text))
                        InvCostUsd = Convert.ToDouble(txtInvCostUsd.Text);
                    else
                        InvCostUsd = 0.00;

                    if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                        DutiesNothers = Convert.ToDouble(txtDutiesNothers.Text);
                    else
                        DutiesNothers = 0.00;

                    if (cmbDepMethod.SelectedIndex > -1)
                    {
                        if (cmbDepMethod.SelectedIndex == 2)
                        {
                            MessageBox.Show(@"Double Declninig Method has not been set for Depreciation Process. Please,
                                                Contact with IT Person to Set.", "Stop",
                                             MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        else
                            DepMethod = cmbDepMethod.Text;
                    }
                    else
                        DepMethod = "";

                    DepEffDate = dtpMonth.Value.Year.ToString() + "-" + dtpYear.Value.Month.ToString() + "-01";

                    if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                        AccDep = Convert.ToDouble(txtAccumulatDep.Text);
                    else
                        AccDep = 0.00;

                    if (!string.IsNullOrEmpty(txtDepRate.Text))
                        DepRate = Convert.ToDouble(txtDepRate.Text);
                    else
                        DepRate = 0.00;

                    if (!string.IsNullOrEmpty(txtAssetLife.Text))
                        AssetLife = Convert.ToDouble(txtAssetLife.Text);
                    else
                        AssetLife = 0.00;

                    if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                    {
                        InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);

                        if (chkWDV.Checked)
                            WDV = Convert.ToDouble(txtInvCostBdt.Text) - AccDep;
                    }
                    else
                    {
                        InvCostBdt = 0.00;

                        if (chkWDV.Checked)
                            WDV = 0.00;
                    }

                    if (!string.IsNullOrEmpty(txtSalvageValue.Text))
                        SalvageAmt = Convert.ToDouble(txtSalvageValue.Text);
                    else
                        SalvageAmt = 0.00;

                    if (cmbActive.SelectedIndex == 0)
                        active = 1;
                    else if (cmbActive.SelectedIndex == 1)
                        active = 0;

                    if (!string.IsNullOrEmpty(txtVoucherNo.Text))
                        VoucherNo = dbConAssetEntry.SingleQuoteHandler(txtVoucherNo.Text);
                    else
                        VoucherNo = "";

                    if (!string.IsNullOrEmpty(txtWPeriod.Text))
                        Warranty = Convert.ToDouble(txtWPeriod.Text);
                    else
                        Warranty = 0.00;

                    if (!string.IsNullOrEmpty(txtComments.Text))
                        Comments = dbConAssetEntry.SingleQuoteHandler(txtComments.Text);
                    else
                        Comments = "";

                    string[] cols = { "[AssetName]", "[GeneralName]", "[CostCentreCode]", "[AssetDescription]", "[CompanyId]", "[PurchaseAmt]", 
                                      "[Quantity]", "[UnitPriceBDT]", "[InvoiceCostBDT]", "[InvoiceCostUSD]", "[DutiesnOthers]", "[WDV]", "[CostAmt]", 
                                      "[SalvageAmt]", "[PurchaseDate]", "[DepreciationMethod]", "[DepreciationRate]", "[AssetLife]",
                                      "[DepreciationEffectDate]", "[AssetLoc]", "[AssetLocId]", "[AssetSubLoc]", "[AssetSubLocId]", 
                                      "[AssetUserCode]", "[AssetUser]", "[SupplierId]", "[Country]", "[Brand]", "[ModelNo]", "[LCNo]", "[VoucherNo]", 
                                      "[RegistrationNo]", "[IsActive]", "[Comments]", "[Warranty]", "[UpdateDateTime]", "[UpdateUserId]",
                                      "[IsDepCalc]", "[IsCompanyAsset]", "[CustodianDeptCode]", "[AccCode]"
                                    };

                    string[] vals = { Asset, GenName, CCCode, AssetDesc, Compld, (InvCostBdt + DutiesNothers).ToString(), 
                                      Quantity.ToString(), UnitPriceBdt.ToString(), InvCostBdt.ToString(), InvCostUsd.ToString(), DutiesNothers.ToString(),WDV.ToString(),
                                      AccDep.ToString(), SalvageAmt.ToString(), purchaseDate, DepMethod, DepRate.ToString(),
                                      AssetLife.ToString(), DepEffDate, dbConAssetEntry.SingleQuoteHandler(txtAssetLoc.Text), AssetLocId.ToString(), 
                                      dbConAssetEntry.SingleQuoteHandler(txtAssetSubLoc.Text), AssetSubLocId.ToString(), AssetUser, txtAssetUser.Text, SupplierId.ToString(),
                                      Country, Brand, ModelNo, LCNo, VoucherNo, RegNo, active.ToString(), 
                                      Comments, Warranty.ToString(),dbConAssetEntry.GetServerCurrentDateTime().ToString(), frmLogin.userId.ToString(), "1", "1",
                                      CustodianDeptCode,dbConAssetEntry.SingleQuoteHandler(txtAccCode.Text)
                                    };
                    string[] whCols = { "AssetId" };
                    string[] whVals = { AssetId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetConfig]", this.Name, 
                                       AssetId.ToString(), 
                                       @"[AssetName],[GeneralName],[CostCentreCode],[AssetDescription],[CompanyId],
                                       [PurchaseAmt],[Quantity],[UnitPriceBDT],[InvoiceCostBDT],[InvoiceCostUSD],[DutiesnOthers],[CostAmt],
                                       [SalvageAmt],[PurchaseDate],[DepreciationMethod],[DepreciationRate],[AssetLife],
                                       [DepreciationEffectDate],[AssetLoc],[AssetLocId],[AssetSubLoc],[AssetSubLocId],
                                       [AssetUserCode],[SupplierId],[Country],[Brand],[ModelNo],[LCNo],[VoucherNo],
                                       [RegistrationNo],[IsActive],[Comments],[Warranty],[IsDepCalc],[IsCompanyAsset],[CustodianDeptCode]", 
                                       LAssetName + "," + LGenName + "," + CCCode + "," + AssetDesc + "," + Compld + "," 
                                       + (LInvCostBdt + LDutiesNothers) + "," + LQuantity + "," + LUnitPriceBdt + "," + LInvCostBdt + "," + InvCostUsd + "," + DutiesNothers + ","
                                       + LAccumulatDep + "," + LSalvageValue + "," + LPurchaseDate + "," + LDepMethod + ", " + LDepRate + ","
                                       + LAssetLife + "," + LDepEffectDate + "," + dbConAssetEntry.SingleQuoteHandler(txtAssetLoc.Text) + ","+AssetLocId+","+dbConAssetEntry.SingleQuoteHandler(txtAssetSubLoc.Text)+","+ AssetSubLocId +","+ AssetUser +", " + LSupplierId + ","
                                       + Country + ","+ Brand +","+ ModelNo+","+ LCNo +","+ VoucherNo +","+ RegNo +","+ LActive +","+ Comments +","+ Warranty +",1,1," + CustodianDeptCode };

                    string sql = dbConAssetEntry.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols, vals, "UPDATE", whCols, whVals);
                    sql += dbConAssetEntry.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConAssetEntry.DBCRUDFunction(sql, false))
                        MessageBox.Show("Asset Information Updated Successfully.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Failed to Update Asset Information.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void EnableUpdate(bool Status) 
        {
            if (Status)
            {
                //btnUpdate.Text = "Update";
                btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
                IsUpdate = true;
                txtAssetCode.ReadOnly = false;
                txtAssetCode.BackColor = Color.White;
                txtAssetCode.ForeColor = Color.Black;
                txtAssetCode.Focus();
                btnCopy.Enabled = true;
                btnSave.Enabled = false;
                if (frmLogin.userId != 1)
                    dtpPurchaseDate.Enabled = false;
                else
                    dtpPurchaseDate.Enabled = true;
                dgvAssetCode.Visible = false;
            }
            else
            {
                dbConAssetEntry.DisposeChecking(txtAssetCode);
                txtAssetCode.Text = "";
                //btnUpdate.Text = "Edit";
                btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_properties_icon;
                IsUpdate = false;
                txtAssetCode.ReadOnly = true;
                txtAssetCode.BackColor = Color.DimGray;
                txtAssetCode.ForeColor = Color.White;
                dtpMonth.Enabled = false;
                dtpYear.Enabled = false;
                dtpDepEffectDate.Enabled = false;
                btnCopy.Enabled = false;
                btnSave.Enabled = true;
                dgvAssetCode.Visible = false;
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtAssetCode);
            if (IsUpdate)
                dbConAssetEntry.DisposeChecking(txtAssetCode);
            else
                EnableUpdate(IsUpdate);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetCode.Visible = false;

            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtInvCostBdt_TextChanged(object sender, EventArgs e)
        {
            double InvCostBdt = 0.00, DutNoth = 0.00, AccDep = 0.00;

            if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
            if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                DutNoth = Convert.ToDouble(txtDutiesNothers.Text);
            if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                AccDep = Convert.ToDouble(txtAccumulatDep.Text);

            txtWrittenDownVal.Text = (InvCostBdt + DutNoth - AccDep).ToString("##,##0.00");
        }

        private void txtInvCostBdt_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvAssetCode.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetCode.Visible = true;
                dataView.RowFilter = dgvAssetCode.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetCode.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetCode.Rows.Count > 0)
            {
                txtAssetCode.Text = dgvAssetCode.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvAssetCode.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvAssetCode.Rows[0].Cells[1].Value.ToString();
                LoadAssetDetails();
                dgvAssetCode.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;

                if (frmLogin.userId != 1)
                {
                    dtpMonth.Enabled = false;
                    dtpYear.Enabled = false;
                    dtpDepEffectDate.Enabled = false;
                }
            }
        }

        private void LoadAssetDetails() 
        {
            //SqlDataReader dr;

            string strSql = @"SELECT aac.AssetName,
                           aac.GeneralName,
                           aac.CostCentreCode,
                           cc.CC_Name,
                           c.compname,	
                           aac.AssetDescription,
                           aac.AssetGroupId,
                           ag.ass_head,
                           aac.CompanyId,
                           aac.PurchaseAmt,
                           aac.Quantity,
                           aac.UnitPriceBDT,
                           aac.InvoiceCostBDT,
                           aac.InvoiceCostUSD,
                           aac.DutiesnOthers,
                           aac.CostAmt,
                           aac.SalvageAmt,
                           aac.PurchaseDate,
                           aac.DepreciationMethod,
                           aac.DepreciationRate,
                           aac.AssetLife,
                           ISNULL(dept.DepartmentName, '') DepartmentName,
                           ISNULL(aac.CustodianDeptCode, 0) DeptCode,
                           aac.DepreciationStartDate,
                           aac.DepreciationEndDate,
                           aac.DepreciationEffectDate, 
                           aac.AssetRecDate,
                           aac.BillDate, 
                           CASE
                                WHEN aac.AssetLocId = 0 THEN aac.AssetLoc
                                ELSE aal.AssetLocation
                           END AssetLoc,
                           CASE
                                WHEN aac.AssetSubLocId = 0 THEN aac.AssetSubLoc
                                ELSE aasl.AssetSubLocation 
                           END AssetSubLoc,
                           aac.AssetSubLocId,
                           aac.AssetLocId,
                           UPPER(ISNULL(e.ForeNames, aac.AssetUser)) AssetUser,
                           UPPER(ISNULL(e.EmployeeCode, 0)) AssetUserCode,
                           aac.SupplierId,
                           ISNULL(aas.Supplier, '') Supplier,
                           aac.Country,
                           aac.Brand,
                           aac.ModelNo,
                           aac.LCNo,
                           aac.VoucherNo,
                           aac.RegistrationNo,
                           aac.IsActive,
                           aac.Comments,
                           aac.Warranty,
                           aac.AccCode,
                           aac.WDV,
                           Case When aac.PurchaseAmt - aac.CostAmt = aac.WDV Then 1 Else 0 End As IsWDVUpdate
                    FROM   [AssetRegistar].[dbo].[AR_AssetConfig] aac
                    INNER JOIN AUTOMATIONXS.KDSXS_Accounts.dbo.CostCentre cc
                        ON  aac.CostCentreCode = cc.CC_Code
                    INNER JOIN AUTOMATIONXS.KDSXS_Accounts.dbo.ass_code ag
                        ON  aac.AssetGroupId = ag.ass_code
                    INNER JOIN AUTOMATIONXS.KDSXS_Accounts.dbo.CompanyName c
                        ON  aac.CompanyId = c.compID
                    LEFT OUTER JOIN [AssetRegistar].[dbo].[AR_AssetLocation] aal 
                        ON aal.AssetLocId = aac.AssetLocId 
                    LEFT OUTER JOIN [AssetRegistar].[dbo].[AR_AssetSubLocation] aasl
                        ON aasl.AssetSubLocId = aac.AssetSubLocId
                    LEFT OUTER JOIN [AssetRegistar].[dbo].[AR_AssetSupplier] aas
                        ON aas.SupplierId = aac.SupplierId
                    LEFT OUTER JOIN AUTOMATIONXS.B_Employer2000.dbo.Employee e
						ON e.EmployeeCode = aac.AssetUserCode
                    LEFT OUTER JOIN [AUTOMATIONXS].[B_Employer2000].[dbo].[Department] dept
                        ON dept.DepartmentCode = aac.CustodianDeptCode
                    WHERE  AssetId = " + AssetId;
            
            //dr = ((SqlDataReader)dbConAssetEntry.DBCRUDFunction(strSql, true));

            DataTable dt = dbConAssetEntry.DBCRUDFunctionAsTable(strSql);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                txtAsset.Text = dr["AssetName"].ToString();
                LAssetName = dr["AssetName"].ToString();
                txtGeneralName.Text = dr["GeneralName"].ToString();
                LGenName = dr["GeneralName"].ToString();
                cmbCC.Text = dr["CC_Name"].ToString();
                CCCode = dr["CostCentreCode"].ToString();
                cmbCC.SelectedValue = int.Parse(CCCode);
                cmbCC_SelectedIndexChanged(cmbCC, new EventArgs());
                sCC = dr["CostCentreCode"].ToString(); 
                txtAssetDesc.Text = dr["AssetDescription"].ToString();
                cmbGroup.Text = dr["ass_head"].ToString();
                GroupId = dr["AssetGroupId"].ToString();
                sGroup = dr["AssetGroupId"].ToString();
                cmbCompany.Text = dr["compname"].ToString();
                Compld = dr["CompanyId"].ToString();
                sCompany = dr["CompanyId"].ToString();
                txtQuantity.Text = dr["Quantity"].ToString();
                LQuantity = dr["Quantity"].ToString();
                txtUnitPrice.Text = dr["UnitPriceBDT"].ToString();
                LUnitPriceBdt = dr["UnitPriceBDT"].ToString();
                txtInvCostBdt.Text = dr["InvoiceCostBDT"].ToString();
                LInvCostBdt = dr["InvoiceCostBDT"].ToString();
                txtInvCostUsd.Text = dr["InvoiceCostUSD"].ToString();
                LInvCostUsd = dr["InvoiceCostUSD"].ToString();
                txtDutiesNothers.Text = dr["DutiesnOthers"].ToString();
                LDutiesNothers = dr["DutiesnOthers"].ToString();
                txtAccumulatDep.Text = dr["CostAmt"].ToString();
                LAccumulatDep = dr["CostAmt"].ToString();
                txtSalvageValue.Text = dr["SalvageAmt"].ToString();
                LSalvageValue = dr["SalvageAmt"].ToString();
                dtpPurchaseDate.Text = dr["PurchaseDate"].ToString();
                LPurchaseDate = dr["PurchaseDate"].ToString();
                cmbDepMethod.Text = dr["DepreciationMethod"].ToString();
                LDepMethod = dr["DepreciationMethod"].ToString();
                txtDepRate.Text = dr["DepreciationRate"].ToString();
                LDepRate = dr["DepreciationRate"].ToString();
                txtAssetLife.Text = dr["AssetLife"].ToString();
                LAssetLife = dr["AssetLife"].ToString();
                dtpYear.Value = Convert.ToDateTime(dr["DepreciationStartDate"].ToString());
                dtpMonth.Value = Convert.ToDateTime(dr["DepreciationStartDate"].ToString());
                dtpDepEffectDate.Value = Convert.ToDateTime(dr["DepreciationEffectDate"].ToString());
                LDepEffectDate = dtpDepEffectDate.Value.Year + "-" + dtpDepEffectDate.Value.Month + "-01";
                txtAssetLoc.Text = dr["AssetLoc"].ToString();
                sAssetLocId = dr["AssetLocId"].ToString();
                dgvAssetLoc.Visible = false;
                txtAssetSubLoc.Text = dr["AssetSubLoc"].ToString();
                sAssetSubLocId = dr["AssetSubLocId"].ToString();
                dgvAssetSubLoc.Visible = false;
                AssetLocId = int.Parse(sAssetLocId);
                LoadAssetSubLoc();
                txtAssetUser.Text = dr["AssetUser"].ToString();
                txtAssetUser.Tag = dr["AssetUserCode"].ToString();
                AssetUser = dr["AssetUserCode"].ToString();
                sAssetUser = dr["AssetUser"].ToString();
                sAssetUserCode = dr["AssetUserCode"].ToString();
                dgvAssetUser.Visible = false;
                txtSupplier.Text = dr["Supplier"].ToString();
                sSupplierId = dr["SupplierId"].ToString();
                SupplierId = int.Parse(dr["SupplierId"].ToString());
                LSupplierId = dr["SupplierId"].ToString();
                dgvSupplier.Visible = false;
                txtCountry.Text = dr["Country"].ToString();
                txtBrand.Text = dr["Brand"].ToString();
                txtModelNo.Text = dr["ModelNo"].ToString();
                txtLCNo.Text = dr["LCNo"].ToString();
                txtVoucherNo.Text = dr["VoucherNo"].ToString();
                txtRegNo.Text = dr["RegistrationNo"].ToString();
                if (Convert.ToInt32(dr["IsActive"].ToString()) == 1)
                {
                    cmbActive.Text = "Yes";
                    LActive = "1";
                }
                else
                {
                    cmbActive.Text = "No";
                    LActive = "0";
                }
                txtComments.Text = dr["Comments"].ToString();
                txtWPeriod.Text = dr["Warranty"].ToString();
                if (string.IsNullOrEmpty(txtWPeriod.Text) || Convert.ToDouble(dr["Warranty"].ToString()) == 0.00)
                    rbNo.Checked = true;
                else
                    rbYes.Checked = true;
                cmbCustodianDept.Text = dr["DepartmentName"].ToString();
                CustodianDeptCode = dr["DeptCode"].ToString();
                txtAccCode.Text = dr["AccCode"].ToString();
                sAccCode = dr["AccCode"].ToString();
                WDV = Convert.ToDouble(dr["WDV"].ToString());
                chkWDV.Checked = dr["IsWDVUpdate"].ToString().Equals("1");
                dtpAssetRecDate.Value = Convert.ToDateTime(dr["AssetRecDate"].ToString());
                dtpInvBillDate.Value = Convert.ToDateTime(dr["BillDate"].ToString());
            }
        }

        private void dgvAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvAssetCode.Rows[dgvAssetCode.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvAssetCode.Rows[dgvAssetCode.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvAssetCode.Rows[dgvAssetCode.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvAssetCode.Visible = false;
                LoadAssetDetails();
            }
        }

        private void dgvAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvAssetCode_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvAssetCode.Rows[dgvAssetCode.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvAssetCode.Rows[dgvAssetCode.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvAssetCode.Rows[dgvAssetCode.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvAssetCode.Visible = false;
            LoadAssetDetails();
        }

        private void txtInvCostUsd_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtDutiesNothers_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAccumulatDep_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtDepRate_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetLife_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtWrittenDownVal_Enter(object sender, EventArgs e)
        {
            //SelectionTextString(sender, e);
        }

        private void txtSalvageValue_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtVoucherNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtWPeriod_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtComments_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetDesc_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtRegNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtSupplier_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtModelNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtBrand_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtLCNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtCountry_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetUser_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetSubLoc_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetLoc_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAssetCode_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAsset_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void cmbDepMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDepMethod.SelectedIndex == 2)
            {
                MessageBox.Show(@"Please Select another Depreciation Method due to Double Declining Method has not been implemented in the System.", 
                                 "STOP", MessageBoxButtons.OK,MessageBoxIcon.Stop);
                cmbDepMethod.SelectedIndex = -1;
            }
        }

        private void cmbCompany_DropDown(object sender, EventArgs e)
        {
            dbConAssetEntry.DisposeChecking(cmbCompany);
        }

        private void cmbCC_DropDown(object sender, EventArgs e)
        {
            dbConAssetEntry.DisposeChecking(cmbCC);
        }

        private void cmbGroup_DropDown(object sender, EventArgs e)
        {
            dbConAssetEntry.DisposeChecking(cmbGroup);
        }

        private void txtAsset_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtAsset);
        }

        private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCompany.SelectedIndex > -1)
            {
                Compld = cmbCompany.SelectedValue.ToString();
            }
        }

        private void txtDutiesNothers_TextChanged(object sender, EventArgs e)
        {
            double InvCostBdt = 0.00, DutNoth = 0.00, AccDep = 0.00;

            if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
            if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                DutNoth = Convert.ToDouble(txtDutiesNothers.Text);
            if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                AccDep = Convert.ToDouble(txtAccumulatDep.Text);

            txtWrittenDownVal.Text = (InvCostBdt + DutNoth - AccDep).ToString("##,##0.00");
        }

        private void txtAccumulatDep_TextChanged(object sender, EventArgs e)
        {
            double InvCostBdt = 0.00, DutNoth = 0.00, AccDep = 0.00;

            if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
            if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                DutNoth = Convert.ToDouble(txtDutiesNothers.Text);
            if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                AccDep = Convert.ToDouble(txtAccumulatDep.Text);

            txtWrittenDownVal.Text = (InvCostBdt + DutNoth - AccDep).ToString("##,##0.00");
        }

        private void txtAccumulatDep_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
            {
                double s = Convert.ToDouble(txtAccumulatDep.Text);
                txtAccumulatDep.Text = s.ToString("##,##0.00");
            }
            else
                txtAccumulatDep.Text = "0.00";
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (TruncateTable())
                InsertDataToTempTable();
        }

        private bool TruncateTable()
        {
            if ((bool)dbConAssetEntry.DBCRUDFunction("TRUNCATE TABLE RptAssetPrintPreview", false))
                return true;
            else
                return false;
        }

        private void InsertDataToTempTable() 
        {
            if (!dbConAssetEntry.CheckEmptyOrNull(cmbCompany, "Select Company") &&
                !dbConAssetEntry.CheckEmptyOrNull(cmbCC, "Select Cost Centre") &&
                !dbConAssetEntry.CheckEmptyOrNull(cmbGroup, "Select Group") &&
                //!dbConAssetEntry.CheckEmptyOrNull(cmbCustodianDept, "Select Custodian Dept.") &&
                !dbConAssetEntry.CheckEmptyOrNull(txtAsset, "Enter Generic Name") && 
                !dbConAssetEntry.CheckEmptyOrNull(txtGeneralName, "Enter General Name") &&
                !dbConAssetEntry.CheckEmptyOrNull(txtAssetCode, "Enter Asset Code") &&
                !dbConAssetEntry.CheckEmptyOrNull(txtInvCostBdt, "Enter Invoice Cost (BDT)"))
            {
                double AccDep, SalvageAmt, DepRate, AssetLife, Warranty, Quantity, UnitPriceBdt, InvCostBdt, InvCostUsd, DutiesNothers, WDV;
                string Asset, GenName, Country, LCNo, Brand, ModelNo, RegNo, AssetDesc, purchaseDate, DepMethod, DepEffDate,
                    VoucherNo, Comments, Active = "", AssetRecDate, ConvRate, CurType, AssetSLNo, CustodianDept;

                if (!string.IsNullOrEmpty(txtAsset.Text))
                    Asset = dbConAssetEntry.SingleQuoteHandler(txtAsset.Text);
                else
                    Asset = "";

                if (!string.IsNullOrEmpty(txtGeneralName.Text))
                    GenName = dbConAssetEntry.SingleQuoteHandler(txtGeneralName.Text);
                else
                    GenName = "";

                if (!string.IsNullOrEmpty(txtCountry.Text))
                    Country = dbConAssetEntry.SingleQuoteHandler(txtCountry.Text);
                else
                    Country = "";

                if (!string.IsNullOrEmpty(txtLCNo.Text))
                    LCNo = dbConAssetEntry.SingleQuoteHandler(txtLCNo.Text);
                else
                    LCNo = "";

                if (!string.IsNullOrEmpty(txtBrand.Text))
                    Brand = dbConAssetEntry.SingleQuoteHandler(txtBrand.Text);
                else
                    Brand = "";

                if (!string.IsNullOrEmpty(txtModelNo.Text))
                    ModelNo = dbConAssetEntry.SingleQuoteHandler(txtModelNo.Text);
                else
                    ModelNo = "";

                if (!string.IsNullOrEmpty(txtRegNo.Text))
                    RegNo = dbConAssetEntry.SingleQuoteHandler(txtRegNo.Text);
                else
                    RegNo = "";

                if (!string.IsNullOrEmpty(txtAssetDesc.Text))
                    AssetDesc = dbConAssetEntry.SingleQuoteHandler(txtAssetDesc.Text);
                else
                    AssetDesc = "";

                purchaseDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

                if (!string.IsNullOrEmpty(txtQuantity.Text))
                    Quantity = Convert.ToInt32(txtQuantity.Text.Replace(",", ""));
                else
                    Quantity = 1;

                if (!string.IsNullOrEmpty(txtUnitPrice.Text))
                    UnitPriceBdt = Convert.ToDouble(txtUnitPrice.Text);
                else
                    UnitPriceBdt = 0.00;

                if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                    InvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
                else
                    InvCostBdt = 0.00;

                if (!string.IsNullOrEmpty(txtInvCostUsd.Text))
                    InvCostUsd = Convert.ToDouble(txtInvCostUsd.Text);
                else
                    InvCostUsd = 0.00;

                if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                    DutiesNothers = Convert.ToDouble(txtDutiesNothers.Text);
                else
                    DutiesNothers = 0.00;

                if (cmbDepMethod.SelectedIndex > -1)
                {
                    if (cmbDepMethod.SelectedIndex == 2)
                    {
                        MessageBox.Show(@"Double Declninig Method has not been set for Depreciation Process. Please,
                                          Contact with IT Person to Set.", "Stop",
                                         MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                        DepMethod = cmbDepMethod.Text;
                }
                else
                    DepMethod = "";

                DepEffDate = dtpMonth.Value.Year + "-" + dtpYear.Value.Month + "-01";

                if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                    AccDep = Convert.ToDouble(txtAccumulatDep.Text);
                else
                    AccDep = 0.00;

                if (!string.IsNullOrEmpty(txtDepRate.Text))
                    DepRate = Convert.ToDouble(txtDepRate.Text);
                else
                    DepRate = 0.00;

                if (!string.IsNullOrEmpty(txtAssetLife.Text))
                    AssetLife = Convert.ToDouble(txtAssetLife.Text);
                else
                    AssetLife = 0.00;

                if (!string.IsNullOrEmpty(txtWrittenDownVal.Text))
                    WDV = Convert.ToDouble(txtWrittenDownVal.Text);
                else
                    WDV = 0.00;

                if (!string.IsNullOrEmpty(txtSalvageValue.Text))
                    SalvageAmt = Convert.ToDouble(txtSalvageValue.Text);
                else
                    SalvageAmt = 0.00;

                if (cmbActive.SelectedIndex == 1)
                    Active = "Yes";
                else if (cmbActive.SelectedIndex == 2)
                    Active = "No";

                if (!string.IsNullOrEmpty(txtVoucherNo.Text))
                    VoucherNo = dbConAssetEntry.SingleQuoteHandler(txtVoucherNo.Text);
                else
                    VoucherNo = "";

                if (!string.IsNullOrEmpty(txtWPeriod.Text))
                    Warranty = Convert.ToDouble(txtWPeriod.Text);
                else
                    Warranty = 0.00;

                if (!string.IsNullOrEmpty(txtComments.Text))
                    Comments = dbConAssetEntry.SingleQuoteHandler(txtComments.Text);
                else
                    Comments = "";

                if (!string.IsNullOrEmpty(txtAssetSlNo.Text))
                    AssetSLNo = txtAssetSlNo.Text;
                else
                    AssetSLNo = "0";

                if (!string.IsNullOrEmpty(txtConvRate.Text))
                    ConvRate = txtConvRate.Text;
                else
                    ConvRate = "0.00";

                if (!string.IsNullOrEmpty(cmbCurrencyType.Text))
                    CurType = cmbCurrencyType.Text;
                else
                    CurType = "";

                if (!string.IsNullOrEmpty(cmbCustodianDept.Text))
                    CustodianDept = cmbCustodianDept.Text;
                else
                    CustodianDept = "";

                AssetRecDate = dtpAssetRecDate.Value.ToString("yyyy-MM-dd");



                string[] cols = { "[Company]","[CostCentre]","[AssetGroup]","[AssetCode]", "[AssetName]", "[GeneralName]", 
                                  "[AssetLocation]","[AssetSubLocation]", "[AssetUser]", "[Country]", "[LCNo]", 
                                  "[Brand]", "[Model]", "[Supplier]", "[RegNo]", "[AssetDesc]", "[PurchaseDate]", "[Quantity]", 
                                  "[UnitPriceBdt]", "[InvoiceCostBdt]", "[InvoiceCostUsd]","[DutiesnOthers]","[DepMethod]","[DepEffectDate]", 
                                  "[AccDep]", "[DepRate]", "[AssetLife]", "[WDV]", "[SalvageValue]", "[IsActive]", 
                                  "[VoucherNo]", "[WPeriod]", "[Comments]", "[CustodianDept]", "[AssetSLNo]", 
                                  "[ConvRate]", "[CurType]", "[AssetRecDate]" };

                string[] vals = { cmbCompany.Text.Trim(),cmbCC.Text.Trim(), cmbGroup.Text.Trim(),AutoGenAssetCode(cmbGroup.SelectedValue.ToString()),
                                  Asset,GenName,AssetLoc,AssetSubLoc,txtAssetUser.Text.Trim(),Country,LCNo,Brand,ModelNo,Supplier,RegNo,AssetDesc,
                                  purchaseDate,Quantity.ToString(),UnitPriceBdt.ToString(),InvCostBdt.ToString(),InvCostUsd.ToString(),DutiesNothers.ToString(),
                                  DepMethod,DepEffDate,AccDep.ToString(),DepRate.ToString(),AssetLife.ToString(),
                                  WDV.ToString(),SalvageAmt.ToString(),Active,VoucherNo, Warranty.ToString(),Comments,
                                  CustodianDept, AssetSLNo, ConvRate, CurType, AssetRecDate
                                };
                //string sql = dbConAssetEntry.CreateSQLStatement("[AssetRegistar].[dbo].[RptAssetPrintPreview]", cols, vals, "INSERT", null, null);

                if ((bool)dbConAssetEntry.DBCRUDFunction(dbConAssetEntry.CreateSQLStatement("[AssetRegistar].[dbo].[RptAssetPrintPreview]", cols, vals, "INSERT", null, null), false))
                {
                    ShowPrintPreview();
                }
            }
        }

        private void ShowPrintPreview()
        {            
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;
            
            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptAssetPrintPreview.rpt");
                cryReport.SetParameterValue("@AssetId", 0);
                dbConAssetEntry.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.Text = "Asset Info. Entry Report";
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.crViewer.Refresh();
                rptForm.crViewer.ShowExportButton = false;
                rptForm.crViewer.ShowPrintButton = false;
                rptForm.Show();
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbCustodianDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCustodianDept.SelectedIndex > -1)
                CustodianDeptCode = cmbCustodianDept.SelectedValue.ToString();
            else
                CustodianDeptCode = "0";
        }

        private void cmbCustodianDept_DropDown(object sender, EventArgs e)
        {
            dbConAssetEntry.DisposeChecking(cmbCustodianDept);
        }

        private void frmAssetConfigN_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "New");
        }

        private void btnPreview_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnPreview, "Preview");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();

            if (IsUpdate)    
                ttip.SetToolTip(this.btnUpdate, "Update");
            else             
                ttip.SetToolTip(this.btnUpdate, "Edit");
        }

        private void btnRfrshSupplier_Click(object sender, EventArgs e)
        {
            dbConAssetEntry.FillDataGridView(dgvSupplier, @"SELECT SupplierId, Supplier FROM [AssetRegistar].[dbo].[AR_AssetSupplier] WHERE IsActive = 1");
            dgvSupplier.Columns[0].Visible = false;
            dgvSupplier.Columns[1].Width = 200;
        }

        private void btnRfrshSupplier_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnRfrshSupplier, "Refresh Supplier");
        }

        private void txtCountry_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtCountry);
        }

        private void txtBrand_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtBrand);
        }

        private void txtConvRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtConvRate, e);
        }

        private void txtConvRate_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtConvRate_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtConvRate.Text))
            {
                double s = Convert.ToDouble(txtConvRate.Text);
                txtConvRate.Text = s.ToString("##,##0.00");
            }
            else
                txtConvRate.Text = "0.00";
        }

        private void txtAssetSlNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.IntergersOnly(txtAssetSlNo, e);
        }

        private void btnRfrshLoc_Click(object sender, EventArgs e)
        {
            if (cmbCC.SelectedIndex > -1)
            {
                CCCode = cmbCC.SelectedValue.ToString();
                dbConAssetEntry.FillDataGridView(dgvAssetLoc, @"SELECT AssetLocId,
                                                                       AssetLocation
                                                                FROM   AR_AssetLocation
                                                                WHERE  CostCentreCode = '" + CCCode + "'");
                dgvAssetLoc.Columns[0].Visible = false;
                dgvAssetLoc.Columns[1].Width = 200;
            }
        }

        private void btnRfrshSubLoc_Click(object sender, EventArgs e)
        {
            if (cmbCC.SelectedIndex > -1 && AssetLocId > 0)
            {
                dbConAssetEntry.FillDataGridView(dgvAssetSubLoc, @"SELECT AssetSubLocId,
                                                                          AssetSubLocation
                                                                   FROM   AR_AssetSubLocation 
                                                                   WHERE  AssetLocId =" + AssetLocId);
                dgvAssetSubLoc.Columns[0].Visible = false;
                dgvAssetSubLoc.Columns[1].Width = 200;
            }
        }

        private string LoadCmbDepMethod(string CompanyId, string GroupId)
        {
            SqlDataReader dr;
            string DepMethod = "";
            dr = (SqlDataReader)dbConAssetEntry.DBCRUDFunction(@"SELECT DepreciationMethod FROM AR_DepreciationConfig 
                                                                 WHERE CompanyId = '" + CompanyId 
                                                                 + "' AND AssetGroupId = '" + GroupId + "'", true);
            if (dr.Read())
                DepMethod = dr["DepreciationMethod"].ToString();

            return DepMethod; 
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (IsUpdate)
            {
                DialogResult result = MessageBox.Show("Would you like to copy asset ", "Copy?",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    if (!dbConAssetEntry.CheckEmptyOrNull(cmbCompany, "Select Company") &&
                        !dbConAssetEntry.CheckEmptyOrNull(cmbCC, "Select Cost Centre") &&
                        !dbConAssetEntry.CheckEmptyOrNull(cmbGroup, "Select Group") &&
                        //!dbConAssetEntry.CheckEmptyOrNull(cmbCustodianDept, "Select Custodian Dept.") &&
                        !dbConAssetEntry.CheckEmptyOrNull(txtAsset, "Enter Generic Name") &&
                        !dbConAssetEntry.CheckEmptyOrNull(txtGeneralName, "Enter General Name") &&
                        !dbConAssetEntry.CheckEmptyOrNull(txtAssetCode, "Enter Asset Code") &&
                        !dbConAssetEntry.CheckEmptyOrNull(txtInvCostBdt, "Enter Invoice Cost (BDT)"))
                    {
                        if (cmbActive.SelectedIndex == 0)
                            sactive = 1;
                        
                        if (!string.IsNullOrEmpty(txtAsset.Text))
                            sAsset = dbConAssetEntry.SingleQuoteHandler(txtAsset.Text);
                        else
                            sAsset = "";

                        if (!string.IsNullOrEmpty(txtGeneralName.Text))
                            sGenName = dbConAssetEntry.SingleQuoteHandler(txtGeneralName.Text);
                        else
                            sGenName = "";

                        if (!string.IsNullOrEmpty(txtCountry.Text))
                            sCountry = dbConAssetEntry.SingleQuoteHandler(txtCountry.Text);
                        else
                            sCountry = "";

                        if (!string.IsNullOrEmpty(txtLCNo.Text))
                            sLCNo = dbConAssetEntry.SingleQuoteHandler(txtLCNo.Text);
                        else
                            sLCNo = "";

                        if (!string.IsNullOrEmpty(txtBrand.Text))
                            sBrand = dbConAssetEntry.SingleQuoteHandler(txtBrand.Text);
                        else
                            sBrand = "";

                        if (!string.IsNullOrEmpty(txtModelNo.Text))
                            sModelNo = dbConAssetEntry.SingleQuoteHandler(txtModelNo.Text);
                        else
                            sModelNo = "";

                        if (!string.IsNullOrEmpty(txtRegNo.Text))
                            sRegNo = dbConAssetEntry.SingleQuoteHandler(txtRegNo.Text);
                        else
                            sRegNo = "";

                        if (!string.IsNullOrEmpty(txtAssetDesc.Text))
                            sAssetDesc = dbConAssetEntry.SingleQuoteHandler(txtAssetDesc.Text);
                        else
                            sAssetDesc = "";

                        sPurchaseDate = dtpPurchaseDate.Value.ToString("yyyy-MM-dd");

                        if (!string.IsNullOrEmpty(txtQuantity.Text))
                            sQuantity = Convert.ToInt32(txtQuantity.Text.Replace(",", ""));
                        else
                            sQuantity = 1;

                        if (!string.IsNullOrEmpty(txtUnitPrice.Text))
                            sUnitPriceBdt = Convert.ToDouble(txtUnitPrice.Text);
                        else
                            sUnitPriceBdt = 0.00;

                        if (!string.IsNullOrEmpty(txtInvCostBdt.Text))
                        {
                            sInvCostBdt = Convert.ToDouble(txtInvCostBdt.Text);
                            sWDV = Convert.ToDouble(txtInvCostBdt.Text);
                        }
                        else
                        {
                            sInvCostBdt = 0.00;
                            sWDV = 0.00;
                        }

                        if (!string.IsNullOrEmpty(txtInvCostUsd.Text))
                            sInvCostUsd = Convert.ToDouble(txtInvCostUsd.Text);
                        else
                            sInvCostUsd = 0.00;

                        if (!string.IsNullOrEmpty(txtDutiesNothers.Text))
                            sDutiesNothers = Convert.ToDouble(txtDutiesNothers.Text);
                        else
                            sDutiesNothers = 0.00;

                        if (cmbDepMethod.SelectedIndex > -1)
                        {
                            if (cmbDepMethod.SelectedIndex == 2)
                            {
                                MessageBox.Show(@"Double Declninig Method has not been set for Depreciation Process. Please,
                                          Contact with IT Person to Set.", "Stop",
                                                 MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            else
                                sDepMethod = cmbDepMethod.Text;
                        }
                        else
                            sDepMethod = "";

                        sDepEffDate = dtpYear.Value.Year + "-" + dtpMonth.Value.Month + "-01";

                        if (!string.IsNullOrEmpty(txtAccumulatDep.Text))
                            sAccDep = Convert.ToDouble(txtAccumulatDep.Text);
                        else
                            sAccDep = 0.00;

                        if (!string.IsNullOrEmpty(txtDepRate.Text))
                            sDepRate = Convert.ToDouble(txtDepRate.Text);
                        else
                            sDepRate = 0.00;

                        if (!string.IsNullOrEmpty(txtAssetLife.Text))
                            sAssetLife = Convert.ToDouble(txtAssetLife.Text);
                        else
                            sAssetLife = 0.00;

                        //if (!string.IsNullOrEmpty(txtWrittenDownVal.Text))
                        //    WDV = Convert.ToDouble(txtWrittenDownVal.Text);
                        //else
                        //    WDV = 0.00;

                        if (!string.IsNullOrEmpty(txtSalvageValue.Text))
                            sSalvageAmt = Convert.ToDouble(txtSalvageValue.Text);
                        else
                            sSalvageAmt = 0.00;

                        if (cmbActive.SelectedIndex == 1)
                            sactive = 1;
                        else if (cmbActive.SelectedIndex == 2)
                            sactive = 0;

                        if (!string.IsNullOrEmpty(txtVoucherNo.Text))
                            sVoucherNo = dbConAssetEntry.SingleQuoteHandler(txtVoucherNo.Text);
                        else
                            sVoucherNo = "";

                        if (!string.IsNullOrEmpty(txtWPeriod.Text))
                            sWarranty = Convert.ToDouble(txtWPeriod.Text);
                        else
                            sWarranty = 0.00;

                        if (cmbCustodianDept.SelectedIndex > -1)
                            sCustDeptId = cmbCustodianDept.SelectedValue.ToString();

                        if (!string.IsNullOrEmpty(txtComments.Text))
                            sComments = dbConAssetEntry.SingleQuoteHandler(txtComments.Text);
                        else
                            sComments = "";

                        if (!string.IsNullOrEmpty(txtAssetSlNo.Text))
                            sAssetSLNo = txtAssetSlNo.Text;
                        else
                            sAssetSLNo = "0";

                        if (!string.IsNullOrEmpty(txtConvRate.Text))
                            sConvRate = Convert.ToDouble(txtConvRate.Text);
                        else
                            sConvRate = 0.00;

                        if (!string.IsNullOrEmpty(cmbCurrencyType.Text))
                            sCurType = cmbCurrencyType.Text;
                        else
                            sCurType = "";

                        //sWDV = (InvCostUsd * ConvRate) + DutiesNothers;

                        sAssetRecDate = dtpAssetRecDate.Value.ToString("yyyy-MM-dd");
                        sAccCode = dbConAssetEntry.SingleQuoteHandler(txtAccCode.Text);
                    }

                    frmShowDialog frm = new frmShowDialog();
                    //frm.Parent = parent;
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.ShowDialog();
                }
            }
        }

        private void btnCopy_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCopy, "Copy");
        }

        private void dgvAssetUser_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                m.MenuItems.Add(new MenuItem("Copy", CopyClick));
                int currentMouseOverRow = dgvAssetUser.HitTest(e.X, e.Y).RowIndex;
                m.Show(dgvAssetUser, new Point(e.X, e.Y));
            }
        }

        //DataGridViewCell ActiveCell = null;
        private void CopyClick(object sender, EventArgs e)
        {
            //DataGridView.HitTestInfo hittestinfo = dgvAssetUser.HitTest(1, 1);
            if(dgvAssetUser.RowCount > 0)
                Clipboard.SetText(dgvAssetUser[1, dgvAssetUser.CurrentCell.RowIndex].Value.ToString()
                    + " ["+dgvAssetUser[2, dgvAssetUser.CurrentCell.RowIndex].Value.ToString()+ "]");
            //if (hittestinfo != null && hittestinfo.Type == DataGridViewHitTestType.Cell)
            //{
                //ActiveCell = dgvAssetUser[hittestinfo.ColumnIndex, hittestinfo.RowIndex];
                //ActiveCell.Selected = true;
                //contextMenu.Show(dataGridView1, new Point(e.X, e.Y));
            //}
        }

        private void dtpPurchaseDate_Leave(object sender, EventArgs e)
        {
            DateTime DepEffectDate = dtpPurchaseDate.Value.Date.AddMonths(2).AddDays(-dtpPurchaseDate.Value.Day);
            dtpMonth.Value = DepEffectDate; dtpYear.Value = DepEffectDate;

            if (!IsUpdate)
                dtpDepEffectDate.Value = DepEffectDate;
        }

        private void txtUnitPrice_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUnitPrice.Text))
            {
                double s = Convert.ToDouble(txtUnitPrice.Text);
                txtUnitPrice.Text = s.ToString("##,##0.00");
            }
            else
                txtUnitPrice.Text = "0.00";

            if (String.IsNullOrEmpty(txtUnitPrice.Text) || String.IsNullOrEmpty(txtQuantity.Text))
                txtInvCostBdt.Text = "0.00";
            else
                txtInvCostBdt.Text = (Convert.ToDouble(txtUnitPrice.Text) * Convert.ToDouble(txtQuantity.Text)).ToString("##,##0.00");
        }

        private void txtUnitPrice_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtQuantity_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtUnitPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtUnitPrice, e);
        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DecimalNumbers(txtQuantity, e);
        }

        private void txtQuantity_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtQuantity.Text))
            {
                double s = Convert.ToDouble(txtQuantity.Text); s = s == 0 ? 1 : s;
                txtQuantity.Text = s.ToString("##,##0");
            }
            else
                txtQuantity.Text = "1";

            if (String.IsNullOrEmpty(txtUnitPrice.Text) || String.IsNullOrEmpty(txtQuantity.Text))
                txtInvCostBdt.Text = "0.00";
            else
                txtInvCostBdt.Text = (Convert.ToDouble(txtUnitPrice.Text) * Convert.ToDouble(txtQuantity.Text)).ToString("##,##0.00");
        }

        private void txtGeneralName_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtGeneralName_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAssetEntry.DisposeChecking(txtGeneralName);
        }

        private void txtGeneralName_TextChanged(object sender, EventArgs e)
        {
            dbConAssetEntry.MakeTextUppercase(txtGeneralName);
        }
    }
}
