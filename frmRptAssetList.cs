﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Runtime.InteropServices;

namespace AssetRegister.Reports
{
    public partial class frmRptAssetList : Form
    {
        private clsCommonFunctions dbConRptAssetList = new clsCommonFunctions();
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmRptAssetList()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;

            string CostCentreCode = "", CompanyId = "", frmPurchaseDate = "", toPurchaseDate = "";
            int IsCompany = 0;

            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "PL";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "CP";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }
            else
                CompanyId = "";

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }

            if (chkOnlyCompany.Checked)
            {
                IsCompany = 1;
            }

            if (!chkAllDate.Checked)
            {
                frmPurchaseDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
                toPurchaseDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            }
            else
            {
                frmPurchaseDate = "";
                toPurchaseDate = "";
            }
            
            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptAssetListLand.rpt");
                cryReport.SetParameterValue("@CompId", CompanyId);
                cryReport.SetParameterValue("@CostCentre", CostCentreCode);
                cryReport.SetParameterValue("@frmPurchaseDate", frmPurchaseDate);
                cryReport.SetParameterValue("@toPurchaseDate", toPurchaseDate);
                cryReport.SetParameterValue("@IsCompany", IsCompany);
                cryReport.SetParameterValue("@UserId", frmLogin.userId);
                dbConRptAssetList.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.Text = "Land Register Report";
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.crViewer.Refresh();
                rptForm.Show();
                
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void frmRptAssetList_Load(object sender, EventArgs e)
        {
            dbConRptAssetList.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptAssetList.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            label3.Text = "Date";
            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;

            dtpFrmDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpToDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
        }

        private void chkAllDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFrmDate.Enabled = !chkAllDate.Checked;
            dtpToDate.Enabled = !chkAllDate.Checked;
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void frmRptAssetList_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }
    }
}
