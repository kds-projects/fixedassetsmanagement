﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace AssetRegister
{
    public partial class frmRptLanDetails : Form
    {
        private clsCommonFunctions dbConRptLand = new clsCommonFunctions();
        private int CostCentreCode = 0, AssetId = 0;
        private string CompanyId = "", AssetCode = "";

        public frmRptLanDetails()
        {
            InitializeComponent();
        }

        private void frmRptLanDetails_Load(object sender, EventArgs e)
        {
            dbConRptLand.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptLand.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            
            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;

            dbConRptLand.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '010110', '', 1");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            
            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "PL";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "CP";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = Convert.ToInt32(cmbCC.SelectedValue);
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }

            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptLandProfile.rpt");
                cryReport.SetParameterValue("@assetId", AssetId);
                dbConRptLand.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.Text = "Land Details Report";
                rptForm.crViewer.Refresh();
                rptForm.Show();
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConRptLand.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }
    }
}
