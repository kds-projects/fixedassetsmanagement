﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmSupplier : Form
    {
        private clsCommonFunctions dbConSupplier = new clsCommonFunctions();
        private int SupplierId = 0;        
        private bool IsUpdate = false;
        private string pSupplier = "", pType = "", pContactNo = "", pAddress = "";
        private int pActive = 0;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmSupplier()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConSupplier.CheckEmptyOrNull(txtSupplier, "Enter Supplier Name") &&
                !dbConSupplier.CheckEmptyOrNull(cmbType, "Select Supplier Type"))
            {
                int IsActive;

                if (rbYes.Checked)
                    IsActive = 1;
                else
                    IsActive = 0;

                string[] cols = { "[Supplier]", "[SupplierCode]", "[SupplierType]", "[Address]", "[ContactNo]", "[IsActive]", "[EntryBy]" };
                string[] vals = { dbConSupplier.SingleQuoteHandler(txtSupplier), GenSuppCode(), cmbType.Text.Trim(), 
                                  dbConSupplier.SingleQuoteHandler(txtAddress), txtContactNo.Text.Trim(), 
                                  IsActive.ToString(), frmLogin.userId.ToString() };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetSupplier]", this.Name, 
                                   dbConSupplier.getMaxIdentity("[AR_AssetSupplier]", "[SupplierId]").ToString(), "", "" };

                string sql = dbConSupplier.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSupplier]", cols, vals, "INSERT", null, null);
                sql += dbConSupplier.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                                
                if ((bool)dbConSupplier.DBCRUDFunction(dbConSupplier.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSupplier]", cols, vals, "INSERT", null, null), false))
                {
                    ResetAll();
                    MessageBox.Show("Supplier Saved Successfully.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dbConSupplier.FillDataGridView(dgvSupplier, @"SELECT SupplierId,
                                                                         Supplier,
                                                                         SupplierCode,
                                                                         SupplierType,
                                                                         [Address],
                                                                         ContactNo,
                                                                         CASE WHEN IsActive = 1 THEN 'YES' ELSE 'NO' END IsActive
                                                                  FROM   AR_AssetSupplier");
                }
                else
                {
                    MessageBox.Show("Failed to Save Supplier Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!dbConSupplier.CheckEmptyOrNull(txtSupplier, "Enter Supplier Name") &&
                !dbConSupplier.CheckEmptyOrNull(cmbType, "Select Supplier Type"))
            {
                int IsActive;

                if (rbYes.Checked)
                    IsActive = 1;
                else
                    IsActive = 0;

                string[] cols = { "[Supplier]", "[SupplierType]", "[Address]", "[ContactNo]", "[IsActive]", "[UpdateBy]", "[UpdateDate]" };
                string[] vals = { dbConSupplier.SingleQuoteHandler(txtSupplier), cmbType.Text.Trim(), 
                                  dbConSupplier.SingleQuoteHandler(txtAddress), txtContactNo.Text.Trim(), 
                                  IsActive.ToString(), frmLogin.userId.ToString(), dbConSupplier.GetServerCurrentDateTime().ToString()
                                };
                string[] whCols = { "[SupplierId]" };
                string[] whVals = { SupplierId.ToString() };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "UPDATE", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetSupplier]", this.Name, SupplierId.ToString(),
                                   "[SupplierId],[Supplier],[SupplierType],[Address],[ContactNo],[IsActive]", 
                                   SupplierId.ToString() + "," + dbConSupplier.SingleQuoteHandler(pSupplier) 
                                   + "," + pType + "," + dbConSupplier.SingleQuoteHandler(pAddress) 
                                   + "," + pContactNo + "," + pActive };

                string sql = dbConSupplier.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSupplier]", cols, vals, "UPDATE", whCols, whVals)
                             +
                             dbConSupplier.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                if ((bool)dbConSupplier.DBCRUDFunction(sql, false))
                {

                    MessageBox.Show("Supplier Updated Successfully.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dbConSupplier.FillDataGridView(dgvSupplier, @"SELECT SupplierId,
                                                                         Supplier,
                                                                         SupplierCode,
                                                                         SupplierType,
                                                                         [Address],
                                                                         ContactNo,
                                                                         CASE WHEN IsActive = 1 THEN 'YES' ELSE 'NO' END IsActive
                                                                  FROM   AR_AssetSupplier");
                }
                else
                {
                    MessageBox.Show("Failed to Update Supplier Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void ResetAll()
        {
            pActive = 0;
            pAddress = "";
            pContactNo = "";
            pSupplier = "";
            pType = "";
            txtSupplier.Text = "";
            txtSuppCode.Text = "";
            txtContactNo.Text = "";
            txtAddress.Text = "";
            cmbType.SelectedIndex = -1;
            txtSupplier.Focus();
            rbNo.Checked = false;
            rbYes.Checked = false;
            txtSuppCode.Text = GenSuppCode();
        }

        private string GenSuppCode()
        {
            SqlDataReader dr;
            string SuppCode = "";
            dr = (SqlDataReader)dbConSupplier.DBCRUDFunction(@"SELECT 'S' + REPLICATE(
                                                                           '0',
                                                                           9 - LEN(
                                                                               ISNULL(MAX(CONVERT(INT, SUBSTRING(SupplierCode, 2, 9))), 0) + 1
                                                                           )
                                                                       )
                                                                       + CAST(
                                                                           ISNULL(MAX(CONVERT(INT, SUBSTRING(SupplierCode, 2, 9))), 0) + 1 AS 
                                                                           VARCHAR
                                                                       ) SuppCode
                                                                FROM   AR_AssetSupplier", true);

            if (dr.Read())
                SuppCode =  dr["SuppCode"].ToString();

            return SuppCode;
        }

        private void frmSupplier_Load(object sender, EventArgs e)
        {
            dbConSupplier.FillDataGridView(dgvSupplier, @"SELECT SupplierId,
                                                                 Supplier,
                                                                 SupplierCode,
                                                                 SupplierType,
                                                                 [Address],
                                                                 ContactNo,
                                                                 CASE WHEN IsActive = 1 THEN 'YES'
                                                                    ELSE 'NO' END IsActive
                                                          FROM   AR_AssetSupplier");

            dgvSupplier.Columns["SupplierId"].Visible = false;
            dgvSupplier.Columns["SupplierCode"].HeaderText = "Supplier Code";
            dgvSupplier.Columns["SupplierType"].HeaderText = "Supplier Type";
            dgvSupplier.Columns["Address"].HeaderText = "Address";
            dgvSupplier.Columns["ContactNo"].HeaderText = "Contact No";
            dgvSupplier.Columns["IsActive"].HeaderText = "Active ?";
            dgvSupplier.Columns["Supplier"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;   

            txtSuppCode.Text = GenSuppCode();
        }

        private void dgvSupplier_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSupplier.Rows.Count > 0)
            {
                SupplierId = Convert.ToInt32(dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells["SupplierId"].Value.ToString());
                LoadSupplierDetails(SupplierId);
                IsUpdate = true;
            }
            if (!IsUpdate)
            {
                //btnSave.Visible = true;
                //btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                //btnSave.Visible = false;
                //btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }
        }

        private void LoadSupplierDetails(int SuppId)
        {
            SqlDataReader dr;
            dr = (SqlDataReader)dbConSupplier.DBCRUDFunction(@"SELECT Supplier,
                                                                      SupplierCode,
                                                                      SupplierType,
                                                                      [Address],
                                                                      ContactNo,
                                                                      IsActive
                                                                FROM  AR_AssetSupplier 
                                                                WHERE SupplierId = " + SuppId, true);
            if (dr.Read())
            {
                txtSupplier.Text = dr["Supplier"].ToString().Trim();
                pSupplier = dr["Supplier"].ToString().Trim();
                txtSuppCode.Text = dr["SupplierCode"].ToString().Trim();
                cmbType.Text = dr["SupplierType"].ToString().Trim();
                pType = dr["SupplierType"].ToString().Trim();
                txtContactNo.Text = dr["ContactNo"].ToString().Trim();
                pContactNo = dr["ContactNo"].ToString().Trim();
                txtAddress.Text = dr["Address"].ToString().Trim();
                pAddress = dr["Address"].ToString().Trim();
                if (Convert.ToInt32(dr["IsActive"].ToString()) == 1)
                    rbYes.Checked = true;
                else
                    rbNo.Checked = true;
                pActive = Convert.ToInt32(dr["IsActive"].ToString());
            }

            dr.Close();
        }

        private void txtSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConSupplier.DisposeChecking(txtSupplier);
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dbConSupplier.DisposeChecking(cmbType);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtSupplier_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtContactNo_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtAddress_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmSupplier_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnUpdate, "Update");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "New");
        }

        private void txtSupplier_TextChanged(object sender, EventArgs e)
        {
            dbConSupplier.MakeTextUppercase(txtSupplier);
        }
    }
}
