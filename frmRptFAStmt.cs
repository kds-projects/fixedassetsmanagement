﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmRptFAStmt : Form
    {
        private clsCommonFunctions dbConRptDepJournalChargeAmt = new clsCommonFunctions();
        private string AssetGroupId = "", CompanyId = "", CostCentreCode = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        int AssetUser = 0; string EmpDetails = "";


        public frmRptFAStmt()
        {
            InitializeComponent();
        }

        private void chkAllDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFrmDate.Enabled = !chkAllDate.Checked;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;

            string frmDate = "", toDate = "";

            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "PL";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "CP";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }
            else
                CompanyId = "";

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }
            else
                CostCentreCode = "";

            if (!chkAllGroup.Checked)
            {
                if (cmbGroup.SelectedIndex > -1)
                {
                    AssetGroupId = cmbGroup.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbGroup.Focus();
                    return;
                }
            }
            else
                AssetGroupId = "";

            if (!chkAllDate.Checked)
            {
                frmDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
                toDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            }
            else
            {
                frmDate = "1900-01-01";
                toDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            }

            //frmDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
            //toDate = dtpToDate.Value.ToString("yyyy-MM-dd");

            if (txtAssetUser.Text.Length == 0)
            {
                AssetUser = 0;
                EmpDetails = "";
            }

            try
            {
                if (!chkClearance.Checked)
                {
                    cryReport.Load(Application.StartupPath + @"\Reports\rptFAStatement.rpt");
                    cryReport.SetParameterValue("@StartDate", frmDate);
                    cryReport.SetParameterValue("@EndDate", toDate);
                    cryReport.SetParameterValue("@CompanyId", CompanyId);
                    cryReport.SetParameterValue("@CcCode", CostCentreCode);
                    cryReport.SetParameterValue("@GroupId", AssetGroupId);
                    cryReport.SetParameterValue("@UserId", frmLogin.userId);
                    cryReport.SetParameterValue("@EmpCode", AssetUser);
                    cryReport.SetParameterValue("@IsDepClose", chkOSV1.Checked ? 1 : 0);
                    cryReport.DataDefinition.FormulaFields["AssetUserName"].Text = "'" + EmpDetails + "'";
                    dbConRptDepJournalChargeAmt.SetReportLogon(cryReport);
                    rptForm.crViewer.ReportSource = null;
                    rptForm.Text = "Fixed Assets Statement Report";
                    rptForm.crViewer.ReportSource = cryReport;
                    rptForm.crViewer.Refresh();
                    rptForm.Show();
                    Cursor.Current = Cursors.Default;
                    frmLoading.Hide();
                }
                else
                {
                    if (AssetUser == 0)
                    {
                        MessageBox.Show("Please select valid employee.", "!!!Warning!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Cursor.Current = Cursors.Default;
                        EmpDetails = "";
                        frmLoading.Hide();
                        txtAssetUser.Focus();

                        return;
                    }
                    cryReport.Load(Application.StartupPath + @"\Reports\rptAssetClearanceCertificate.rpt");
                    cryReport.SetParameterValue("@EmployeeId", AssetUser);
                    cryReport.SetParameterValue("@UserId", frmLogin.userId);
                    dbConRptDepJournalChargeAmt.SetReportLogon(cryReport);
                    rptForm.crViewer.ReportSource = null;
                    rptForm.Text = "Clearance Certificate";
                    rptForm.crViewer.ReportSource = cryReport;
                    rptForm.crViewer.Refresh();
                    rptForm.Show();
                    Cursor.Current = Cursors.Default;
                    frmLoading.Hide();
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void frmRptAssetRegister_Load(object sender, EventArgs e)
        {
            dbConRptDepJournalChargeAmt.FillDataGridView(dgvAssetUser, @"EXEC prcCmbLoadEmployee 0");
            dgvAssetUser.Columns[0].Visible = false;
            dgvAssetUser.Columns[1].Width = 200;
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 2");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;

            dtpFrmDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpToDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void chkAllGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !chkAllGroup.Checked;
        }

        private void frmRptFAStmt_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }

        private void txtAssetUser_TextChanged(object sender, EventArgs e)
        {
            dbConRptDepJournalChargeAmt.MakeTextUppercase(txtAssetUser);
            var dataTable = (DataTable)dgvAssetUser.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvAssetUser.Visible = true;
                //dataView.RowFilter = dgvAssetUser.Columns[1].Name + " LIKE '%" + txtAssetUser.Text + "%'";
                dataView.RowFilter = string.Format("CONVERT(" + dgvAssetUser.Columns[1].Name + ", 'System.String') LIKE '%" + txtAssetUser.Text + "%'") + " OR "
                                   + string.Format("CONVERT(" + dgvAssetUser.Columns[2].Name + ", 'System.String') LIKE '%" + txtAssetUser.Text + "%'");
            }
        }

        private void txtAssetUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConRptDepJournalChargeAmt.DisposeChecking(txtAssetUser);
            if (e.KeyChar == (char)Keys.Escape)
                dgvAssetUser.Visible = false;
        }

        private void txtAssetUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvAssetUser.Focus();

            if (e.KeyCode == Keys.Enter && dgvAssetUser.Rows.Count > 0)
            {
                txtAssetUser.Text = dgvAssetUser.Rows[0].Cells[1].Value.ToString().Trim();
                AssetUser = Convert.ToInt32(dgvAssetUser.Rows[0].Cells[0].Value.ToString());
                EmpDetails = "[" + dgvAssetUser.Rows[0].Cells[2].Value.ToString().Trim() + "] - " + dgvAssetUser.Rows[0].Cells[1].Value.ToString().Trim();

                //AssetLoc = dgvAssetUser.Rows[0].Cells[1].Value.ToString();
                //LoadAssetDetails();
                dgvAssetUser.Visible = false;
                txtAssetUser.SelectionStart = txtAssetUser.TextLength;
            }
        }

        private void dgvAssetUser_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetUser = Convert.ToInt32(dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[0].Value.ToString());
            EmpDetails = "[" + dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[2].Value.ToString().Trim() + "] - " + dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
            txtAssetUser.Text = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
            txtAssetUser.Focus();
            txtAssetUser.SelectionStart = txtAssetUser.TextLength;
            dgvAssetUser.Visible = false;
        }

        private void dgvAssetUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetUser = Convert.ToInt32(dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[0].Value.ToString());
                EmpDetails = "[" + dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[2].Value.ToString().Trim() + "] - " + dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString().Trim();
                //AssetLoc = dgvAssetLoc.Rows[dgvAssetLoc.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetUser.Text = dgvAssetUser.Rows[dgvAssetUser.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetUser.Focus();
                txtAssetUser.SelectionStart = txtAssetUser.TextLength;
                dgvAssetUser.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvAssetUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetUser.Focus();
            txtAssetUser.SelectionStart = txtAssetUser.TextLength;
        }

        private void dgvAssetUser_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
