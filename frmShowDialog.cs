﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmShowDialog : Form
    {
        private clsCommonFunctions dbConShowDialog = new clsCommonFunctions();
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmShowDialog()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmSupplier_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnAccept, "Save");
        }

        private void txtTimes_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            string[] ACode = new string[Convert.ToInt32(txtTimes.Text)];
            int j = GetMaxCode(frmAssetConfigN.AssetCode.Substring(0, 6));

            int l = 7 - j.ToString().Length;
            string zeros = "";
            for (int z = 0; z < l; z++)
            {
                zeros += "0";
            }

            for (int runs = 0; runs < ACode.Length; runs++)
            {
                j = j + 1;
                string str = frmAssetConfigN.AssetCode.Substring(0, 6) + zeros + j.ToString();
                ACode[runs] = str;
            }

            string sql = "";
            for (int i = 0; i < Convert.ToInt32(txtTimes.Text); i++)
            {
                string[] cols = { "[AssetName]", "[GeneralName]", "[CostCentreCode]", "[AssetCode]", "[AssetDescription]", "[AssetGroupId]",
                              "[CompanyId]", "[PurchaseAmt]", "[Quantity]", "[UnitPriceBDT]", "[InvoiceCostBDT]", "[InvoiceCostUSD]", "[DutiesnOthers]", "[WDV]",
                              "[CostAmt]", "[SalvageAmt]", "[PurchaseDate]", "[DepreciationMethod]", "[DepreciationRate]",
                              "[AssetLife]","DepreciationStartDate", "DepreciationEndDate","[DepreciationEffectDate]","[AssetLocId]",
                              "[AssetSubLocId]", "[AssetUserCode]", "[AssetUser]", "[SupplierId]", "[Country]", "[Brand]", "[ModelNo]", "[LCNo]", 
                              "[VoucherNo]", "[RegistrationNo]", "[IsActive]", "[Comments]", "[Warranty]", "[UserId]", "[IsDepCalc]", "[IsCompanyAsset]",
                              "[CustodianDeptCode]", "[AssetSLNo]", "[ConvRate]", "[CurType]", "[AssetRecDate]", "[AccCode]"
                            };

                string[] vals = { frmAssetConfigN.sAsset, frmAssetConfigN.sGenName, frmAssetConfigN.sCC, ACode[i],
                              frmAssetConfigN.sAssetDesc, frmAssetConfigN.sGroup,frmAssetConfigN.sCompany, frmAssetConfigN.sInvCostBdt.ToString(), 
                              frmAssetConfigN.sQuantity.ToString(), frmAssetConfigN.sUnitPriceBdt.ToString(), frmAssetConfigN.sInvCostBdt.ToString(), frmAssetConfigN.sInvCostUsd.ToString(), frmAssetConfigN.sDutiesNothers.ToString(),
                              frmAssetConfigN.sWDV.ToString(), frmAssetConfigN.sAccDep.ToString(), frmAssetConfigN.sSalvageAmt.ToString(), frmAssetConfigN.sPurchaseDate, frmAssetConfigN.sDepMethod, frmAssetConfigN.sDepRate.ToString(),
                              frmAssetConfigN.sAssetLife.ToString(), frmAssetConfigN.sDepEffDate, 
                              DateTime.Parse(frmAssetConfigN.sDepEffDate).AddDays(1).AddMonths(Convert.ToInt32(frmAssetConfigN.sAssetLife)*12).AddDays(-1).ToString("yyyy-MM-dd"), 
                              frmAssetConfigN.sDepEffDate, frmAssetConfigN.sAssetLocId.ToString(), frmAssetConfigN.sAssetSubLocId.ToString(), frmAssetConfigN.sAssetUserCode, frmAssetConfigN.sAssetUser, frmAssetConfigN.sSupplierId.ToString(),
                              frmAssetConfigN.sCountry, frmAssetConfigN.sBrand, frmAssetConfigN.sModelNo, frmAssetConfigN.sLCNo, frmAssetConfigN.sVoucherNo, frmAssetConfigN.sRegNo, frmAssetConfigN.sactive.ToString(), 
                              frmAssetConfigN.sComments, frmAssetConfigN.sWarranty.ToString(), frmLogin.userId.ToString(), "1", "1", 
                              frmAssetConfigN.sCustDeptId, frmAssetConfigN.sAssetSLNo, frmAssetConfigN.sConvRate.ToString(), frmAssetConfigN.sCurType, frmAssetConfigN.sAssetRecDate, frmAssetConfigN.sAccCode
                            };
                sql += dbConShowDialog.CreateSQLStatement("AR_AssetConfig", cols, vals, "INSERT", null, null);
            }

            if ((bool)dbConShowDialog.DBCRUDFunction(sql, false))
            {
                MessageBox.Show("Asset Copied Successfully", "Success Copy", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Failed to Copy Assets", "Failed Copy", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Hide();
        }

        private int GetMaxCode(string prefix)
        {
            SqlDataReader dr;
            int MaxCode = 0;
            dr = (SqlDataReader)dbConShowDialog.DBCRUDFunction(@"SELECT MAX(CAST(SUBSTRING(AssetCode,7,LEN(AssetCode) - 6) AS BIGINT)) MaxCode FROM AR_AssetConfig WHERE AssetCode LIKE '" + prefix + "%'", true);

            if (dr.Read())
            {
                MaxCode = Convert.ToInt32(dr["MaxCode"].ToString());
            }

            return MaxCode;
        }

        private void txtTimes_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConShowDialog.IntergersOnly(txtTimes, e);
        }
    }
}
