﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace AssetRegister
{
    public partial class frmRptDepExp : Form
    {
        private clsCommonFunctions dbConRptDepExp = new clsCommonFunctions();
        private int AssetId = 0;
        private string CompanyId = "", CostCentreCode = "", GroupId = "", AssetCode = "";
          
        public frmRptDepExp()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;

            string FrmDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
            string ToDate = dtpToDate.Value.ToString("yyyy-MM-dd");

            if (!cbCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "CP";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "PL";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }

            if (!cbCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please Select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }

            if (!cbGroup.Checked)
            {
                if (cmbGroup.SelectedIndex > -1)
                {
                    GroupId = cmbGroup.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please Select Asset Group.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbGroup.Focus();
                    return;
                }
            }

            if (string.IsNullOrEmpty(txtAssetCode.Text.Trim()))
                AssetId = 0;

            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptDepExpenses.rpt");
                cryReport.SetParameterValue("@CompanyId", CompanyId);
                cryReport.SetParameterValue("@CCCode", CostCentreCode);
                cryReport.SetParameterValue("@GroupId", GroupId);
                cryReport.SetParameterValue("@AssetId", AssetId);
                cryReport.SetParameterValue("@FrmDate", FrmDate);
                cryReport.SetParameterValue("@ToDate", ToDate);
                dbConRptDepExp.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.Text = "Depreciation Expense Report";
                rptForm.crViewer.Refresh();
                rptForm.Show();
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void cbCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !cbCompany.Checked;
        }

        private void cbCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !cbCC.Checked;
        }

        private void cbGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !cbGroup.Checked;
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConRptDepExp.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvListData.Focus();

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
            }
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void frmRptDepExp_Load(object sender, EventArgs e)
        {
            dbConRptDepExp.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptDepExp.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;

            dbConRptDepExp.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '', '', 0");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
            dtpFrmDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
    }
}
