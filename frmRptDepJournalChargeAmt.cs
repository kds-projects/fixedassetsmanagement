﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmRptDepJournalChargeAmt : Form
    {
        private clsCommonFunctions dbConRptDepJournalChargeAmt = new clsCommonFunctions();
        private string AssetGroupId = "", CompanyId = "", CostCentreCode = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmRptDepJournalChargeAmt()
        {
            InitializeComponent();
        }

        private void chkAllDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFrmDate.Enabled = !chkAllDate.Checked;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;

            string frmDate = "", toDate = "";

            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "CP";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "PL";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }
            else
                CompanyId = "";

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }
            else
                CostCentreCode = "";

            if (!chkAllGroup.Checked)
            {
                if (cmbGroup.SelectedIndex > -1)
                {
                    AssetGroupId = cmbGroup.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbGroup.Focus();
                    return;
                }
            }
            else
                AssetGroupId = "";

            //if (!chkAllDate.Checked)
            //{
            //    frmDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
            //    toDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            //}
            //else
            //{
            //    frmDate = "";
            //    toDate = dtpToDate.Value.ToString("yyyy-MM-dd");
            //}

            frmDate = dtpFrmDate.Value.ToString("yyyy-MM-dd");
            toDate = dtpToDate.Value.ToString("yyyy-MM-dd");

            try
            {
                cryReport.Load(Application.StartupPath + @"\Reports\rptDepJournalChargeAmt.rpt");
                cryReport.SetParameterValue("@SDate", frmDate);
                cryReport.SetParameterValue("@FDate", toDate);
                cryReport.SetParameterValue("@CompanyId", CompanyId);
                cryReport.SetParameterValue("@CcCode", CostCentreCode);
                cryReport.SetParameterValue("@GroupId", AssetGroupId);
                cryReport.SetParameterValue("@UserName", frmLogin.userFullName);
                dbConRptDepJournalChargeAmt.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.Text = "Depreciation Journal (Charged Amount) Report";
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.crViewer.Refresh();
                rptForm.Show();
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void frmRptAssetRegister_Load(object sender, EventArgs e)
        {
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 2");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;

            dtpFrmDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtpToDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void chkAllGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !chkAllGroup.Checked;
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }

        private void frmRptDepJournalChargeAmt_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
