﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    class mTextBox : TextBox
    {
        private Color _color;

        private const int WM_PAINT = 15;
        public Color BorderColor
        {
            get { return _color; }
            set { this._color = value; }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_PAINT:
                    base.WndProc(ref m);
                    OnPaint();
                    break;
                // break
                default:
                    base.WndProc(ref m);
                    break;
                // break
            }
        }

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetWindowDC(IntPtr hDC);

        protected void OnPaint()
        {
            Rectangle rcItem = new Rectangle(0, 0, this.Bounds.Width, this.Bounds.Height);
            IntPtr hDC = GetWindowDC(this.Handle);
            Graphics gfx = Graphics.FromHdc(hDC);
            DrawBorder(gfx, rcItem, BorderColor);

            gfx.Dispose();
        }

        private void DrawBorder(Graphics arGfx, Rectangle arRC, Color arcColor)
        {
            Pen lpPen = new Pen(arcColor, 1);
            IntPtr hDC = GetWindowDC(this.Handle);
            Graphics gfx = Graphics.FromHdc(hDC);

            gfx.DrawLine(lpPen, arRC.X, arRC.Y + arRC.Height - 1, arRC.X, arRC.Y);
            gfx.DrawLine(lpPen, arRC.X, arRC.Y, arRC.X + arRC.Width - 1, arRC.Y);
            if (!(arRC.Width == 0))
            {
                gfx.DrawLine(lpPen, arRC.X + arRC.Width - 1, arRC.Y, arRC.X + arRC.Width - 1, arRC.Top + arRC.Height - 1);
                gfx.DrawLine(lpPen, arRC.X + arRC.Width - 1, arRC.Top + arRC.Height - 1, arRC.X, arRC.Top + arRC.Height - 1);
                lpPen.Dispose();
            }
        }
    }
}
