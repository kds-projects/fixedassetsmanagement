﻿namespace AssetRegister
{
    partial class frmAssetConfigN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label55 = new System.Windows.Forms.Label();
            this.rbNo = new System.Windows.Forms.RadioButton();
            this.rbYes = new System.Windows.Forms.RadioButton();
            this.label54 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.label63 = new System.Windows.Forms.Label();
            this.dgvAssetLoc = new System.Windows.Forms.DataGridView();
            this.dgvAssetUser = new System.Windows.Forms.DataGridView();
            this.dgvAssetSubLoc = new System.Windows.Forms.DataGridView();
            this.dgvSupplier = new System.Windows.Forms.DataGridView();
            this.dgvAssetCode = new System.Windows.Forms.DataGridView();
            this.btnPreview = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.txtGeneralName = new AssetRegister.FlatTextBox();
            this.txtQuantity = new AssetRegister.FlatTextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.txtUnitPrice = new AssetRegister.FlatTextBox();
            this.dtpInvBillDate = new AssetRegister.FlatDateTimePicker();
            this.chkWDV = new System.Windows.Forms.CheckBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtAccCode = new AssetRegister.FlatTextBox();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnRfrshSubLoc = new System.Windows.Forms.Button();
            this.btnRfrshLoc = new System.Windows.Forms.Button();
            this.dtpAssetRecDate = new AssetRegister.FlatDateTimePicker();
            this.cmbCurrencyType = new AssetRegister.FlatComboBox();
            this.txtConvRate = new AssetRegister.FlatTextBox();
            this.txtAssetSlNo = new AssetRegister.FlatTextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.btnRfrshSupplier = new System.Windows.Forms.Button();
            this.cmbCustodianDept = new AssetRegister.FlatComboBox();
            this.dtpYear = new AssetRegister.FlatDateTimePicker();
            this.dtpMonth = new AssetRegister.FlatDateTimePicker();
            this.dtpPurchaseDate = new AssetRegister.FlatDateTimePicker();
            this.txtSupplier = new AssetRegister.FlatTextBox();
            this.txtAssetUser = new AssetRegister.FlatTextBox();
            this.txtAssetSubLoc = new AssetRegister.FlatTextBox();
            this.txtAssetLoc = new AssetRegister.FlatTextBox();
            this.txtDutiesNothers = new AssetRegister.FlatTextBox();
            this.txtInvCostBdt = new AssetRegister.FlatTextBox();
            this.txtRegNo = new AssetRegister.FlatTextBox();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            this.txtWPeriod = new AssetRegister.FlatTextBox();
            this.cmbDepMethod = new AssetRegister.FlatComboBox();
            this.cmbActive = new AssetRegister.FlatComboBox();
            this.txtComments = new AssetRegister.FlatTextBox();
            this.txtVoucherNo = new AssetRegister.FlatTextBox();
            this.txtSalvageValue = new AssetRegister.FlatTextBox();
            this.txtWrittenDownVal = new AssetRegister.FlatTextBox();
            this.txtAssetLife = new AssetRegister.FlatTextBox();
            this.txtDepRate = new AssetRegister.FlatTextBox();
            this.txtAccumulatDep = new AssetRegister.FlatTextBox();
            this.txtInvCostUsd = new AssetRegister.FlatTextBox();
            this.txtAssetDesc = new AssetRegister.FlatTextBox();
            this.txtModelNo = new AssetRegister.FlatTextBox();
            this.txtBrand = new AssetRegister.FlatTextBox();
            this.txtLCNo = new AssetRegister.FlatTextBox();
            this.txtCountry = new AssetRegister.FlatTextBox();
            this.txtAsset = new AssetRegister.FlatTextBox();
            this.cmbGroup = new AssetRegister.FlatComboBox();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.label70 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.dtpDepEffectDate = new AssetRegister.FlatDateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCross = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetSubLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetCode)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(1, 1);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(793, 435);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(1, 438);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(793, 39);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::AssetRegister.Properties.Resources.Actions_document_update_all_icon;
            this.btnUpdate.Location = new System.Drawing.Point(682, 439);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(38, 38);
            this.btnUpdate.TabIndex = 41;
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            this.btnUpdate.MouseHover += new System.EventHandler(this.btnUpdate_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(756, 439);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 42;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::AssetRegister.Properties.Resources.Actions_stock_save_as_icon;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(645, 439);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(38, 38);
            this.btnSave.TabIndex = 40;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(718, 346);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(12, 13);
            this.label55.TabIndex = 364;
            this.label55.Text = ":";
            // 
            // rbNo
            // 
            this.rbNo.AutoSize = true;
            this.rbNo.Checked = true;
            this.rbNo.Location = new System.Drawing.Point(627, 344);
            this.rbNo.Name = "rbNo";
            this.rbNo.Size = new System.Drawing.Size(40, 17);
            this.rbNo.TabIndex = 34;
            this.rbNo.TabStop = true;
            this.rbNo.Text = "No";
            this.rbNo.UseVisualStyleBackColor = true;
            this.rbNo.CheckedChanged += new System.EventHandler(this.rbNo_CheckedChanged);
            // 
            // rbYes
            // 
            this.rbYes.AutoSize = true;
            this.rbYes.Location = new System.Drawing.Point(576, 344);
            this.rbYes.Name = "rbYes";
            this.rbYes.Size = new System.Drawing.Size(44, 17);
            this.rbYes.TabIndex = 33;
            this.rbYes.Text = "Yes";
            this.rbYes.UseVisualStyleBackColor = true;
            this.rbYes.CheckedChanged += new System.EventHandler(this.rbYes_CheckedChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(671, 346);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(43, 13);
            this.label54.TabIndex = 361;
            this.label54.Text = "Period";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(560, 280);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(12, 13);
            this.label47.TabIndex = 360;
            this.label47.Text = ":";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(400, 280);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(88, 13);
            this.label48.TabIndex = 359;
            this.label48.Text = "Salvage Value";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(560, 346);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 13);
            this.label29.TabIndex = 358;
            this.label29.Text = ":";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(400, 346);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 357;
            this.label46.Text = "Warranty";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(560, 236);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 13);
            this.label28.TabIndex = 356;
            this.label28.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(560, 214);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(12, 13);
            this.label23.TabIndex = 355;
            this.label23.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(400, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 354;
            this.label11.Text = "Asset Life";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(400, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 353;
            this.label1.Text = "Depreciation Rate";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(400, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 13);
            this.label7.TabIndex = 351;
            this.label7.Text = "Accumulated Depreciation";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(560, 148);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(12, 13);
            this.label17.TabIndex = 352;
            this.label17.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(560, 38);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 13);
            this.label30.TabIndex = 344;
            this.label30.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(560, 192);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(12, 13);
            this.label12.TabIndex = 350;
            this.label12.Text = ":";
            this.label12.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(400, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 13);
            this.label6.TabIndex = 349;
            this.label6.Text = "Depreciation Start From";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(560, 258);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 13);
            this.label18.TabIndex = 348;
            this.label18.Text = ":";
            this.label18.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(560, 302);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(12, 13);
            this.label22.TabIndex = 343;
            this.label22.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(400, 258);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 347;
            this.label8.Text = "Written Down Value";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label8.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(400, 302);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 342;
            this.label21.Text = "Is Active ";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(400, 16);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(160, 13);
            this.label43.TabIndex = 345;
            this.label43.Text = "Purchase Date (AMS Date)";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(560, 16);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(12, 13);
            this.label44.TabIndex = 346;
            this.label44.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(400, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 339;
            this.label2.Text = "Invoice Cost (FC)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(400, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 338;
            this.label3.Text = "Depreciation Method";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(560, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 13);
            this.label14.TabIndex = 340;
            this.label14.Text = ":";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(562, 401);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(12, 13);
            this.label20.TabIndex = 337;
            this.label20.Text = ":";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(560, 324);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(12, 13);
            this.label42.TabIndex = 336;
            this.label42.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(139, 236);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(12, 13);
            this.label41.TabIndex = 335;
            this.label41.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(400, 401);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 333;
            this.label10.Text = "Comments";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(400, 324);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 13);
            this.label40.TabIndex = 332;
            this.label40.Text = "Voucher No.";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(15, 236);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(45, 13);
            this.label39.TabIndex = 334;
            this.label39.Text = "LC No.";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(139, 377);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 331;
            this.label13.Text = ":";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(140, 280);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(12, 13);
            this.label51.TabIndex = 330;
            this.label51.Text = ":";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(16, 280);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(63, 13);
            this.label52.TabIndex = 329;
            this.label52.Text = "Model No.";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(140, 258);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(12, 13);
            this.label49.TabIndex = 328;
            this.label49.Text = ":";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(16, 258);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 13);
            this.label50.TabIndex = 327;
            this.label50.Text = "Brand";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 377);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 13);
            this.label15.TabIndex = 326;
            this.label15.Text = "Asset Description";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(139, 214);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(12, 13);
            this.label36.TabIndex = 325;
            this.label36.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(15, 214);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(106, 13);
            this.label35.TabIndex = 324;
            this.label35.Text = "Country of Origin";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(139, 192);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(12, 13);
            this.label38.TabIndex = 323;
            this.label38.Text = ":";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(139, 170);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(12, 13);
            this.label34.TabIndex = 321;
            this.label34.Text = ":";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(139, 148);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(12, 13);
            this.label32.TabIndex = 322;
            this.label32.Text = ":";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(15, 192);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(68, 13);
            this.label37.TabIndex = 318;
            this.label37.Text = "Asset User";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(15, 170);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(115, 13);
            this.label33.TabIndex = 320;
            this.label33.Text = "Asset Sub Location";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(15, 148);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 13);
            this.label31.TabIndex = 319;
            this.label31.Text = "Asset Location";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(139, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 13);
            this.label19.TabIndex = 317;
            this.label19.Text = ":";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(15, 82);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(88, 13);
            this.label27.TabIndex = 316;
            this.label27.Text = "Generic Name";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(139, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 315;
            this.label9.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 60);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 314;
            this.label16.Text = "Group";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 312;
            this.label4.Text = "Company Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(139, 126);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(12, 13);
            this.label26.TabIndex = 309;
            this.label26.Text = ":";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(139, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 13);
            this.label24.TabIndex = 310;
            this.label24.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(15, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(76, 13);
            this.label45.TabIndex = 311;
            this.label45.Text = "Cost Center";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(139, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 313;
            this.label5.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(15, 126);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 308;
            this.label25.Text = "Code";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(15, 302);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(54, 13);
            this.label53.TabIndex = 320;
            this.label53.Text = "Supplier";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(139, 302);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(12, 13);
            this.label56.TabIndex = 321;
            this.label56.Text = ":";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(15, 324);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(110, 13);
            this.label57.TabIndex = 332;
            this.label57.Text = "Reg. No. [Vehicle]";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(139, 324);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(12, 13);
            this.label58.TabIndex = 336;
            this.label58.Text = ":";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(400, 104);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(117, 13);
            this.label59.TabIndex = 339;
            this.label59.Text = "Invoice Cost (BDT)";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(560, 104);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(12, 13);
            this.label60.TabIndex = 344;
            this.label60.Text = ":";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(560, 60);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(12, 13);
            this.label61.TabIndex = 344;
            this.label61.Text = ":";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(400, 60);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(97, 13);
            this.label62.TabIndex = 339;
            this.label62.Text = "Duties && Others";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReset
            // 
            this.btnReset.Image = global::AssetRegister.Properties.Resources.Action_file_new_icon1;
            this.btnReset.Location = new System.Drawing.Point(571, 439);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(38, 38);
            this.btnReset.TabIndex = 38;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.MouseHover += new System.EventHandler(this.btnReset_MouseHover);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(725, 214);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(59, 13);
            this.label63.TabIndex = 402;
            this.label63.Text = " Per Year";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvAssetLoc
            // 
            this.dgvAssetLoc.AllowUserToAddRows = false;
            this.dgvAssetLoc.AllowUserToDeleteRows = false;
            this.dgvAssetLoc.AllowUserToResizeColumns = false;
            this.dgvAssetLoc.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetLoc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAssetLoc.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetLoc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetLoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetLoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAssetLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetLoc.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAssetLoc.EnableHeadersVisualStyles = false;
            this.dgvAssetLoc.Location = new System.Drawing.Point(820, 12);
            this.dgvAssetLoc.MultiSelect = false;
            this.dgvAssetLoc.Name = "dgvAssetLoc";
            this.dgvAssetLoc.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetLoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvAssetLoc.RowHeadersVisible = false;
            this.dgvAssetLoc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAssetLoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetLoc.Size = new System.Drawing.Size(205, 307);
            this.dgvAssetLoc.TabIndex = 403;
            this.dgvAssetLoc.Visible = false;
            this.dgvAssetLoc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetLoc_CellDoubleClick);
            this.dgvAssetLoc.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAssetLoc_DataError);
            this.dgvAssetLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetLoc_KeyDown);
            this.dgvAssetLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetLoc_KeyPress);
            // 
            // dgvAssetUser
            // 
            this.dgvAssetUser.AllowUserToAddRows = false;
            this.dgvAssetUser.AllowUserToDeleteRows = false;
            this.dgvAssetUser.AllowUserToResizeColumns = false;
            this.dgvAssetUser.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetUser.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvAssetUser.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetUser.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetUser.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvAssetUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetUser.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvAssetUser.EnableHeadersVisualStyles = false;
            this.dgvAssetUser.Location = new System.Drawing.Point(820, 12);
            this.dgvAssetUser.MultiSelect = false;
            this.dgvAssetUser.Name = "dgvAssetUser";
            this.dgvAssetUser.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetUser.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvAssetUser.RowHeadersVisible = false;
            this.dgvAssetUser.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAssetUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetUser.Size = new System.Drawing.Size(320, 464);
            this.dgvAssetUser.TabIndex = 404;
            this.dgvAssetUser.Visible = false;
            this.dgvAssetUser.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetUser_CellDoubleClick);
            this.dgvAssetUser.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvAssetUser_MouseClick);
            this.dgvAssetUser.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAssetUser_DataError);
            this.dgvAssetUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetUser_KeyDown);
            this.dgvAssetUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetUser_KeyPress);
            // 
            // dgvAssetSubLoc
            // 
            this.dgvAssetSubLoc.AllowUserToAddRows = false;
            this.dgvAssetSubLoc.AllowUserToDeleteRows = false;
            this.dgvAssetSubLoc.AllowUserToResizeColumns = false;
            this.dgvAssetSubLoc.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetSubLoc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvAssetSubLoc.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetSubLoc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetSubLoc.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetSubLoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvAssetSubLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetSubLoc.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvAssetSubLoc.EnableHeadersVisualStyles = false;
            this.dgvAssetSubLoc.Location = new System.Drawing.Point(820, 12);
            this.dgvAssetSubLoc.MultiSelect = false;
            this.dgvAssetSubLoc.Name = "dgvAssetSubLoc";
            this.dgvAssetSubLoc.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetSubLoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvAssetSubLoc.RowHeadersVisible = false;
            this.dgvAssetSubLoc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvAssetSubLoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetSubLoc.Size = new System.Drawing.Size(205, 307);
            this.dgvAssetSubLoc.TabIndex = 405;
            this.dgvAssetSubLoc.Visible = false;
            this.dgvAssetSubLoc.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetSubLoc_CellDoubleClick);
            this.dgvAssetSubLoc.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvAssetSubLoc_DataError);
            this.dgvAssetSubLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetSubLoc_KeyDown);
            this.dgvAssetSubLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetSubLoc_KeyPress);
            // 
            // dgvSupplier
            // 
            this.dgvSupplier.AllowUserToAddRows = false;
            this.dgvSupplier.AllowUserToDeleteRows = false;
            this.dgvSupplier.AllowUserToResizeColumns = false;
            this.dgvSupplier.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvSupplier.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvSupplier.BackgroundColor = System.Drawing.Color.White;
            this.dgvSupplier.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvSupplier.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSupplier.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvSupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSupplier.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvSupplier.EnableHeadersVisualStyles = false;
            this.dgvSupplier.Location = new System.Drawing.Point(820, 12);
            this.dgvSupplier.MultiSelect = false;
            this.dgvSupplier.Name = "dgvSupplier";
            this.dgvSupplier.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSupplier.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvSupplier.RowHeadersVisible = false;
            this.dgvSupplier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSupplier.Size = new System.Drawing.Size(217, 307);
            this.dgvSupplier.TabIndex = 408;
            this.dgvSupplier.Visible = false;
            this.dgvSupplier.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSupplier_CellDoubleClick);
            this.dgvSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSupplier_KeyDown);
            this.dgvSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvSupplier_KeyPress);
            // 
            // dgvAssetCode
            // 
            this.dgvAssetCode.AllowUserToAddRows = false;
            this.dgvAssetCode.AllowUserToDeleteRows = false;
            this.dgvAssetCode.AllowUserToResizeColumns = false;
            this.dgvAssetCode.AllowUserToResizeRows = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvAssetCode.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvAssetCode.BackgroundColor = System.Drawing.Color.White;
            this.dgvAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvAssetCode.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvAssetCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAssetCode.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgvAssetCode.EnableHeadersVisualStyles = false;
            this.dgvAssetCode.Location = new System.Drawing.Point(820, 12);
            this.dgvAssetCode.MultiSelect = false;
            this.dgvAssetCode.Name = "dgvAssetCode";
            this.dgvAssetCode.ReadOnly = true;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAssetCode.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvAssetCode.RowHeadersVisible = false;
            this.dgvAssetCode.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAssetCode.Size = new System.Drawing.Size(205, 307);
            this.dgvAssetCode.TabIndex = 409;
            this.dgvAssetCode.Visible = false;
            this.dgvAssetCode.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAssetCode_CellDoubleClick);
            this.dgvAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvAssetCode_KeyDown);
            this.dgvAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvAssetCode_KeyPress);
            // 
            // btnPreview
            // 
            this.btnPreview.Image = global::AssetRegister.Properties.Resources.Actions_document_find_icon;
            this.btnPreview.Location = new System.Drawing.Point(608, 439);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(38, 38);
            this.btnPreview.TabIndex = 39;
            this.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            this.btnPreview.MouseHover += new System.EventHandler(this.btnPreview_MouseHover);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(560, 368);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(12, 13);
            this.label64.TabIndex = 413;
            this.label64.Text = ":";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(400, 368);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(99, 13);
            this.label65.TabIndex = 414;
            this.label65.Text = "Custodian Dept.";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.dgvAssetUser);
            this.panel1.Controls.Add(this.dgvAssetCode);
            this.panel1.Controls.Add(this.dgvAssetLoc);
            this.panel1.Controls.Add(this.dgvAssetSubLoc);
            this.panel1.Controls.Add(this.dgvSupplier);
            this.panel1.Controls.Add(this.label77);
            this.panel1.Controls.Add(this.label78);
            this.panel1.Controls.Add(this.txtGeneralName);
            this.panel1.Controls.Add(this.txtQuantity);
            this.panel1.Controls.Add(this.label75);
            this.panel1.Controls.Add(this.label76);
            this.panel1.Controls.Add(this.txtUnitPrice);
            this.panel1.Controls.Add(this.dtpInvBillDate);
            this.panel1.Controls.Add(this.chkWDV);
            this.panel1.Controls.Add(this.label73);
            this.panel1.Controls.Add(this.label74);
            this.panel1.Controls.Add(this.label71);
            this.panel1.Controls.Add(this.label72);
            this.panel1.Controls.Add(this.txtAccCode);
            this.panel1.Controls.Add(this.btnCopy);
            this.panel1.Controls.Add(this.btnRfrshSubLoc);
            this.panel1.Controls.Add(this.btnRfrshLoc);
            this.panel1.Controls.Add(this.dtpAssetRecDate);
            this.panel1.Controls.Add(this.cmbCurrencyType);
            this.panel1.Controls.Add(this.txtConvRate);
            this.panel1.Controls.Add(this.txtAssetSlNo);
            this.panel1.Controls.Add(this.label68);
            this.panel1.Controls.Add(this.btnRfrshSupplier);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.cmbCustodianDept);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label64);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label65);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.btnPreview);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.dtpYear);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.dtpMonth);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.dtpPurchaseDate);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.txtSupplier);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.txtAssetUser);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.txtAssetSubLoc);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.txtAssetLoc);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.label63);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label62);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label61);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Controls.Add(this.txtDutiesNothers);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.label59);
            this.panel1.Controls.Add(this.label52);
            this.panel1.Controls.Add(this.label60);
            this.panel1.Controls.Add(this.label51);
            this.panel1.Controls.Add(this.txtInvCostBdt);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label57);
            this.panel1.Controls.Add(this.label39);
            this.panel1.Controls.Add(this.label58);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.txtRegNo);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label53);
            this.panel1.Controls.Add(this.label41);
            this.panel1.Controls.Add(this.label56);
            this.panel1.Controls.Add(this.label42);
            this.panel1.Controls.Add(this.txtAssetCode);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txtWPeriod);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.cmbDepMethod);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbActive);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtComments);
            this.panel1.Controls.Add(this.label44);
            this.panel1.Controls.Add(this.txtVoucherNo);
            this.panel1.Controls.Add(this.label43);
            this.panel1.Controls.Add(this.txtSalvageValue);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.txtWrittenDownVal);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtAssetLife);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.txtDepRate);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtAccumulatDep);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtInvCostUsd);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtAssetDesc);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.txtModelNo);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txtBrand);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtLCNo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtCountry);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtAsset);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.cmbGroup);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.cmbCC);
            this.panel1.Controls.Add(this.label46);
            this.panel1.Controls.Add(this.cmbCompany);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.label48);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.label54);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.rbYes);
            this.panel1.Controls.Add(this.label55);
            this.panel1.Controls.Add(this.rbNo);
            this.panel1.Controls.Add(this.shapeContainer2);
            this.panel1.Controls.Add(this.label70);
            this.panel1.Controls.Add(this.label67);
            this.panel1.Controls.Add(this.label69);
            this.panel1.Controls.Add(this.dtpDepEffectDate);
            this.panel1.Location = new System.Drawing.Point(5, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(796, 480);
            this.panel1.TabIndex = 415;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(15, 104);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(89, 13);
            this.label77.TabIndex = 435;
            this.label77.Text = "General Name";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(139, 104);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(12, 13);
            this.label78.TabIndex = 436;
            this.label78.Text = ":";
            // 
            // txtGeneralName
            // 
            this.txtGeneralName.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtGeneralName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGeneralName.Location = new System.Drawing.Point(156, 100);
            this.txtGeneralName.Name = "txtGeneralName";
            this.txtGeneralName.Size = new System.Drawing.Size(209, 21);
            this.txtGeneralName.TabIndex = 5;
            this.txtGeneralName.TextChanged += new System.EventHandler(this.txtGeneralName_TextChanged);
            this.txtGeneralName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGeneralName_KeyPress);
            this.txtGeneralName.Enter += new System.EventHandler(this.txtGeneralName_Enter);
            // 
            // txtQuantity
            // 
            this.txtQuantity.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuantity.Location = new System.Drawing.Point(715, 78);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(70, 21);
            this.txtQuantity.TabIndex = 23;
            this.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuantity.Leave += new System.EventHandler(this.txtQuantity_Leave);
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantity_KeyPress);
            this.txtQuantity.Enter += new System.EventHandler(this.txtQuantity_Enter);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(400, 82);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(163, 13);
            this.label75.TabIndex = 432;
            this.label75.Text = "Unit Price (BDT) && Quantity";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(560, 82);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(12, 13);
            this.label76.TabIndex = 433;
            this.label76.Text = ":";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtUnitPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUnitPrice.Location = new System.Drawing.Point(576, 78);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(138, 21);
            this.txtUnitPrice.TabIndex = 22;
            this.txtUnitPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUnitPrice.Leave += new System.EventHandler(this.txtUnitPrice_Leave);
            this.txtUnitPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUnitPrice_KeyPress);
            this.txtUnitPrice.Enter += new System.EventHandler(this.txtUnitPrice_Enter);
            // 
            // dtpInvBillDate
            // 
            this.dtpInvBillDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpInvBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvBillDate.Location = new System.Drawing.Point(576, 188);
            this.dtpInvBillDate.Name = "dtpInvBillDate";
            this.dtpInvBillDate.Size = new System.Drawing.Size(209, 21);
            this.dtpInvBillDate.TabIndex = 26;
            // 
            // chkWDV
            // 
            this.chkWDV.AutoSize = true;
            this.chkWDV.Location = new System.Drawing.Point(461, 451);
            this.chkWDV.Name = "chkWDV";
            this.chkWDV.Size = new System.Drawing.Size(98, 17);
            this.chkWDV.TabIndex = 430;
            this.chkWDV.Text = "WDV Update";
            this.chkWDV.UseVisualStyleBackColor = true;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(560, 170);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(12, 13);
            this.label73.TabIndex = 428;
            this.label73.Text = ":";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(400, 170);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(145, 13);
            this.label74.TabIndex = 429;
            this.label74.Text = "Depreciation Next Effect";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(16, 412);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(63, 13);
            this.label71.TabIndex = 425;
            this.label71.Text = "A/C Code";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(139, 412);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(12, 13);
            this.label72.TabIndex = 426;
            this.label72.Text = ":";
            // 
            // txtAccCode
            // 
            this.txtAccCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAccCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccCode.Location = new System.Drawing.Point(156, 408);
            this.txtAccCode.Name = "txtAccCode";
            this.txtAccCode.Size = new System.Drawing.Size(209, 21);
            this.txtAccCode.TabIndex = 424;
            // 
            // btnCopy
            // 
            this.btnCopy.Image = global::AssetRegister.Properties.Resources.copy_icon;
            this.btnCopy.Location = new System.Drawing.Point(719, 439);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(38, 38);
            this.btnCopy.TabIndex = 423;
            this.btnCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            this.btnCopy.MouseHover += new System.EventHandler(this.btnCopy_MouseHover);
            // 
            // btnRfrshSubLoc
            // 
            this.btnRfrshSubLoc.Image = global::AssetRegister.Properties.Resources.Refresh_2_icon;
            this.btnRfrshSubLoc.Location = new System.Drawing.Point(367, 166);
            this.btnRfrshSubLoc.Name = "btnRfrshSubLoc";
            this.btnRfrshSubLoc.Size = new System.Drawing.Size(30, 21);
            this.btnRfrshSubLoc.TabIndex = 422;
            this.btnRfrshSubLoc.UseVisualStyleBackColor = true;
            this.btnRfrshSubLoc.Click += new System.EventHandler(this.btnRfrshSubLoc_Click);
            // 
            // btnRfrshLoc
            // 
            this.btnRfrshLoc.Image = global::AssetRegister.Properties.Resources.Refresh_2_icon;
            this.btnRfrshLoc.Location = new System.Drawing.Point(367, 144);
            this.btnRfrshLoc.Name = "btnRfrshLoc";
            this.btnRfrshLoc.Size = new System.Drawing.Size(30, 21);
            this.btnRfrshLoc.TabIndex = 422;
            this.btnRfrshLoc.UseVisualStyleBackColor = true;
            this.btnRfrshLoc.Click += new System.EventHandler(this.btnRfrshLoc_Click);
            // 
            // dtpAssetRecDate
            // 
            this.dtpAssetRecDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpAssetRecDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAssetRecDate.Location = new System.Drawing.Point(156, 342);
            this.dtpAssetRecDate.Name = "dtpAssetRecDate";
            this.dtpAssetRecDate.Size = new System.Drawing.Size(209, 21);
            this.dtpAssetRecDate.TabIndex = 15;
            // 
            // cmbCurrencyType
            // 
            this.cmbCurrencyType.FormattingEnabled = true;
            this.cmbCurrencyType.Items.AddRange(new object[] {
            "USD",
            "EUR",
            "GBP",
            "JPY",
            "CHF"});
            this.cmbCurrencyType.Location = new System.Drawing.Point(512, 34);
            this.cmbCurrencyType.Name = "cmbCurrencyType";
            this.cmbCurrencyType.Size = new System.Drawing.Size(48, 21);
            this.cmbCurrencyType.TabIndex = 18;
            // 
            // txtConvRate
            // 
            this.txtConvRate.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtConvRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConvRate.Location = new System.Drawing.Point(715, 34);
            this.txtConvRate.Name = "txtConvRate";
            this.txtConvRate.Size = new System.Drawing.Size(70, 21);
            this.txtConvRate.TabIndex = 20;
            this.txtConvRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtConvRate.Leave += new System.EventHandler(this.txtConvRate_Leave);
            this.txtConvRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConvRate_KeyPress);
            this.txtConvRate.Enter += new System.EventHandler(this.txtConvRate_Enter);
            // 
            // txtAssetSlNo
            // 
            this.txtAssetSlNo.BackColor = System.Drawing.Color.White;
            this.txtAssetSlNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetSlNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetSlNo.Location = new System.Drawing.Point(576, 254);
            this.txtAssetSlNo.Name = "txtAssetSlNo";
            this.txtAssetSlNo.Size = new System.Drawing.Size(209, 21);
            this.txtAssetSlNo.TabIndex = 29;
            this.txtAssetSlNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetSlNo_KeyPress);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(15, 346);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(98, 13);
            this.label68.TabIndex = 419;
            this.label68.Text = "Asset Rec. Date";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRfrshSupplier
            // 
            this.btnRfrshSupplier.Image = global::AssetRegister.Properties.Resources.Refresh_2_icon;
            this.btnRfrshSupplier.Location = new System.Drawing.Point(367, 298);
            this.btnRfrshSupplier.Name = "btnRfrshSupplier";
            this.btnRfrshSupplier.Size = new System.Drawing.Size(30, 21);
            this.btnRfrshSupplier.TabIndex = 415;
            this.btnRfrshSupplier.UseVisualStyleBackColor = true;
            this.btnRfrshSupplier.Click += new System.EventHandler(this.btnRfrshSupplier_Click);
            this.btnRfrshSupplier.MouseHover += new System.EventHandler(this.btnRfrshSupplier_MouseHover);
            // 
            // cmbCustodianDept
            // 
            this.cmbCustodianDept.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCustodianDept.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCustodianDept.FormattingEnabled = true;
            this.cmbCustodianDept.Location = new System.Drawing.Point(576, 364);
            this.cmbCustodianDept.Name = "cmbCustodianDept";
            this.cmbCustodianDept.Size = new System.Drawing.Size(209, 21);
            this.cmbCustodianDept.TabIndex = 36;
            this.cmbCustodianDept.SelectedIndexChanged += new System.EventHandler(this.cmbCustodianDept_SelectedIndexChanged);
            this.cmbCustodianDept.DropDown += new System.EventHandler(this.cmbCustodianDept_DropDown);
            // 
            // dtpYear
            // 
            this.dtpYear.CustomFormat = "yyyy";
            this.dtpYear.Enabled = false;
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Location = new System.Drawing.Point(692, 144);
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowUpDown = true;
            this.dtpYear.Size = new System.Drawing.Size(93, 21);
            this.dtpYear.TabIndex = 25;
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MMMM";
            this.dtpMonth.Enabled = false;
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(576, 144);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Size = new System.Drawing.Size(115, 21);
            this.dtpMonth.TabIndex = 24;
            // 
            // dtpPurchaseDate
            // 
            this.dtpPurchaseDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPurchaseDate.Location = new System.Drawing.Point(576, 12);
            this.dtpPurchaseDate.Name = "dtpPurchaseDate";
            this.dtpPurchaseDate.Size = new System.Drawing.Size(209, 21);
            this.dtpPurchaseDate.TabIndex = 17;
            this.dtpPurchaseDate.Leave += new System.EventHandler(this.dtpPurchaseDate_Leave);
            // 
            // txtSupplier
            // 
            this.txtSupplier.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtSupplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSupplier.Location = new System.Drawing.Point(156, 298);
            this.txtSupplier.Name = "txtSupplier";
            this.txtSupplier.Size = new System.Drawing.Size(209, 21);
            this.txtSupplier.TabIndex = 13;
            this.txtSupplier.TextChanged += new System.EventHandler(this.txtSupplier_TextChanged);
            this.txtSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupplier_KeyDown);
            this.txtSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSupplier_KeyPress);
            this.txtSupplier.Enter += new System.EventHandler(this.txtSupplier_Enter);
            // 
            // txtAssetUser
            // 
            this.txtAssetUser.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetUser.Location = new System.Drawing.Point(156, 188);
            this.txtAssetUser.Name = "txtAssetUser";
            this.txtAssetUser.Size = new System.Drawing.Size(209, 21);
            this.txtAssetUser.TabIndex = 8;
            this.txtAssetUser.TextChanged += new System.EventHandler(this.txtAssetUser_TextChanged);
            this.txtAssetUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetUser_KeyDown);
            this.txtAssetUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetUser_KeyPress);
            this.txtAssetUser.Enter += new System.EventHandler(this.txtAssetUser_Enter);
            // 
            // txtAssetSubLoc
            // 
            this.txtAssetSubLoc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetSubLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetSubLoc.Location = new System.Drawing.Point(156, 166);
            this.txtAssetSubLoc.Name = "txtAssetSubLoc";
            this.txtAssetSubLoc.Size = new System.Drawing.Size(209, 21);
            this.txtAssetSubLoc.TabIndex = 7;
            this.txtAssetSubLoc.TextChanged += new System.EventHandler(this.txtAssetSubLoc_TextChanged);
            this.txtAssetSubLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetSubLoc_KeyDown);
            this.txtAssetSubLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetSubLoc_KeyPress);
            this.txtAssetSubLoc.Enter += new System.EventHandler(this.txtAssetSubLoc_Enter);
            // 
            // txtAssetLoc
            // 
            this.txtAssetLoc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetLoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetLoc.Location = new System.Drawing.Point(156, 144);
            this.txtAssetLoc.Name = "txtAssetLoc";
            this.txtAssetLoc.Size = new System.Drawing.Size(209, 21);
            this.txtAssetLoc.TabIndex = 6;
            this.txtAssetLoc.TextChanged += new System.EventHandler(this.txtAssetLoc_TextChanged);
            this.txtAssetLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetLoc_KeyDown);
            this.txtAssetLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetLoc_KeyPress);
            this.txtAssetLoc.Enter += new System.EventHandler(this.txtAssetLoc_Enter);
            // 
            // txtDutiesNothers
            // 
            this.txtDutiesNothers.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDutiesNothers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDutiesNothers.Location = new System.Drawing.Point(576, 56);
            this.txtDutiesNothers.Name = "txtDutiesNothers";
            this.txtDutiesNothers.Size = new System.Drawing.Size(209, 21);
            this.txtDutiesNothers.TabIndex = 21;
            this.txtDutiesNothers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDutiesNothers.TextChanged += new System.EventHandler(this.txtDutiesNothers_TextChanged);
            this.txtDutiesNothers.Leave += new System.EventHandler(this.txtDutiesNothers_Leave);
            this.txtDutiesNothers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDutiesNothers_KeyPress);
            this.txtDutiesNothers.Enter += new System.EventHandler(this.txtDutiesNothers_Enter);
            // 
            // txtInvCostBdt
            // 
            this.txtInvCostBdt.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtInvCostBdt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvCostBdt.Enabled = false;
            this.txtInvCostBdt.Location = new System.Drawing.Point(576, 100);
            this.txtInvCostBdt.Name = "txtInvCostBdt";
            this.txtInvCostBdt.Size = new System.Drawing.Size(209, 21);
            this.txtInvCostBdt.TabIndex = 23;
            this.txtInvCostBdt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInvCostBdt.TextChanged += new System.EventHandler(this.txtInvCostBdt_TextChanged);
            this.txtInvCostBdt.Leave += new System.EventHandler(this.txtInvCostBdt_Leave);
            this.txtInvCostBdt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInvCostBdt_KeyPress);
            this.txtInvCostBdt.Enter += new System.EventHandler(this.txtInvCostBdt_Enter);
            // 
            // txtRegNo
            // 
            this.txtRegNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtRegNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegNo.Location = new System.Drawing.Point(156, 320);
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(209, 21);
            this.txtRegNo.TabIndex = 14;
            this.txtRegNo.Enter += new System.EventHandler(this.txtRegNo_Enter);
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BackColor = System.Drawing.Color.DimGray;
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssetCode.ForeColor = System.Drawing.Color.White;
            this.txtAssetCode.Location = new System.Drawing.Point(156, 122);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.ReadOnly = true;
            this.txtAssetCode.Size = new System.Drawing.Size(209, 21);
            this.txtAssetCode.TabIndex = 5;
            this.txtAssetCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            this.txtAssetCode.Enter += new System.EventHandler(this.txtAssetCode_Enter);
            // 
            // txtWPeriod
            // 
            this.txtWPeriod.BackColor = System.Drawing.Color.White;
            this.txtWPeriod.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtWPeriod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWPeriod.Location = new System.Drawing.Point(731, 342);
            this.txtWPeriod.Name = "txtWPeriod";
            this.txtWPeriod.ReadOnly = true;
            this.txtWPeriod.Size = new System.Drawing.Size(54, 21);
            this.txtWPeriod.TabIndex = 35;
            this.txtWPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWPeriod_KeyPress);
            this.txtWPeriod.Enter += new System.EventHandler(this.txtWPeriod_Enter);
            // 
            // cmbDepMethod
            // 
            this.cmbDepMethod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDepMethod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDepMethod.FormattingEnabled = true;
            this.cmbDepMethod.Items.AddRange(new object[] {
            "STRAIGHT LINE",
            "REDUCING BALANCE",
            "DOUBLE DECLINING"});
            this.cmbDepMethod.Location = new System.Drawing.Point(576, 122);
            this.cmbDepMethod.Name = "cmbDepMethod";
            this.cmbDepMethod.Size = new System.Drawing.Size(209, 21);
            this.cmbDepMethod.TabIndex = 23;
            this.cmbDepMethod.SelectedIndexChanged += new System.EventHandler(this.cmbDepMethod_SelectedIndexChanged);
            // 
            // cmbActive
            // 
            this.cmbActive.FormattingEnabled = true;
            this.cmbActive.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbActive.Location = new System.Drawing.Point(576, 298);
            this.cmbActive.Name = "cmbActive";
            this.cmbActive.Size = new System.Drawing.Size(209, 21);
            this.cmbActive.TabIndex = 31;
            // 
            // txtComments
            // 
            this.txtComments.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComments.Location = new System.Drawing.Point(576, 386);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(209, 43);
            this.txtComments.TabIndex = 37;
            this.txtComments.Enter += new System.EventHandler(this.txtComments_Enter);
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtVoucherNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVoucherNo.Location = new System.Drawing.Point(576, 320);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(209, 21);
            this.txtVoucherNo.TabIndex = 32;
            this.txtVoucherNo.Enter += new System.EventHandler(this.txtVoucherNo_Enter);
            // 
            // txtSalvageValue
            // 
            this.txtSalvageValue.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtSalvageValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSalvageValue.Location = new System.Drawing.Point(576, 276);
            this.txtSalvageValue.Name = "txtSalvageValue";
            this.txtSalvageValue.Size = new System.Drawing.Size(209, 21);
            this.txtSalvageValue.TabIndex = 30;
            this.txtSalvageValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalvageValue_KeyPress);
            this.txtSalvageValue.Enter += new System.EventHandler(this.txtSalvageValue_Enter);
            // 
            // txtWrittenDownVal
            // 
            this.txtWrittenDownVal.BackColor = System.Drawing.Color.White;
            this.txtWrittenDownVal.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtWrittenDownVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWrittenDownVal.Location = new System.Drawing.Point(576, 254);
            this.txtWrittenDownVal.Name = "txtWrittenDownVal";
            this.txtWrittenDownVal.ReadOnly = true;
            this.txtWrittenDownVal.Size = new System.Drawing.Size(209, 21);
            this.txtWrittenDownVal.TabIndex = 26;
            this.txtWrittenDownVal.Visible = false;
            this.txtWrittenDownVal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWrittenDownVal_KeyPress);
            this.txtWrittenDownVal.Enter += new System.EventHandler(this.txtWrittenDownVal_Enter);
            // 
            // txtAssetLife
            // 
            this.txtAssetLife.BackColor = System.Drawing.Color.White;
            this.txtAssetLife.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetLife.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetLife.Location = new System.Drawing.Point(576, 232);
            this.txtAssetLife.Name = "txtAssetLife";
            this.txtAssetLife.ReadOnly = true;
            this.txtAssetLife.Size = new System.Drawing.Size(209, 21);
            this.txtAssetLife.TabIndex = 28;
            this.txtAssetLife.Text = "0.00";
            this.txtAssetLife.Enter += new System.EventHandler(this.txtAssetLife_Enter);
            // 
            // txtDepRate
            // 
            this.txtDepRate.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtDepRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDepRate.Location = new System.Drawing.Point(576, 210);
            this.txtDepRate.Name = "txtDepRate";
            this.txtDepRate.Size = new System.Drawing.Size(144, 21);
            this.txtDepRate.TabIndex = 27;
            this.txtDepRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDepRate.TextChanged += new System.EventHandler(this.txtDepRate_TextChanged);
            this.txtDepRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDepRate_KeyPress);
            this.txtDepRate.Enter += new System.EventHandler(this.txtDepRate_Enter);
            // 
            // txtAccumulatDep
            // 
            this.txtAccumulatDep.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAccumulatDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAccumulatDep.Location = new System.Drawing.Point(576, 188);
            this.txtAccumulatDep.Name = "txtAccumulatDep";
            this.txtAccumulatDep.Size = new System.Drawing.Size(209, 21);
            this.txtAccumulatDep.TabIndex = 23;
            this.txtAccumulatDep.Visible = false;
            this.txtAccumulatDep.TextChanged += new System.EventHandler(this.txtAccumulatDep_TextChanged);
            this.txtAccumulatDep.Leave += new System.EventHandler(this.txtAccumulatDep_Leave);
            this.txtAccumulatDep.Enter += new System.EventHandler(this.txtAccumulatDep_Enter);
            // 
            // txtInvCostUsd
            // 
            this.txtInvCostUsd.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtInvCostUsd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInvCostUsd.Location = new System.Drawing.Point(576, 34);
            this.txtInvCostUsd.Name = "txtInvCostUsd";
            this.txtInvCostUsd.Size = new System.Drawing.Size(138, 21);
            this.txtInvCostUsd.TabIndex = 19;
            this.txtInvCostUsd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInvCostUsd.Leave += new System.EventHandler(this.txtInvCostUsd_Leave);
            this.txtInvCostUsd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInvCostUsd_KeyPress);
            this.txtInvCostUsd.Enter += new System.EventHandler(this.txtInvCostUsd_Enter);
            // 
            // txtAssetDesc
            // 
            this.txtAssetDesc.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetDesc.Location = new System.Drawing.Point(156, 364);
            this.txtAssetDesc.Multiline = true;
            this.txtAssetDesc.Name = "txtAssetDesc";
            this.txtAssetDesc.Size = new System.Drawing.Size(209, 43);
            this.txtAssetDesc.TabIndex = 16;
            this.txtAssetDesc.Enter += new System.EventHandler(this.txtAssetDesc_Enter);
            // 
            // txtModelNo
            // 
            this.txtModelNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtModelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtModelNo.Location = new System.Drawing.Point(156, 276);
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new System.Drawing.Size(209, 21);
            this.txtModelNo.TabIndex = 12;
            this.txtModelNo.Enter += new System.EventHandler(this.txtModelNo_Enter);
            // 
            // txtBrand
            // 
            this.txtBrand.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtBrand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBrand.Location = new System.Drawing.Point(156, 254);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(209, 21);
            this.txtBrand.TabIndex = 11;
            this.txtBrand.TextChanged += new System.EventHandler(this.txtBrand_TextChanged);
            this.txtBrand.Enter += new System.EventHandler(this.txtBrand_Enter);
            // 
            // txtLCNo
            // 
            this.txtLCNo.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtLCNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLCNo.Location = new System.Drawing.Point(156, 232);
            this.txtLCNo.Name = "txtLCNo";
            this.txtLCNo.Size = new System.Drawing.Size(209, 21);
            this.txtLCNo.TabIndex = 10;
            this.txtLCNo.Enter += new System.EventHandler(this.txtLCNo_Enter);
            // 
            // txtCountry
            // 
            this.txtCountry.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCountry.Location = new System.Drawing.Point(156, 210);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(209, 21);
            this.txtCountry.TabIndex = 9;
            this.txtCountry.TextChanged += new System.EventHandler(this.txtCountry_TextChanged);
            this.txtCountry.Enter += new System.EventHandler(this.txtCountry_Enter);
            // 
            // txtAsset
            // 
            this.txtAsset.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAsset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAsset.Location = new System.Drawing.Point(156, 78);
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.Size = new System.Drawing.Size(209, 21);
            this.txtAsset.TabIndex = 4;
            this.txtAsset.TextChanged += new System.EventHandler(this.txtAsset_TextChanged);
            this.txtAsset.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAsset_KeyPress);
            this.txtAsset.Enter += new System.EventHandler(this.txtAsset_Enter);
            // 
            // cmbGroup
            // 
            this.cmbGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(156, 56);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(209, 21);
            this.cmbGroup.TabIndex = 3;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            this.cmbGroup.DropDown += new System.EventHandler(this.cmbGroup_DropDown);
            // 
            // cmbCC
            // 
            this.cmbCC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCC.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(156, 34);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(209, 21);
            this.cmbCC.TabIndex = 2;
            this.cmbCC.SelectedIndexChanged += new System.EventHandler(this.cmbCC_SelectedIndexChanged);
            this.cmbCC.DropDown += new System.EventHandler(this.cmbCC_DropDown);
            // 
            // cmbCompany
            // 
            this.cmbCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(156, 12);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(209, 21);
            this.cmbCompany.TabIndex = 1;
            this.cmbCompany.SelectedIndexChanged += new System.EventHandler(this.cmbCompany_SelectedIndexChanged);
            this.cmbCompany.DropDown += new System.EventHandler(this.cmbCompany_DropDown);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(796, 480);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(400, 258);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(112, 13);
            this.label70.TabIndex = 421;
            this.label70.Text = "Asset Own SL. No.";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(400, 192);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(102, 13);
            this.label67.TabIndex = 416;
            this.label67.Text = "Invoice/Bill Date";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(138, 346);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(12, 13);
            this.label69.TabIndex = 420;
            this.label69.Text = ":";
            // 
            // dtpDepEffectDate
            // 
            this.dtpDepEffectDate.CustomFormat = "dd-MMM-yyyy, dddd";
            this.dtpDepEffectDate.Enabled = false;
            this.dtpDepEffectDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDepEffectDate.Location = new System.Drawing.Point(576, 166);
            this.dtpDepEffectDate.Name = "dtpDepEffectDate";
            this.dtpDepEffectDate.Size = new System.Drawing.Size(209, 21);
            this.dtpDepEffectDate.TabIndex = 427;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel2.Controls.Add(this.btnCross);
            this.panel2.Controls.Add(this.label66);
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(796, 38);
            this.panel2.TabIndex = 416;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(765, 3);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 417;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(15, 11);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(306, 16);
            this.label66.TabIndex = 415;
            this.label66.Text = "Fixed Assets - New Asset Purchase Entry";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmAssetConfigN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(806, 530);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAssetConfigN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asset Information Entry";
            this.Load += new System.EventHandler(this.frmAssetConfigN_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmAssetConfigN_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetSubLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAssetCode)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private FlatTextBox txtAssetCode;
        private FlatTextBox txtWPeriod;
        private AssetRegister.FlatComboBox cmbDepMethod;
        private AssetRegister.FlatComboBox cmbActive;
        private FlatTextBox txtComments;
        private FlatTextBox txtVoucherNo;
        private FlatTextBox txtSalvageValue;
        private FlatTextBox txtWrittenDownVal;
        private FlatTextBox txtAssetLife;
        private FlatTextBox txtDepRate;
        private FlatTextBox txtAccumulatDep;
        private FlatTextBox txtInvCostUsd;
        private FlatTextBox txtAssetDesc;
        private FlatTextBox txtModelNo;
        private FlatTextBox txtBrand;
        private FlatTextBox txtLCNo;
        private FlatTextBox txtCountry;
        private FlatTextBox txtAsset;
        private AssetRegister.FlatComboBox cmbGroup;
        private AssetRegister.FlatComboBox cmbCC;
        private AssetRegister.FlatComboBox cmbCompany;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.RadioButton rbNo;
        private System.Windows.Forms.RadioButton rbYes;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label56;
        private FlatTextBox txtRegNo;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private FlatTextBox txtInvCostBdt;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private FlatTextBox txtDutiesNothers;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.DataGridView dgvAssetLoc;
        private System.Windows.Forms.DataGridView dgvAssetUser;
        private System.Windows.Forms.DataGridView dgvAssetSubLoc;
        private FlatTextBox txtAssetLoc;
        private FlatTextBox txtAssetSubLoc;
        private FlatTextBox txtAssetUser;
        private FlatTextBox txtSupplier;
        private System.Windows.Forms.DataGridView dgvSupplier;
        private System.Windows.Forms.DataGridView dgvAssetCode;
        private AssetRegister.FlatDateTimePicker dtpPurchaseDate;
        private AssetRegister.FlatDateTimePicker dtpMonth;
        private AssetRegister.FlatDateTimePicker dtpYear;
        private System.Windows.Forms.Button btnPreview;
        private AssetRegister.FlatComboBox cmbCustodianDept;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Panel panel1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Button btnRfrshSupplier;
        private AssetRegister.FlatDateTimePicker dtpInvBillDate;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private AssetRegister.FlatDateTimePicker dtpAssetRecDate;
        private FlatTextBox txtAssetSlNo;
        private FlatTextBox txtConvRate;
        private AssetRegister.FlatComboBox cmbCurrencyType;
        private System.Windows.Forms.Button btnRfrshSubLoc;
        private System.Windows.Forms.Button btnRfrshLoc;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private FlatTextBox txtAccCode;
        private FlatDateTimePicker dtpDepEffectDate;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.CheckBox chkWDV;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private FlatTextBox txtUnitPrice;
        private FlatTextBox txtQuantity;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private FlatTextBox txtGeneralName;
    }
}