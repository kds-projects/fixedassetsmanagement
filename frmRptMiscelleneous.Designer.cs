﻿namespace AssetRegister
{
    partial class frmRptMiscelleneous
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvListData = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCross = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbLandDoc = new System.Windows.Forms.RadioButton();
            this.rbDisposalForm = new System.Windows.Forms.RadioButton();
            this.rbTransferForm = new System.Windows.Forms.RadioButton();
            this.rbNewAssetForm = new System.Windows.Forms.RadioButton();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rbExistAddition = new System.Windows.Forms.RadioButton();
            this.rbLandExisting = new System.Windows.Forms.RadioButton();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BackColor = System.Drawing.Color.Gainsboro;
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(364, 117);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 121);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(364, 39);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 337;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 336;
            this.label2.Text = "Asset Code";
            // 
            // btnShow
            // 
            this.btnShow.Image = global::AssetRegister.Properties.Resources.Actions_document_find_icon;
            this.btnShow.Location = new System.Drawing.Point(291, 122);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(38, 38);
            this.btnShow.TabIndex = 338;
            this.btnShow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            this.btnShow.MouseHover += new System.EventHandler(this.btnShow_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(328, 122);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 339;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // dgvListData
            // 
            this.dgvListData.AllowUserToAddRows = false;
            this.dgvListData.AllowUserToDeleteRows = false;
            this.dgvListData.AllowUserToResizeColumns = false;
            this.dgvListData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvListData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListData.BackgroundColor = System.Drawing.Color.White;
            this.dgvListData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvListData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvListData.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvListData.EnableHeadersVisualStyles = false;
            this.dgvListData.Location = new System.Drawing.Point(217, 2);
            this.dgvListData.MultiSelect = false;
            this.dgvListData.Name = "dgvListData";
            this.dgvListData.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvListData.RowHeadersVisible = false;
            this.dgvListData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListData.Size = new System.Drawing.Size(150, 159);
            this.dgvListData.TabIndex = 340;
            this.dgvListData.Visible = false;
            this.dgvListData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListData_CellDoubleClick);
            this.dgvListData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvListData_KeyDown);
            this.dgvListData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvListData_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Controls.Add(this.label66);
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(369, 44);
            this.panel1.TabIndex = 341;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(337, 7);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 420;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(7, 15);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(315, 16);
            this.label66.TabIndex = 418;
            this.label66.Text = "Fixed Assets - Asset Miscelleneous Report";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.dgvListData);
            this.panel2.Controls.Add(this.rbLandExisting);
            this.panel2.Controls.Add(this.rbExistAddition);
            this.panel2.Controls.Add(this.rbLandDoc);
            this.panel2.Controls.Add(this.rbDisposalForm);
            this.panel2.Controls.Add(this.rbTransferForm);
            this.panel2.Controls.Add(this.rbNewAssetForm);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtAssetCode);
            this.panel2.Controls.Add(this.btnShow);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(4, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(369, 163);
            this.panel2.TabIndex = 342;
            // 
            // rbLandDoc
            // 
            this.rbLandDoc.AutoSize = true;
            this.rbLandDoc.Location = new System.Drawing.Point(170, 6);
            this.rbLandDoc.Name = "rbLandDoc";
            this.rbLandDoc.Size = new System.Drawing.Size(147, 17);
            this.rbLandDoc.TabIndex = 342;
            this.rbLandDoc.TabStop = true;
            this.rbLandDoc.Text = "Land Document Form";
            this.rbLandDoc.UseVisualStyleBackColor = true;
            this.rbLandDoc.CheckedChanged += new System.EventHandler(this.rbLandDoc_CheckedChanged);
            // 
            // rbDisposalForm
            // 
            this.rbDisposalForm.AutoSize = true;
            this.rbDisposalForm.Location = new System.Drawing.Point(14, 54);
            this.rbDisposalForm.Name = "rbDisposalForm";
            this.rbDisposalForm.Size = new System.Drawing.Size(106, 17);
            this.rbDisposalForm.TabIndex = 341;
            this.rbDisposalForm.TabStop = true;
            this.rbDisposalForm.Text = "Disposal Form";
            this.rbDisposalForm.UseVisualStyleBackColor = true;
            this.rbDisposalForm.CheckedChanged += new System.EventHandler(this.rbDisposalForm_CheckedChanged);
            // 
            // rbTransferForm
            // 
            this.rbTransferForm.AutoSize = true;
            this.rbTransferForm.Location = new System.Drawing.Point(14, 30);
            this.rbTransferForm.Name = "rbTransferForm";
            this.rbTransferForm.Size = new System.Drawing.Size(105, 17);
            this.rbTransferForm.TabIndex = 341;
            this.rbTransferForm.TabStop = true;
            this.rbTransferForm.Text = "Transfer Form";
            this.rbTransferForm.UseVisualStyleBackColor = true;
            this.rbTransferForm.CheckedChanged += new System.EventHandler(this.rbTransferForm_CheckedChanged);
            // 
            // rbNewAssetForm
            // 
            this.rbNewAssetForm.AutoSize = true;
            this.rbNewAssetForm.Location = new System.Drawing.Point(14, 6);
            this.rbNewAssetForm.Name = "rbNewAssetForm";
            this.rbNewAssetForm.Size = new System.Drawing.Size(117, 17);
            this.rbNewAssetForm.TabIndex = 341;
            this.rbNewAssetForm.TabStop = true;
            this.rbNewAssetForm.Text = "New Asset Form";
            this.rbNewAssetForm.UseVisualStyleBackColor = true;
            this.rbNewAssetForm.CheckedChanged += new System.EventHandler(this.rbNewAssetForm_CheckedChanged);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(369, 163);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // rbExistAddition
            // 
            this.rbExistAddition.AutoSize = true;
            this.rbExistAddition.Location = new System.Drawing.Point(170, 30);
            this.rbExistAddition.Name = "rbExistAddition";
            this.rbExistAddition.Size = new System.Drawing.Size(152, 17);
            this.rbExistAddition.TabIndex = 343;
            this.rbExistAddition.TabStop = true;
            this.rbExistAddition.Text = "Existing Addition Form";
            this.rbExistAddition.UseVisualStyleBackColor = true;
            this.rbExistAddition.CheckedChanged += new System.EventHandler(this.rbExistAddition_CheckedChanged);
            // 
            // rbLandExisting
            // 
            this.rbLandExisting.AutoSize = true;
            this.rbLandExisting.Location = new System.Drawing.Point(170, 54);
            this.rbLandExisting.Name = "rbLandExisting";
            this.rbLandExisting.Size = new System.Drawing.Size(183, 17);
            this.rbLandExisting.TabIndex = 344;
            this.rbLandExisting.TabStop = true;
            this.rbLandExisting.Text = "Land Existing Addition Form";
            this.rbLandExisting.UseVisualStyleBackColor = true;
            this.rbLandExisting.CheckedChanged += new System.EventHandler(this.rbLandExisting_CheckedChanged);
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Location = new System.Drawing.Point(107, 88);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.Size = new System.Drawing.Size(209, 21);
            this.txtAssetCode.TabIndex = 335;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            // 
            // frmRptMiscelleneous
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(377, 214);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRptMiscelleneous";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asset Addition List Report";
            this.Load += new System.EventHandler(this.frmRptAdditionList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private FlatTextBox txtAssetCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvListData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.RadioButton rbDisposalForm;
        private System.Windows.Forms.RadioButton rbTransferForm;
        private System.Windows.Forms.RadioButton rbNewAssetForm;
        private System.Windows.Forms.RadioButton rbLandDoc;
        private System.Windows.Forms.RadioButton rbLandExisting;
        private System.Windows.Forms.RadioButton rbExistAddition;
    }
}