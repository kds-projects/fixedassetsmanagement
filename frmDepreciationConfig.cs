﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmDepreciationConfig : Form
    {
        private clsCommonFunctions dbConDepConfig = new clsCommonFunctions();
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmDepreciationConfig()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDepreciationConfig_Load(object sender, EventArgs e)
        {
            dbConDepConfig.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            cmbCompany.SelectedIndex = -1;
            dbConDepConfig.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 0");
            cmbGroup.SelectedIndex = -1;           
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dbConDepConfig.CheckEmptyOrNull(cmbCompany, "Select Company") &&
                    !dbConDepConfig.CheckEmptyOrNull(cmbGroup, "Select Group") &&
                    !dbConDepConfig.CheckEmptyOrNull(cmbDepMethod, "Select Depreciation Method"))
                {
                    string[] cols = { "CompanyId", "AssetGroupId", "DepreciationMethod", "UserId" };
                    string[] vals = { cmbCompany.SelectedValue.ToString(), cmbGroup.SelectedValue.ToString(),
                                      cmbDepMethod.Text, frmLogin.userId.ToString()};
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_DepreciationConfig]", this.Name, 
                                       dbConDepConfig.getMaxIdentity("[AR_DepreciationConfig]", "[TrnNo]").ToString(), "", "" };

                    string sql = dbConDepConfig.CreateSQLStatement("[AssetRegistar].[dbo].[AR_DepreciationConfig]", cols, vals, "INSERT", null, null);
                    sql += dbConDepConfig.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConDepConfig.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Information Saved Successfully.", "Success", 
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbCompany.SelectedIndex = -1;
                        cmbGroup.SelectedIndex = -1;
                        cmbDepMethod.SelectedIndex = -1;
                    }
                    else
                    {
                        MessageBox.Show("Failed to Save Information.", "Failed", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmDepreciationConfig_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }
    }
}
