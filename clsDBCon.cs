﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;

namespace AssetRegister
{
    class clsDBCon
    {
        //public static string DBHostLocal = "192.168.0.26";
        //public static string DBHostReal = "192.168.0.26";
        public static string DBHostLocal = "192.168.0.181";
        public static string DBHostReal = "203.82.207.170";
        public static string DBHostConnected = "";
        public static string DBName = "AssetRegistar";
        public static string DBUserId = "sa";
        //public static string DBPassword = "123456";
        public static string DBPassword = "dbsrv170@sdk";
        public SqlConnection DBCon = new SqlConnection(ConnectionString());
        public SqlCommand sqlCmd;
        public SqlDataReader sqlDtRdr;
        public SqlDataAdapter sqlDtAdapter;

        public static string ConnectionString()
        {
            if (PingHost(DBHostLocal, 1433))
            {
                DBHostConnected = DBHostLocal;

                return @"Server = " + DBHostLocal +
                        ";Database = " + DBName +
                        ";User Id = " + DBUserId +
                        ";Password = " + DBPassword +
                        ";Connection Timeout=120;";
            }
            else if (PingHost(DBHostReal, 1433))
            {
                DBHostConnected = DBHostReal;

                return @"Server = " + DBHostReal +
                       ";Database = " + DBName +
                       ";User Id = " + DBUserId +
                       ";Password = " + DBPassword + ";";
            }
            else
                return "Server is Unreachable, Please try after some times.";
        }

        public static bool PingHost(string NameOrAddress)
        {
            bool isConnected = false;
            Ping pinger = new Ping();

            try
            {
                PingReply reply = pinger.Send(NameOrAddress);
                isConnected = reply.Status == IPStatus.Success;
            }
            catch (PingException e)
            {
                MessageBox.Show(e.Message, "Host Error");
            }

            return isConnected;
        }

        public static bool PingHost(string Server, int Port)
        {
            System.Net.Sockets.TcpClient tcpClient = null;

            try
            {
                tcpClient = new System.Net.Sockets.TcpClient();
                int timeout = (Server.Substring(0, 3).Equals("192") ? 120 : 1000);
                tcpClient.ReceiveTimeout = tcpClient.SendTimeout = timeout;

                IPAddress address;
                if (IPAddress.TryParse(Server, out address))
                {
                    var endPoint = new System.Net.IPEndPoint(address, Port);
                    tcpClient.Connect(endPoint);
                }
                else
                {
                    tcpClient.Connect(Server, Port);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (tcpClient != null)
                    tcpClient.Close();
            }
        }

        public object DBCRUDFunction(string sql, bool isRetrieval)
        {
            if (DBCon.State.Equals(ConnectionState.Open))
                DBCon.Close();

            try
            {
                DBCon.Open();
                sqlCmd = new SqlCommand(sql, DBCon);
                sqlCmd.CommandTimeout = 120;
                if (isRetrieval)
                {
                    sqlDtRdr = sqlCmd.ExecuteReader();
                    return sqlDtRdr;
                }
                else
                {
                    sqlCmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "DB CRUD FUNCTION ERROR");
                return false;
            }
        }

        public DataTable DBCRUDFunctionAsTable(string sql)
        {
            if (DBCon.State.Equals(ConnectionState.Open))
                DBCon.Close();

            DataTable dt = new DataTable();

            try
            {
                DBCon.Open();
                sqlCmd = new SqlCommand(sql, DBCon);
                sqlCmd.CommandTimeout = 120;

                sqlDtAdapter = new SqlDataAdapter(sqlCmd);

                sqlDtAdapter.Fill(dt);

                return dt;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "DB CRUD FUNCTION ERROR");
                return dt;
            }
        }
    }
}
