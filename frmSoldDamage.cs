﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AssetRegister
{
    public partial class frmSoldDamage : Form
    {
        private clsCommonFunctions dbConSDRec = new clsCommonFunctions();
        private int AssetId = 0;
        private string AssetCode = "", LastDepreciationEffectDate = "";

        public frmSoldDamage()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbEntryType.SelectedIndex == 0)
                SellInfoSave();
            else if (cmbEntryType.SelectedIndex == 1)
                DamageInfoSave();
            else if (cmbEntryType.SelectedIndex == 2)
                RetirmentInfoSave();
            else if (cmbEntryType.SelectedIndex == 3)
                DepSeizedInfoSave();
            else
                MessageBox.Show("Please Select an Item.", "Save Failed", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void SellInfoSave()
        {
            if (AssetId > 0 && !dbConSDRec.CheckEmptyOrNull(txtAssetCode, "Select Asset"))
            {
                string[] scols = { "[AssetId]", "[SoldDate]", "[SoldAmount]", "[Buyer]", "[BuyerAddress]", 
                                   "[BuyerContactNo]", "[SoldApprovedBy]", "[VoucherNo]", "[VoucherDate]",
                                   "[SellReason]", "[Comments]", "[EntryBy]" };
                string[] svals = { AssetId.ToString(), dtpSDDate.Value.ToString("yyyy-MM-dd"), txtAmount.Text,
                                   dbConSDRec.SingleQuoteHandler(txtBuyer), dbConSDRec.SingleQuoteHandler(txtAddress),
                                   dbConSDRec.SingleQuoteHandler(txtContactNo), dbConSDRec.SingleQuoteHandler(txtApprovedBy),
                                   dbConSDRec.SingleQuoteHandler(txtVoucherNo), dtpVoucherDate.Value.ToString("yyyy-MM-dd"), 
                                   dbConSDRec.SingleQuoteHandler(txtReason), dbConSDRec.SingleQuoteHandler(txtComments),
                                   frmLogin.userId.ToString() };
                string[] cols = { "[IsSoldOrIsDamage]", "[SoldOrDamageDate]", "[DepreciationEffectDate]",
                                  "[SoldOrDamageComments]", "[IsDepCalc]", "[UpdateDateTime]", "[UpdateUserId]" };
                string[] vals = { "1", dtpSDDate.Value.ToString("yyyy-MM-dd"), dtpSDDate.Value.AddDays(1-dtpSDDate.Value.Day).ToString("yyyy-MM-dd"),
                                  dbConSDRec.SingleQuoteHandler(txtComments), "0", dbConSDRec.GetServerCurrentDateTime().ToString(), 
                                  frmLogin.userId.ToString() };
                string[] whCols = { "[AssetId]" };
                string[] whVals = { AssetId.ToString() };
                string[] whOp = { "=" };
                string[] whColsD = { "[AssetId]", "[ProcessDate]" };
                string[] whValsD = { AssetId.ToString(), dtpSDDate.Value.AddDays(1 - dtpSDDate.Value.Day).ToString("yyyy-MM-dd") };
                string[] whOpD = { "=", ">=" };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT,UPDATE,DELETE,DELETE", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetSellInfo],[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_Process],[AssetRegistar].[dbo].[AR_ProcessAddition]", this.Name, 
                                   dbConSDRec.getMaxIdentity("[AR_AssetSellInfo]", "[TrnNo]").ToString(), "[AssetId],[IsSoldOrIsDamage],[SoldOrDamageDate],[DepreciationEffectDate],[SoldOrDamageComments],[IsDepCalc]",
                                   AssetId + ",1," + dtpSDDate.Value.ToString("yyyy-MM-dd") + "," + LastDepreciationEffectDate + "," + dbConSDRec.SingleQuoteHandler(txtComments) + ",0" };

                string sql = dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetSellInfo]", scols, svals, "INSERT", null, null);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols, vals, "UPDATE", whCols, whVals, whOp);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_Process]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_ProcessAddition]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                if ((bool)dbConSDRec.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Information saved successfully.", "Save",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssetCode.Text = "";
                    AssetId = 0;
                    txtComments.Text = "";
                    AssetCode = "";
                    txtAssetCode.Focus();
                    dgvListData.Visible = false;
                    txtApprovedBy.Text = "";
                    txtReason.Text = "";
                    txtVoucherNo.Text = "";
                    txtAmount.Text = "";
                    txtBuyer.Text = "";
                    txtAddress.Text = "";
                    txtContactNo.Text = "";
                    cmbEntryType.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Failed to save information.", "Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void DamageInfoSave()
        {
            if (AssetId > 0 && !dbConSDRec.CheckEmptyOrNull(txtAssetCode, "Select Asset"))
            {
                string[] scols = { "[AssetId]", "[DamageDate]", "[AuthorisedBy]", "[VoucherNo]", "[VoucherDate]",
                                   "[DamageReason]", "[Comments]", "[EntryBy]" };
                string[] svals = { AssetId.ToString(), dtpSDDate.Value.ToString("yyyy-MM-dd"), 
                                   dbConSDRec.SingleQuoteHandler(txtApprovedBy), dbConSDRec.SingleQuoteHandler(txtVoucherNo), 
                                   dtpVoucherDate.Value.ToString("yyyy-MM-dd"), dbConSDRec.SingleQuoteHandler(txtReason), 
                                   dbConSDRec.SingleQuoteHandler(txtComments), frmLogin.userId.ToString() };
                string[] cols = { "[IsSoldOrIsDamage]", "[SoldOrDamageDate]", "[DepreciationEffectDate]",
                                  "[SoldOrDamageComments]", "[IsDepCalc]", "[UpdateDateTime]", "[UpdateUserId]" };
                string[] vals = { "1", dtpSDDate.Value.ToString("yyyy-MM-dd"), dtpSDDate.Value.AddDays(1-dtpSDDate.Value.Day).ToString("yyyy-MM-dd"),
                                  dbConSDRec.SingleQuoteHandler(txtComments), "0", dbConSDRec.GetServerCurrentDateTime().ToString(), 
                                  frmLogin.userId.ToString() };
                string[] whCols = { "[AssetId]" };
                string[] whVals = { AssetId.ToString() };
                string[] whOp = { "=" };
                string[] whColsD = { "[AssetId]", "[ProcessDate]" };
                string[] whValsD = { AssetId.ToString(), dtpSDDate.Value.AddDays(1 - dtpSDDate.Value.Day).ToString("yyyy-MM-dd") };
                string[] whOpD = { "=", ">=" };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT,UPDATE,DELETE,DELETE", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetDamageInfo],[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_Process],[AssetRegistar].[dbo].[AR_ProcessAddition]", this.Name, 
                                   dbConSDRec.getMaxIdentity("[AR_AssetDamageInfo]", "[TrnNo]").ToString(), "[AssetId],[IsSoldOrIsDamage],[SoldOrDamageDate],[DepreciationEffectDate],[SoldOrDamageComments],[IsDepCalc]",
                                   AssetId + ",1," + dtpSDDate.Value.ToString("yyyy-MM-dd") + "," + LastDepreciationEffectDate + "," + dbConSDRec.SingleQuoteHandler(txtComments) + ",0" };

                string sql = dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetDamageInfo]", scols, svals, "INSERT", null, null);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols, vals, "UPDATE", whCols, whVals, whOp);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_Process]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_ProcessAddition]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                if ((bool)dbConSDRec.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Information saved successfully.", "Save",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssetCode.Text = "";
                    AssetId = 0;
                    txtComments.Text = "";
                    AssetCode = "";
                    txtAssetCode.Focus();
                    dgvListData.Visible = false;
                    txtApprovedBy.Text = "";
                    txtReason.Text = "";
                    txtVoucherNo.Text = "";
                    txtAmount.Text = "";
                    txtBuyer.Text = "";
                    txtAddress.Text = "";
                    txtContactNo.Text = "";
                    cmbEntryType.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Failed to save information.", "Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void RetirmentInfoSave()
        {
            if (AssetId > 0 && !dbConSDRec.CheckEmptyOrNull(txtAssetCode, "Select Asset"))
            {
                string[] scols = { "[AssetId]", "[RetirementDate]", "[AuthorisedBy]", "[VoucherNo]", "[VoucherDate]",
                                   "[RetirementReason]", "[Comments]", "[EntryBy]" };
                string[] svals = { AssetId.ToString(), dtpSDDate.Value.ToString("yyyy-MM-dd"), 
                                   dbConSDRec.SingleQuoteHandler(txtApprovedBy), dbConSDRec.SingleQuoteHandler(txtVoucherNo), 
                                   dtpVoucherDate.Value.ToString("yyyy-MM-dd"), dbConSDRec.SingleQuoteHandler(txtReason), 
                                   dbConSDRec.SingleQuoteHandler(txtComments), frmLogin.userId.ToString() };
                string[] cols = { "[IsSoldOrIsDamage]", "[SoldOrDamageDate]", "[DepreciationEffectDate]",
                                  "[SoldOrDamageComments]", "[IsDepCalc]", "[UpdateDateTime]", "[UpdateUserId]" };
                string[] vals = { "1", dtpSDDate.Value.ToString("yyyy-MM-dd"), dtpSDDate.Value.AddDays(1-dtpSDDate.Value.Day).ToString("yyyy-MM-dd"),
                                  dbConSDRec.SingleQuoteHandler(txtComments), "0", dbConSDRec.GetServerCurrentDateTime().ToString(), 
                                  frmLogin.userId.ToString() };
                string[] whCols = { "[AssetId]" };
                string[] whVals = { AssetId.ToString() };
                string[] whOp = { "=" };
                string[] whColsD = { "[AssetId]", "[ProcessDate]" };
                string[] whValsD = { AssetId.ToString(), dtpSDDate.Value.AddDays(1 - dtpSDDate.Value.Day).ToString("yyyy-MM-dd") };
                string[] whOpD = { "=", ">=" };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT,UPDATE,DELETE,DELETE", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetRetirementInfo],[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_Process],[AssetRegistar].[dbo].[AR_ProcessAddition]", this.Name, 
                                   dbConSDRec.getMaxIdentity("[AR_AssetRetirementInfo]", "[TrnNo]").ToString(), "[AssetId],[IsSoldOrIsDamage],[SoldOrDamageDate],[DepreciationEffectDate],[SoldOrDamageComments],[IsDepCalc]",
                                   AssetId + ",1," + dtpSDDate.Value.ToString("yyyy-MM-dd") + "," + LastDepreciationEffectDate + ","  + dbConSDRec.SingleQuoteHandler(txtComments) + ",0" };

                string sql = dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetRetirementInfo]", scols, svals, "INSERT", null, null);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols, vals, "UPDATE", whCols, whVals,whOp);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_Process]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_ProcessAddition]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                if ((bool)dbConSDRec.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Information saved successfully.", "Save",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssetCode.Text = "";
                    AssetId = 0;
                    txtComments.Text = "";
                    AssetCode = "";
                    txtAssetCode.Focus();
                    dgvListData.Visible = false;
                    txtApprovedBy.Text = "";
                    txtReason.Text = "";
                    txtVoucherNo.Text = "";
                    txtAmount.Text = "";
                    txtBuyer.Text = "";
                    txtAddress.Text = "";
                    txtContactNo.Text = "";
                    cmbEntryType.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Failed to save information.", "Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        
        private void DepSeizedInfoSave()
        {
            if (AssetId > 0 && !dbConSDRec.CheckEmptyOrNull(txtAssetCode, "Select Asset"))
            {
                string[] scols = { "[AssetId]", "[RecordDate]", "[ApprovedBy]", "[SeizedReason]", //"[Comments]", 
                                   "[EntryBy]" };
                string[] svals = { AssetId.ToString(), dtpSDDate.Value.ToString("yyyy-MM-dd"), 
                                   dbConSDRec.SingleQuoteHandler(txtApprovedBy),
                                   dbConSDRec.SingleQuoteHandler(txtReason), frmLogin.userId.ToString() };
                string[] cols = { "[IsSoldOrIsDamage]", "[SoldOrDamageDate]", "[DepreciationEffectDate]",
                                  "[SoldOrDamageComments]", "[IsDepCalc]", "[UpdateDateTime]", "[UpdateUserId]" };
                string[] vals = { "1", dtpSDDate.Value.ToString("yyyy-MM-dd"), dtpSDDate.Value.AddDays(1-dtpSDDate.Value.Day).ToString("yyyy-MM-dd"),
                                    dbConSDRec.SingleQuoteHandler(txtComments), "0", dbConSDRec.GetServerCurrentDateTime().ToString(), 
                                    frmLogin.userId.ToString() };
                string[] whCols = { "[AssetId]" };
                string[] whVals = { AssetId.ToString() };
                string[] whOp = { "=" };
                string[] whColsD = { "[AssetId]", "[ProcessDate]" };
                string[] whValsD = { AssetId.ToString(), dtpSDDate.Value.AddDays(1 - dtpSDDate.Value.Day).ToString("yyyy-MM-dd") };
                string[] whOpD = { "=", ">=" };
                string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                               "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                string[] lVals = { frmLogin.userId.ToString(), "INSERT,UPDATE,DELETE,DELETE", frmLogin.ipAddress, 
                                   "[AssetRegistar].[dbo].[AR_AssetDepSeizedInfo],[AssetRegistar].[dbo].[AR_AssetConfig],[AssetRegistar].[dbo].[AR_Process],[AssetRegistar].[dbo].[AR_ProcessAddition]", this.Name, 
                                   dbConSDRec.getMaxIdentity("[AR_AssetDepSeizedInfo]", "[TrnNo]").ToString(), "[AssetId],[IsSoldOrIsDamage],[SoldOrDamageDate],[DepreciationEffectDate],[SoldOrDamageComments],[IsDepCalc]",
                                   AssetId + ",1," + dtpSDDate.Value.ToString("yyyy-MM-dd") + "," + LastDepreciationEffectDate + "," + dbConSDRec.SingleQuoteHandler(txtComments) + ",0" };

                string sql = dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetDepSeizedInfo]", scols, svals, "INSERT", null, null);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetConfig]", cols, vals, "UPDATE", whCols, whVals, whOp);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_Process]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_ProcessAddition]", null, null, "DELETE", whColsD, whValsD, whOpD);
                sql += dbConSDRec.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                if ((bool)dbConSDRec.DBCRUDFunction(sql, false))
                {
                    MessageBox.Show("Information saved successfully.", "Save",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssetCode.Text = "";
                    AssetId = 0;
                    txtComments.Text = "";
                    AssetCode = "";
                    txtAssetCode.Focus();
                    dgvListData.Visible = false;
                    txtApprovedBy.Text = "";
                    txtReason.Text = "";
                    txtVoucherNo.Text = "";
                    txtAmount.Text = "";
                    txtBuyer.Text = "";
                    txtAddress.Text = "";
                    txtContactNo.Text = "";
                    cmbEntryType.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Failed to save information.", "Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }        
        
        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConSDRec.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConSDRec.DisposeChecking(txtAssetCode);
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
            LoadAssetDetails();
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                LoadAssetDetails();
            }
        }

        private void LoadAssetDetails() 
        {
            SqlDataReader dr;
            dr =(SqlDataReader) dbConSDRec.DBCRUDFunction(@"SELECT * FROM [AssetRegistar].[dbo].[AR_Assetconfig] WHERE AssetId = " + AssetId, true);

            if (dr.Read())
            {
                LastDepreciationEffectDate = Convert.ToDateTime(dr["DepreciationEffectDate"].ToString()).ToString("yyyy-MM-dd");
            }
            
            dr.Close();
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void frmSoldDamage_Load(object sender, EventArgs e)
        {
            dbConSDRec.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '0', '', '', 2");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbEntryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbEntryType.SelectedIndex == 0)
            {
                label9.Visible = true;
                label10.Visible = true;
                txtAmount.Visible = true;

                label11.Visible = true;
                label12.Visible = true;
                txtBuyer.Visible = true;

                label21.Visible = true;
                label22.Visible = true;
                txtAddress.Visible = true;

                label23.Visible = true;
                label24.Visible = true;
                txtContactNo.Visible = true;

                label17.Visible = true;
                label18.Visible = true;
                txtVoucherNo.Visible = true;

                label19.Visible = true;
                label20.Visible = true;
                dtpVoucherDate.Visible = true;

                label5.Visible = true;
                label6.Visible = true;
                txtComments.Visible = true;
            }
            else if (cmbEntryType.SelectedIndex == 3)
            {
                label9.Visible = false;
                label10.Visible = false;
                txtAmount.Visible = false;

                label11.Visible = false;
                label12.Visible = false;
                txtBuyer.Visible = false;

                label21.Visible = false;
                label22.Visible = false;
                txtAddress.Visible = false;

                label23.Visible = false;
                label24.Visible = false;
                txtContactNo.Visible = false;

                label17.Visible = false;
                label18.Visible = false;
                txtVoucherNo.Visible = false;

                label19.Visible = false;
                label20.Visible = false;
                dtpVoucherDate.Visible = false;

                label5.Visible = false;
                label6.Visible = false;
                txtComments.Visible = false;
            }
            else
            {
                label9.Visible = false;
                label10.Visible = false;
                txtAmount.Visible = false;

                label11.Visible = false;
                label12.Visible = false;
                txtBuyer.Visible = false;

                label21.Visible = false;
                label22.Visible = false;
                txtAddress.Visible = false;

                label23.Visible = false;
                label24.Visible = false;
                txtContactNo.Visible = false;

                label17.Visible = true;
                label18.Visible = true;
                txtVoucherNo.Visible = true;

                label19.Visible = true;
                label20.Visible = true;
                dtpVoucherDate.Visible = true;

                label5.Visible = true;
                label6.Visible = true;
                txtComments.Visible = true;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConSDRec.DecimalNumbers(txtAmount, e);
        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAmount.Text))
            {
                double s = Convert.ToDouble(txtAmount.Text);
                txtAmount.Text = s.ToString("##,##0.00");
            }
            else
                txtAmount.Text = "0.00";
        }

        private void txtAmount_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }
    }
}
