﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAdditionToAsset : Form
    {
        private clsCommonFunctions dbConAA = new clsCommonFunctions();
        private int AssetId = 0, SupplierId = 0;
        private string AssetCode = "", CompanyId = "", Supplier = "";
        private bool IsUpdate = false;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAdditionToAsset()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string GetAdditionItemCode()
        {
            string AICode = "";
            SqlDataReader r;
            r = (SqlDataReader)dbConAA.DBCRUDFunction(@"EXEC prcGetAdditionItemCode " + AssetId, true);

            if (r.Read())
            {
                AICode = r["AICode"].ToString();
            }
            r.Close();

            return AICode;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConAA.CheckEmptyOrNull(txtAssetCode, "Enter Asset") &&
                //!dbConAA.CheckEmptyOrNull(txtAddItem, "Enter Item") &&
                !dbConAA.CheckEmptyOrNull(txtAddAmt, "Enter Amount")&&
                !dbConAA.CheckEmptyOrNull(txtAssetLife, "Enter Addition Life"))
            {
                try                
                {
                    if (!IsUpdate)
                    {
                        string ICode = GetAdditionItemCode();
                        string[] cols = { "[AdditionItem]", "[AssetId]", "[CompanyId]", "[AdditionItemCode]", "[AdditionCost]", 
                                          "[InvoiceCostFC]", "[ConvRate]", "[CurType]", "[AddDate]", "[AdditionDate]", "[AddLife]", 
                                          "[DepRate]", "[Description]", "[Country]", "[LCNo]", "[Brand]", "[SupplierId]", 
                                          "[RecDate]", "[VoucherNo]", "[ModelNo]", "[Comments]", "[AccCode]","[UserId]"
                                        };

                        string[] vals = { dbConAA.SingleQuoteHandler(txtAddItem),
                                          AssetId.ToString(),
                                          CompanyId,
                                          ICode,
                                          dbConAA.SingleQuoteHandler(txtAddAmt),
                                          txtInvCostUsd.Text,
                                          txtConvRate.Text,
                                          cmbCurrencyType.Text,
                                          dtpAddDate.Value.ToString("yyyy-MM-dd"),
                                          dtpYear.Value.Year.ToString() + "-" + dtpMonth.Value.Month.ToString() + "-01",
                                          txtAssetLife.Text,
                                          txtDepRate.Text,
                                          dbConAA.SingleQuoteHandler(txtAddDesc),
                                          dbConAA.SingleQuoteHandler(txtCountry),
                                          dbConAA.SingleQuoteHandler(txtLCNo),
                                          dbConAA.SingleQuoteHandler(txtBrand),
                                          SupplierId.ToString(),
                                          dtpAssetRecDate.Value.ToString("yyyy-MM-dd"),
                                          dbConAA.SingleQuoteHandler(txtVoucherNo),
                                          dbConAA.SingleQuoteHandler(txtModelNo),
                                          dbConAA.SingleQuoteHandler(txtComments),
                                          dbConAA.SingleQuoteHandler(txtAccCode),
                                          frmLogin.userId.ToString() };
                        string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                       "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                        string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                           "[AssetRegistar].[dbo].[AR_AssetAddition]", this.Name, 
                                           dbConAA.getMaxIdentity("[AR_AssetAddition]", "[TrnNo]").ToString(), "", "" };

                        string sql = dbConAA.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetAddition]", cols, vals, "INSERT", null, null)
                                     +
                                     dbConAA.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                        DateTime? LastProcessDate = IsProcesDeprecitation(AssetId);
                        if (LastProcessDate != null)
                        {
                            MessageBox.Show(@"Process has been done for the Asset of this Addition for the given year " + Environment.NewLine +
                                             "and also process continuing for this new addition for the existing Asset too.", 
                                             "Proceeding . . .", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            
                            if ((bool)dbConAA.DBCRUDFunction(sql, false))
                            {
                                SqlDataReader r;
                                r = (SqlDataReader)dbConAA.DBCRUDFunction("SELECT MAX(TrnNo) AddTrnNo FROM AssetRegistar.dbo.AR_AssetAddition WHERE AssetId = " + AssetId, true);

                                if (r.Read())
                                {
                                    ProcessAddition(AssetId, Convert.ToInt32(r["AddTrnNo"].ToString()), dtpYear.Value.AddDays(1 - dtpYear.Value.Day), LastProcessDate.Value);
                                    //ProcessAddition(Convert.ToInt32(r["AddTrnNo"].ToString()), dtpYear.Value.Year + "-12-31");
                                    MessageBox.Show("Information Saved Successfully to Code: " + ICode, "Save",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ClearAll();
                                }

                                r.Close();
                            }
                            else
                            {
                                MessageBox.Show("Failed to Save Information.", "Error",
                                                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            if ((bool)dbConAA.DBCRUDFunction(sql, false))
                            {
                                MessageBox.Show("Information Saved Successfully to Code: " + ICode, "Save",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ClearAll();
                            }
                            else
                            {
                                MessageBox.Show("Failed to Save Information.", "Error",
                                                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } 
            }
        }

        private bool ProcessAddition(int AssetId, int AddId, DateTime FDate, DateTime TDate)
        {
            //@"EXEC prcProcessDepreciationOnCarryingOnly " + AssetId + ",'" + PDate + "', " + frmLogin.userId, false))
            if ((bool)dbConAA.DBCRUDFunction(@"EXEC prcProcessDepreciation '" + AssetId + "', '', " +
                                                                                     "'" + FDate.ToString("dd-MMM-yyyy") + "', " +
                                                                                     "'" + TDate.ToString("dd-MMM-yyyy") + "', " +
                                                                                     frmLogin.userId + ", '" +
                                                                                     frmLogin.ipAddress + "', 2, " + AddId, false))
                return true;
            else
                return false;

        }

        private DateTime? IsProcesDeprecitation(int AddId)
        {
            SqlDataReader dr; DateTime? LastDepDate = null;
            dr = (SqlDataReader)dbConAA.DBCRUDFunction(@"SELECT Top 1 * FROM AR_Process "
                                                        + "WHERE ProcessDate >= '" + dtpYear.Value + "' "
                                                        + "Order By ProcessDate Desc", true);

            if (dr.Read())
            {
                LastDepDate = Convert.ToDateTime(dr["ProcessDate"]);
            }

            return LastDepDate;

        }

        private void ClearAll()
        {
            txtAddAmt.Text = "";
            txtAddDesc.Text = "";
            txtAddItem.Text = "";
            txtAssetCode.Text = "";
            txtAssetCode.Focus();
            dgvListData.Visible = false;
            AssetId = 0;
            IsUpdate = false;
            cmbCurrencyType.SelectedIndex = -1;
            txtConvRate.Text = "";
            txtCountry.Text = "";
            txtInvCostUsd.Text = "";
            txtLCNo.Text = "";
            txtBrand.Text = "";
            txtModelNo.Text = "";
            txtSupplier.Text = "";
            SupplierId = 0;
            dgvSupplier.Visible = false;
            txtVoucherNo.Text = "";
            txtAccCode.Text = "";
            CompanyId = "";
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConAA.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DisposeChecking(txtAssetCode);
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();
                CompanyId = dgvListData.Rows[0].Cells[2].Value.ToString();
                //LoadAssetDetails();
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            }
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString()); 
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            CompanyId = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[2].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
            //LoadAssetDetails();
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                CompanyId = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[2].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
                //LoadAssetDetails();
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void frmAdditionToAsset_Load(object sender, EventArgs e)
        {
            dbConAA.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode 0, '', '', 2");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
            dbConAA.FillDataGridView(dgvSupplier, @"SELECT SupplierId, Supplier FROM [AssetRegistar].[dbo].[AR_AssetSupplier] WHERE IsActive = 1");
            dgvSupplier.Columns[0].Visible = false;
            dgvSupplier.Columns[1].Width = 200;
        }

        private void txtAddAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DisposeChecking(txtAddAmt);
            dbConAA.DecimalNumbers(txtAddAmt, e);
        }

        private void txtAddItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DisposeChecking(txtAddItem);
        }

        private void txtAssetLife_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DecimalNumbers(txtAssetLife, e);
        }

        private void txtAssetLife_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtAssetLife_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAssetLife.Text))
            {
                double s = Convert.ToDouble(txtAssetLife.Text);
                txtAssetLife.Text = s.ToString("##,##0.00");
            }
            else
                txtAssetLife.Text = "0.00";
        }

        private void txtDepRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DecimalNumbers(txtDepRate, e);
        }

        private void txtDepRate_TextChanged(object sender, EventArgs e)
        {
            double DepRate = 0.00;

            if (!string.IsNullOrEmpty(txtDepRate.Text))
                DepRate = Convert.ToDouble(txtDepRate.Text);

            if (DepRate > 0.00)
                txtAssetLife.Text = Math.Round((100 / Convert.ToDouble(txtDepRate.Text))).ToString();
            else
                txtAssetLife.Text = "0.00";
        }

        private void txtAddItem_TextChanged(object sender, EventArgs e)
        {
            dbConAA.MakeTextUppercase(txtAddItem);
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void frmAdditionToAsset_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void txtInvCostUsd_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtInvCostUsd_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DecimalNumbers(txtInvCostUsd, e);
        }

        private void txtInvCostUsd_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtInvCostUsd.Text))
            {
                double s = Convert.ToDouble(txtInvCostUsd.Text);
                txtInvCostUsd.Text = s.ToString("##,##0.00");
            }
            else
                txtInvCostUsd.Text = "0.00";
        }

        private void txtConvRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DecimalNumbers(txtConvRate, e);
        }

        private void txtConvRate_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtConvRate.Text))
            {
                double s = Convert.ToDouble(txtConvRate.Text);
                txtConvRate.Text = s.ToString("##,##0.00");
            }
            else
                txtConvRate.Text = "0.00";
        }

        private void txtConvRate_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void txtSupplier_TextChanged(object sender, EventArgs e)
        {
            dbConAA.MakeTextUppercase(txtSupplier);
            var dataTable = (DataTable)dgvSupplier.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvSupplier.Visible = true;
                dataView.RowFilter = dgvSupplier.Columns[1].Name + " LIKE '%" + txtSupplier.Text + "%'";
            }
        }

        private void txtSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConAA.DisposeChecking(txtSupplier);
            if (e.KeyChar == (char)Keys.Escape)
                dgvSupplier.Visible = false;
        }

        private void txtSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
                dgvSupplier.Focus();

            if (e.KeyCode == Keys.Enter && dgvSupplier.Rows.Count > 0)
            {
                txtSupplier.Text = dgvSupplier.Rows[0].Cells[1].Value.ToString();
                SupplierId = Convert.ToInt32(dgvSupplier.Rows[0].Cells[0].Value.ToString());
                Supplier = dgvSupplier.Rows[0].Cells[1].Value.ToString();
                dgvSupplier.Visible = false;
                txtSupplier.SelectionStart = txtSupplier.TextLength;
            }
        }

        private void dgvSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SupplierId = Convert.ToInt32(dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[0].Value.ToString());
                Supplier = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSupplier.Text = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtSupplier.Focus();
                txtSupplier.SelectionStart = txtSupplier.TextLength;
                dgvSupplier.Visible = false;
            }
        }

        private void dgvSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtSupplier.Focus();
            txtSupplier.SelectionStart = txtSupplier.TextLength;
        }

        private void dgvSupplier_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SupplierId = Convert.ToInt32(dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[0].Value.ToString());
            Supplier = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSupplier.Text = dgvSupplier.Rows[dgvSupplier.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtSupplier.Focus();
            txtSupplier.SelectionStart = txtSupplier.TextLength;
            dgvSupplier.Visible = false;
        }

        private void btnRfrshSupplier_Click(object sender, EventArgs e)
        {
            dbConAA.FillDataGridView(dgvSupplier, @"SELECT SupplierId, Supplier FROM [AssetRegistar].[dbo].[AR_AssetSupplier] WHERE IsActive = 1");
            dgvSupplier.Columns[0].Visible = false;
            dgvSupplier.Columns[1].Width = 200;
        }

        private void txtCountry_TextChanged(object sender, EventArgs e)
        {
            dbConAA.MakeTextUppercase(txtCountry);
        }

        private void rbLand_CheckedChanged(object sender, EventArgs e)
        {
            cmbEntryType.Visible = true;
            txtAddItem.Visible = false;
            dbConAA.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode 0, '', '', 1");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
            txtAssetCode.Text = "";
            dgvListData.Visible = false;
        }

        private void rbOthers_CheckedChanged(object sender, EventArgs e)
        {
            cmbEntryType.Visible = false;
            txtAddItem.Visible = true;
            dbConAA.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode 0, '', '', 0");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;
            txtAssetCode.Text = "";
            dgvListData.Visible = false;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (IsUpdate) 
            {
                if (!dbConAA.CheckEmptyOrNull(txtAssetCode, "Enter Asset") &&
                    //!dbConAA.CheckEmptyOrNull(txtAddItem, "Enter Item") &&
                    !dbConAA.CheckEmptyOrNull(txtAddAmt, "Enter Amount") &&
                    !dbConAA.CheckEmptyOrNull(txtAssetLife, "Enter Addition Life"))
                {
 
                }
            }
        }
    }
}
