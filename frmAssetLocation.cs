﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmAssetLocation : Form
    {
        private clsCommonFunctions dbConLoc = new clsCommonFunctions();
        private bool IsUpdate = false;
        private int LocId = 0;
        private string pAssetLoc = "", pCcCode = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmAssetLocation()
        {
            InitializeComponent();
        }

        private void frmAssetLocation_Load(object sender, EventArgs e)
        {
            LoadLocation();
            dbConLoc.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");

            if (!IsUpdate)
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }
        }

        private void LoadLocation()
        {
            dbConLoc.FillDataGridView(dgvLocation, @"SELECT A.AssetLocId, A.AssetLocation, B.CC_Name FROM AR_AssetLocation As A " +
                "Inner Join AUTOMATIONXS.KDSXS_Accounts.dbo.CostCentre As B On B.CC_CODE = A.CostCentreCode");
            dbConLoc.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");

            dgvLocation.Columns["AssetLocId"].Visible = false;
            dgvLocation.Columns["AssetLocation"].HeaderText = "Location";
            dgvLocation.Columns["AssetLocation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvLocation.Columns["CC_Name"].HeaderText = "Cost Center";
            dgvLocation.Columns["CC_Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvLocation.Columns["CC_Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvLocation.Columns["AssetLocation"].Width = 320;
        }

        private void dgvLocation_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvLocation.Rows.Count > 0)
            {
                LocId = Convert.ToInt32(dgvLocation.Rows[dgvLocation.CurrentCell.RowIndex].Cells["AssetLocId"].Value.ToString());
                LoadLocDetails(LocId);
                IsUpdate = true;
            }
            if (!IsUpdate)
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }
        }

        private void LoadLocDetails(int LocId)
        {
            SqlDataReader dr;
            dr = (SqlDataReader)dbConLoc.DBCRUDFunction(@"SELECT aal.AssetLocation, aal.CostCentreCode,cc.CC_NAME
                                                          FROM   AR_AssetLocation aal
                                                                 INNER JOIN AUTOMATIONXS.KDSXS_Accounts.dbo.CostCentre cc
                                                                      ON  aal.CostCentreCode = cc.CC_Code
                                                          WHERE aal.AssetLocId = " + LocId, true);
            if (dr.Read())
            {
                txtLocation.Text = dr["AssetLocation"].ToString();
                cmbCC.Text = dr["CC_NAME"].ToString();
                pAssetLoc = dr["AssetLocation"].ToString();
            }

            dr.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            dbConLoc.MakeTextUppercase(txtSearch);
            var dataTable = (DataTable)dgvLocation.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dataView.RowFilter = dgvLocation.Columns["AssetLocation"].Name + " LIKE '%" + txtSearch.Text + "%'";
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            txtSearch.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!dbConLoc.CheckEmptyOrNull(txtLocation, "Enter Location Name") &&
                !dbConLoc.CheckEmptyOrNull(cmbCC, "Select Cost Centre"))
            {
                DialogResult result = MessageBox.Show(@"Would You Like to Save New Location " + txtLocation.Text.Trim() 
                                                        + " ?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes) 
                {
                    string[] cols = { "[AssetLocation]", "[CostCentreCode]", "[EntryBy]" };
                    string[] vals = { dbConLoc.SingleQuoteHandler(txtLocation), cmbCC.SelectedValue.ToString(), frmLogin.userId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "INSERT", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetLocation]", this.Name, 
                                       dbConLoc.getMaxIdentity("[AR_AssetLocation]", "[AssetLocId]").ToString(), "", "" };

                    string sql = dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetLocation]", cols, vals, "INSERT", null, null)
                                 +
                                 dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLoc.DBCRUDFunction(sql, false))
                    {
                        txtLocation.Text = "";
                        MessageBox.Show("Location Saved Successfully.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadLocation();
                    }
                    else
                        MessageBox.Show("Failed to Save Location.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            pCcCode = "";
            pAssetLoc = "";
            txtLocation.Text = "";
            IsUpdate = false;

            if (!IsUpdate)
            {
                btnSave.Visible = true;
                btnDelete.Visible = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnSave.Visible = false;
                btnDelete.Visible = true;
                btnUpdate.Enabled = true;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (IsUpdate && LocId > 0 && !dbConLoc.CheckEmptyOrNull(txtLocation, "Enter Location"))
            {
                DialogResult result = MessageBox.Show(@"Would You Like to Update Location " + txtLocation.Text.Trim() 
                                                        + " ?", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    string[] cols = { "[AssetLocation]", "[CostCentreCode]", "[UpdateDate]", "[UpdateBy]" };
                    string[] vals = { dbConLoc.SingleQuoteHandler(txtLocation), cmbCC.SelectedValue.ToString(), 
                                  dbConLoc.GetServerCurrentDateTime().ToString(), frmLogin.userId.ToString() };
                    string[] whCols = { "[AssetLocId]" };
                    string[] whVals = { LocId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "UPDATE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetLocation]", this.Name, LocId.ToString(),
                                       "[AssetLocId], [AssetLocation], [CostCentreCode]", 
                                       LocId.ToString() + ", " + dbConLoc.SingleQuoteHandler(pAssetLoc) 
                                       + ", " + pCcCode };

                    string sql = dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetLocation]", cols, vals, "UPDATE", whCols, whVals)
                                + dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLoc.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Location Info Updated Successfully.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadLocation();
                    }
                    else
                    {
                        MessageBox.Show("Failed to Update Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid Operation to Update Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (LocId > 0 && !dbConLoc.CheckEmptyOrNull(txtLocation, "Enter Location"))
            {
                DialogResult result = MessageBox.Show(@"Would You Like to Delete Location " + txtLocation.Text.Trim() 
                                                        + " ?", "Delete?", 
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    string[] whCols = { "[AssetLocId]" };
                    string[] whVals = { LocId.ToString() };
                    string[] lCols = { "UserId", "Functions", "IPAddress", "TableName",
	                                   "FormName", "EntryReferenceId", "EntityName", "EntityValue" };
                    string[] lVals = { frmLogin.userId.ToString(), "DELETE", frmLogin.ipAddress, 
                                       "[AssetRegistar].[dbo].[AR_AssetLocation]", this.Name, LocId.ToString(),
                                       "[AssetLocId], [AssetLocation], [CostCentreCode]", 
                                       LocId.ToString() + ", " + dbConLoc.SingleQuoteHandler(pAssetLoc) 
                                       + ", " + pCcCode };

                    string sql = dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_AssetLocation]", null, null, "DELETE", whCols, whVals)
                                 +
                                 dbConLoc.CreateSQLStatement("[AssetRegistar].[dbo].[AR_UsersLog]", lCols, lVals, "INSERT", null, null);

                    if ((bool)dbConLoc.DBCRUDFunction(sql, false))
                    {
                        MessageBox.Show("Location Info Deleted Successfully.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dbConLoc.FillDataGridView(dgvLocation, @"SELECT AssetLocId, AssetLocation FROM   AR_AssetLocation");
                        LocId = 0;
                        txtLocation.Text = "";
                        btnDelete.Visible = false;
                        btnSave.Visible = true;
                        btnUpdate.Enabled = false;
                        IsUpdate = false;
                    }
                    else
                    {
                        MessageBox.Show("Failed to Delete Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid Operation to Delete Location Info.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtLocation_TextChanged(object sender, EventArgs e)
        {
            dbConLoc.MakeTextUppercase(txtLocation);
        }

        private void txtLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            dbConLoc.DisposeChecking(txtLocation);
        }

        private void SelectionTextString(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                ((TextBox)sender).SelectAll();
            });
        }

        private void txtLocation_Enter(object sender, EventArgs e)
        {
            SelectionTextString(sender, e);
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmAssetLocation_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnUpdate, "Update");
        }

        private void btnDelete_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnDelete, "Delete");
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnSave, "Save");
        }

        private void btnReset_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnReset, "New");
        }
    }
}
