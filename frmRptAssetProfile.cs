﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmRptAssetProfile : Form
    {
        private clsCommonFunctions dbConAP = new clsCommonFunctions();
        private int AssetId = 0;
        private string AssetCode = "", GroupId = "", CompanyId = "", CcCode = "", PDate = "";
        private bool cmbLoadFlag = false;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmRptAssetProfile()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            /**********For Crystal Report****************************/
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;
            PDate = dtpPDate.Value.ToString("yyyy-MM-dd");

            /***********For Stimulsoft Report**************************/
            //StiReport report = new StiReport();
            //frmReportViewer rptForm = new frmReportViewer();                       

            try
            {
                /**********For Crystal Report****************************/
                cryReport.Load(Application.StartupPath + @"\Reports\rptAssetCompleteHistory1.rpt");
                cryReport.SetParameterValue("@assetId", AssetId);
                cryReport.SetParameterValue("@UserName", frmLogin.userFullName);
                cryReport.SetParameterValue("@ProcessDate", PDate);
                dbConAP.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.crViewer.Refresh();
                rptForm.Text = "Asset Full Profile Report";
                rptForm.Show();
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
                /***********For Stimulsoft Report**************************/
                //report.Load(Application.StartupPath + @"\Reports\rptAssetFullProfile.mrt");
                //rptForm.stiViewerControl.Report = report;
                //rptForm.Show();
                //report.Compile();

                //report["assetId"] = assetId;
                //report.Render(); 
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtAssetCode_TextChanged(object sender, EventArgs e)
        {
            dbConAP.MakeTextUppercase(txtAssetCode);
            var dataTable = (DataTable)dgvListData.DataSource;
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var dataView = dataTable.DefaultView;
                dgvListData.Visible = true;
                dataView.RowFilter = dgvListData.Columns[1].Name + " LIKE '%" + txtAssetCode.Text + "%'";
            }
        }

        private void txtAssetCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
                dgvListData.Visible = false;
        }

        private void txtAssetCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                dgvListData.Focus();
                //dgvListData.Rows[0].Cells[1].Selected = true;
            }

            if (e.KeyCode == Keys.Enter && dgvListData.Rows.Count > 0)
            //if (e.KeyCode == Keys.Enter)
            {
                txtAssetCode.Text = dgvListData.Rows[0].Cells[1].Value.ToString();
                AssetId = Convert.ToInt32(dgvListData.Rows[0].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[0].Cells[1].Value.ToString();                
                dgvListData.Visible = false;
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                btnShow.Focus();
            }
        }

        private void dgvListData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
            AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
            dgvListData.Visible = false;
        }

        private void dgvListData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AssetId = Convert.ToInt32(dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[0].Value.ToString());
                AssetCode = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Text = dgvListData.Rows[dgvListData.CurrentCell.RowIndex].Cells[1].Value.ToString();
                txtAssetCode.Focus();
                txtAssetCode.SelectionStart = txtAssetCode.TextLength;
                dgvListData.Visible = false;
            }
        }

        private void dgvListData_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtAssetCode.Focus();
            txtAssetCode.SelectionStart = txtAssetCode.TextLength;
        }

        private void dgvListData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void frmRptAssetProfile_Load(object sender, EventArgs e)
        {
            dbConAP.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConAP.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConAP.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 0");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;

            dbConAP.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '', '', '', 0");
            dgvListData.Columns[0].Visible = false;
            dgvListData.Columns[1].Width = 145;

            cmbLoadFlag = true;
        }

        private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbCompany.SelectedIndex > -1)
            {
                CompanyId = cmbCompany.SelectedValue.ToString();                
                dbConAP.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '" + CcCode + "', '"
                                                                                 + GroupId + "', '"
                                                                                 + CompanyId + "', 0");
                //dgvListData.Columns[0].Visible = false;
                //dgvListData.Columns[1].Width = 145;
            }
        }

        private void cmbCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbCC.SelectedIndex > -1)
            {
                CcCode = cmbCC.SelectedValue.ToString();
                dbConAP.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '" + CcCode + "', '"
                                                                                 + GroupId + "', '"
                                                                                 + CompanyId + "', 0");
                //dgvListData.Columns[0].Visible = false;
                //dgvListData.Columns[1].Width = 145;
            }
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoadFlag && cmbGroup.SelectedIndex > -1)
            {
                GroupId = cmbGroup.SelectedValue.ToString();
                string str = @"EXEC prcLoadAssetCode '" + CcCode + "', '"
                                                                                 + GroupId + "', '"
                                                                                 + CompanyId + "', 0";
                dbConAP.FillDataGridView(dgvListData, @"EXEC prcLoadAssetCode '" + CcCode + "', '"
                                                                                 + GroupId + "', '"
                                                                                 + CompanyId + "', 0");
                //dgvListData.Columns[0].Visible = false;
                //dgvListData.Columns[1].Width = 145;
            }
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmRptAssetProfile_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }
    }
}
