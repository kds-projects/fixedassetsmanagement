﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using System.Runtime.InteropServices;

namespace AssetRegister
{
    public partial class frmrptMonthlyCostDisposal : Form
    {
        private clsCommonFunctions dbConRptDepJournalChargeAmt = new clsCommonFunctions();
        private string AssetGroupId = "", CompanyId = "", Company = "", CostCentreCode = "", CC = "";
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public frmrptMonthlyCostDisposal()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ReportDocument cryReport = new ReportDocument();
            frmLoadReport rptForm = new frmLoadReport();
            frmLoading frmLoading = new frmLoading();
            frmLoading.Show();
            frmLoading.TopMost = true;
            Cursor.Current = Cursors.AppStarting;

            int Month = 0, Year = 0;

            if (!chkAllCompany.Checked)
            {
                if (cmbCompany.SelectedIndex == 0)
                {
                    CompanyId = "IM";
                    //Company = "Dominox IM Ltd.";
                }
                else if (cmbCompany.SelectedIndex == 1)
                {
                    CompanyId = "GP";
                    //Company = "KDS Accessories Ltd.";
                }
                else if (cmbCompany.SelectedIndex == 2)
                {
                    CompanyId = "PL";
                    //Company = "KDS Poly Industries Ltd.";
                }
                else if (cmbCompany.SelectedIndex == 3)
                {
                    CompanyId = "CP";
                    //Company = "KDS Thread Limited.";
                }
                else
                {
                    MessageBox.Show("Please Select Company.", "Company",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCompany.Focus();
                    return;
                }
            }
            else
                CompanyId = "";

            if (!chkAllCC.Checked)
            {
                if (cmbCC.SelectedIndex > -1)
                {
                    CostCentreCode = cmbCC.SelectedValue.ToString();
                    CC = cmbCC.Text.Trim();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbCC.Focus();
                    return;
                }
            }
            else
                CostCentreCode = "";

            if (!chkAllGroup.Checked)
            {
                if (cmbGroup.SelectedIndex > -1)
                {
                    AssetGroupId = cmbGroup.SelectedValue.ToString();
                }
                else
                {
                    MessageBox.Show("Please select Cost Centre.", "Cost Centre Code",
                                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    cmbGroup.Focus();
                    return;
                }
            }
            else
                AssetGroupId = "";

            Month = dtpMonth.Value.Month;
            Year = dtpYear.Value.Year;

            try
            {
                if (chkCostCenterWise.Checked)
                    cryReport.Load(Application.StartupPath + @"\Reports\rptMonthWiseDisposal_CCW.rpt");
                else
                    cryReport.Load(Application.StartupPath + @"\Reports\rptMonthWiseDisposal.rpt");

                cryReport.SetParameterValue("@CompanyId", CompanyId);
                cryReport.SetParameterValue("@CC_Code", CostCentreCode);
                cryReport.SetParameterValue("@Year", Year);
                cryReport.SetParameterValue("@Month", Month);
                cryReport.SetParameterValue("@UserName", frmLogin.userFullName);
                dbConRptDepJournalChargeAmt.SetReportLogon(cryReport);
                rptForm.crViewer.ReportSource = null;
                rptForm.Text = "Monthwise Cost Disposal Report";
                rptForm.crViewer.ReportSource = cryReport;
                rptForm.crViewer.Refresh();
                rptForm.Show();
                Cursor.Current = Cursors.Default;
                frmLoading.Hide();
            }
            catch (Exception ex)
            {
                frmLoading.Hide();
                MessageBox.Show(ex.Message);
            }
        }

        private void frmRptAssetRegister_Load(object sender, EventArgs e)
        {
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCompany, @"EXEC prcFrmAssetLedgerLoadCmbCompany");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbCC, @"EXEC prcFrmAssetLedgerLoadCmbCostCenter");
            dbConRptDepJournalChargeAmt.FillComboBox(cmbGroup, @"EXEC prcFrmAssetLedgerLoadCmbGroup 2");

            cmbCompany.SelectedIndex = -1;
            cmbCC.SelectedIndex = -1;
            cmbGroup.SelectedIndex = -1;
            dtpMonth.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }

        private void chkAllCompany_CheckedChanged(object sender, EventArgs e)
        {
            cmbCompany.Enabled = !chkAllCompany.Checked;
        }

        private void chkAllCC_CheckedChanged(object sender, EventArgs e)
        {
            cmbCC.Enabled = !chkAllCC.Checked;
        }

        private void chkAllGroup_CheckedChanged(object sender, EventArgs e)
        {
            cmbGroup.Enabled = !chkAllGroup.Checked;
        }

        private void frmRptDepNCost_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnClose, "Close");
        }

        private void btnShow_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnShow, "Show");
        }

        private void btnCross_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCross_MouseHover(object sender, EventArgs e)
        {
            ToolTip ttip = new ToolTip();
            ttip.SetToolTip(this.btnCross, "Close");
        }
    }
}
