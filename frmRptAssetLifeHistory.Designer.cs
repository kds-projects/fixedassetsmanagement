﻿namespace AssetRegister
{
    partial class frmRptAssetLifeHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.chkAllCC = new System.Windows.Forms.CheckBox();
            this.chkAllCompany = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvListData = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCross = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbCompany = new AssetRegister.FlatComboBox();
            this.txtAssetCode = new AssetRegister.FlatTextBox();
            this.cmbCC = new AssetRegister.FlatComboBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkAllCC
            // 
            this.chkAllCC.AutoSize = true;
            this.chkAllCC.Checked = true;
            this.chkAllCC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCC.Location = new System.Drawing.Point(343, 40);
            this.chkAllCC.Name = "chkAllCC";
            this.chkAllCC.Size = new System.Drawing.Size(37, 17);
            this.chkAllCC.TabIndex = 4;
            this.chkAllCC.Text = "All";
            this.chkAllCC.UseVisualStyleBackColor = true;
            this.chkAllCC.CheckedChanged += new System.EventHandler(this.chkAllCC_CheckedChanged);
            // 
            // chkAllCompany
            // 
            this.chkAllCompany.AutoSize = true;
            this.chkAllCompany.Checked = true;
            this.chkAllCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllCompany.Location = new System.Drawing.Point(343, 18);
            this.chkAllCompany.Name = "chkAllCompany";
            this.chkAllCompany.Size = new System.Drawing.Size(37, 17);
            this.chkAllCompany.TabIndex = 2;
            this.chkAllCompany.Text = "All";
            this.chkAllCompany.UseVisualStyleBackColor = true;
            this.chkAllCompany.CheckedChanged += new System.EventHandler(this.chkAllCompany_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 299;
            this.label4.Text = "Company Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(113, 42);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 297;
            this.label24.Text = ":";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(12, 42);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(65, 13);
            this.label45.TabIndex = 298;
            this.label45.Text = "Cost Center";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(113, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 300;
            this.label5.Text = ":";
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape2.Location = new System.Drawing.Point(2, 101);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(386, 39);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.rectangleShape1.Location = new System.Drawing.Point(2, 2);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(386, 97);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 307;
            this.label1.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 306;
            this.label2.Text = "Asset Code";
            // 
            // btnShow
            // 
            this.btnShow.Image = global::AssetRegister.Properties.Resources.Actions_document_find_icon;
            this.btnShow.Location = new System.Drawing.Point(313, 102);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(38, 38);
            this.btnShow.TabIndex = 6;
            this.btnShow.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            this.btnShow.MouseHover += new System.EventHandler(this.btnShow_MouseHover);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::AssetRegister.Properties.Resources.Actions_system_shutdown_icon;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(350, 102);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(38, 38);
            this.btnClose.TabIndex = 7;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseHover += new System.EventHandler(this.btnClose_MouseHover);
            // 
            // dgvListData
            // 
            this.dgvListData.AllowUserToAddRows = false;
            this.dgvListData.AllowUserToDeleteRows = false;
            this.dgvListData.AllowUserToResizeColumns = false;
            this.dgvListData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.dgvListData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListData.BackgroundColor = System.Drawing.Color.White;
            this.dgvListData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvListData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvListData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvListData.ColumnHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvListData.EnableHeadersVisualStyles = false;
            this.dgvListData.Location = new System.Drawing.Point(239, 2);
            this.dgvListData.MultiSelect = false;
            this.dgvListData.Name = "dgvListData";
            this.dgvListData.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvListData.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvListData.RowHeadersVisible = false;
            this.dgvListData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListData.Size = new System.Drawing.Size(150, 98);
            this.dgvListData.TabIndex = 308;
            this.dgvListData.Visible = false;
            this.dgvListData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListData_CellDoubleClick);
            this.dgvListData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvListData_KeyDown);
            this.dgvListData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvListData_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.btnCross);
            this.panel1.Controls.Add(this.label66);
            this.panel1.Location = new System.Drawing.Point(5, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(391, 44);
            this.panel1.TabIndex = 323;
            // 
            // btnCross
            // 
            this.btnCross.FlatAppearance.BorderSize = 0;
            this.btnCross.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCross.Image = global::AssetRegister.Properties.Resources.delete_icon;
            this.btnCross.Location = new System.Drawing.Point(359, 7);
            this.btnCross.Name = "btnCross";
            this.btnCross.Size = new System.Drawing.Size(30, 30);
            this.btnCross.TabIndex = 419;
            this.btnCross.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCross.UseVisualStyleBackColor = true;
            this.btnCross.Click += new System.EventHandler(this.btnCross_Click);
            this.btnCross.MouseHover += new System.EventHandler(this.btnCross_MouseHover);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.White;
            this.label66.Location = new System.Drawing.Point(7, 13);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(284, 16);
            this.label66.TabIndex = 419;
            this.label66.Text = "Fixed Assets - Asset Historical Record";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.dgvListData);
            this.panel2.Controls.Add(this.chkAllCompany);
            this.panel2.Controls.Add(this.chkAllCC);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.btnShow);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.cmbCompany);
            this.panel2.Controls.Add(this.txtAssetCode);
            this.panel2.Controls.Add(this.cmbCC);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.shapeContainer2);
            this.panel2.Location = new System.Drawing.Point(5, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(391, 143);
            this.panel2.TabIndex = 324;
            // 
            // cmbCompany
            // 
            this.cmbCompany.Enabled = false;
            this.cmbCompany.FormattingEnabled = true;
            this.cmbCompany.Location = new System.Drawing.Point(130, 16);
            this.cmbCompany.Name = "cmbCompany";
            this.cmbCompany.Size = new System.Drawing.Size(209, 21);
            this.cmbCompany.TabIndex = 1;
            this.cmbCompany.SelectedIndexChanged += new System.EventHandler(this.cmbCompany_SelectedIndexChanged);
            // 
            // txtAssetCode
            // 
            this.txtAssetCode.BorderColor = System.Drawing.SystemColors.ControlDark;
            this.txtAssetCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssetCode.Location = new System.Drawing.Point(130, 60);
            this.txtAssetCode.Name = "txtAssetCode";
            this.txtAssetCode.Size = new System.Drawing.Size(209, 21);
            this.txtAssetCode.TabIndex = 5;
            this.txtAssetCode.TextChanged += new System.EventHandler(this.txtAssetCode_TextChanged);
            this.txtAssetCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAssetCode_KeyDown);
            this.txtAssetCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAssetCode_KeyPress);
            // 
            // cmbCC
            // 
            this.cmbCC.Enabled = false;
            this.cmbCC.FormattingEnabled = true;
            this.cmbCC.Location = new System.Drawing.Point(130, 38);
            this.cmbCC.Name = "cmbCC";
            this.cmbCC.Size = new System.Drawing.Size(209, 21);
            this.cmbCC.TabIndex = 3;
            this.cmbCC.SelectedIndexChanged += new System.EventHandler(this.cmbCC_SelectedIndexChanged);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1,
            this.rectangleShape2});
            this.shapeContainer2.Size = new System.Drawing.Size(391, 143);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // frmRptAssetLifeHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(401, 196);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRptAssetLifeHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Land Details";
            this.Load += new System.EventHandler(this.frmRptLanDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListData)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkAllCC;
        private System.Windows.Forms.CheckBox chkAllCompany;
        private AssetRegister.FlatComboBox cmbCC;
        private AssetRegister.FlatComboBox cmbCompany;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label5;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private FlatTextBox txtAssetCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvListData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCross;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
    }
}